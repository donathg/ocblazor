﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using Force;

namespace Common.CloneExtensions
{
   public static class CloneExtensions
    {

        public static T DeepClone<T>(this T obj)
        {
            return Force.DeepCloner.DeepClonerExtensions.DeepClone<T>(obj);
        }
    
    }
}
