﻿using System;

namespace Common
{
    public static class EnDeCode
    {
        public static String EncodeStringToBase64(String s)
        {
            byte[] encodedBytes = System.Text.Encoding.Unicode.GetBytes(s);
            return Convert.ToBase64String(encodedBytes);

        }
        public static String DecodeBase64ToString(String s)
        {
            byte[] decodedBytes = Convert.FromBase64String(s);
            string decodedTxt = System.Text.Encoding.UTF8.GetString(decodedBytes);
            return System.Text.Encoding.Unicode.GetString(decodedBytes);
         

        }

    }
}
