﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessObjects
{
    public class DAOArtikel
    {
  
        public int Id { get; set; }

        public String Name { get; set; }
        public Decimal Prijs { get; set; }

        public String Maat { get; set; }

        public String StuksPerVerpakking { get; set; }

        public String ArtikelNr { get; set; }
    }
}
