﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessObjects
{
    public class DAOClient
    {
        public DAOClient()
        {

        }
        public int Id { get; set; }

        public String Code { get; set; }
        public String Rijksregisternr { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public bool Actif { get; set; }

        public DateTime? OpenameEinde { get; set; }
        public List<DAOLeefgroep> Leefgroep { get; set; } = new List<DAOLeefgroep>();

        public String LeefgroepenString 
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (DAOLeefgroep lfg in Leefgroep)
                
                    if (lfg.Id == Leefgroep[0].Id)
                        sb.Append(lfg.Name);
                     else
                        sb.Append(", " + lfg.Name);

                return sb.ToString();
            }
               
        }

    }
}
