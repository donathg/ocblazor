﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace DataAccessObjects
{

    


    public class DAOGebruiker
    {
        public DAOGebruiker()
        {
        }
        public int Id { get; set; }

        public String Code { get; set; }

        public String Name { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }

        public String Phone { get; set; }
        public String MobilePhone { get; set; }
        public String InternalPhoneNumber { get; set; }
        public String InternGroupPhoneNumber { get; set; }
        public String Function { get; set; }
        public String Workplace { get; set; }

        public Byte[] Foto { get; set; }

        public String FotoString
        {
            get
            {
                if (Foto != null)
                {

                    return "data:image/jpeg;charset=utf-8;base64, " + Convert.ToBase64String(Foto);
                }
                else return String.Empty;
            }
        }
    }
}
