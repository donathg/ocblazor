﻿using System;
 

namespace DataAccessObjects
{
    public class DAOLeefgroep
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public Char FromOrbis { get; set; }


        public bool IsFromOrbis { get { return FromOrbis == 'Y'; } }


    }
}
