﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccesObjects
{
    public class AankoopArtikelDao
    {
        public int DatabaseId { get; set; }
        public int? BewonerId { get; set; }
        public int? LeefgroepId { get; set; }
        public int ArtikelId { get; set; }
        public Double ArtikelPrijs { get; set; }
        public int Aantal { get; set; }
        public String LeefgroepInfo { get; set; }
        public String ArtikelInfo { get; set; }
        public DateTime CreationDate { get; set; }
        public int Creator { get; set; }
        public DateTime? ClosedDate { get; set; }
        public int? Closer { get; set; }

    }

    public class AankoopArtikelDaoMap : EntityMap<AankoopArtikelDao>
    {
        internal AankoopArtikelDaoMap()
        {
            Map(u => u.DatabaseId).ToColumn("Id");
            Map(u => u.BewonerId).ToColumn("BewonerId");
            Map(u => u.ArtikelId).ToColumn("ArtikelId");
            Map(u => u.LeefgroepId).ToColumn("LeefgroepId");
            Map(u => u.ArtikelPrijs).ToColumn("ArtikelPrijs");
            Map(u => u.Aantal).ToColumn("Aantal");
            Map(u => u.LeefgroepInfo).ToColumn("LeefgroepInfo");
            Map(u => u.ArtikelInfo).ToColumn("ArtikelInfo");
            Map(u => u.CreationDate).ToColumn("CreationDate");
            Map(u => u.Creator).ToColumn("Creator");
            Map(u => u.ClosedDate).ToColumn("ClosedDate");
            Map(u => u.Closer).ToColumn("Closer");

        }
    }
 
}
