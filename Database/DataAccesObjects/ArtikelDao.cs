﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccessObjects
{
    public class ArtikelDao
    {
        public int ArtikelId { get; set; }

        public String Name { get; set; }
        public Decimal Prijs { get; set; }

        public String Maat { get; set; }

        public String StuksPerVerpakking { get; set; }

        public String ArtikelNr { get; set; } 
        
        public String KleurCode { get; set; }

        public int ArtikelCategorieId { get; set; }
        public String ArtikelCategorieNaam { get; set; }
    }
    public class ArtikelDaoMap : EntityMap<ArtikelDao>
    {
        internal ArtikelDaoMap()
        {
            Map(u => u.ArtikelId).ToColumn("id");
            Map(u => u.Maat).ToColumn("maat");
            Map(u => u.Name).ToColumn("naam");
            Map(u => u.Prijs).ToColumn("prijs");
            Map(u => u.StuksPerVerpakking).ToColumn("stuksPerVerpakking");
            Map(u => u.ArtikelNr).ToColumn("artikelNr");
            Map(u => u.KleurCode).ToColumn("KleurCode");
            Map(u => u.ArtikelCategorieId).ToColumn("ArtikelCategorieId");
            Map(u => u.ArtikelCategorieNaam).ToColumn("ArtikelCategorieNaam");

        }
    }
}
