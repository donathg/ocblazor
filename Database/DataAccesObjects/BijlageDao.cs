﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccesObjects
{
    public class BijlageDao
    {
        public int Id { get; set; }
        public String Filename { get; set; }
        public String FileExtention { get; set; }
        public String Creator { get; set; }
        public DateTime Creationdate { get; set; }
        public String Description { get; set; }
        public byte[] FileData { get; set; }
    }
    public class BijlageDaoMap : EntityMap<BijlageDao>
    {
        internal BijlageDaoMap()
        {
            Map(u => u.Id).ToColumn("Id");
            Map(u => u.Filename).ToColumn("fName");
            Map(u => u.FileExtention).ToColumn("fExtension");
            Map(u => u.Creator).ToColumn("Creator");
            Map(u => u.Creationdate).ToColumn("Creationdate");
            Map(u => u.Description).ToColumn("Description");
            Map(u => u.FileData).ToColumn("FileData");
        }
    }
}
