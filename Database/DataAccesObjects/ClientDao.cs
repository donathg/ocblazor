﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccessObjects
{
    public class ClientDao
    {
        public ClientDao()
        {

        }
        public int Id { get; set; }

        public String Code { get; set; }
        public String Rijksregisternr { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public bool Actif { get; set; }

        public DateTime? OpenameEinde { get; set; }
        public List<LeefgroepDao> Leefgroep { get; set; } = new List<LeefgroepDao>();

        public String LeefgroepenString 
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (LeefgroepDao lfg in Leefgroep)
                
                    if (lfg.Id == Leefgroep[0].Id)
                        sb.Append(lfg.Name);
                     else
                        sb.Append(", " + lfg.Name);

                return sb.ToString();
            }
               
        }

    }
    public class ClientDaoMap : EntityMap<ClientDao>
    {
        internal ClientDaoMap()
        {
            Map(u => u.Id).ToColumn("Id");
            Map(u => u.Code).ToColumn("code");
            Map(u => u.FirstName).ToColumn("voornaam");
            Map(u => u.LastName).ToColumn("achternaam");
            Map(u => u.OpenameEinde).ToColumn("opnameeinde");

        }
    }
}
