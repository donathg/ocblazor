﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class DAOHelperIntInt
    {
        public int Integer1 { get; set; }
        public int Integer2 { get; set; }
    }
    public class DAOHelperIntIntMap : EntityMap<DAOHelperIntInt>
    {
        internal DAOHelperIntIntMap()
        {
            Map(u => u.Integer1).ToColumn("Integer1");
            Map(u => u.Integer2).ToColumn("Integer2");
        }
    }
}
