﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class ExitFormDAO
    {
        public int ExitFormId { get; set; }
        public int ItemId { get; set; }

        public DateTime? ResultDate { get; set; }
        public int? ResultInt { get; set; }
        public double? ResultNumber { get; set; }
        public string? ResultText { get; set; }

        public int? StatusId { get; set; }
    }
    public class ExitFormDAOMap : EntityMap<ExitFormDAO>
    {
        internal ExitFormDAOMap()
        {
            Map(u => u.ExitFormId).ToColumn("ExitFormId");
            Map(u => u.ItemId).ToColumn("ItemId");
            Map(u => u.ResultDate).ToColumn("ResultDate");
            Map(u => u.ResultInt).ToColumn("ResultInt");
            Map(u => u.ResultNumber).ToColumn("ResultNumber");
            Map(u => u.ResultText).ToColumn("ResultText");
            Map(u => u.StatusId).ToColumn("StatusId");
        }
    }
}
