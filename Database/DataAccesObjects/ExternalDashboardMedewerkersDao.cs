﻿using Dapper.FluentMap.Mapping;
using Database.DataAccessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    
    public class ExternalDashboardMedewerkersDao
    {
        public String GroepCode { get; set; }
        public int Voltijds { get; set; }
        public int Deeltijds { get; set; }
        public int Aanwerving { get; set; }
        public int UitDienst { get; set; }
       // public double Ziekte { get; set; }
        public double ZiekteMinderJaar { get; set; }  
        public double ZiekteMeerMaand { get; set; }
        public double ZiekteMeerJaar { get; set; }
        public double ZiekteZonderBriefje { get; set; }
        public double Arbeidsongeval { get; set; }
        public int Werkhervatting { get; set; }
        public double VormingsurenExtern { get; set; }
    }

    public class ExternalDashboardMedewerkersDaoMap : EntityMap<ExternalDashboardMedewerkersDao>
    {
        internal ExternalDashboardMedewerkersDaoMap()
        {
            Map(u => u.GroepCode).ToColumn("GroepCode");
            Map(u => u.Voltijds).ToColumn("Voltijds");
            Map(u => u.Deeltijds).ToColumn("Deeltijds");
            Map(u => u.Aanwerving).ToColumn("Aanwerving");
            Map(u => u.UitDienst).ToColumn("UitDienst");
            Map(u => u.ZiekteMinderJaar).ToColumn("ZiekteMinderJaar");
            Map(u => u.ZiekteMeerMaand).ToColumn("ZiekteMeerMaand");
            Map(u => u.ZiekteMeerJaar).ToColumn("ZiekteMeerJaar");
            Map(u => u.ZiekteZonderBriefje).ToColumn("ZiekteZonderBriefje");
            Map(u => u.Arbeidsongeval).ToColumn("Arbeidsongeval");
            Map(u => u.Werkhervatting).ToColumn("Werkhervatting");
            Map(u => u.VormingsurenExtern).ToColumn("VormingsurenExtern");
        }
    }
}
