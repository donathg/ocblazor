﻿using Dapper.FluentMap.Mapping;
using Logic.DataBusinessObjects.MultiSelectNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class GOGItemDAO
    {
        public int GogId { get; set; }
        public int GogCreatorId { get; set; }
        public DateTime GogCreationDate { get; set; }
        public int? GogModifier { get; set; }
        public string GogModifierNaam { get; set; }
        public DateTime GogModificationDate { get; set; }
        public int LeefgroepId { get; set; }
        public int BewonerId { get; set; }

        public int ItemId { get; set; }
        public DateTime? ResultDate { get; set; }
        public int? ResultInt { get; set; }
        public double? ResultNumber { get; set; }
        public string? ResultText { get; set; }
        public int AantalBijlagen { get; set; }

        public int? StatusId { get; set; }

        public DateTime? DatumIncident { get; set; }

        public String AardVanIncident
        {

            get;
            set;



        }


    }
    public class GOGItemDAOMap : EntityMap<GOGItemDAO>
    {
        internal GOGItemDAOMap()
        {
            Map(u => u.GogId).ToColumn("GogId");
            Map(u => u.GogCreatorId).ToColumn("GogCreatorId");
            Map(u => u.GogCreationDate).ToColumn("GogCreationDate");
            Map(u => u.GogModifier).ToColumn("GogModifier");
            Map(u => u.GogModifierNaam).ToColumn("GogModifierNaam");
            Map(u => u.GogModificationDate).ToColumn("GogModificationDate");
            Map(u => u.LeefgroepId).ToColumn("leefgroepId");
            Map(u => u.BewonerId).ToColumn("bewonerId");
            Map(u => u.ItemId).ToColumn("ItemId");
            Map(u => u.ResultDate).ToColumn("ResultDate");
            Map(u => u.ResultInt).ToColumn("ResultInt");
            Map(u => u.ResultNumber).ToColumn("ResultNumber");
            Map(u => u.ResultText).ToColumn("ResultText");
            Map(u => u.AantalBijlagen).ToColumn("AantalBijlagen");
            Map(u => u.StatusId).ToColumn("StatusId");
            Map(u => u.DatumIncident).ToColumn("datumIncident");
            Map(u => u.AardVanIncident).ToColumn("AardVanIncident");
        }
    }
}
