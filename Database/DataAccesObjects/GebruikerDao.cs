﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Database.DataAccessObjects
{

    


    public class GebruikerDao
    {
        public GebruikerDao()
        {
        }
        public int Id { get; set; }

        public String Code { get; set; }

        public String Name { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }

        public String Phone { get; set; }
        public String MobilePhone { get; set; }
        public String InternalPhoneNumber { get; set; }
        public String InternGroupPhoneNumber { get; set; }
        public String Function { get; set; }
        public String Workplace { get; set; }

        public Byte[] Foto { get; set; }

        public String FotoString
        {
            get
            {
                if (Foto != null)
                {

                    return "data:image/jpeg;charset=utf-8;base64, " + Convert.ToBase64String(Foto);
                }
                else return String.Empty;
            }
        }
    }
    public class GebruikerDaoMap : EntityMap<GebruikerDao>
    {
        internal GebruikerDaoMap()
        {
            Map(u => u.Id).ToColumn("Id");
            Map(u => u.FirstName).ToColumn("voornaam");
            Map(u => u.LastName).ToColumn("achternaam");
            Map(u => u.Code).ToColumn("personeelscode");
            Map(u => u.Email).ToColumn("email");
            Map(u => u.Foto).ToColumn("foto");
            Map(u => u.Function).ToColumn("functiebschrijving");
            Map(u => u.Workplace).ToColumn("werkplaats");
            Map(u => u.MobilePhone).ToColumn("gsm");
            Map(u => u.Phone).ToColumn("telefoon");
            Map(u => u.InternGroupPhoneNumber).ToColumn("telefoon_intern_groep");
            Map(u => u.InternalPhoneNumber).ToColumn("telefoon_intern");
        }
    }
}
