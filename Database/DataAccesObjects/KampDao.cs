﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccesObjects
{
    public class KampDao
    {
       public int Id { get; set; }

       public string Nr { get; set; }

       public string Name { get; set; }

        public bool Active { get; set; }
        public string Bestemming { get; set; }
        public string Wie { get; set; }

        public DateTime Van { get; set; }

        public DateTime Tot { get; set; }
    }
    public class KampDaoMap : EntityMap<KampDao>
    {
        internal KampDaoMap()
        {
            Map(u => u.Id).ToColumn("id");
            Map(u => u.Nr).ToColumn("nr");
            Map(u => u.Bestemming).ToColumn("bestemming");
            Map(u => u.Wie).ToColumn("bestemming");
            Map(u => u.Active).ToColumn("aktief");

            Map(u => u.Van).ToColumn("datum_van");
            Map(u => u.Tot).ToColumn("datum_tot");

        }
    }
}
