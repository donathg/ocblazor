﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccesObjects
{
    public class KilometerTypeDao
    {
        
        public String Code { get; set; }

        public String Name { get; set; }
    }
    public class KilometerTypeDaoMap : EntityMap<KilometerTypeDao>
    {
        internal KilometerTypeDaoMap()
        {
    
            Map(u => u.Code).ToColumn("code");
            Map(u => u.Name).ToColumn("naam");
             

          

        }
    }

}
