﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccesObjects
{
   public class KilometerDao
    {
        public int Id { get; set; }

        public bool AfgeslotenDoorPersoneelsdienst { get; set; }

        public String KilometerTypeCode { get; set; }
        public DateTime DatumIngave { get; set; }
 
        public string Bestuurder { get; set; }
        public String Reden { get; set; }

        public String Reisroute { get; set; }
        public Double Bedrag { get; set; }
        public Double Kilometers { get; set; }
        public DateTime DatumRit { get; set; }

        public int? Beginstand { get; set; }
        public int? Eindstand { get; set; }


        public int WagenId { get; set; }

    }
    public class KilometerDaoMap : EntityMap<KilometerDao>
    {
        internal KilometerDaoMap()
        {
            Map(u => u.Id).ToColumn("id");
            Map(u => u.DatumIngave).ToColumn("datumingave");
            Map(u => u.Bestuurder).ToColumn("bestuurder");
            Map(u => u.Reden).ToColumn("reden");
            Map(u => u.Bedrag).ToColumn("bedrag");
            Map(u => u.Kilometers).ToColumn("kilometers");
            Map(u => u.DatumRit).ToColumn("datumrit");
            Map(u => u.AfgeslotenDoorPersoneelsdienst).ToColumn("afgesloten");
            Map(u => u.Reisroute).ToColumn("reisroute");
            Map(u => u.KilometerTypeCode).ToColumn("code");
            Map(u => u.WagenId).ToColumn("wagenid");


        }
    }

}
