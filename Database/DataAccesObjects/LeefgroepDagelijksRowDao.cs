﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class LeefgroepDagelijksRowDao
    {

        public int LeefgroepId { get; set; }

        public string LeefgroepNaam { get; set; }
        public string DagDeel { get; set; }

        public string KoudWarm { get; set; }
        public string DieetEnBereiding { get; set; }

    }

    public class LeefgroepGroepringRowDao
    {

        public int? ChildId { get; set; }
        public int ParentId { get; set; }

        public String ChildName { get; set; } = "";
        public string ParentName { get; set; } = "";

        public int Sortorder { get; set; }

        public string GroepNaam { get; set; } = "";
    }

}
