﻿using Dapper.FluentMap.Mapping;
using System;
 

namespace Database.DataAccessObjects
{
    public class LeefgroepDao
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public Char FromOrbis { get; set; }


        public bool IsFromOrbis { get { return FromOrbis == 'Y'; } }


    }

    public class LeefgroepDaoMap : EntityMap<LeefgroepDao>
    {
        internal LeefgroepDaoMap()
        {
            Map(u => u.Id).ToColumn("id");
            Map(u => u.Name).ToColumn("naam");
            Map(u => u.FromOrbis).ToColumn("fromorbis");

        }
    }
}
