﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccesObjects
{
    public class LocatieDao
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public String Naam { get; set; }

    }
    public class LocatieDaoMap : EntityMap<LocatieDao>
    {
        internal LocatieDaoMap()
        {
            Map(u => u.Id).ToColumn("id");
            Map(u => u.ParentId).ToColumn("parent_id");
            Map(u => u.Naam).ToColumn("naam");
        }
    }
}
