﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccessObjects
{ 
    public class LogonDao
    {

        public bool LoggedOn { get; set; }
       
        public String ErrorMessage { get; set; }
 
        public bool NeedNewPassword { get; set; }
    
        public int UserId { get; set; }
        [DataMember]
        public String Name { get; set; }
 
        public String Email { get; set; }
        [DataMember]
        public String AuthorisationToken { get; set; }
 
        public int CompanyId { get; set; }

     
        public Byte[] CompanyLogo { get; set; }
  
        public string DatabaseAliasName { get; set; }

 
        public DateTime? LogonDate { get; set; }

        public LogonDao()
        {

        }
    }
}
