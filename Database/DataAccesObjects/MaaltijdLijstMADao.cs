﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccessObjects
{
    public class MaaltijdLijstMADao
    {
        public int OnderwerpGroepId { get; set; }
        public int OnderwerpId { get; set; }
        public DateTime Datum { get; set; }
        public String ProfielVeldNaam { get; set; }
    }
    public class MaaltijdLijstMADaoMap : EntityMap<MaaltijdLijstMADao>
    {
        internal MaaltijdLijstMADaoMap()
        {
            Map(u => u.OnderwerpGroepId).ToColumn("OnderwerpGroepId");
            Map(u => u.OnderwerpId).ToColumn("OnderwerpId");
            Map(u => u.ProfielVeldNaam).ToColumn("ProfielVeldNaam");
            Map(u => u.Datum).ToColumn("Datum");
        }
    }
}