﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class MultiSelectDao
    {
        public int Id { get; set; }
        public string ModuleCode { get; set; }
        public string Title { get; set; }
        public string ExtraInfo { get; set; }
        public string ChoiceType { get; set; }
        public bool IsMandatory { get; set; }
        public int ItemId { get; set; }
        public string ItemDescription { get; set; }
        public bool ItemIsDefaultSelected { get; set; }
        public bool ItemIsOther { get; set; }
        public int ItemOrderNum { get; set; }
        public bool Active { get; set; }
        public bool ActiveInDevelopment { get; set; }
        public int OrderNumReport { get; set; }
    }
}
