﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccessObjects
{
    public class OnkostennotaCategorieDao
    {
        public int Id { get; set; }

        public string Naam { get; set; }
        public string Beschrijving { get; set; }

        public bool Actief { get; set; }

 

       
    }
    public class OnkostennotaCategorieDaoMap : EntityMap<OnkostennotaCategorieDao>
    {
        internal OnkostennotaCategorieDaoMap()
        {
            Map(u => u.Id).ToColumn("Id");
            Map(u => u.Naam).ToColumn("Naam");
            Map(u => u.Beschrijving).ToColumn("Beschrijving");
            Map(u => u.Actief).ToColumn("Actief");
        }
    }
}
