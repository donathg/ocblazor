﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccessObjects
{

    public class MaaltijdAandachtspuntenDao
    {
        public int Id { get; set; }

        public String Naam { get; set; }
         

  
    }

    public class OnkostennotaItemDao
    {
        public int Id { get; set; }

        public int GebruikerId { get; set; }
        public DateTime CreationDate { get; set; }

        public int CategorieId { get; set; }
        public String CategorieNaam { get; set; }


        public DateTime OnkostenNotaDate { get; set; }

        public String Naam { get; set; }

        public String Beschrijving { get; set; }

        public double Bedrag { get; set; }

        public bool IsAfgerekend { get; set; }

        public string ClientenStringFromDB { get; set; }

        public string LeefgroepName { get; set; }

        public int? AandachtspuntenEcqareId { get; set; }

        public string AandachtspuntenEcqareNaam { get; set; }
    }

    public class OnkostennotaCategorieItemDao
    {

        public int Id { get; set; }

        public string Naam { get; set; }


        public string Code { get; set; }


        public string ModuleCode { get; set; }

    }


    public class OnkostennotaDaoMap : EntityMap<OnkostennotaItemDao>
    {
        internal OnkostennotaDaoMap()
        {
            Map(u => u.Id).ToColumn("Id");
            Map(u => u.GebruikerId).ToColumn("GebruikerId");
            Map(u => u.CreationDate).ToColumn("CreationDate");
            Map(u => u.CategorieId).ToColumn("CategorieId");
            Map(u => u.CategorieNaam).ToColumn("CategorieNaam");
            Map(u => u.OnkostenNotaDate).ToColumn("OnkostenNotaDate");
            Map(u => u.Naam).ToColumn("Naam");
            Map(u => u.Beschrijving).ToColumn("Beschrijving");
            Map(u => u.Bedrag).ToColumn("Bedrag");
            Map(u => u.IsAfgerekend).ToColumn("IsAfgerekend");
            Map(u => u.ClientenStringFromDB).ToColumn("clienten");
            Map(u => u.LeefgroepName).ToColumn("LeefgroepName");
            Map(u => u.AandachtspuntenEcqareId).ToColumn("AandachtspuntenEcqareId");
            Map(u => u.AandachtspuntenEcqareNaam).ToColumn("AandachtspuntenEcqareNaam");
             

    }
    }
    public class OnkostennotaCategorieItemDaoMap : EntityMap<OnkostennotaCategorieItemDao>
    {
        internal OnkostennotaCategorieItemDaoMap()
        {
            Map(u => u.Id).ToColumn("Id");
            Map(u => u.Naam).ToColumn("Naam");
            Map(u => u.Code).ToColumn("Code");
            Map(u => u.ModuleCode).ToColumn("moduleCode");

        }
    }

    public class MaaltijdAandachtspuntenDaoMap : EntityMap<MaaltijdAandachtspuntenDao>
    {
        internal MaaltijdAandachtspuntenDaoMap()
        {
            Map(u => u.Id).ToColumn("Id");
            Map(u => u.Naam).ToColumn("Naam");
         

        }
    }
}
