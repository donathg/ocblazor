﻿using Dapper.FluentMap.Mapping;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class StandaardPrijzenDAO
    {
        public int Id { get; set; }

        public string ModuleCode { get; set; }
        public string SubType_1 { get; set; }
        public string SubType_2 { get; set; }
        public decimal Bedrag { get; set; }
        public DateTime StartDatum { get; set; }
        public DateTime? EindDatum { get; set; }
    }
    public class StandaardPrijzenDAOMap : EntityMap<StandaardPrijzenDAO>
    {
        internal StandaardPrijzenDAOMap()
        {
            Map(u => u.Id).ToColumn("id");
            Map(u => u.ModuleCode).ToColumn("moduleCode");
            Map(u => u.SubType_1).ToColumn("SubType_1");
            Map(u => u.SubType_2).ToColumn("SubType_2");
            Map(u => u.Bedrag).ToColumn("bedrag");
            Map(u => u.StartDatum).ToColumn("startDatum");
            Map(u => u.EindDatum).ToColumn("eindDatum");
        }
    }
}
