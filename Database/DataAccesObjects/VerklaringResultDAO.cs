﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class VerklaringResultDAO
    {
        public string BatchNr { get; set; }
        public int GebruikerId { get; set; }
        public int? VerklaringItemId { get; set; }
        public DateTime ModificationDate { get; set; }
        public double? AfstandEnkeleReisFiets { get; set; }
        public double? AfstandEnkeleReisWagen { get; set; }
        public bool VerklaringOndertekend { get; set; }
    }
    public class VerklaringResultDAOMap : EntityMap<VerklaringResultDAO>
    {
        public VerklaringResultDAOMap()
        {
            Map(u => u.BatchNr).ToColumn("BatchNr");
            Map(u => u.GebruikerId).ToColumn("GebruikerId");
            Map(u => u.VerklaringItemId).ToColumn("VerklaringItemId");
            Map(u => u.ModificationDate).ToColumn("ModificationDate");
            Map(u => u.AfstandEnkeleReisWagen).ToColumn("AfstandEnkeleReisWagen");
            Map(u => u.AfstandEnkeleReisFiets).ToColumn("AfstandEnkeleReisFiets");
            Map(u => u.VerklaringOndertekend).ToColumn("VerklaringOndertekend");
        }
    }
}
