﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccesObjects
{
    public class WagenDao
    {
        public int Id { get; set; }
        public String Naam { get; set; }

        public String Nummerplaat { get; set; }

        public String Merk { get; set; }

        public String LocatieNaam { get; set; }
    }
    public class WagenDaoMap : EntityMap<WagenDao>
    {
        internal WagenDaoMap()
        {
            Map(u => u.Id).ToColumn("id");
            Map(u => u.Naam).ToColumn("naam");
            Map(u => u.Nummerplaat).ToColumn("nummerplaat");
            Map(u => u.LocatieNaam).ToColumn("locatienaam");
            Map(u => u.Merk).ToColumn("merk");
        }
    }
}
