﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.DataAccesObjects
{
    public class WebserviceExternalDao
    {
         
            public int Id { get; set; }
            public string CustomerName { get; set; }
            public string ApiName { get; set; }
            public string ApiLocation { get; set; }
            public string ApiContent { get; set; }
            public string ApiDescription { get; set; }
            public DateTime? LastRunDate { get; set; }
            public string LastRunStatus { get; set; }
            public string LastRunError { get; set; }
         


    }
    public class WebserviceDaoMap : EntityMap<WebserviceExternalDao>
    {
        internal WebserviceDaoMap()
        {
            Map(u => u.Id).ToColumn("id");
            Map(u => u.CustomerName).ToColumn("customerName");
            Map(u => u.ApiName).ToColumn("api_name");
            Map(u => u.ApiLocation).ToColumn("api_location");
            Map(u => u.ApiContent).ToColumn("api_content");
            Map(u => u.ApiDescription).ToColumn("api_description");
            Map(u => u.LastRunDate).ToColumn("last_run_date");
            Map(u => u.LastRunStatus).ToColumn("last_run_status");
            Map(u => u.LastRunError).ToColumn("last_run_error");
        }
    }
}
