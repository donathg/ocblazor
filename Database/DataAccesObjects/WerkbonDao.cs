﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.DataAccessObjects
{
    public class WerkbonDao
    {
        public int Id { get; set; }
        public String Titel { get; set; }
    }
    public class WerkbonDaoMap : EntityMap<WerkbonDao>
    {
        internal WerkbonDaoMap()
        {
            Map(u => u.Id).ToColumn("wo_id");
            Map(u => u.Titel).ToColumn("wo_titel");

        }
    }
}
