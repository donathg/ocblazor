using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Dapper;
namespace Database
{


    public class CrudActionInfo
    {
        public object Data { get; set; }

        public Action<TransactionActionContext> Action { get; set; }
    }

    public class TransactionActionContext
    {
        public IDbConnection Connection { get; set; }
        public IDbTransaction Transaction { get; set; }
        public object Data { get; set; }

    }
    public static class DatabaseExtentions
    {
        /*   
         *   EXEMPLE
         *   
         *   _dbConnection.ExecuteInTransaction(
                      new CrudActionInfo() { Action = DeleteTest, Data = new { id = 5 } },
                      new CrudActionInfo() { Action = InsertTest, Data = new List<BusinessServices.DataBusinessObjects.Mandant.Mandant>() }
                      ) ;
         }
         private void DeleteTest(TransactionActionContext context)
         {
               if (context.id is int id)
               {
                        context.Connection.Excecute("delete from test where id= :idToDelete", new {idToDelete = id})
               }
         }
         private void InsertTest(TransactionActionContext context)
         {


         }
              */

        public static void ExecuteInTransaction(this IDbConnection connection, params CrudActionInfo[] crudActionInfo)
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                try
                {

                    foreach (CrudActionInfo cai in crudActionInfo)
                    {
                        cai.Action(new TransactionActionContext() { Connection = connection, Transaction = transaction, Data = cai.Data });
                    }
            
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }

            }

        }


    }
}
