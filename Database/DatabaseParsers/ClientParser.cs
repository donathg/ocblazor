﻿ 
using Database.DataAccessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Logic.DataBusinessObjects;

namespace Database.DatabaseParsers
{
    public static class ClientParser
    {
        public static Client ParseDataTable(DataRow dr)
        {
            try
            {
                Client client = new Client();
                if (dr != null)
                {
                    if (dr.Table.Columns.Contains("BewonerID")) //fk
                        client.Id = (int)dr["BewonerID"];
                    else if (dr.Table.Columns.Contains("Id")) //id
                        client.Id = (int)dr["Id"];

                    if (dr.Table.Columns.Contains("Code"))
                        client.Code = (String)dr["Code"];
                    if (dr.Table.Columns.Contains("actif"))
                        client.Actif = (bool)dr["actif"];
                    if (dr.Table.Columns.Contains("opnameeinde") && dr.IsNull("opnameeinde") == false)
                        client.OpenameEinde = (DateTime)dr["opnameeinde"];
                    if (dr.Table.Columns.Contains("voornaam"))
                        client.FirstName = (String)dr["voornaam"];
                    if (dr.Table.Columns.Contains("achternaam"))
                        client.LastName = (String)dr["achternaam"];
                    if (dr.Table.Columns.Contains("Rijksregisternummer") && dr.IsNull("Rijksregisternummer") == false)
                        client.Rijksregisternr = (String)dr["Rijksregisternummer"];
                    if (dr.Table.Columns.Contains("ecqareId") && dr["ecqareId"] is Guid guid) //id
                        client.EcqareId = guid;
                    if (dr.Table.Columns.Contains("dossierId") && dr["dossierId"] is Guid guidDossier) 
                        client.DossierId = guidDossier;

                    Leefgroep leefGroep = new Leefgroep();
                    if (dr.Table.Columns.Contains("LeefgroepId") && dr["LeefgroepId"] is int leefgroepId)
                    {
                        leefGroep.Id = leefgroepId;
                    }
                    if (dr.Table.Columns.Contains("LeefgroepName") && dr["LeefgroepName"] is string leefgroepNaam)
                    {
                        leefGroep.Name = leefgroepNaam;
                    }
                    client.Leefgroep.Add(leefGroep);

                }
                return client;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
