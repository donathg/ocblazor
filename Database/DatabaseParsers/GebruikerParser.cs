﻿
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
 

namespace Database.DatabaseParsers
{
    public static class GebruikerParser
    {
        public static Gebruiker ParseDataTable(DataRow dr)
        {
            Gebruiker gebruiker = new Gebruiker();
            if (dr != null)
            {
                if (dr.Table.Columns.Contains("GebruikerID") && dr.IsNull("GebruikerID") == false) //fk
                    gebruiker.Id = (int)dr["GebruikerID"];
                else if (dr.Table.Columns.Contains("Id")) //id
                    gebruiker.Id = (int)dr["Id"];
                if (dr.Table.Columns.Contains("wachtwoord"))
                    gebruiker.Password = (String)dr["wachtwoord"];
                if (dr.Table.Columns.Contains("voornaam"))
                    gebruiker.FirstName = (String)dr["voornaam"];
                if (dr.Table.Columns.Contains("achternaam"))
                    gebruiker.LastName = (String)dr["achternaam"];
                gebruiker.Name = gebruiker.FirstName + " " + gebruiker.LastName;

                if (dr.Table.Columns.Contains("personeelscode") && dr.IsNull("personeelscode") == false)
                    gebruiker.Code = (String)dr["personeelscode"];

                if (dr.Table.Columns.Contains("Email") && dr.IsNull("Email") == false)
                    gebruiker.Email = (String)dr["Email"];

                if (dr.Table.Columns.Contains("telefoon") && dr.IsNull("telefoon") == false)
                    gebruiker.Phone = (String)dr["telefoon"];

                if (dr.Table.Columns.Contains("gsm") && dr.IsNull("gsm") == false)
                    gebruiker.MobilePhone = (String)dr["gsm"];

                if (dr.Table.Columns.Contains("telefoon_intern_groep") && dr.IsNull("telefoon_intern_groep") == false)
                    gebruiker.InternGroupPhoneNumber = (String)dr["telefoon_intern_groep"];

                if (dr.Table.Columns.Contains("telefoon_intern") && dr.IsNull("telefoon_intern") == false)
                    gebruiker.InternalPhoneNumber = (String)dr["telefoon_intern"];

                if (dr.Table.Columns.Contains("functiebeschrijving") && dr.IsNull("functiebeschrijving") == false)
                    gebruiker.Function = (String)dr["functiebeschrijving"];

                if (dr.Table.Columns.Contains("werkplaats") && dr.IsNull("werkplaats") == false)
                    gebruiker.Workplace = (String)dr["werkplaats"];

                if (dr.Table.Columns.Contains("foto") && dr.IsNull("foto") == false)
                    gebruiker.Foto = (Byte[])dr["foto"];

                if (dr.Table.Columns.Contains("actif") && dr.IsNull("actif") == false)
                    gebruiker.Active = (bool)dr["actif"];

            }
            return gebruiker;
        }

    }
}
