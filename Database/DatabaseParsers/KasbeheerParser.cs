﻿
using Database.DataAccessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
 

namespace Database.DatabaseParsers
{
    public static class KasbeheerParser
    {
        
        public static KasbeheerTransactieType ParseDataTableKasbeheerTransactieType(DataRow dr, params String[] excludeColumns)
        {
            List<String> excludes = new List<string>();

            if (excludeColumns != null)
            {
                foreach (String s in excludeColumns)
                    excludes.Add(s);
            }

            KasbeheerTransactieType kasbeheerTransactie = new KasbeheerTransactieType();
            if (dr != null)
            {

                if (dr.Table.Columns.Contains("Id") && excludes.Contains("Id") == false)
                    kasbeheerTransactie.Id = (int)dr["Id"];
                else if (dr.Table.Columns.Contains("transactieTypeId"))
                    kasbeheerTransactie.Id = (int)dr["transactieTypeId"];
                if (dr.Table.Columns.Contains("Naam") && excludes.Contains("Naam") == false)
                    kasbeheerTransactie.Name = (String)dr["Naam"];
                else if (dr.Table.Columns.Contains("transactieTypeNaam") && excludes.Contains("transactieTypeNaam") == false)
                    kasbeheerTransactie.Name = (String)dr["transactieTypeNaam"];

              if (dr.Table.Columns.Contains("isClientBased") && excludes.Contains("isClientBased") == false)
                    kasbeheerTransactie.IsClientBased = (bool)dr["isClientBased"];
             else if (dr.Table.Columns.Contains("transactieTypeIsClientBased") && excludes.Contains("transactieTypeIsClientBased") == false)
                    kasbeheerTransactie.IsClientBased = (bool)dr["transactieTypeIsClientBased"];
            }
            return kasbeheerTransactie;
        }
        public static KasbeheerTransactieCodering ParseDataTableKasbeheerTransactieCodering(DataRow dr, params String[] excludeColumns)
        {
            List<String> excludes = new List<string>();

            if (excludeColumns != null)
            {
                foreach (String s in excludeColumns)
                    excludes.Add(s);
            }

            KasbeheerTransactieCodering kasbeheerTransactie = new KasbeheerTransactieCodering();
            if (dr != null)
            {
                
                if (dr.Table.Columns.Contains("kode") && excludes.Contains("kode") == false)
                    kasbeheerTransactie.Kode = (string)dr["kode"];
                else if (dr.Table.Columns.Contains("transactieKode"))
                    kasbeheerTransactie.Kode = (string)dr["transactieKode"];

                if (dr.Table.Columns.Contains("omschrijving") && excludes.Contains("omschrijving") == false)
                    kasbeheerTransactie.Name = (String)dr["omschrijving"];
                

                if (dr.Table.Columns.Contains("isClientBased") && excludes.Contains("isClientBased") == false)
                    kasbeheerTransactie.IsClientBased = (bool)dr["isClientBased"];
                else if (dr.Table.Columns.Contains("transactieCodeIsClientBased") && excludes.Contains("transactieCodeIsClientBased") == false)
                    kasbeheerTransactie.IsClientBased = (bool)dr["transactieCodeIsClientBased"];
            }
            return kasbeheerTransactie;
        }
        public static KasbeheerRekening ParseDataTableKasbeheerRekening(DataRow dr, params String[] excludeColumns)
        {
            List<String> excludes = new List<string>();

            if (excludeColumns != null)
            {
                foreach (String s in excludeColumns)
                    excludes.Add(s);
            }
            KasbeheerRekening kasbeheerTransactie = new KasbeheerRekening();
            if (dr != null)
            {
                if (dr.Table.Columns.Contains("Id") && excludes.Contains("Id") == false)
                    kasbeheerTransactie.Id = (int)dr["Id"];
                else
                if (dr.Table.Columns.Contains("RekeningId"))
                    kasbeheerTransactie.Id = (int)dr["RekeningId"];
                if (dr.Table.Columns.Contains("Naam") && excludes.Contains("Naam") == false)
                    kasbeheerTransactie.Name = (String)dr["name"];
                else if (dr.Table.Columns.Contains("RekeningNaam") && excludes.Contains("RekeningNaam") == false)
                    kasbeheerTransactie.Name = (String)dr["RekeningNaam"];
            }
            return kasbeheerTransactie;
        }
        public static KasbeheerTransactie ParseDataTableKasbeheerTransactie(DataRow dr, params String[] excludeColumns)
        {
            List<String> excludes = new List<string>();
            if (excludeColumns != null)
            {
                foreach (String s in excludeColumns)
                    excludes.Add(s);
            }
            KasbeheerTransactie kasbeheerTransactie = new KasbeheerTransactie();
            if (dr != null)
            {
                if (dr.Table.Columns.Contains("Id"))
                    kasbeheerTransactie.Id = (int)dr["Id"];

                if (dr.Table.Columns.Contains("TransactieDatum"))
                    kasbeheerTransactie.TransactieDatum = (DateTime)dr["transactieDatum"];

                if (dr.Table.Columns.Contains("TransactieOmschrijving"))
                    kasbeheerTransactie.TransactieOmschrijving = (String)dr["transactieOmschrijving"];

                if (dr.Table.Columns.Contains("Transactiebedrag"))
                    kasbeheerTransactie.TransactieBedrag = (decimal)dr["Transactiebedrag"];

                if (dr.Table.Columns.Contains("afgerekend"))
                    kasbeheerTransactie.Afgerekend = (bool)dr["afgerekend"];
                

                kasbeheerTransactie.Rekening = KasbeheerParser.ParseDataTableKasbeheerRekening(dr, "Id");
                kasbeheerTransactie.Leefgroep = LeefgroepParser.ParseDataTable(dr, "Id");
                kasbeheerTransactie.TransactieType = KasbeheerParser.ParseDataTableKasbeheerTransactieType(dr, "Id");
                kasbeheerTransactie.TransactieCode = KasbeheerParser.ParseDataTableKasbeheerTransactieCodering(dr, "Id");

            }
            return kasbeheerTransactie;
        }

    }
}
