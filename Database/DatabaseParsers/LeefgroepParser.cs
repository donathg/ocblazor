﻿ 
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
 

namespace Database.DatabaseParsers
{
    public static class LeefgroepParser
    {
        public static List<Leefgroep> ParseDataTable(DataTable dt)
        {
            List<Leefgroep> leefgroepen = new List<Leefgroep>();

            foreach (DataRow dr in dt.Rows)
            {
                Leefgroep lfg = new Leefgroep
                {
                    Code = dr["code"] != DBNull.Value ? dr["code"].ToString() : null,
                    Name = dr["naam"] != DBNull.Value ? dr["naam"].ToString() : null,
                    Actief = dr["actief"].ToString() == "Y",
                    Id = dr["id"] != DBNull.Value ? Convert.ToInt32(dr["id"]) : 0,
                    NietVerbondenAanSchool = dr["nietVerbondenAanSchool"] != DBNull.Value && Convert.ToBoolean(dr["nietVerbondenAanSchool"]),
                    DisplayNaam = dr["displayNaam"] != DBNull.Value ? dr["displayNaam"].ToString() : null,
                    StartDatum = dr["startDatum"] != DBNull.Value ? Convert.ToDateTime(dr["startDatum"]) : DateTime.MinValue,
                    EindDatum = dr["eindDatum"] != DBNull.Value ? Convert.ToDateTime(dr["eindDatum"]) : DateTime.MinValue,
                    Ondersteuningsvorm = dr["ondersteuningsvorm"] != DBNull.Value ? dr["ondersteuningsvorm"].ToString() : null,

                    EcqareId = dr["ecqareId"] != DBNull.Value ? Guid.Parse(dr["ecqareId"].ToString()) : Guid.Empty,
                    EcqareParentId = dr["ecqareParentId"] != DBNull.Value ? Guid.Parse(dr["ecqareParentId"].ToString()) : Guid.Empty,
                    EcqareCode = dr["ecqareCode"] != DBNull.Value ? dr["ecqareCode"].ToString() : null,
                    IsEcqareLowestLevel = dr["isEcqareLowestLevel"] != DBNull.Value && Convert.ToBoolean(dr["isEcqareLowestLevel"]),

                    MaaltijdlijstenOrderNum = dr["maaltijdlijstenOrderNum"] != DBNull.Value ? Convert.ToInt32(dr["maaltijdlijstenOrderNum"]) : 0,
                    MaaltijdlijstenGroepering = dr["maaltijdlijstenGroepering"] != DBNull.Value ? dr["maaltijdlijstenGroepering"].ToString() : null,
                    MaaltijdlijstenVacuumVerpakking = dr["maaltijdlijstenVacuumVerpakking"] != DBNull.Value && Convert.ToBoolean(dr["maaltijdlijstenVacuumVerpakking"]),
                    HeeftMaaltijdLevering = dr["heeftMaaltijdLevering"] != DBNull.Value && Convert.ToBoolean(dr["heeftMaaltijdLevering"]),

                    IsMaaltijdAdministratieBudgetVervangend = dr["isMaaltijdAdministratieBudgetVervangend"] != DBNull.Value && Convert.ToBoolean(dr["isMaaltijdAdministratieBudgetVervangend"]),
                    IsMaaltijdAdministratieGeenLeveringen = dr["isMaaltijdAdministratieGeenLeveringen"] != DBNull.Value && Convert.ToBoolean(dr["isMaaltijdAdministratieGeenLeveringen"]),
                    MaaltijdlijstenGroeperingAdministratie = dr["maaltijdlijstenGroeperingAdministratie"] != DBNull.Value ? dr["maaltijdlijstenGroeperingAdministratie"].ToString() : null
                };

                leefgroepen.Add(lfg);
            }
            return leefgroepen;
        }
        public static Leefgroep ParseDataTable(DataRow dr, params String[] excludeColumns)
        {

            List<String> excludes = new List<string>();

            if (excludeColumns != null)
            {
                foreach (String s in excludeColumns)
                    excludes.Add(s);
            }



            Leefgroep Leefgroep = new Leefgroep();
            if (dr != null)
            {
                if (dr.Table.Columns.Contains("Id") && excludes.Contains("Id") == false)
                    Leefgroep.Id = (int)dr["Id"];
                else if (dr.Table.Columns.Contains("LeefgroepId"))
                    Leefgroep.Id = (int)dr["LeefgroepId"];

                if (dr.Table.Columns.Contains("Naam") && excludes.Contains("Naam") == false)
                    Leefgroep.Name = (String)dr["Naam"];
                else if (dr.Table.Columns.Contains("LeefgroepName") && excludes.Contains("LeefgroepName") == false)
                    Leefgroep.Name = (String)dr["LeefgroepName"];
            }
            return Leefgroep;
        }
    }
}
