﻿using System;
using System.Collections.Generic;
 
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.Global
{
    public class GlobalMethods
    {
        public static String EFConnectionBuilder(string serverName, string databaseName, string userId, string password, bool IntegratedSecurity, bool TrustServerCertificate, bool Encrypt)
        {
            string providerName = "System.Data.SqlClient";

            SqlConnectionStringBuilder sqlBuilder =
            new SqlConnectionStringBuilder();

            // Set the properties for the data source.
            sqlBuilder.DataSource = serverName;
            sqlBuilder.InitialCatalog = databaseName;

            sqlBuilder.Password = password;
            sqlBuilder.UserID = userId;



            sqlBuilder.IntegratedSecurity = IntegratedSecurity;
            sqlBuilder.Encrypt = Encrypt;
            sqlBuilder.TrustServerCertificate = TrustServerCertificate;

            return "";



            // Build the SqlConnection connection string.
         //   string providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
         //   EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

            //Set the provider name.
           // entityBuilder.Provider = providerName;

            // Set the provider-specific connection string.
           // entityBuilder.ProviderConnectionString = providerString;

            // Set the Metadata location.
           // entityBuilder.Metadata = @"res://*/Models.Model1.csdl|res://*/Models.Model1.ssdl|res://*/Models.Model1.msl";
            //return entityBuilder.ConnectionString;
        }

        public static List<String> GetAllProperties(object obj)
        {
             List<String> propertiesList = new List<string>();
             Type typeSource = obj.GetType();
             PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
             foreach (PropertyInfo property in propertyInfo)
             {
                 propertiesList.Add(property.Name);
             }
             return propertiesList;
     
        }
        public static void CopyPublicProperties(object objSource, object objTarget, params string[] excludePropertyName)
        {
            //step : 1 Get the type of source object and create a new instance of that type
             Type typeSource = objSource.GetType();
              //Step2 : Get all the properties of source object type

              PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

              //Step : 3 Assign all source property to taget object 's properties
              foreach (PropertyInfo property in propertyInfo)
              {
                  if (excludePropertyName.Contains(property.Name))
                      continue;
                  //Check whether property can be written to
                  if (property.CanWrite)

                  {
                      //Step : 4 check whether property type is value type, enum or string type
                      if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(System.String)))
                      {
                          property.SetValue(objTarget, property.GetValue(objSource, null), null);
                      }
                      //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached


                  }

              }
              

        }
    }
}
