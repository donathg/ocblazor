﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using Dapper.FluentMap;
using Dapper.FluentMap.Mapping;
using Database.DataAccesObjects;
using Database.DataAccessObjects;

namespace Database
{
    public static class InitializeMappings
    {

        public static void Initialize()
        {

            FluentMapper.Initialize(config =>
            {
                config.AddMap(new GebruikerDaoMap());
                config.AddMap(new LeefgroepDaoMap());
                config.AddMap(new ClientDaoMap());
                config.AddMap(new ArtikelDaoMap());
                config.AddMap(new LocatieDaoMap());
                config.AddMap(new WerkbonDaoMap());
                config.AddMap(new AankoopArtikelDaoMap());
                config.AddMap(new BijlageDaoMap());
                config.AddMap(new KilometerDaoMap());
                config.AddMap(new KilometerTypeDaoMap());
                config.AddMap(new WagenDaoMap());
                config.AddMap(new KampDaoMap());
                config.AddMap(new MaaltijdLijstMADaoMap());
                config.AddMap(new ExternalDashboardMedewerkersDaoMap());
                config.AddMap(new WebserviceDaoMap());
                config.AddMap(new OnkostennotaCategorieDaoMap());
                config.AddMap(new OnkostennotaDaoMap());
                config.AddMap(new OnkostennotaCategorieItemDaoMap());
                config.AddMap(new MaaltijdAandachtspuntenDaoMap());

                

            });
        }

    }
}
