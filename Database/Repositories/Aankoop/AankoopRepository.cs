﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.ArtikelNs;
using Logic.DataBusinessObjects.Aankoop;
using Database.DataAccesObjects;
using Dapper;
using Database.Sql;

namespace DataBase.Repositories.Aankoop
{



    public class AankoopRepository : IAankoopRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;
        public AankoopRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _sqlManager) = (connection, userService, appSettings.Value, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

        public void Delete(int aankoopArtikelId)
        {
            throw new NotImplementedException();
        }

        public List<EntiteitAankoop> Get(int artikelCategorieId)
        {
            
            String sql = _sqlManager.GetSQL("SelectAankoopArtikel.sql");
            try
            {
                _connection.Open();
                IEnumerable<AankoopArtikelDao> listDaos  = _connection.Query<AankoopArtikelDao>(sql, new { ArtikelCategorieId = artikelCategorieId });
                return ConvertDaos(listDaos.ToList());
            }
            finally
            {
                _connection.Close();
            }
        }

        public void InsertClient(Client client, AankoopArtikel artikel)
        {
            AankoopArtikelDao dao = ConvertToDao(client, artikel);
            String sql = _sqlManager.GetSQL("InsertAankoopArtikel.sql");
            try
            {
                _connection.Open();
                artikel.DatabaseId = dao.DatabaseId = _connection.ExecuteScalar<int>(sql, dao);
            }
            finally
            {
                 _connection.Close();
            }
        }
        public void InsertLeefgroep(Leefgroep leefgroep, AankoopArtikel artikel)
        {
            AankoopArtikelDao dao = ConvertToDao(leefgroep, artikel);
            String sql = _sqlManager.GetSQL("InsertAankoopArtikel.sql");
            try
            {
                _connection.Open();
                artikel.DatabaseId = dao.DatabaseId = _connection.ExecuteScalar<int>(sql, dao);
            }
            finally
            {
                _connection.Close();
            }
        }
        public void UpdateArtikel(int databaseId, int aantal)
        {
            String sql = _sqlManager.GetSQL("UpdateAankoopArtikel.sql");
            try
            {
                _connection.Open();
                _connection.Execute(sql, new { aantal = aantal , id = databaseId }); 
            }
            finally
            {
                _connection.Close();
            }
        }
        public void DeleteArtikel(int databaseId)
        {
            String sql = _sqlManager.GetSQL("DeleteAankoopArtikel.sql");
            try
            {
                _connection.Open();
                _connection.Execute(sql, new {  id = databaseId });
            }
            finally
            {
                _connection.Close();
            }
        }

         

        private AankoopArtikelDao ConvertToDao(Client client, AankoopArtikel artikel)
        {
             
            AankoopArtikelDao dao = new AankoopArtikelDao();
            dao.Aantal = artikel.Aantal;
            dao.ArtikelId = artikel.ArtikelId;
            dao.ArtikelPrijs = artikel.Prijs;
            dao.Aantal = artikel.Aantal;
           
            dao.LeefgroepInfo = client.LeefgroepenString;
            dao.ArtikelInfo = $"{artikel.Name}@@{artikel.ArtikelNr}@@{artikel.Maat}@@{artikel.KleurCode}@@{artikel.StuksPerVerpakking}@@{artikel.Prijs}";
            dao.BewonerId = client.Id;
            dao.LeefgroepId = null;
            dao.DatabaseId = artikel.DatabaseId;
            dao.CreationDate = DateTime.Now;
            dao.ClosedDate = null;
            dao.Closer = null;
            dao.Creator = _userService.LoggedOnUser.UserId;

            return dao;
        }
        private AankoopArtikelDao ConvertToDao(Leefgroep leefgroep, AankoopArtikel artikel)
        {

            AankoopArtikelDao dao = new AankoopArtikelDao();
            dao.Aantal = artikel.Aantal;
            dao.ArtikelId = artikel.ArtikelId;
            dao.ArtikelPrijs = artikel.Prijs;
            dao.Aantal = artikel.Aantal;

            dao.LeefgroepInfo = leefgroep.Name;
            dao.ArtikelInfo = $"{artikel.Name}@@{artikel.ArtikelNr}@@{artikel.Maat}@@{artikel.KleurCode}@@{artikel.StuksPerVerpakking}@@{artikel.Prijs}";
            dao.BewonerId = null;
            dao.LeefgroepId = leefgroep.Id;
            dao.DatabaseId = artikel.DatabaseId;
            dao.CreationDate = DateTime.Now;
            dao.ClosedDate = null;
            dao.Closer = null;
            dao.Creator = _userService.LoggedOnUser.UserId;

            return dao;
        }
        private List<EntiteitAankoop> ConvertDaos(List<AankoopArtikelDao> daos)
        {
            List<EntiteitAankoop> Aankoop = new List<EntiteitAankoop>();
            foreach (AankoopArtikelDao dao in daos)
            {

                EntiteitAankoop ca = Aankoop.Where(x => x.Client.Id == dao.BewonerId).FirstOrDefault();
                if (ca==null)
                    ca = Aankoop.Where(x => x.Leefgroep.Id == dao.LeefgroepId).FirstOrDefault();

                if (ca == null)
                {
                    ca = new EntiteitAankoop();
                    if (dao.BewonerId.GetValueOrDefault()>0)
                         ca.Client = new Client() { Id = dao.BewonerId.GetValueOrDefault() };
                    if (dao.LeefgroepId.GetValueOrDefault() > 0)
                        ca.Leefgroep = new Leefgroep() { Id = dao.LeefgroepId.GetValueOrDefault() };

                    Aankoop.Add(ca);
                }
                ca.Artikelen.Add(new AankoopArtikel() { ArtikelId = dao.ArtikelId, Aantal = dao.Aantal, DatabaseId = dao.DatabaseId });
     
                





            }
            return Aankoop;

        }

    }
}
