﻿insert into aankoop.bestelling
(
	
	BewonerId,
	ArtikelId,
	ArtikelPrijs,
	Aantal,
	LeefgroepId,
	LeefgroepInfo,
	ArtikelInfo,
	CreationDate,
	Creator,
	ClosedDate

)
values
(
	@BewonerId,
	@ArtikelId,
	@ArtikelPrijs,
	@Aantal,
	@LeefgroepId,
	@LeefgroepInfo,
	@ArtikelInfo,
	@CreationDate,
	@Creator,
	@ClosedDate
);
SELECT CAST(SCOPE_IDENTITY() as int)