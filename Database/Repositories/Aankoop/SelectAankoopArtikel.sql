﻿select

	aankoop.bestelling.Id Id,
	BewonerId,
	ArtikelId,
	LeefgroepId,
	ArtikelPrijs,
	Aantal,
	LeefgroepInfo,
    ArtikelInfo,
	CreationDate,
	Creator,
	ClosedDate,
	Closer,
artikelCategorie.id ArtikelCategorieId, artikelCategorie.naam ArtikelCategorieNaam
from
	aankoop.bestelling,artikelen,artikelCategorie
where aankoop.bestelling.ArtikelId = artikelen.id
and artikelen.categorieId = artikelCategorie.Id
and ClosedDate is null
and artikelen.categorieId  = @ArtikelCategorieId 
 