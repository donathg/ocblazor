﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.ArtikelNs;
using Database.Sql;

namespace DataBase.Repositories.Artikelen
{
    public class ArtikelRepository : IArtikelRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;
        public ArtikelRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

        public List<Artikel> GetArtikelen(bool aktiv)
        {
            String sql = _sqlManager.GetSQL("Artikelen.sql");
            


            try
            {
                _connection.Open();
                var artikelen = _connection.Query<ArtikelDao>(sql).ToList();
                return _mapper.Map<List<Artikel>>(artikelen).ToList();
            }
            finally
            {
                _connection.Close();
            }


         
        }
    }
}
