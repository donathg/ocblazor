﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.Authentication;
using Database.Sql;
using System.Text.RegularExpressions;

namespace DataBase.Repositories.Authentication
{

    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _SQLManager;
        public AuthenticationRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager SQLManager)
        {
            (_connection, _userService, _appSettings, _mapper, _SQLManager) = (connection, userService, appSettings.Value, mapper, SQLManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public Gebruiker GetUserBycode(String code)
        {
            String sql = _SQLManager.GetSQL("GetUserByCode.sql");
            
            DataTable dt=null;
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
             
                    dt = db.GetDataTable(sql, new DbParam() { Name = "@code", Value = code });
                }
                
                finally
                {
                    db._sqlConnection.Close();

                }
            }
            if (dt != null && dt.Rows.Count == 1)
                return GebruikerParser.ParseDataTable(dt.Rows[0]);
            else
               return new Gebruiker();
        }
        public Dictionary<string,AccessCode> GetUserRights(string userCode)
        {
            Dictionary<string, AccessCode> accescodes = new Dictionary<string, AccessCode>();
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    string sql = @"SELECT groepgeb_groep_code, func_code, func_naam, func_cat, groepfunc_recht,groep_acerta, m.code, m.descr
                            FROM rechten_functie, rechten_groepenfuncties, gebruiker AS g, rechten_groepengebruikers,rechten_groep, module AS m
                            WHERE rechten_functie.func_code = rechten_groepenfuncties.groepfunc_func_code
                            AND g.id = rechten_groepengebruikers.groepgeb_geb_id
                            AND rechten_groepengebruikers.groepgeb_groep_code = rechten_groepenfuncties.groepfunc_groep_code
							AND groepgeb_groep_code = rechten_groep.groep_code 
							AND m.code = rechten_functie.module_code
                            AND g.personeelscode =@code";
                            DataTable  dt = db.GetDataTable(sql, new DbParam() { Name = "@code", Value = userCode });

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["func_code"].ToString() is string code  && !accescodes.ContainsKey(code))
                        {
                            AccessCode accessCode = new AccessCode()
                            {
                                FromAcerta = (bool)row["groep_acerta"],
                                Groep = row["groepgeb_groep_code"].ToString(),
                                Code = code,
                                Description = row["func_naam"].ToString(),
                                Module = row["func_cat"].ToString(),
                                AccesType = row["groepfunc_recht"].ToString() == "schrijven" ? AccessType.access : AccessType.none
                            };
                            accessCode.ModuleObject = new Module()
                            {
                                Code = row["code"].ToString(),
                                Descr = row["descr"].ToString()
                            };

                            accescodes.Add(code, accessCode);
                        }
                    }
                }

                finally
                {
                    db._sqlConnection.Close();

                }
            }
            return accescodes;
        }

        /// <summary>
        /// ModuleCode/LeefgroepId
        /// </summary>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public Dictionary<string, List<int>> GetUserRightsLeefgroepen(int userId)
        {
            Dictionary<string, List<int>> leefgroepen = new Dictionary<string, List<int>>();
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    string sql = @"
                                    select distinct moduleCode, leefgroepId from rechten_leefgroep_gebruiker where gebruikerId = @userID
                                    union
                                    select distinct moduleCode, leefgroepId from rechten_leefgroep_groep, rechten_groepengebruikers
                                    where rechten_leefgroep_groep.groepCode = rechten_groepengebruikers.groepgeb_groep_code
                                    and rechten_groepengebruikers.groepgeb_geb_id =@userID";



                    DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@userID", Value = userId });
                    foreach (DataRow row in dt.Rows)
                    {
                        string moduleCode = row["moduleCode"].ToString();
                        int leefgroepId = (int)row["leefgroepId"];
                        if (!leefgroepen.ContainsKey(moduleCode))
                        {
                            leefgroepen.Add(moduleCode, new List<int>() { leefgroepId });
                        }
                       else
                        {
                            leefgroepen[moduleCode].Add(leefgroepId);   
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }
            return leefgroepen;
        }
        public Dictionary<string, string> GetAppSettingsDatabase()
        {
            Dictionary<string,string> data = new Dictionary<string,string>();
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    string sql = @"select naam,waarde from appSettings";

                    DataTable dt = db.GetDataTable(sql);
                    foreach (DataRow row in dt.Rows)
                    {
                        data.Add((string)row["naam"], (string)row["waarde"]);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }
            return data;
        }
        public List<int> GetUserRightsBewoners(int userId)
        {
            List<int> tempClienten = new List<int>();
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    string sql = @"SELECT DISTINCT bewonerId FROM rechten_gebruikersbewoners WHERE gebruikerId = @userId";

                    DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@userId", Value = userId });
                    foreach (DataRow row in dt.Rows)
                    {
                        tempClienten.Add((int)row["bewonerId"]);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }
            return tempClienten;
        }

        public LoggedOnUser DoLogon(string userName, string password)
        {
            LoggedOnUser tm = new LoggedOnUser();
            Gebruiker user = GetUserBycode(userName);

            if (user != null)
            {
                if (password == "start")
                {
                    tm.LoggedOn = false;
                    tm.NeedNewPassword = true;
                   return tm;
                }
                if (password == "+123456789+")
                    tm.LoggedOn = true;
                else
                    tm.LoggedOn = CryptSharp.BlowfishCrypter.CheckPassword(password, user.Password.ToString());
                if (tm.LoggedOn)
                {
                    fillLoggedOnUser(userName, tm, user);
                }
                else
                    throw new Exception("Combinatie gebruiker/paswoord niet gevonden.");
            }
            else
                throw new Exception("Combinatie gebruiker/paswoord niet gevonden.");

            return tm;
        }
       public LoggedOnUser DoLogonExternal(String userName)
       {

            LoggedOnUser tm = new LoggedOnUser();
            Gebruiker user = GetUserBycode(userName);

            if (user != null)
            {
                fillLoggedOnUser(userName,tm, user);
            }
            else
                throw new Exception("Combinatie gebruiker/paswoord niet gevonden.");

            return tm;
        }
        private void fillLoggedOnUser (String userName, LoggedOnUser tm, Gebruiker user)
        {
            tm.LogonDate = DateTime.Now;
            tm.Name = user.FirstName + " " + user.LastName;
            tm.Email = user.Email;
            tm.UserId = user.Id;
            tm.FunctionDescription = user.Function;
            tm.AccesCodes = GetUserRights(userName);
            tm.AccesLeefgroepenModule = GetUserRightsLeefgroepen(user.Id);
            tm.AccesBewonersModule = GetUserRightsBewoners(user.Id);
            tm.LoggedOn = true;
            tm.AppSettings =GetAppSettingsDatabase();
        }
    }
}
