﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.Clienten;
using Database.Sql;
using Logic.BijlageManagerServiceNS;
using Logic.DataBusinessObjects.BijlageManager;
using Database.DataAccesObjects;
using System.IO;

namespace DataBase.Repositories.Clienten
{
    public class BijlageManagerRepository : IBijlageManagerRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public BijlageManagerRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public Bijlage AddWerkopdrachtFile(Bijlage bijlage)
        {
            _connection.Open();
            bijlage.CreatorId = _userService.LoggedOnUser.UserId;
            bijlage.Creationdate = DateTime.Now;
            string sql = @"INSERT INTO werkopdracht_files(creator, creationdate, wo_id, fName, fExtension, filedata, description, category) 
                           VALUES (@CreatorId, @Creationdate, @ForeignKeyId, @FileName, @FileExtention, @FileData, @Description, @Category)";
            bijlage.Id = _connection.ExecuteScalar<int>(sql, bijlage);

            _connection.Close();

            return bijlage;
        }
        public Bijlage AddInventarisFile(Bijlage bijlage)
        {
            _connection.Open();
            bijlage.CreatorId = _userService.LoggedOnUser.UserId;
            bijlage.Creationdate = DateTime.Now;
            string sql = @"INSERT INTO inventaris_files(creator, creationdate, inv_id, fName, fExtension, filedata, description, category) 
                           VALUES (@CreatorId, @Creationdate, @ForeignKeyId, @FileName, @FileExtention, @FileData, @Description, @Category)";
            bijlage.Id = _connection.ExecuteScalar<int>(sql, bijlage);

            _connection.Close();
            return bijlage;
        }
        public Bijlage AddGogFile(Bijlage bijlage)
        {
            _connection.Open();
            bijlage.CreatorId = _userService.LoggedOnUser.UserId;
            bijlage.Creationdate = DateTime.Now;
            string sql = @"INSERT INTO gog.gog_files(creator, creationdate, gogFormId, fName, fExtension, filedata, description, category) 
                           VALUES (@CreatorId, @Creationdate, @ForeignKeyId, @FileName, @FileExtention, @FileData, @Description, @Category)";
            bijlage.Id = _connection.ExecuteScalar<int>(sql, bijlage);

            _connection.Close();
            return bijlage;
        }
        public Bijlage AddOnkostenNotaFile(Bijlage bijlage)
        {
          
            _connection.Open();
            bijlage.CreatorId = _userService.LoggedOnUser.UserId;
            bijlage.Creationdate = DateTime.Now;
            string sql = @"INSERT INTO acerta.onkostenNotafiles(creator, creationdate, onkostenNotaId, fName, fExtension, filedata, description, category) 
                           VALUES (@CreatorId, @Creationdate, @ForeignKeyId, @FileName, @FileExtention, @FileData, @Description, @Category)";
            bijlage.Id = _connection.ExecuteScalar<int>(sql, bijlage);

            _connection.Close();
            return bijlage;
        }
        public List<Bijlage> GetWerkbonnenbeheerBijlages(int id)
        {
            _connection.Open();
            String sql = _sqlManager.GetSQL("WerkbonnenBijlages.sql");
            List<BijlageDao> daos = _connection.Query<BijlageDao>(sql,new { woId = id}).ToList();
            _connection.Close();
            return _mapper.Map<List<Bijlage>>(daos);
        }
        public List<Bijlage> GetInventarisBijlages(int id)
        {
            _connection.Open();
            String sql = _sqlManager.GetSQL("InventarisBijlages.sql");
            List<BijlageDao> daos = _connection.Query<BijlageDao>(sql, new { woId = id }).ToList();
            _connection.Close();
            return _mapper.Map<List<Bijlage>>(daos);
        }
        public List<Bijlage> GetGOGBijlages(int id)
        {
            _connection.Open();
            string sql = _sqlManager.GetSQL("GogBijlages.sql");
            List<BijlageDao> daos = _connection.Query<BijlageDao>(sql, new { @gogId = id }).ToList();
            _connection.Close();
            return _mapper.Map<List<Bijlage>>(daos);
        }

        public List<Bijlage> GetOnkostenNotaFiles(int id)
        {
            _connection.Open();
            string sql = _sqlManager.GetSQL("OnkostenNotaBijlages.sql");
            List<BijlageDao> daos = _connection.Query<BijlageDao>(sql, new { @gogId = id }).ToList();
            _connection.Close();
            return _mapper.Map<List<Bijlage>>(daos);
        }
    

        public  void DeleteWerkopdrachtFile( int id)
        {
            _connection.Open();
            string sql = @"delete from werkopdracht_files where id=@id";
            _connection.Execute(sql, new { id = id });
            _connection.Close();

        }
        public void DeleteInventarisFile( int id)
        {
            _connection.Open();
            string sql = @"delete from inventaris_files where id=@id";
            _connection.Execute(sql, new { id = id });
            _connection.Close();
        }
        public void DeleteGogFile(int id)
        {
            _connection.Open();
            string sql = @"delete from gog.gog_files where id=@id";
            _connection.Execute(sql, new { id = id });
            _connection.Close();
        }   
        
        public void DeleteOnkostennotaFile(int id)
        {
            _connection.Open();
            string sql = @"delete from acerta.onkostenNotafiles where id=@id";
            _connection.Execute(sql, new { id = id });
            _connection.Close();
        }
    }
}
