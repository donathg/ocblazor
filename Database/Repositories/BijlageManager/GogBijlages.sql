﻿SELECT gf.id, gf.fName, gf.fExtension, CONCAT(g.voornaam, ' ', g.achternaam) as creator, gf.creationDate, gf.description, gf.fileData
FROM gebruiker as g
	inner join gog.gog_files as gf on g.id = gf.creator
	LEFT OUTER JOIN file_cats as fc ON fc.code = gf.category
WHERE gf.gogFormId = @gogId