﻿SELECT inventaris_files.id                  id,
       fname                                  AS filename,
       fextension,
       Concat (g.voornaam, ' ', g.achternaam) creator,
       creationdate,
       description, filedata
FROM   gebruiker AS g,
       inventaris_files
       LEFT OUTER JOIN file_cats
                    ON file_cats.code = inventaris_files.category
WHERE  g.id = creator  AND inv_id = @invId