﻿SELECT werkopdracht_files.id                  id,
       fname                                  AS filename,
       fextension,
       Concat (g.voornaam, ' ', g.achternaam) creator,
       creationdate,
       description, filedata
FROM   gebruiker AS g,
       werkopdracht_files
       LEFT OUTER JOIN file_cats
                    ON file_cats.code = werkopdracht_files.category
WHERE  g.id = creator  AND wo_id = @woId