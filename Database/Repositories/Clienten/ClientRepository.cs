﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.Clienten;
using Database.Sql;

namespace DataBase.Repositories.Clienten
{
    public class ClientRepository  : IClientRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public ClientRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

        public List<Client> GetAllClienten()
        {
            List<Client> clients = new List<Client>();
            List<Client> list = new List<Client>();
            DataTable dt;

            String sql = _sqlManager.GetSQL("Clienten.sql");
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    dt = db.GetDataTable(sql);
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }
            List<Client> tempList = new List<Client>();
            foreach (DataRow dr in dt.Rows)
            {
                tempList.Add(ClientParser.ParseDataTable(dr));
            }
            foreach (Client client in tempList)
            {
                Client c = clients.Where(x => x.Id == client.Id).FirstOrDefault();
                if (c == null)
                    clients.Add(client);
                else
                {
                    if (client.Leefgroep.Count > 0)
                        c.Leefgroep.Add(client.Leefgroep[0]);
                }
            }
            return clients;
        }

        public Dictionary<int, string> GetAttentionPointsForGebruiker(DateTime startDate, DateTime endDate)
        {
            try
            {
                Dictionary<int, string> bewonerAttentionpoints = new Dictionary<int, string>();

                using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
                {
                    string sql = @" SELECT ba.bewonerId,
                                        STUFF((
                                            SELECT '/' + a.naam
                                            FROM AandachtspuntenEcqare AS a
                                            INNER JOIN BewonerAandachtspuntenEcqare AS ba_sub
                                                ON a.id = ba_sub.aandachtspuntId
                                            WHERE ba.bewonerId = ba_sub.bewonerId
                                                AND a.ParentName != 'Voedsel: Extra'
                                            FOR XML PATH(''), TYPE
                                        ).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS points
                                    FROM BewonerAandachtspuntenEcqare AS ba
                                    WHERE ba.startDate <= @startDatum
	                                    AND ba.endDate is null or ba.endDate >= @endDate
                                    GROUP BY ba.bewonerId;";

                    List<DbParam> parameters = new List<DbParam>()
                    {
                        new DbParam() { Name = "@startDatum", Value = startDate },
                        new DbParam() { Name = "@endDate", Value = endDate }
                    };

                    foreach (DataRow row in db.GetDataTable(sql, parameters.ToArray()).Rows)
                    {
                        if (!string.IsNullOrEmpty(row["points"].ToString()))
                        {
                            bewonerAttentionpoints.Add((int)row["bewonerId"], row["points"].ToString());
                        }
                    }

                    return bewonerAttentionpoints;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public Dictionary<int, string> GetExtrasForGebruiker(DateTime startDate, DateTime endDate)
        {
            try
            {
                Dictionary<int, string> bewonerAttentionpoints = new Dictionary<int, string>();

                using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
                {
                    string sql = @" SELECT ba.bewonerId,
                                        STUFF((
                                            SELECT '/' + a.naam
                                            FROM AandachtspuntenEcqare AS a
                                            INNER JOIN BewonerAandachtspuntenEcqare AS ba_sub
                                                ON a.id = ba_sub.aandachtspuntId
                                            WHERE ba.bewonerId = ba_sub.bewonerId
                                                AND a.ParentName = 'Voedsel: Extra'
                                            FOR XML PATH(''), TYPE
                                        ).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS points
                                    FROM BewonerAandachtspuntenEcqare AS ba
                                    WHERE ba.startDate <= @startDatum
	                                    AND ba.endDate is null or ba.endDate >= @endDate
                                    GROUP BY ba.bewonerId;";

                    List<DbParam> parameters = new List<DbParam>()
                    {
                        new DbParam() { Name = "@startDatum", Value = startDate },
                        new DbParam() { Name = "@endDate", Value = endDate }
                    };

                    foreach (DataRow row in db.GetDataTable(sql, parameters.ToArray()).Rows)
                    {
                        if (!string.IsNullOrEmpty(row["points"].ToString()))
                        {
                            bewonerAttentionpoints.Add((int)row["bewonerId"], row["points"].ToString());
                        }
                    }

                    return bewonerAttentionpoints;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
