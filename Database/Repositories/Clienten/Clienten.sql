﻿select 
    b.*, 
    l.Code LeefgroepCode, 
    l.Naam LeefgroepName, 
    l.id LeefgroepId 
from 
    bewoner as b 
	left outer join bewonerleefgroep as bw on bw.bewonerid = b.id
	left outer join leefgroep as l on l.id = bw.leefgroepid