﻿using Dapper;
using Database.DataAccesObjects;
using DataBase.Services;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.ExitFormulier;
using Logic.DataBusinessObjects.GOG;
using Logic.DataBusinessObjects.MultiSelectNS;
using Logic.ExitFormulier;
using Logic.Gebruikers;
using Logic.LeefgroepNS;
using Logic.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Repositories.ExitFormulier
{
    public class ExitFormRepository : IExitFormRepository
    {
        private readonly IDbConnection _connection;
        private readonly AppSettings _appSettings;

        public ExitFormRepository(IDbConnection connection, IOptions<AppSettings> appSettings)
        {
            (_connection, _appSettings) = (connection, appSettings.Value);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }


        private List<ExitFormItem> Parse(List<ExitFormDAO> daos)
        {
            List<ExitFormItem> list = new List<ExitFormItem>();
            foreach (ExitFormDAO dao in daos)
            {
                ExitFormItem tempGogItem = list.FirstOrDefault(x => x.Id == dao.ExitFormId);

                if (tempGogItem == null)
                {
                    tempGogItem = new ExitFormItem()
                    {
                        Id = dao.ExitFormId,                        
                        PreviousStatusId = dao.StatusId,
                        StatusId = dao.StatusId,
                        SelectedMultiSelectItemsResults = new List<MultiSelectItemResult>(),
                    };
                    list.Add(tempGogItem);
                }
                if (dao.ItemId != 0)
                {
                    tempGogItem.SelectedMultiSelectItemsResults.Add(new MultiSelectItemResult()
                    {
                        MultiSelectItemId = dao.ItemId,
                        IsSelected = true,
                        IntValue = dao.ResultInt,
                        DateValue = dao.ResultDate,
                        DoubleValue = dao.ResultNumber,
                        StringValue = dao.ResultText
                    });
                }

            }
            return list;
        }

        public void CreateExitFormItem(ExitFormItem item)
        {
            _connection.Open();
            IDbTransaction transaction = _connection.BeginTransaction();
            MultiSelectItemResult _current;
            try
            {
                // global insert of gogItem
                string sqlExitForm = @"INSERT INTO exitform.form (statusId)
                                VALUES (@statusId);
                                SELECT SCOPE_IDENTITY();";

                int ExitFormId = item.Id = (int)(decimal)_connection.ExecuteScalar(sqlExitForm, new { @statusId = item.StatusId }, transaction);

                // insert of all results (inputted data) of the ggItem
                foreach (MultiSelectItemResult result in item.SelectedMultiSelectItemsResults)
                {
                    string sqlResult = @"INSERT INTO exitform.MultiSelectItemResult(exitformId, multiSelectItemsId, inputDate, inputInteger, inputNumber, InputText)
                                         VALUES (@exitformId, @MultiSelectItemsId, @inputDate, @inputInteger, @inputNumber, @InputText)";
                    _current = result;
                    _connection.Execute(sqlResult, new
                    {
                        @exitformId = ExitFormId,
                        @MultiSelectItemsId = result.MultiSelectItemId,
                        @inputDate = result.DateValue,
                        @inputInteger = result.IntValue,
                        @inputNumber = result.DoubleValue,
                        @InputText = result.StringValue
                    }, transaction);
                }
                // send mail when leefgroep & bewoners are available, otherwise where do I send it to?
                /*
                if (item.SelectedMultiSelectItemsResults.Exists(x => x.MultiSelectItemId == 21 || x.MultiSelectItemId == 22))
                {
                    if (leefgroepId != null && item.BetrokkenBewoners != null && item.BetrokkenBewoners.Count != 0 && item.StatusId.HasValue)
                        SendMail(item, transaction);
                }
                else
                    SendMail(item, transaction);*/
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}
