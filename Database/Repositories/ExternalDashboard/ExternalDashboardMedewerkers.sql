﻿select w.VWERKPLOMS as GroepCode, ISNULL(results.Voltijds, 0) as Voltijds, ISNULL(results.Deeltijds, 0) as Deeltijds, results.Aanwerving, results.UitDienst, isnull(results.ZiekteMinderJaar, 0.00) as ZiekteMinderJaar,
	isnull(results.ZiekteMeerMaand, 0.00) as ZiekteMeerMaand, isnull(results.ZiekteMeerJaar, 0.00) as ZiekteMeerJaar, isnull(results.ZiekteZonderBriefje, 0.00) as ZiekteZonderBriefje, isnull(results.Arbeidsongeval, 0.00) as Arbeidsongeval,
	isnull(results.Werkhervatting, 0) as Werkhervatting, isnull(results.VormingsurenExtern, 0.00) as VormingsurenExtern
from (
	select head.VSUBDNSCOD as GroepCode,
		(select count(*)
		 from (select b.VPERSCOD, b.VSUBDNSCOD, sum(pres.Teller) as Teller
			from orbis.dbo.BPRHCONT b
				inner join orbis.dbo.AVW_PrestatieUren pres on pres.VHCONTRNR = b.VHCONTRNR and pres.VPERSCOD = b.VPERSCOD
			where (b.VHCONTRDATE >= @endDate or b.VHCONTRDATE is null) 
				and VHCONTRDATB <= @endDate
			group by b.VPERSCOD, b.VSUBDNSCOD
			having sum(pres.Teller) = 38.00) as s1	 
		 where s1.VSUBDNSCOD = head.VSUBDNSCOD
		 group by s1.VSUBDNSCOD) as Voltijds,
		(select count(*)
		 from (select b.VPERSCOD, b.VSUBDNSCOD, sum(pres.Teller) as Teller
			from orbis.dbo.BPRHCONT b
				inner join orbis.dbo.AVW_PrestatieUren pres on pres.VHCONTRNR = b.VHCONTRNR and pres.VPERSCOD = b.VPERSCOD
			where (b.VHCONTRDATE >= @endDate or b.VHCONTRDATE is null)
				and VHCONTRDATB <= @endDate
			group by b.VPERSCOD, b.VSUBDNSCOD
			having sum(pres.Teller) != 38.00) as s2	 
		 where s2.VSUBDNSCOD = head.VSUBDNSCOD
		 group by s2.VSUBDNSCOD) as Deeltijds,
		(select count(*)
		 from orbis.dbo.BPRHCONT as s3
			inner join orbis.dbo.BPRHCONAC as stat on stat.VPERSCOD = s3.VPERSCOD and stat.VHISTDATB = s3.VHCONTRDATB
		 where s3.VHCONTRDATB between @startDate and @endDate
			and stat.VHTYPE != 15 -- geen jobstudent
			and 1 not in (
				select case when b.VHCONTRDATE is null or s3.VHCONTRDATB - 1 = b.VHCONTRDATE then 1 else 0 end
				from orbis.dbo.BPRHCONT as b 
				where b.VPERSCOD = s3.VPERSCOD
					and b.VHCONTRNR != s3.VHCONTRNR) -- check of medewerker actief contract heeft voordien of tijdens
			and s3.VSUBDNSCOD = head.VSUBDNSCOD) as Aanwerving,
		(select count(distinct s4.VPERSCOD)
		 from orbis.dbo.BPRHCONT as s4
			inner join orbis.dbo.BPRHCONAC as stat on stat.VPERSCOD = s4.VPERSCOD and stat.VHISTDATB = s4.VHCONTRDATB
		 where s4.VHCONTRDATE between @startDate and @endDate -- tussen begin en einddatum
			and stat.VHTYPE != 15 -- geen jobstudent
			and s4.VBARCOD not like '%LEEG%' -- werkcontract, geen speciaal statuut
			and s4.VPERSCOD not in (
				select VPERSCOD
				from orbis.dbo.BPRHCONT as b 
				where (b.VHCONTRDATE > @endDate or b.VHCONTRDATE is null)) -- check of medewerker actief contract heeft nadien
			and s4.VSUBDNSCOD = head.VSUBDNSCOD) as UitDienst,
		(select round(sum(FLOOR(s5.VUREN) + (s5.VUREN - FLOOR(s5.VUREN)) * 100 / 60), 2)
		 from [ORBIS].[dbo].[BPUROOSTER] as s5
			inner join orbis.dbo.BPUWERKPL w on w.VWERKPL_ID = s5.VWERKPL_ID
		 where s5.VPRESCODEG in ('ZI')
			and s5.VDATUM >= @startDate and s5.VDATUM <= @endDate
			and s5.VUREN != 0.00
			and w.VSUBDNSCOD = head.VSUBDNSCOD
		 group by VWERKPLOMS) as ZiekteMinderJaar,
		(select round(sum(FLOOR(s5.VUREN) + (s5.VUREN - FLOOR(s5.VUREN)) * 100 / 60), 2)
		 from [ORBIS].[dbo].[BPUROOSTER] as s5
			inner join orbis.dbo.BPUWERKPL w on w.VWERKPL_ID = s5.VWERKPL_ID
		 where s5.VPRESCODEG in ('ZM')
			and s5.VDATUM >= @startDate and s5.VDATUM <= @endDate
			and s5.VUREN != 0.00
			and w.VSUBDNSCOD = head.VSUBDNSCOD
		 group by VWERKPLOMS) as ZiekteMeerMaand,
		 (select round(sum(FLOOR(s5.VUREN) + (s5.VUREN - FLOOR(s5.VUREN)) * 100 / 60), 2)
		 from [ORBIS].[dbo].[BPUROOSTER] as s5
			inner join orbis.dbo.BPUWERKPL w on w.VWERKPL_ID = s5.VWERKPL_ID
		 where s5.VPRESCODEG in ('ZJ')
			and s5.VDATUM >= @startDate and s5.VDATUM <= @endDate
			and s5.VUREN != 0.00
			and w.VSUBDNSCOD = head.VSUBDNSCOD
		 group by VWERKPLOMS) as ZiekteMeerJaar,
		 (select round(sum(FLOOR(s5.VUREN) + (s5.VUREN - FLOOR(s5.VUREN)) * 100 / 60), 2)
		 from [ORBIS].[dbo].[BPUROOSTER] as s5
			inner join orbis.dbo.BPUWERKPL w on w.VWERKPL_ID = s5.VWERKPL_ID
		 where s5.VPRESCODEG in ('Z1', 'Z2', 'Z3')
			and s5.VDATUM >= @startDate and s5.VDATUM <= @endDate
			and s5.VUREN != 0.00
			and w.VSUBDNSCOD = head.VSUBDNSCOD
		 group by VWERKPLOMS) as ZiekteZonderBriefje,
		(select round(sum(FLOOR(s6.VUREN) + (s6.VUREN - FLOOR(s6.VUREN)) * 100 / 60), 2)
		 from [ORBIS].[dbo].[BPUROOSTER] as s6
			inner join orbis.dbo.BPUWERKPL w on w.VWERKPL_ID = s6.VWERKPL_ID
		 where s6.VPRESCODEG = 'AO'
			and s6.VDATUM >= @startDate and s6.VDATUM <= @endDate
			and s6.VUREN != 0.00
			and w.VSUBDNSCOD = head.VSUBDNSCOD
		 group by VWERKPLOMS) as Arbeidsongeval,
		 (select count(*)
		 from orbis.dbo.BPRHCONT s7
		 where (s7.VHCONTRDATE >= @endDate or s7.VHCONTRDATE is null)
			and (s7.VFUNCOD like '%MUT%' and VBARCOD = 'LEEG')
			and s7.VHCONTRDATB <= @endDate
			and s7.VSUBDNSCOD = head.VSUBDNSCOD
		 group by VSUBDNSCOD) as Werkhervatting,
		 (select round(sum(FLOOR(s8.VUREN) + (s8.VUREN - FLOOR(s8.VUREN)) * 100 / 60), 2)
		  from [ORBIS].[dbo].[BPUROOSTER] as s8
			inner join orbis.dbo.BPUWERKPL w on w.VWERKPL_ID = s8.VWERKPL_ID
		  where s8.VPRESCODEG in ('E1', 'E2')
			and s8.VDATUM >= @startDate and s8.VDATUM <= @endDate
			and s8.VUREN != 0.00
			and w.VSUBDNSCOD = head.VSUBDNSCOD
		  group by VWERKPLOMS) as VormingsurenExtern
	from orbis.dbo.BPRHCONT head
	where head.VSUBDNSCOD is not null
	group by head.VSUBDNSCOD) results
		inner join orbis.dbo.BPUWERKPL as w on w.VWERKPL_ID = (select max(orbis.dbo.BPUWERKPL.VWERKPL_ID) from orbis.dbo.BPUWERKPL where results.GroepCode = orbis.dbo.BPUWERKPL.VSUBDNSCOD)
where (results.Voltijds > 0 or results.Deeltijds > 0) 
	and results.GroepCode not in ('CONVB','GEDET')
order by w.VWERKPLOMS