﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.Clienten;
using Database.Sql;
using Logic.ExternalDashboard;
using Logic.DataBusinessObjects.ExternalDashboard;
using Database.DataAccesObjects;

namespace DataBase.Repositories.Clienten
{
    public class ExternalDashboardRepository : IExternalDashboardRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public ExternalDashboardRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
       public List<ExternalDashboardMedewerkers> GetExternalDashboardMedewerkers(OrbisDates dates)
       {
            String sql = _sqlManager.GetSQL("ExternalDashboardMedewerkers.sql");
            try
            {
                _connection.Open();
                IEnumerable<ExternalDashboardMedewerkersDao> list = _connection.Query<ExternalDashboardMedewerkersDao>(sql, new {startDate =  dates.StartDate, endDate = dates.EndDate });
                List<ExternalDashboardMedewerkers> data = _mapper.Map<List<ExternalDashboardMedewerkers>>(list).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}
