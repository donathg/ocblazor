﻿using Dapper;
using Database.DataAccesObjects;
using DataBase.Services;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.GOG;
using Logic.DataBusinessObjects.MultiSelectNS;
using Logic.Gebruikers;
using Logic.GOG;
using Logic.LeefgroepNS;
using Logic.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Database.Repositories.GOG
{
    public class GOGItemRepository : IGOGItemRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly AppSettings _appSettings;
        private readonly IGebruikerService _gebruikerService;
        private readonly ILeefgroepService _leefgroepService;
        private readonly IClientService _clientService;

        public GOGItemRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IGebruikerService gebruikerService,
            ILeefgroepService leefgroepService, IClientService clientService)
        {
            (_connection, _userService, _appSettings, _gebruikerService, _leefgroepService, _clientService) =
                (connection, userService, appSettings.Value, gebruikerService, leefgroepService, clientService);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

        private List<GOGItem> Parse(List<GOGItemDAO> daos)
        {
            List<GOGItem> list = new List<GOGItem>();
            foreach (GOGItemDAO dao in daos)
            {
                GOGItem tempGogItem = list.FirstOrDefault(x => x.Id == dao.GogId);

                if (tempGogItem == null)
                {
                    tempGogItem = new GOGItem()
                    {
                        Id = dao.GogId,
                        Creator = _gebruikerService.GetGebruikerFromId(dao.GogCreatorId),
                        Leefgroep = _leefgroepService.GetLeefgroepFromId(dao.LeefgroepId),
                        CreationDate = dao.GogCreationDate,
                        Modifier = dao.GogModifier,
                        ModifierVoornaamAchternaam = dao.GogModifierNaam,
                        ModificationDate = dao.GogModificationDate,
                        PreviousStatusId = dao.StatusId,
                        StatusId = dao.StatusId,
                        BetrokkenBegeleiders = new List<Gebruiker>(),
                        BetrokkenBewoners = new List<Client>(),
                        SelectedMultiSelectItemsResults = new List<MultiSelectItemResult>(),
                        DatumIncident = dao.DatumIncident,
                        AardVanIncident = dao.AardVanIncident,
                        AantalBijlagen = dao.AantalBijlagen
                    };
                    list.Add(tempGogItem);
                }
                if (dao.ItemId != 0)
                {
                    tempGogItem.SelectedMultiSelectItemsResults.Add(new MultiSelectItemResult()
                    {
                        MultiSelectItemId = dao.ItemId,
                        IsSelected = true,
                        IntValue = dao.ResultInt,
                        DateValue = dao.ResultDate,
                        DoubleValue = dao.ResultNumber,
                        StringValue = dao.ResultText
                    });
                }

            }
            return list;
        }


        public List<GOGItem> SelectAllCrisisgroepItemsItems()
        {

            _connection.Open();
            IDbTransaction transaction = _connection.BeginTransaction();

            List<GOGItem> tempList = new List<GOGItem>();
            try
            {
                string sql = @"SELECT f.id AS GogId, f.creator AS GogCreatorId, f.creationDate AS GogCreationDate, f.modifier AS GogModifier, (modi.voornaam + ' ' + modi.achternaam) AS GogModifierNaam,
                                    f.modificationDate AS GogModificationDate, f.leefgroepId, f.StatusId,
                                    (SELECT count(*) FROM gog.gog_files WHERE gogFormId = f.id) AS AantalBijlagen,
	                                (select inputDate from gog.MultiSelectItemResult where gog.MultiSelectItemResult.MultiSelectItemsId = 7 and gogFormId = f.id) AS datumIncident,
                                    (SELECT description FROM MultiSelectItem, gog.MultiSelectItemResult
	                                    WHERE MultiSelectItem.MultiSelectId = 10
		                                    and gog.MultiSelectItemResult.MultiSelectItemsId = MultiSelectItem.id
		                                    and MultiSelectItemResult.gogFormId = f.id
                                    ) as AardVanIncident
                                FROM gog.form AS f
	                                INNER JOIN gebruiker AS crea ON crea.id = f.creator
	                                LEFT OUTER JOIN gebruiker AS modi ON modi.id = f.modifier
	                                LEFT OUTER JOIN gog.bewoners AS bew ON bew.gogFormId = f.id
                                WHERE (SELECT MultiSelectItem.id FROM MultiSelectItem, gog.MultiSelectItemResult
		                                WHERE MultiSelectItem.MultiSelectId = 10
			                                and gog.MultiSelectItemResult.MultiSelectItemsId = MultiSelectItem.id
			                                and MultiSelectItemResult.gogFormId = f.id) in (134, 135, 136, 137)
	                                AND f.leefgroepId is null
	                                AND bew.id is null
                                ORDER BY f.id desc";

                List<GOGItemDAO> daos = _connection.Query<GOGItemDAO>(sql, null, transaction).ToList(); // null is needed, otherwise transaction is seen as parameter => error
                tempList = Parse(daos);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                _connection.Close();
            }

            return tempList;
        }
        //AardVanIncident
        public List<GOGItem> SelectAllGOGItemsForBewonersAndOwn(List<int> bewonersIds, DateTime startDate, DateTime EndDate )        
        {
            List<GOGItem> tempList = new List<GOGItem>();
            _connection.Open();
            IDbTransaction transaction = _connection.BeginTransaction();

            try
            {
                string sql = @" SELECT f.id AS GogId, f.creator AS GogCreatorId, f.creationDate AS GogCreationDate, f.modifier AS GogModifier, (modi.voornaam + ' ' + modi.achternaam) AS GogModifierNaam,
                                    f.modificationDate AS GogModificationDate, f.leefgroepId, f.StatusId,
                                    (SELECT count(*) FROM gog.gog_files WHERE gogFormId = f.id) AS AantalBijlagen,
                                    (SELECT inputDate FROM gog.MultiSelectItemResult WHERE gog.MultiSelectItemResult.MultiSelectItemsId = 7 and gogFormId = f.id) AS datumIncident,
                                    (SELECT description FROM MultiSelectItem, gog.MultiSelectItemResult
								     WHERE MultiSelectItem.MultiSelectId = 10
									    and gog.MultiSelectItemResult.MultiSelectItemsId = MultiSelectItem.id
									    and MultiSelectItemResult.gogFormId = f.id
                                    ) as AardVanIncident
                                FROM  
	                                gog.form AS f
	                                INNER JOIN gebruiker AS crea ON crea.id = f.creator
	                                LEFT OUTER JOIN gebruiker AS modi ON modi.id = f.modifier
	                                LEFT OUTER JOIN gog.bewoners AS bew ON bew.gogFormId = f.id
                                WHERE (bew.bewonerId IN @ids OR f.creator = @userId)
                                and (select inputDate from gog.MultiSelectItemResult where gog.MultiSelectItemResult.MultiSelectItemsId = 7
and gogFormId = f.id) between @start and @end
                                ORDER BY f.id desc";

                List<GOGItemDAO> daos = _connection.Query<GOGItemDAO>(sql, new { @ids = bewonersIds.ToArray(), @userId = _userService.LoggedOnUser.UserId , @start= startDate , @end=EndDate }, transaction).ToList();
                tempList = Parse(daos);

                // alle bewoners ophalen uit DB
                string sqlBewoners = @" SELECT f.id AS Integer1, geb.bewonerId AS Integer2
                                        FROM gog.form AS f
	                                        INNER JOIN gog.bewoners AS geb ON geb.gogFormId = f.id
                                        WHERE f.id in @ids";
                List<DAOHelperIntInt> BetrokkenBewonersIds = _connection.Query<DAOHelperIntInt>(sqlBewoners, new { @ids = tempList.Select(x => x.Id) }, transaction).ToList();

                // alle gebruikers ophalen uit DB
                string sqlGebruikers = @"SELECT f.id AS Integer1, geb.gebruikerId AS Integer2
                                         FROM gog.form AS f
	                                        INNER JOIN gog.gebruikers AS geb ON geb.gogFormId = f.id
                                         WHERE f.id in @ids";

                List<DAOHelperIntInt> BetrokkenBegeleiderIds = _connection.Query<DAOHelperIntInt>(sqlGebruikers, new { @ids = tempList.Select(x => x.Id) }, transaction).ToList();

                foreach (GOGItem i in tempList)
                {
                    i.BetrokkenBewoners = BetrokkenBewonersIds.Where(x => x.Integer1 == i.Id).Select(x => _clientService.GetClient(x.Integer2)).ToList();
                    i.BetrokkenBegeleiders = BetrokkenBegeleiderIds.Where(x => x.Integer1 == i.Id).Select(x => _gebruikerService.GetGebruikerFromId(x.Integer2)).ToList();
                }

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {                
                _connection.Close();
            }

            return tempList;
        }

        public List<GOGItem> SelectAllGOGItemsForLeefgroepenAndOwn(List<int> leefgroepIds, DateTime startDate, DateTime EndDate )
        {
            List<GOGItem> tempList = new List<GOGItem>();
            _connection.Open();
            IDbTransaction transaction = _connection.BeginTransaction();

            try
            {
                string sql = @" SELECT f.id AS GogId, f.creator AS GogCreatorId, f.creationDate AS GogCreationDate, f.modifier AS GogModifier, (modi.voornaam + ' ' + modi.achternaam) AS GogModifierNaam,
                                    f.modificationDate AS GogModificationDate, f.leefgroepId, f.StatusId, 
                                    (SELECT count(*) FROM gog.gog_files WHERE gogFormId = f.id) AS AantalBijlagen,
                                    (SELECT inputDate FROM gog.MultiSelectItemResult where gog.MultiSelectItemResult.MultiSelectItemsId = 7 and gogFormId = f.id) AS datumIncident,
                                    (SELECT description FROM MultiSelectItem, gog.MultiSelectItemResult
								     WHERE MultiSelectItem.MultiSelectId = 10
									    and gog.MultiSelectItemResult.MultiSelectItemsId = MultiSelectItem.id
									    and MultiSelectItemResult.gogFormId = f.id
                                    ) as AardVanIncident
                                FROM gog.form AS f 
	                                INNER JOIN gebruiker AS crea ON crea.id = f.creator
	                                LEFT OUTER JOIN gebruiker AS modi ON modi.id = f.modifier
                                WHERE (leefgroepId IN @ids OR f.creator = @userId)
                                    and (select inputDate from gog.MultiSelectItemResult where gog.MultiSelectItemResult.MultiSelectItemsId = 7 and gogFormId = f.id) between @start and @end
                                ORDER BY f.id desc";

                List<GOGItemDAO> daos = _connection.Query<GOGItemDAO>(sql, new { ids = leefgroepIds.ToArray(), userId = _userService.LoggedOnUser.UserId, @start = startDate, @end = EndDate }, transaction).ToList();
                tempList = Parse(daos);

                // alle bewoners ophalen uit DB
                string sqlBewoners = @" SELECT f.id AS Integer1, geb.bewonerId AS Integer2
                                        FROM gog.form AS f
	                                        INNER JOIN gog.bewoners AS geb ON geb.gogFormId = f.id
                                        WHERE f.id in @ids";
                List<DAOHelperIntInt> BetrokkenBewonersIds = _connection.Query<DAOHelperIntInt>(sqlBewoners, new { ids = tempList.Select(x => x.Id) }, transaction).ToList();

                // alle gebruikers ophalen uit DB
                string sqlGebruikers = @" SELECT f.id AS Integer1, geb.gebruikerId AS Integer2
                                        FROM gog.form AS f
	                                        INNER JOIN gog.gebruikers AS geb ON geb.gogFormId = f.id
                                        WHERE f.id in @ids";

                List<DAOHelperIntInt> BetrokkenBegeleiderIds = _connection.Query<DAOHelperIntInt>(sqlGebruikers, new { ids = tempList.Select(x => x.Id) }, transaction).ToList();

                foreach (GOGItem i in tempList)
                {
                    i.BetrokkenBewoners = BetrokkenBewonersIds.Where(x => x.Integer1 == i.Id).Select(x => _clientService.GetClient(x.Integer2)).ToList();
                    i.BetrokkenBegeleiders = BetrokkenBegeleiderIds.Where(x => x.Integer1 == i.Id).Select(x => _gebruikerService.GetGebruikerFromId(x.Integer2)).ToList();
                }

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                _connection.Close();
            }

            return tempList;
        }
        /// <summary>
        /// Selecteert de details voor het formulier
        /// </summary>
        /// <param name="id">id van de gogItem</param>
        /// <returns></returns>
        public GOGItem SelectGOGItemFromId(int id)
        {
            GOGItem tempItem = new GOGItem();
            _connection.Open(); 

            try
            {                
                string sql = @" SELECT f.id AS GogId, f.creator AS GogCreatorId, f.creationDate AS GogCreationDate, f.modifier AS GogModifier, (modi.voornaam + ' ' + modi.achternaam) AS GogModifierNaam,
                                    f.modificationDate AS GogModificationDate, f.leefgroepId, f.StatusId, results.MultiSelectItemsId AS ItemId, results.inputDate AS ResultDate, 
                                    results.inputInteger AS ResultInt, results.inputNumber AS ResultNumber, results.InputText As ResultText
                                FROM gog.MultiSelectItemResult AS results
	                                INNER JOIN gog.form AS f on f.id = results.gogFormId
	                                INNER JOIN gebruiker AS crea ON crea.id = f.creator
	                                LEFT OUTER JOIN gebruiker AS modi ON modi.id = f.modifier
                                WHERE f.id = @id";

                List<GOGItemDAO> daos = _connection.Query<GOGItemDAO>(sql, new { id }).ToList();
                tempItem = Parse(daos).FirstOrDefault();

                if (tempItem == null)
                {
                    throw new Exception("Geen GogItem gevonden met id: " + id);
                }
                else
                {
                    // alle bewoners ophalen uit DB
                    string sqlBewoners = @" SELECT f.id AS Integer1, geb.bewonerId AS Integer2
                                        FROM gog.form AS f
	                                        INNER JOIN gog.bewoners AS geb ON geb.gogFormId = f.id
                                        WHERE f.id = @id";
                    List<DAOHelperIntInt> BetrokkenBewonersIds = _connection.Query<DAOHelperIntInt>(sqlBewoners, new { id }).ToList();

                    // alle gebruikers ophalen uit DB
                    string sqlGebruikers = @" SELECT f.id AS Integer1, geb.gebruikerId AS Integer2
                                        FROM gog.form AS f
	                                        INNER JOIN gog.gebruikers AS geb ON geb.gogFormId = f.id
                                        WHERE f.id = @id";
                    List<DAOHelperIntInt> BetrokkenBegeleiderIds = _connection.Query<DAOHelperIntInt>(sqlGebruikers, new { id }).ToList();

                    tempItem.BetrokkenBewoners = BetrokkenBewonersIds.Where(x => x.Integer1 == id).Select(x => _clientService.GetClient(x.Integer2)).ToList();
                    tempItem.BetrokkenBegeleiders = BetrokkenBegeleiderIds.Where(x => x.Integer1 == id).Select(x => _gebruikerService.GetGebruikerFromId(x.Integer2)).ToList();
                }

                 
            }
            catch (Exception)
            {
         
                throw;
            }
            finally
            {
                _connection.Close();
            }

            return tempItem;
        }
        public void SaveGogItem(GOGItem item)
        {
            if (item.Id == 0)
                CreateGogItem(item);
            else
                UpdateGogItem(item);
        }
        private void CreateGogItem(GOGItem item)
        {
            _connection.Open();
            IDbTransaction transaction = _connection.BeginTransaction();
            MultiSelectItemResult _current;
            try
            {
                item.CreationDate = DateTime.Now;
                item.Creator.Id = _userService.LoggedOnUser.UserId;

                int? leefgroepId = null;
                if (item.Leefgroep != null && item.Leefgroep.Id != 0)
                    leefgroepId = item.Leefgroep.Id;

                // global insert of gogItem
                string sqlGog = @"INSERT INTO gog.form (creator, creationDate, modifier, modificationDate, leefgroepId, statusId)
                                VALUES (@creator, @creationDate, null, null, @leefgroepId, @statusId);
                                SELECT SCOPE_IDENTITY();";

                int GogId = item.Id = (int)(decimal)_connection.ExecuteScalar(sqlGog, new { @creator = item.Creator.Id, @creationDate = item.CreationDate, @leefgroepId = leefgroepId, @statusId = item.StatusId }, transaction);
            
                // insert of all gog.bewoners of the gogItem if necessary
                if (item.BetrokkenBewoners != null && item.BetrokkenBewoners.Count != 0)
                {
                    foreach (Client bewoner in item.BetrokkenBewoners)
                    {
                        string sqlBewoner = @"INSERT INTO gog.bewoners(gogFormId, bewonerId)
                                            VALUES (@gogId, @bewonerId)";
                        _connection.Execute(sqlBewoner, new { @gogId = GogId, @bewonerId = bewoner.Id }, transaction);
                    }
                }
                // insert of all gog.gebruikers of the gogItem
                foreach (Gebruiker geb in item.BetrokkenBegeleiders)
                {
                    string sqlGebruiker = @"INSERT INTO gog.gebruikers(gogFormId, gebruikerId)
                                            VALUES (@gogId, @gebruikerId)";
                    _connection.Execute(sqlGebruiker, new { @gogId = GogId, @gebruikerId = geb.Id }, transaction);
                }
                // insert of all results (inputted data) of the ggItem
                foreach (MultiSelectItemResult result in item.SelectedMultiSelectItemsResults)
                {
                    string sqlResult = @"INSERT INTO gog.MultiSelectItemResult(gogFormId, MultiSelectItemsId, inputDate, inputInteger, inputNumber, InputText)
                                         VALUES (@gogFormId, @MultiSelectItemsId, @inputDate, @inputInteger, @inputNumber, @InputText)";
                    _current = result;
                    _connection.Execute(sqlResult, new
                    {
                        @gogFormId = GogId,
                        @MultiSelectItemsId = result.MultiSelectItemId,
                        @inputDate = result.DateValue,
                        @inputInteger = result.IntValue,
                        @inputNumber = result.DoubleValue,
                        @InputText = result.StringValue
                    }, transaction);
                }
                // send mail when leefgroep & bewoners are available, otherwise where do I send it to?
                if (item.SelectedMultiSelectItemsResults.Exists(x => x.MultiSelectItemId == 21 || x.MultiSelectItemId == 22))
                {
                    if (leefgroepId != null && item.BetrokkenBewoners != null && item.BetrokkenBewoners.Count != 0 && item.StatusId.HasValue)
                        SendMail(item, transaction);
                }
                else
                        SendMail(item, transaction);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                _connection.Close();
            }
        }
        private void UpdateGogItem(GOGItem item)
        {
            _connection.Open();
            IDbTransaction transaction = _connection.BeginTransaction();

            try
            {
                item.ModificationDate = DateTime.Now;
                item.Modifier = _userService.LoggedOnUser.UserId;

                int? leefgroepId = null;
                if (item.Leefgroep != null && item.Leefgroep.Id != 0)
                    leefgroepId = item.Leefgroep.Id;

                string sqlGog = @"UPDATE gog.form
                                  SET modifier = @modifier, modificationDate = @modificationDate, leefgroepId = @leefgroepId, statusId = @statusId
                                  WHERE id = @id";

                _connection.Execute(sqlGog, new { @id = item.Id, @modifier = item.Modifier, @modificationDate = item.ModificationDate, @leefgroepId = leefgroepId, @statusId = item.StatusId }, transaction);

                // delete all gog.bewoners
                string sqlBewonersDelete = @"DELETE FROM gog.bewoners WHERE gogFormId = @id";
                _connection.Execute(sqlBewonersDelete, new { id = item.Id }, transaction);
                // insert of all new gog.bewoners of the gogItem if necessary
                if (item.BetrokkenBewoners != null && item.BetrokkenBewoners.Count != 0)
                {
                    foreach (Client bewoner in item.BetrokkenBewoners)
                    {
                        string sqlBewoner = @"INSERT INTO gog.bewoners(gogFormId, bewonerId)
                                            VALUES (@gogId, @bewonerId)";
                        _connection.Execute(sqlBewoner, new { @gogId = item.Id, @bewonerId = bewoner.Id }, transaction);
                    }
                }
                // delete all gog.gebruikers
                string sqlGebruikersDelete = @"DELETE FROM gog.gebruikers WHERE gogFormId = @id";
                _connection.Execute(sqlGebruikersDelete, new { id = item.Id }, transaction);

                foreach (Gebruiker geb in item.BetrokkenBegeleiders)
                {
                    // insert all new gog.gebruikers
                    string sqlGebruikers = @"INSERT INTO gog.gebruikers(gogFormId, gebruikerId)
                                            VALUES (@gogId, @gebruikerId)";
                    _connection.Execute(sqlGebruikers, new { @gogId = item.Id, @gebruikerId = geb.Id }, transaction);
                }


                // delete all results from gogItem
                string sqlResultDelete = @"DELETE FROM gog.MultiSelectItemResult WHERE gogFormId = @id";
                _connection.Execute(sqlResultDelete, new { id = item.Id }, transaction);

                foreach (MultiSelectItemResult result in item.SelectedMultiSelectItemsResults)
                {
                    // insert all new results from gogItem
                    string sqlResultInsert = @"INSERT INTO gog.MultiSelectItemResult(gogFormId, MultiSelectItemsId, inputDate, inputInteger, inputNumber, InputText)
                                               VALUES (@gogFormId, @MultiSelectItemsId, @inputDate, @inputInteger, @inputNumber, @InputText)";

                    _connection.Execute(sqlResultInsert, new { @gogFormId = item.Id, @MultiSelectItemsId = result.MultiSelectItemId, @inputDate = result.DateValue,
                        @inputInteger = result.IntValue, @inputNumber = result.DoubleValue, @InputText = result.StringValue }, transaction);
                }

                // send mail when leefgroep & bewoners are available, otherwise where do I send it to?. NODIG door AUTO-SAVE !!!!!
                if (item.SelectedMultiSelectItemsResults.Exists(x => x.MultiSelectItemId == 21 || x.MultiSelectItemId == 22))
                {
                    if (leefgroepId != null && item.BetrokkenBewoners != null && item.BetrokkenBewoners.Count != 0 && item.StatusId.HasValue && item.StatusIdNotNull == 1 && !item.PreviousStatusId.HasValue)
                        SendMail(item, transaction);
                }
                else
                    SendMail(item, transaction);

                transaction.Commit();              
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                _connection.Close();
            }
        }
        private void SendMail(GOGItem item, IDbTransaction transaction)
        {
            List<int> aardIds = new List<int>()
            {
                21,
                22,
                134,
                135,
                136,
                137
            };

            string sqlSendMail = @"DECLARE @list AS udtGogBewoners;";
            string listClienten = "";
            if (item.BetrokkenBewoners != null && item.BetrokkenBewoners.Count != 0)
            {
                foreach (Client bewoner in item.BetrokkenBewoners)
                {
                    sqlSendMail += "INSERT @list(BewonerId) VALUES(" + bewoner.Id + ");";
                    listClienten += bewoner.FirstNameLastname + ", ";
                }
            }
            string titleMail = $"Er is een Gog-Registratie voor {(item.Leefgroep != null ? item.Leefgroep.Name : "NIET - GEKENDE LEEFGROEP")} gebeurd ";
            string bodyMail = "Op " + item.CreationDate.ToString() + " werd een nieuwe GOG-Registratie ingevuld door " + item.Creator.Name;
            if (!string.IsNullOrEmpty(listClienten))
                bodyMail += " voor " + listClienten.Remove(listClienten.Length-2);
            sqlSendMail += "EXEC sendMailGOG @title, @body, @leefgroepId, @list, @aardId;";

            _connection.Execute(sqlSendMail, new
            {
                @title = titleMail,
                @body = bodyMail,
                @leefgroepId = item.Leefgroep != null ? item.Leefgroep.Id : 0,
                @aardId = item.SelectedMultiSelectItemsResults.First(x => aardIds.Contains(x.MultiSelectItemId)).MultiSelectItemId
            }, transaction);
        }
    }
}
