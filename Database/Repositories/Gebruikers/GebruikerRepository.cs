﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.Clienten;
using Database.Sql;
using Logic.Gebruikers;

namespace DataBase.Repositories.Gebruikers
{
    public class GebruikerRepository : IGebruikerRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public GebruikerRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

        public List<Gebruiker> GetAllGebruikers()
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    string sql = @"SELECT * FROM gebruiker";
                    DataTable dt = db.GetDataTable(sql);

                    List<Gebruiker> tempList = new List<Gebruiker>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        tempList.Add(GebruikerParser.ParseDataTable(dr));
                    }
                    return tempList;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }
        }
    }     
}
