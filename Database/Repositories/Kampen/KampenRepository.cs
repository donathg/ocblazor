﻿using AutoMapper;
using Dapper;
using Database.DataAccesObjects;
using Database.Sql;
using DataBase.Services;
using Logic.WagenNS;
using Logic.Settings;
using Logic.Werkbonnenbeheer;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Logic.DataBusinessObjects;
using Logic.Kampen;

namespace Database.Repositories.Kilometervergoeding
{
    public class KampenRepository : IKampenRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public KampenRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public List<Kamp> GetKampen()
        {

            String sql = _sqlManager.GetSQL("Kampen.sql");

            try
            {
                _connection.Open();
                var kampen = _connection.Query<KampDao>(sql);
                return _mapper.Map<List<Kamp>>(kampen);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _connection.Close();
            }
        }
            
        }
  
}
