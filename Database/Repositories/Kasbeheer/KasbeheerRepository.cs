﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;


namespace DataBase.Repositories.Kasbeheer
{
    public class KasbeheerRepository : IKasbeheerRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public KasbeheerRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper)
        {
            (_connection, _userService, _appSettings, _mapper) = (connection, userService, appSettings.Value, mapper);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public List<KasbeheerTransactie> GetTransacties(int leefgroepId)
        {

            List<KasbeheerTransactie> list = new List<KasbeheerTransactie>();
            DataTable dt = null;
            String sql = @"select t.afgerekend,	t.id,transactieKode, transactieDatum,transactieOmschrijving, transactiebedrag,
l.id as leefgroepId, r.id as rekeningId , r.naam RekeningNaam, l.naam LeefgroepName
, tt.id as transactieTypeId , tt.naam transactieTypeNaam, tt.IsClientBased transactieTypeIsClientBased
from kasbeheer.transacties t, kasbeheer.rekening r,leefgroep l, kasbeheer.transactieType tt
            where t.leefgroepId = @leefgroepId
            and t.rekeningId=  r.id
and tt.id = t.transactieTypeId
and t.leefgroepId = l.id
            order by transactieDatum desc
";
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
              
                    dt = db.GetDataTable(sql, new DbParam() { Name = "@leefgroepId", Value = leefgroepId });
                }
                catch (Exception ex)
                {
                    db._sqlConnection.Close();
                }



            }
            foreach (DataRow dr in dt.Rows)
            {
                KasbeheerTransactie kt = KasbeheerParser.ParseDataTableKasbeheerTransactie(dr);
                list.Add(kt);
            }
            return list;
        }
        public List<Leefgroep> GetLeefgroepRechten(int userId)
        {
            String sql = @"select leefgroep.id Id,leefgroep.naam Name, leefgroep.FromOrbis FromOrbis
                           from kasbeheer.RechtenKasbeheer, leefgroep
                           where leefgroep.id = kasbeheer.RechtenKasbeheer.leefGroepId
                                and kasbeheer.RechtenKasbeheer.gebruikerId = @userId 
                                and actief = 'Y'
                           order by naam asc";

            try
            {
                _connection.Open();
                return _connection.Query<Leefgroep>(sql, new { userId }).ToList();
            }
            finally
            {
                _connection.Close();
            }
        }
        public List<Leefgroep> GetLeefgroepTesting()
        {
            String sql = @"select leefgroep.id Id,leefgroep.naam Name, leefgroep.FromOrbis FromOrbis
                           from  leefgroep
                       
                           order by naam asc";

            try
            {
                _connection.Open();
                return _connection.Query<Leefgroep>(sql).ToList();
            }
            finally
            {
                _connection.Close();
            }
        }
        public KasbeheerTransactie GetTransactie(int transactieId)
        {
            List<KasbeheerTransactie> list = new List<KasbeheerTransactie>();
            DataTable dt;
            String sql = @"select 	t.id,transactieKode, transactieDatum,transactieOmschrijving, transactiebedrag,
l.id as leefgroepId, r.id as rekeningId , r.naam RekeningNaam, l.naam LeefgroepName
, tt.id as transactieTypeId , tt.naam transactieTypeNaam,t.afgerekend
from kasbeheer.transacties t, kasbeheer.rekening r,leefgroep l, kasbeheer.transactieType tt
            where t.id = @id
            and t.rekeningId=  r.id
and tt.id = t.transactieTypeId
and t.leefgroepId = l.id
            order by transactieDatum desc";
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {

                try
                {
                 
                    dt = db.GetDataTable(sql, new DbParam() { Name = "@id", Value = transactieId });
                }
                finally
                {
                    db._sqlConnection.Close();
                }

            }
            KasbeheerTransactie kt = KasbeheerParser.ParseDataTableKasbeheerTransactie(dt.Rows[0]);
            return kt;
        }
        //SaveTransactie
        public KasbeheerTransactie SaveTransactie(int leefgroepId, KasbeheerTransactie transactie)
        {
            if (transactie.Id != 0)
                return UpdateTransactie(_userService.LoggedOnUser.UserId, leefgroepId, transactie);
            else
                return InsertTransactie(_userService.LoggedOnUser.UserId, leefgroepId, transactie);
        }
        public void DeleteTransactie(int transactieId)
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
          
                    db.BeginTransaction();
                    List<DbCommand> commandList = new List<DbCommand>();

                    String sql = @"delete from kasbeheer.transactieClient where transactieId = @transactieId";
                    commandList.Add(new DbCommand(DbCommandType.crud, sql, new DbParam() { Name = "@transactieId", Value = transactieId }));

                    sql = @"delete from kasbeheer.transacties where id = @transactieId";
                    commandList.Add(new DbCommand(DbCommandType.crud, sql, new DbParam() { Name = "@transactieId", Value = transactieId }));


                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }
        }
        private KasbeheerTransactie UpdateTransactie(int gebruikerId, int leefgroepId, KasbeheerTransactie transactie)
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                  
                    db.BeginTransaction();
                    String sql = @"update kasbeheer.transacties set 
            rekeningId=@rekeningId,
            leefgroepId=@leefgroepId,
            transactieTypeId=@transactieTypeId,
            transactieDatum=@transactieDatum,
            transactieBedrag=@transactieBedrag,
            transactieOmschrijving=@transactieOmschrijving,
            modifier=@modifier,
            modificationDate=@modificationDate,
transactieKode = @transactieKode
            where afgerekend = 0 and id = @id";

                    List<DbCommand> commandList = new List<DbCommand>();
                    DbCommand command = new DbCommand(DbCommandType.crud, sql,
                    new DbParam() { Name = "@rekeningId", Value = transactie.Rekening.Id },
                    new DbParam() { Name = "@LeefgroepId", Value = leefgroepId },
                    new DbParam() { Name = "@transactieTypeId", Value = transactie.TransactieType.Id },
                    new DbParam() { Name = "@transactieDatum", Value = transactie.TransactieDatum },
                    new DbParam() { Name = "@transactieBedrag", Value = transactie.TransactieBedrag },
                    new DbParam() { Name = "@transactieOmschrijving", Value = transactie.TransactieOmschrijving },
                    new DbParam() { Name = "@modifier", Value = gebruikerId },
                    new DbParam() { Name = "@modificationDate", Value = DateTime.Now },
                    new DbParam() { Name = "@id", Value = transactie.Id },
                    new DbParam() { Name = "@transactieKode", Value = transactie.TransactieCode.Kode }
                );

                    commandList.Add(command);

                    String sql2 = @"delete from kasbeheer.transactieClient where transactieId = @transactieId";
                    DbCommand command2 = new DbCommand(DbCommandType.crud, sql2,

                  new DbParam() { Name = "@transactieId", Value = transactie.Id }
              );
                    commandList.Add(command2);

                    if (transactie.TransactieType.IsClientBased)
                    {

                        foreach (KasbeheerTransactieClientInfo c in transactie.Clienten.Where(x => x.Bedrag != 0))
                        {

                            String sqlClient = @"insert into kasbeheer.transactieClient (transactieId, bewonerId,bedrag) values (@transactieId,@bewonerId,@bedrag)";
                            commandList.Add(new DbCommand(DbCommandType.crud, sqlClient,
                    new DbParam() { Name = "@transactieId", Value = transactie.Id },
                    new DbParam() { Name = "@bewonerId", Value = c.Id },
                    new DbParam() { Name = "@bedrag", Value = c.Bedrag }));

                        }

                    }
                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }
            return transactie;
        }
        private KasbeheerTransactie InsertTransactie(int gebruikerId, int leefgroepId, KasbeheerTransactie transactie)
        {

            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                   
                    db.BeginTransaction();
                    String sql = @"insert into kasbeheer.transacties
            (
            rekeningId,
            leefgroepId,
            transactieTypeId,
            transactieDatum,
            transactieBedrag,
            transactieOmschrijving,
            modifier,
            modificationDate,
            afgerekend,
            afgerekendDatum,
            afgerekendDoor,
       transactieKode
            )
values
(
            @rekeningId,
            @leefgroepId,
            @transactieTypeId,
            @transactieDatum,
            @transactieBedrag,
            @transactieOmschrijving,
            @modifier,
            @modificationDate,
            @afgerekend,
            @afgerekendDatum,
            @afgerekendDoor,@transactieKode
);SELECT CAST(scope_identity() AS int)";

                    List<DbCommand> commandList = new List<DbCommand>();
                    DbCommand command = new DbCommand(DbCommandType.crud, sql,
                    new DbParam() { Name = "@rekeningId", Value = transactie.Rekening.Id },
                    new DbParam() { Name = "@LeefgroepId", Value = leefgroepId },
                    new DbParam() { Name = "@transactieTypeId", Value = transactie.TransactieType.Id },
                    new DbParam() { Name = "@transactieDatum", Value = transactie.TransactieDatum },
                    new DbParam() { Name = "@transactieBedrag", Value = transactie.TransactieBedrag },
                    new DbParam() { Name = "@transactieOmschrijving", Value = transactie.TransactieOmschrijving },
                    new DbParam() { Name = "@modifier", Value = gebruikerId },
                    new DbParam() { Name = "@modificationDate", Value = DateTime.Now },
                    new DbParam() { Name = "@afgerekend", Value = 0 },
                    new DbParam() { Name = "@afgerekendDatum", Value = null },
                    new DbParam() { Name = "@afgerekendDoor", Value = null },
                           new DbParam() { Name = "@transactieKode", Value = transactie.TransactieCode.Kode }
                    );

                    transactie.Id = Convert.ToInt32(db.ExcecuteScalar(command));
                    String sql2 = @"delete from kasbeheer.transactieClient where transactieId = @transactieId";
                    DbCommand command2 = new DbCommand(DbCommandType.crud, sql2,

                  new DbParam() { Name = "@transactieId", Value = transactie.Id }
              );
                    commandList.Add(command2);

                    if (transactie.TransactieType.IsClientBased)
                    {

                        foreach (KasbeheerTransactieClientInfo c in transactie.Clienten.Where(x => x.Bedrag != 0))
                        {


                            String sqlClient = @"insert into kasbeheer.transactieClient (transactieId, bewonerId,bedrag) values (@transactieId,@bewonerId,@bedrag)";
                            commandList.Add(new DbCommand(DbCommandType.crud, sqlClient,
                    new DbParam() { Name = "@transactieId", Value = transactie.Id },
                    new DbParam() { Name = "@bewonerId", Value = c.Id },
                    new DbParam() { Name = "@bedrag", Value = c.Bedrag }));

                        }

                    }


                    db.Excecute(commandList);
                    db.CommitTransaction();

                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw ex;
                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }
            return transactie;














        }
        public List<KasbeheerTransactieClientInfo> GetSelectedClientenForTransactie(int transactieId)
        {
            List<ClientDao> list = new List<ClientDao>();
            DataTable dt;
            String sql = @"select 
bewoner.*, kasbeheer.transactieClient.Bedrag as Bedrag  from bewoner,kasbeheer.transactieClient where 
    bewoner.id =  kasbeheer.transactieClient.bewonerId and kasbeheer.transactieClient.transactieId =  @transactieId";
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
           
                    dt = db.GetDataTable(sql, new DbParam() { Name = "@transactieId", Value = transactieId });
                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }

            List<KasbeheerTransactieClientInfo> tempList = new List<KasbeheerTransactieClientInfo>();

            foreach (DataRow dr in dt.Rows)
            {
                //alle clienten :  clienten komen meerdere keren voor omdat ze in meerdere leefgroepen kunnen zitten
                Client c = ClientParser.ParseDataTable(dr);
                KasbeheerTransactieClientInfo cm = new KasbeheerTransactieClientInfo();
                cm.Code = c.Code;
                cm.Id = c.Id;
                cm.FirstName = c.FirstName;
                cm.LastName = c.LastName;
                cm.Rijksregisternr = c.Rijksregisternr;
                cm.Bedrag = (Decimal)dr["Bedrag"];
                tempList.Add(cm);
            }
            return tempList;
        }

        public List<KasbeheerTransactieClientInfo> GetPreviousUsedClientsForLeefgroepTransactie(int transactieId)
        {
            List<ClientDao> list = new List<ClientDao>();
            DataTable dt;
            String sql = @"	select distinct
bewoner.*  from bewoner,kasbeheer.transactieClient,kasbeheer.transacties
where 
    bewoner.id =  kasbeheer.transactieClient.bewonerId and kasbeheer.transactieClient.transactieId =  kasbeheer.transacties.id
	and kasbeheer.transacties.leefgroepId  = (


	select leefgroepId from kasbeheer.transacties where id =  @transactieId) 
	and kasbeheer.transacties.transactieDatum > dateadd(month, datediff(month, 0, getdate())-2, 0)";
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {

                    dt = db.GetDataTable(sql, new DbParam() { Name = "@transactieId", Value = transactieId });
                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }

            List<KasbeheerTransactieClientInfo> tempList = new List<KasbeheerTransactieClientInfo>();

            foreach (DataRow dr in dt.Rows)
            {
                //alle clienten :  clienten komen meerdere keren voor omdat ze in meerdere leefgroepen kunnen zitten
                Client c = ClientParser.ParseDataTable(dr);
                KasbeheerTransactieClientInfo cm = new KasbeheerTransactieClientInfo();
                cm.Code = c.Code;
                cm.Id = c.Id;
                cm.FirstName = c.FirstName;
                cm.LastName = c.LastName;
                cm.Rijksregisternr = c.Rijksregisternr;
                cm.Bedrag = 0;
                tempList.Add(cm);
            }
            return tempList;
        }

        public List<KasbeheerRekening> GetRekeningen(int leefgroepId)
        {

            List<KasbeheerRekening> rekeningen = new List<KasbeheerRekening>();
            DataTable dt;
            String sql = @"select id, naam, (select sum( kasbeheer.transacties.transactieBedrag)  from  kasbeheer.transacties  
where kasbeheer.transacties.rekeningId = kasbeheer.rekening.id and kasbeheer.transacties .leefgroepId = @leefgroepId) Bedrag from  kasbeheer.rekening
where kasbeheer.rekening.actif=1";
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {

                try
                {
           
                    dt = db.GetDataTable(sql, new DbParam() { Name = "@leefgroepId", Value = leefgroepId });
                }
                finally
                {
                    db._sqlConnection.Close();

                }
                
            }
            foreach (DataRow dr in dt.Rows)
            {
                KasbeheerRekening rekening = new KasbeheerRekening();
                rekening.Id = (int)dr["Id"];
                rekening.Name = (String)dr["Naam"];
                if (!dr.IsNull("Bedrag"))
                    rekening.Bedrag = (Decimal)dr["Bedrag"];
                rekeningen.Add(rekening);
            }
            return rekeningen;
        }
        public List<KasbeheerTransactieType> GetKasbeheerTransactieTypes()
        {

            List<KasbeheerTransactieType> types = new List<KasbeheerTransactieType>();
            DataTable dt;
            String sql = @"select * from kasbeheer.transactieType";

            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
              
                try
                {
             
                    dt = db.GetDataTable(sql);
                }
                finally
                {
                    db._sqlConnection.Close();

                }

            }
            foreach (DataRow dr in dt.Rows)
            {
                KasbeheerTransactieType kasbeheerType = new KasbeheerTransactieType();
                kasbeheerType.Id = (int)dr["Id"];
                kasbeheerType.Name = (String)dr["Naam"];
                kasbeheerType.IsClientBased = (bool)dr["IsClientBased"];
                types.Add(kasbeheerType);
            }
            return types;
        }
        public List<KasbeheerTransactieCodering> GetKasbeheerTransactieCoderingen()
        {

            List<KasbeheerTransactieCodering> types = new List<KasbeheerTransactieCodering>();
            DataTable dt;
            String sql = @"select * from kasbeheer.transactieCodering";

            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
               

                try
                {
              
                    dt = db.GetDataTable(sql);
                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }
            foreach (DataRow dr in dt.Rows)
            {
                KasbeheerTransactieCodering codering = new KasbeheerTransactieCodering();
                codering.Kode = dr["kode"].ToString();
                codering.Name = (String)dr["omschrijving"];
                codering.IsClientBased = (bool)dr["IsClientBased"];
                types.Add(codering);
            }
            return types;
        }

    }
}
