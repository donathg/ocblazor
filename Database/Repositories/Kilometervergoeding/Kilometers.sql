﻿
select 
kilometers.km_kmt_code code,
km_id Id,
km_datum datumRit,
km_creationdate DatumIngave,
 concat(voornaam,' ', achternaam) bestuurder,
 km_reden reden,kilometers.km_bestemming reisroute,
 km_bedrag bedrag,
 km_kilometers kilometers,
 km_kmw_id wagenId,
 case when km_closed_persdienst_user is null then 0 else 1 end afgesloten,
  kilometers.km_beginstand beginstand,
  kilometers.km_eindstand eindstand
from kilometers,gebruiker
where gebruiker.id = km_bestuurder
and km_datum >= DATEADD(year,-1,GETDATE())
and km_bestuurder = @usrId