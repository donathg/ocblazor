﻿using AutoMapper;
using Dapper;
using Database.DataAccesObjects;
using Database.Sql;
using DataBase.Services;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.Kampen;
using Logic.Kilometervergoeding;
using Logic.LeefgroepNS;
using Logic.Settings;
using Logic.WagenNS;
using Logic.Werkbonnenbeheer;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Database.Repositories.Kilometervergoeding
{
    public class KilometervergoedingRepository : IKilometervergoedingRepository

    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;
        private readonly IWagenService _wagenService;
        private readonly IClientService _clientService;

        private readonly IKampenService _kampenService;
        private readonly ILeefgroepService _leefgroepService;
     
        protected IKilometervergoedingTypesService _typeService { get; set; }
        public KilometervergoedingRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager,  IKilometervergoedingTypesService typeService, IWagenService wagenService, IClientService clientService
            , IKampenService kampenService, ILeefgroepService leefgroepService)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager, _typeService, _wagenService, _clientService, _kampenService, _leefgroepService
                ) = (connection, userService, appSettings.Value, mapper, sqlManager, typeService, wagenService,clientService,
                kampenService, leefgroepService);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }


       
      
        public void SaveKilometers(Kilometer km)
        {
            DataTable dtLeefgroepen = new DataTable();
            dtLeefgroepen.Columns.Add("id");
            foreach (Leefgroep lfg in km.Leefgroepen)
                dtLeefgroepen.Rows.Add(lfg.Id);

            //2 Procedure aanroepen
            DataTable dtBewoners = new DataTable();

            dtBewoners.Columns.Add("id");
           foreach (Client c in km.Clienten)
                dtBewoners.Rows.Add(c.Id);

            //2 Procedure aanroepen
            DataTable dtKampen = new DataTable();

            dtKampen.Columns.Add("id");
            foreach (Kamp kamp in km.Kampen)
                dtKampen.Rows.Add(kamp.Id);


            km.Bestuurder = _userService.LoggedOnUser.Name;

            List<SqlParameter> paramList = new List<SqlParameter>()
                    {
                        new SqlParameter() { ParameterName  = "@retval", Direction = ParameterDirection.ReturnValue  },
                        new SqlParameter() { ParameterName  = "@id", Value = km.Id  ?? Convert.DBNull },
                        new SqlParameter() { ParameterName  = "@type", Value = km.KilometerType.Code },
                        new SqlParameter() { ParameterName  = "@wagenId", Value = km.Wagen.Id },
                        new SqlParameter() { ParameterName  = "@modifier", Value = _userService.LoggedOnUser.UserId},
                        new SqlParameter() { ParameterName  = "@bestuurder", Value = _userService.LoggedOnUser.UserId },
                        new SqlParameter() { ParameterName  = "@datum", Value = km.DatumRit },
                        new SqlParameter() { ParameterName  = "@bestemming", Value = km.Reisroute },
                        new SqlParameter() { ParameterName  = "@reden", Value = km.Reden },
                        new SqlParameter() { ParameterName  = "@beginstand", Value = km.Beginstand  ?? Convert.DBNull   },
                        new SqlParameter() { ParameterName  = "@eindstand", Value = km.Eindstand  ?? Convert.DBNull },
                        new SqlParameter() { ParameterName  = "@kilometers", Value = km.Kilometers.GetValueOrDefault() },
                        new SqlParameter() { ParameterName  = "@leefgroepenList", Value = dtLeefgroepen, SqlDbType =  SqlDbType.Structured },
                        new SqlParameter() { ParameterName  = "@bewonersList", Value = dtBewoners, SqlDbType = SqlDbType.Structured },
                        new SqlParameter() { ParameterName  = "@kampenList", Value = dtKampen, SqlDbType = SqlDbType.Structured },
                        new SqlParameter() { ParameterName  = "@o_bedrag", Direction = ParameterDirection.Output, SqlDbType = SqlDbType.Decimal, Precision = 10, Scale = 4 },
                        new SqlParameter() { ParameterName  = "@o_bedrag_omnium", Direction = ParameterDirection.Output, SqlDbType = SqlDbType.Decimal, Precision = 10, Scale = 4 },
                        new SqlParameter() { ParameterName  = "@o_omnium", Direction = ParameterDirection.Output, SqlDbType = SqlDbType.VarChar, Size = 1 }
                    };

            using (SqlConnection conn = new SqlConnection(_connection.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[saveKilometer2]", conn );
                foreach (SqlParameter p in paramList)
                {
                    cmd.Parameters.Add(p);
                }
                conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                Decimal o_bedrag;
                if (cmd.Parameters["@o_bedrag"].Value != DBNull.Value)
                    km.Bedrag = Convert.ToDouble(cmd.Parameters["@o_bedrag"].Value);
                if (cmd.Parameters["@retval"].Value != DBNull.Value)
                    km.Id = Convert.ToInt32(cmd.Parameters["@retval"].Value);
                conn.Close();
               
             
            }
          

        }
        public List<Client> GetClienten(int kmId)
        {
            String sql = _sqlManager.GetSQL("KilometersClienten.sql");
            try
            {
                _connection.Open();
                var clientIds = _connection.Query<int>(sql, new { kmId = kmId });
                List<Client> clienten = new List<Client>();
                foreach (int clientId in clientIds)
                {
                    clienten.Add(_clientService.GetClient(clientId));
                }
                return clienten;
            }
            finally
            {
                _connection.Close();
            }
        }

        public List<Kamp> GetKampen(int kmId)
        {
            String sql = _sqlManager.GetSQL("KilometersKampen.sql");
            try
            {
                _connection.Open();
                var kampIds = _connection.Query<int>(sql, new { kmId = kmId });
                List<Kamp> kampen = new List<Kamp>();
                foreach (int kampId in kampIds)
                {
                    kampen.Add(_kampenService.GetKampenById(kampId));
                }
                return kampen;
            }
            finally
            {
                _connection.Close();
            }
        }

        public List<Leefgroep> GetLeefgroepen(int kmId)
        {
            String sql = _sqlManager.GetSQL("KilometersLeefgroepen.sql");
            try
            {
                _connection.Open();
                var leefgroepIds = _connection.Query<int>(sql, new { kmId = kmId });
                List<Leefgroep> leefgroepen = new List<Leefgroep>();
                foreach (int lfgId in leefgroepIds)
                {
                     leefgroepen.Add(_leefgroepService.GetLeefgroepFromId(lfgId));
                }
                return leefgroepen;
            }
            finally
            {
                _connection.Close();
            }
        }
        public List<Kilometer> GetKilometers(int userId)
        {
            String sql = _sqlManager.GetSQL("Kilometers.sql");
            try
            {
                _connection.Open();
                var doas = _connection.Query<KilometerDao>(sql, new { usrId = userId }); ;
                List<Kilometer> kilometers = new List<Kilometer>();
                foreach (KilometerDao dao in doas)
                {
                    Kilometer km = _mapper.Map<Kilometer>(dao);
                    km.KilometerType = _typeService.GetKilometerByCode(dao.KilometerTypeCode);
                    km.Wagen = _wagenService.GetWagenById(dao.WagenId);
                    km.SetBeginEindstandNoUpdate(dao.Beginstand, dao.Eindstand);
                    kilometers.Add(km);
                }
                return kilometers;
            }
            finally
            {
                _connection.Close();
            }
        }

        public  void DeleteKilometers(int kmId)
        {

            String sql1 = "delete from kilometer_leefgroepen where kmlfg_km_id = @id";
            String sql2 = "delete from kilometer_bewoners where kmbew_km_id= @id";
            String sql3 = "Delete from kilometer_kampen where kmkamp_km_id = @id";
            String sql4 = @"delete from kilometers
                                where km_id = @id
                                and km_closed_boekh_date is null
                                and km_closed_bewoners_date is null
                                and km_closed_persdienst_date is null";   


                _connection.Open();
            using (var transaction = _connection.BeginTransaction())
            {
                try
                {
                    _connection.Execute(sql1, new { id = kmId }, transaction);
                    _connection.Execute(sql2, new { id = kmId }, transaction);
                    _connection.Execute(sql3, new { id = kmId }, transaction);
                    _connection.Execute(sql4, new { id = kmId }, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {

                    _connection.Close();
                }
            }
        }



        public List<String> GetRedenSuggestions(int userId, int wagenId, String typeCode)
        {
            string sql = @"select distinct LTRIM(RTRIM(km_reden)) reden from kilometers 
                where 

                km_bestuurder = @userId
                and km_kmw_id = @wagenId
                and km_kmt_code = @type
                order by  LTRIM(RTRIM(km_reden)) ";



            List<String> results = _connection.Query<string>(sql, new { userId = userId, wagenId = wagenId, type = typeCode }).AsList();
            return results;

        }

        public List<String> GetRouteSuggestions(int userId, int wagenId, String typeCode)
        {
            string sql = @"SELECT distinct LTRIM(RTRIM(km_bestemming)) bestemming 
                                   FROM kilometers 
                                   WHERE km_bestuurder = @userId
                                 and km_kmw_id = @wagenId
                                  and km_kmt_code = @type
                                  ORDER BY LTRIM(RTRIM(km_bestemming)) ";



            List<String> results = _connection.Query<string>(sql, new { userId = userId, wagenId = wagenId, type = typeCode }).AsList();
            return results;

        }
        public VerklaringResult GetVerklaringResult(int gebruikerId, string batchNummer)
        {
            string sql = @"SELECT id Id, gebruikerId, batchNr BatchNr, verklaringItemId VerklaringItemId, aanpasDatum ModificationDate, 
                                  afstandWagen AfstandEnkeleReisWagen, afstandFiets AfstandEnkeleReisFiets, ondertekend VerklaringOndertekend
                           FROM kilometer_verklaring
                           WHERE batchNr = @batchNummer
	                            AND gebruikerId = @gebruikerId";

            VerklaringResult verklaringResult = new VerklaringResult();
            VerklaringResultDAO dao = _connection.Query<VerklaringResultDAO>(sql, new { batchNummer, gebruikerId }).FirstOrDefault();
            if (dao != null)
            {
                verklaringResult = new VerklaringResult()
                {
                    BatchNr = dao.BatchNr,
                    GebruikerId = dao.GebruikerId,
                    ModificationDate = dao.ModificationDate,
                    VerklaringItemId = dao.VerklaringItemId,
                    AfstandEnkeleReisWagen = dao.AfstandEnkeleReisWagen,
                    AfstandEnkeleReisFiets = dao.AfstandEnkeleReisFiets,
                    VerklaringOndertekend = dao.VerklaringOndertekend
                };
            }
            return verklaringResult;
        }

        public void SaveVerklaringResult(VerklaringResult result)
        {
            _connection.Open();
            IDbTransaction transaction = _connection.BeginTransaction();

            try
            {

                string delete = @"DELETE FROM kilometer_verklaring
                                  WHERE batchNr = @batchNr
	                              AND gebruikerId = @gebruikerId";

                _connection.Execute(delete, new { @batchNr = result.BatchNr, @gebruikerId = result.GebruikerId }, transaction);

                string insert = @"INSERT INTO kilometer_verklaring (batchNr, gebruikerId, aanpasDatum, verklaringItemId, afstandWagen, afstandFiets, ondertekend)
                                  VALUES(@batchNr, @gebruikerId, @aanpasDatum, @verklaringItemId, @afstandWagen, @afstandFiets, @ondertekend)";

                _connection.Execute(insert, new
                {
                    @batchNr = result.BatchNr,
                    @gebruikerId = result.GebruikerId,
                    @aanpasDatum = result.ModificationDate,
                    @verklaringItemId = result.VerklaringItemId,
                    @afstandWagen = result.AfstandEnkeleReisWagen,
                    @afstandFiets = result.AfstandEnkeleReisFiets,
                    @ondertekend = result.VerklaringOndertekend
                }, transaction);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally 
            { 
                _connection.Close(); 
            }
        }
    }
}
   
 
