﻿using AutoMapper;
using Dapper;
using Database.DataAccesObjects;
using Database.Sql;
using DataBase.Services;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.Settings;
using Logic.Werkbonnenbeheer;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Database.Repositories.Kilometervergoeding
{
    public class KilometervergoedingTypesRepository : IKilometervergoedingTypesRepository

    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public KilometervergoedingTypesRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

      
        public List<KilometerType> GetKilometerTypes()
        {

            String sql  = _sqlManager.GetSQL("KilometerTypes.sql");

            try
            {
                _connection.Open();
                var kilometers = _connection.Query<KilometerTypeDao>(sql);
                return _mapper.Map<List<KilometerType>>(kilometers);
            }
            catch(Exception ex)
            {
                throw;
            }
            finally
            {
                _connection.Close();
            }

          
        }
    }
}
