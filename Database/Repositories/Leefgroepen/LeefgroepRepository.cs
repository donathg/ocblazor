﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Database.Sql;
using Logic.DataBusinessObjects.Webservices.eCQare;

namespace DataBase.Repositories.Leefgroepen
{



    public class LeefgroepRepository : ILeefgroepRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;
        public LeefgroepRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        //public List<Leefgroep> GetLeefgroepen()
        //{
        //    List<Leefgroep> list = new List<Leefgroep>();
        //    try
        //    {
        //        _connection.Open();
            
        //        String sql = _sqlManager.GetSQL("Leefgroepen.sql");
        //        list = _connection.Query<Leefgroep>(sql).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        int i = 0;
        //    }
        //    finally { _connection.Close(); }

        //    return list;
        //}
        //
        public List<Leefgroep> GetLeefgroepen()
        {
            DataTable dt = null;
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                String sql = "select * from leefgroep";
                dt = db.GetDataTable(sql);
                return LeefgroepParser.ParseDataTable(dt);
            }
        }
        //private List<Leefgroep> ParseDataTabe (DataTable dt)
        //{
        //    List<Leefgroep> leefgroepen = new List<Leefgroep>();

        //    foreach (DataRow dr in dt.Rows)
        //    {

        //        Leefgroep lfg = new Leefgroep();
        //        leefgroepen.Add(lfg);
        //        lfg.Code = dr["code"] != DBNull.Value ? dr["code"].ToString() : null;
        //        lfg.Name = dr["naam"] != DBNull.Value ? dr["naam"].ToString() : null;
        //        lfg.Actief = dr["actief"].ToString() == "Y";
        //        lfg.Id = dr["id"] != DBNull.Value ? Convert.ToInt32(dr["id"]) : 0;
        //        lfg.NietVerbondenAanSchool = dr["nietVerbondenAanSchool"] != DBNull.Value ? Convert.ToBoolean(dr["nietVerbondenAanSchool"]) : false;
        //        lfg.DisplayNaam = dr["displayNaam"] != DBNull.Value ? dr["displayNaam"].ToString() : null;
        //        lfg.EcqareId = dr["ecqareId"] != DBNull.Value ? Guid.Parse(dr["ecqareId"].ToString()) : Guid.Empty;
        //        lfg.EcqareParentId = dr["ecqareParentId"] != DBNull.Value ? Guid.Parse(dr["ecqareParentId"].ToString()) : Guid.Empty;
        //        lfg.StartDatum = dr["startDatum"] != DBNull.Value ? Convert.ToDateTime(dr["startDatum"]) : DateTime.MinValue;
        //        lfg.EindDatum = dr["eindDatum"] != DBNull.Value ? Convert.ToDateTime(dr["eindDatum"]) : DateTime.MinValue;
        //        lfg.EcqareCode = dr["ecqareCode"] != DBNull.Value ? dr["ecqareCode"].ToString() : null;
        //        lfg.MaaltijdlijstenOrderNum = dr["maaltijdlijstenOrderNum"] != DBNull.Value ? Convert.ToInt32(dr["maaltijdlijstenOrderNum"]) : 0;
        //        lfg.MaaltijdlijstenGroepering = dr["maaltijdlijstenGroepering"] != DBNull.Value ? dr["maaltijdlijstenGroepering"].ToString() : null;
        //        lfg.IsEcqareLowestLevel = dr["isEcqareLowestLevel"] != DBNull.Value ? Convert.ToBoolean(dr["isEcqareLowestLevel"]) : false;
        //        lfg.MaaltijdlijstenVacuumVerpakking = dr["maaltijdlijstenVacuumVerpakking"] != DBNull.Value ? Convert.ToBoolean(dr["maaltijdlijstenVacuumVerpakking"]) : false;

        //        leefgroepen.Add(lfg);
        //    }
        //    return leefgroepen; 
        //}
    }
}
