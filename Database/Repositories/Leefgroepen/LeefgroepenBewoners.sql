﻿select 
    bewoner.*, 
    leefgroep.Code LeefgroepCode, 
    leefgroep.Naam LeefgroepName, 
    leefgroep.id LeefgroepId 
from 
    bewoner,bewonerleefgroep,leefgroep 
where 
    bewonerid = bewoner.id
    and bewonerleefgroep.leefgroepid = leefgroep.id 
order by bewoner.voornaam, bewoner.achternaam