﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Database.Sql;
using Logic.LocatieNs;
using Database.DataAccesObjects;

namespace DataBase.Repositories.LocatieNs
{



    public class LocatieRepository : ILocatieRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public LocatieRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public List<Locatie> GetLocaties()
        {

            String sql = _sqlManager.GetSQL("Locatie.sql");
            List<Locatie> list = new List<Locatie>();


            try
            {
                _connection.Open();
                var daos = _connection.Query<LocatieDao>(sql).ToList();
                list = ParseAndAssignParents(daos);
            }
            finally
            {
                _connection.Close();
            }
            return list;
        }

        private Locatie Parse (LocatieDao dao)
        {
            Locatie loc = new Locatie();
            loc.Id = dao.Id;
            loc.ParentId = dao.ParentId;
            loc.Naam = dao.Naam;
            return loc;

        }
        private List<Locatie> ParseAndAssignParents(List<LocatieDao> daos)
        {
           List<Locatie> list = new List<Locatie>();
           foreach (LocatieDao dao in daos )
           {
                list.Add(Parse(dao));
           }
           foreach (Locatie l in list)
           {
               
                l.Parent = list.Where(x => x.Id == l.ParentId).FirstOrDefault();
                l.Children = list.Where(x => x.ParentId == l.Id).ToList();
           }



            return list;

        }
    }
}
