﻿select p.id Id,p.ModuleCode ModuleCode , p.Title Title, p.ExtraInfo ExtraInfo,p.ChoiceType ChoiceType, p.IsMandatory IsMandatory,p.orderNumReport,
	c.Id ItemId, c.description ItemDescription,c.isOther ItemIsOther, c.isDefaultSelected ItemIsDefaultSelected, c.OrderNum ItemOrderNum, c.Active active, c.activeInDevelopment
from MultiSelect p 
	left outer join MultiSelectItem c on c.MultiSelectId = p.id
order by moduleCode,p.id, c.orderNum
