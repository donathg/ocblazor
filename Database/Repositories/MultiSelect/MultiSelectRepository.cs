﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Database.Sql;
using Logic.LocatieNs;
using Database.DataAccesObjects;

using Logic.DataBusinessObjects.MultiSelectNS;

namespace DataBase.Repositories.LocatieNs
{



    public class MultiSelectRepository : IMultiSelectRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public MultiSelectRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public List<MultiSelect> GetAll()
        {

            String sql = _sqlManager.GetSQL("MultiSelect.sql");
            List<MultiSelect> list = new List<MultiSelect>();


            try
            {
                _connection.Open();
                List<MultiSelectDao> daos = _connection.Query<MultiSelectDao>(sql).ToList();
                list = Parse(daos);
            }
            finally
            {
                _connection.Close();
            }
            return list;
        }

      
        private List<MultiSelect> Parse(List<MultiSelectDao> daos)
        {
           List<MultiSelect> list = new List<MultiSelect>();
           foreach (MultiSelectDao dao in daos )
           {
          
                MultiSelect parent = list.FirstOrDefault(x=>x.Id==dao.Id);
                if (parent == null)
                {
                    parent = new MultiSelect()
                    {
                        Id = dao.Id,
                        ChoiceType = dao.ChoiceType,
                        ExtraInfo = dao.ExtraInfo,
                        IsMandatory = dao.IsMandatory,
                        Items = new List<MultiSelectItem>(),
                        ModuleCode = dao.ModuleCode,
                        Title = dao.Title,
                        OrderNumReport = dao.OrderNumReport,
                    };
                    list.Add(parent);
                }
                parent.Items.Add(new MultiSelectItem()
                {
                    Parent = parent,
                    Description = dao.ItemDescription,
                    Id = dao.ItemId,
                    IsDefaultSelected = dao.ItemIsDefaultSelected,
                    IsOther = dao.ItemIsOther,
                    Ordernum = dao.ItemOrderNum,
                    Active = dao.Active,
                    ActiveInDev = dao.ActiveInDevelopment,
                    Result = new MultiSelectItemResult() { MultiSelectItemId = dao.ItemId }
                });
           }
           return list;
        }
    }
}
