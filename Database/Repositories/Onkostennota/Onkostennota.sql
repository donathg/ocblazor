﻿select  n.*, c.naam CategorieNaam, 
(select COUNT(*) from acerta.onkostenNotafiles where onkostennotaId = n.id ) aantalBijlages,
(
SELECT STUFF((
    SELECT ', ' + voornaam + ' ' + achternaam
    FROM acerta.onkostennota_clienten a
    JOIN bewoner b ON a.bewonerId = b.id where a.onkostennotaId = n.id
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') 

) clienten,
(select naam from leefgroep where id = n.leefgroepId) LeefgroepName


from acerta.onkostennota n, acerta.onkostennota_categorie c 
where n.categorieId = c.id
and gebruikerId = @gebruikerId 
and IsAkkoordMetVoorwaarden = 1
and moduleCode = 'ONKOSTENNOTA'
order by n.onkostennotaDate desc