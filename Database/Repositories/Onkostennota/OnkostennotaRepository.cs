﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Database.Sql;
using Logic.LocatieNs;
using Database.DataAccesObjects;
using Logic.ArtikelNs;
using Logic.DataBusinessObjects.Onkostennota;
using Logic.DataBusinessObjects.Aankoop;

namespace DataBase.Repositories.LocatieNs
{



    public class OnkostennotaRepository : IOnkostennotaRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public OnkostennotaRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }


         public List<OnkostennotaItem> LoadMaaltijdregistraties()
 
        {
            String sql = _sqlManager.GetSQL("Maaltijdregistraties.sql");
            try
            {
                _connection.Open();
                
                IEnumerable<OnkostennotaItemDao> listDaos = _connection.Query<OnkostennotaItemDao>(sql, new { @gebruikerId = _userService.LoggedOnUser.UserId });
                return _mapper.Map<List<OnkostennotaItem>>(listDaos);
            }
            finally
            {
                _connection.Close();
            }
        }


        public List<MaaltijdAandachtspuntenItem> LoadAandachtspunten()

        {
            String sql = _sqlManager.GetSQL("MaaltijdAandachtspunten.sql");
            try
            {
                _connection.Open();

                IEnumerable<MaaltijdAandachtspuntenDao> listDaos = _connection.Query<MaaltijdAandachtspuntenDao>(sql);
                return _mapper.Map<List<MaaltijdAandachtspuntenItem>>(listDaos);
            }
            catch(Exception ex)
            {
                int i = 0;
                return new List<MaaltijdAandachtspuntenItem>();
            }
            finally
            {
                _connection.Close();
            }
        }

        public List<OnkostennotaItem> LoadOnkostenNotas()
        {
            String sql = _sqlManager.GetSQL("Onkostennota.sql");
            try
            {
                _connection.Open();
                IEnumerable<OnkostennotaItemDao> listDaos = _connection.Query<OnkostennotaItemDao>(sql, new { @gebruikerId = _userService.LoggedOnUser.UserId });
                return _mapper.Map<List<OnkostennotaItem>>(listDaos);
            }
            finally
            {
                _connection.Close();
            }
        }
        public List<OnkostennotaCategorieItem> GetCategorien()
        {
            String sql = "select * from acerta.onkostennota_categorie where actief = 1";
            try
            {
                _connection.Open();
                IEnumerable<OnkostennotaCategorieItemDao> listDaos = _connection.Query<OnkostennotaCategorieItemDao>(sql, new { @gebruikerId = _userService.LoggedOnUser.UserId });
                return _mapper.Map<List<OnkostennotaCategorieItem>>(listDaos);
            }
            finally
            {
                _connection.Close();
            }
        }


        public OnkostennotaItem InsertOnkostenNota(OnkostennotaItem item, string moduleCode)
        {

          
               
                String sql = _sqlManager.GetSQL("InsertOnkostenNota.sql");
            String sqlClienten = _sqlManager.GetSQL("InsertOnkostenNotaClienten.sql");
            try
                {

                   _connection.Open();
                   item.GebruikerId = _userService.LoggedOnUser.UserId;
                   item.Id = _connection.ExecuteScalar<int>(sql, item);
                   foreach (Client c in item.Clienten)
                   {
                         _connection.ExecuteScalar<int>(sqlClienten, new { @onkostennotaId = item.Id, @bewonerId = c.Id });
                   }



                   return item;
                }
                finally
                {
                    _connection.Close();
                }
             
                



        }




        public void DeleteOnkostenNota(OnkostennotaItem item)
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {

                    db.BeginTransaction();
                    List<DbCommand> commandList = new List<DbCommand>();

                    String sql = @"delete from acerta.onkostennota_clienten where onkostennotaId = @id";
                    commandList.Add(new DbCommand(DbCommandType.crud, sql, new DbParam() { Name = "@id", Value = item.Id }));
                    
                    sql = @"delete from acerta.onkostenNotafiles where onkostennotaId = @id";
                    commandList.Add(new DbCommand(DbCommandType.crud, sql, new DbParam() { Name = "@id", Value = item.Id }));
                    
                    sql = @"delete from acerta.onkostennota where id = @id";
                    commandList.Add(new DbCommand(DbCommandType.crud, sql, new DbParam() { Name = "@id", Value = item.Id }));


                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();

                }
            }


        }
        public  OnkostennotaItem UpdateOnkostenNotaMetAkkoordMetVoorwaarden(OnkostennotaItem item)
        {
            String sql = _sqlManager.GetSQL("UpdateOnkostenNotaMetAkkoordMetVoorwaarden.sql");
            try
            {

              
                 _connection.ExecuteScalar<int>(sql, new  { Id = item.Id});

                return item;
            }
            finally
            {
                _connection.Close();
            }

        }

    }
}
