﻿using AutoMapper;
using Dapper;
using Database.DataAccesObjects;
using Database.Sql;
using DataBase.Global;
using DataBase.Services;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Webservices;
using Logic.Prijzen;
using Logic.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Repositories.Prijzen
{
    public class PrijzenRepository : IPrijzenRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public PrijzenRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

        public List<StandaardPrijzen> SelectAllPrijzen()
        {
            try
            {
                _connection.Open();

                string sql = @"SELECT * FROM StandaardPrijzen";
                var prijzen = _connection.Query<StandaardPrijzenDAO>(sql);

                return _mapper.Map<List<StandaardPrijzen>>(prijzen);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}
