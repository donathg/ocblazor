﻿using DataBase.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database.DatabaseParsers;
using Dapper;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Logic.Settings;
using Microsoft.Extensions.Options;
using AutoMapper;
using Logic.Telefoonboek;
using Database.Sql;

namespace DataBase.Repositories.Telefoonboek
{

 




    public class TelefoonboekRepository : ITelefoonboekRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;
        public TelefoonboekRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper,sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public List<Gebruiker> GetTelefoonboek ()
        {

            String sql = _sqlManager.GetSQL("Telefoonboek.sql");


            try
            {
                _connection.Open();
                IEnumerable<GebruikerDao> list = _connection.Query<GebruikerDao>(sql);
                List<Gebruiker> gebruikers = _mapper.Map<List<Gebruiker>>(list).ToList();
                return gebruikers;
            }
            finally
            {
                _connection.Close();
            }


            
        }

        private List<T> List<T>()
        {
            throw new NotImplementedException();
        }
    }
}
