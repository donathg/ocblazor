﻿using AutoMapper;
using Dapper;
using Database.DataAccesObjects;
using Database.Sql;
using DataBase.Services;
using Logic.WagenNS;
using Logic.Settings;
using Logic.Werkbonnenbeheer;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Logic.DataBusinessObjects;

namespace Database.Repositories.Kilometervergoeding
{
    public class WagenRepository : IWagenRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public WagenRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public List<Wagen> GetWagens()
        {

            String sql = _sqlManager.GetSQL("Wagen.sql");

            try
            {
                _connection.Open();
                var kilometers = _connection.Query<WagenDao>(sql);
                return _mapper.Map<List<Wagen>>(kilometers);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _connection.Close();
            }
        }
            public  void LoadWagens()
            {
//                using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
//                {
//                    return db.GetDataTable(@"select kilometer_wagen.*,locatie.naam LocatieNaam from kilometer_wagen left outer join locatie 
//on kilometer_wagen.locatieId = locatie.id where actief=1
//order by (case when  kmw_naam  = 'eigen wagen' then -1
// when  kmw_naam  = 'eigen fiets' then -0
// else kmw_id end)");
//                }
           }
            public static void GetVerplaatsingSuggestions(int userId, int wagenId, String typeCode)
            {
                //using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
                //{
                //    string sql = @"SELECT distinct LTRIM(RTRIM(km_bestemming)) bestemming 
                //                   FROM kilometers 
                //                   WHERE km_bestuurder = @userId
                //                   and km_kmw_id = @wagenId
                //                   and km_kmt_code = @type
                //                   ORDER BY 
                //                   LTRIM(RTRIM(km_bestemming))";
                //    DataTable dt = db.GetDataTable(sql, new DbParam() { Name = "@userId", Value = userId }, new DbParam() { Name = "@wagenId", Value = wagenId }, new DbParam() { Name = "@type", Value = typeCode });

                //    List<string> suggesties = new List<string>();
                //    foreach (DataRow dr in dt.Rows)
                //    {
                //        if (!dr.IsNull("BESTEMMING"))
                //            suggesties.Add(dr["BESTEMMING"].ToString());
                //    }

                //    return suggesties;
                //}
            }
            public static void GetRedenSuggestions(int userId, int wagenId, String typeCode)
            {

                //            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
                //            {
                //                DataTable dt = db.GetDataTable(@"select distinct LTRIM(RTRIM(km_reden)) reden from kilometers 
                //where 

                //km_bestuurder = @userId
                //and km_kmw_id = @wagenId
                //and km_kmt_code = @type
                //order by  LTRIM(RTRIM(km_reden))", new DbParam() { Name = "@userId", Value = userId }, new DbParam() { Name = "@wagenId", Value = wagenId }, new DbParam() { Name = "@type", Value = typeCode });
                //                List<string> suggesties = new List<string>();
                //                foreach (DataRow dr in dt.Rows)
                //                {
                //                    if (!dr.IsNull("REDEN"))
                //                        suggesties.Add(dr["REDEN"].ToString());
                //                }

                //                return suggesties;
                //}
            }
            public static void LoadTypes()
            {
                //            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
                //            {
                //                return db.GetDataTable(@"select * from kilometer_type order by kmt_code");
                //            }
                //        }
                //        public static DataTable LoadWagens()
                //        {
                //            using (DatabaseCentral db = new DatabaseCentral(GlobalData.Instance.ConnectionStringADONET))
                //            {
                //                return db.GetDataTable(@"select kilometer_wagen.*,locatie.naam LocatieNaam from kilometer_wagen left outer join locatie 
                //on kilometer_wagen.locatieId = locatie.id where actief=1
                //order by (case when  kmw_naam  = 'eigen wagen' then -1
                // when  kmw_naam  = 'eigen fiets' then -0
                // else kmw_id end)");
                //            }
            }
        }
  
}
