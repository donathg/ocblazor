﻿using AutoMapper;
using Dapper;
using Database.DataAccesObjects;
using Database.Sql;
using DataBase.Services;
using Logic.WagenNS;
using Logic.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using Logic.DataBusinessObjects.Webservices;
using System.Threading.Tasks;
using Logic.DataBusinessObjects.Webservices.eCQare;
using DataBase.Global;
using System.Linq;
using Logic.DataBusinessObjects.Webservices.Acerta;
using System.Data.SqlClient;

namespace Database.Repositories.Kilometervergoeding
{
    public class WebServicesRepository : IWebserviceRepository
    {
        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;


        public WebServicesRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }

        public Task<WebserviceExternal> Save_eCQare_Clienten_ToDatabase(WebserviceExternal ws, List<EcqareClientV2File> files)
        {
            List<string> errors = new List<string>();

            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    List<DbCommand> commandList = new List<DbCommand>();

                    // DELETE ALL ENTRIES FROM TEMP TABLE
                    string sqlDeleteAll = @"DELETE FROM webservices.import_Ecqare_Clienten";
                    db.Excecute(new DbCommand(DbCommandType.crud, sqlDeleteAll));
                    string sqlDeleteAllAandachtspunten = @"DELETE FROM webservices.import_Ecqare_Aandachtspunten";
                    db.Excecute(new DbCommand(DbCommandType.crud, sqlDeleteAllAandachtspunten));

                    // Keep list of active and inactive clients
                    List<EcqareClientV2File> activeClients = files.Where(x => x.Departments.Any(y => !y.DateEnd.HasValue || y.DateEnd >= DateTime.Today)).ToList();
                    List<EcqareClientV2File> inactiveClients = files.Where(x => x.Departments.All(y => y.DateEnd.HasValue && y.DateEnd < DateTime.Today)).ToList();

                    // INSERT ALL ACTIVE ROWS FROM ECQARE
                    foreach (EcqareClientV2File file in activeClients)
                    {
                        /// create insert statement for active departments.
                        /// Inactive departments are not processed because the entire list of links between client and leefgroep are deleted before import.
                        /// Therefore only the active ones need to be imported!!!
                        foreach (DepartmentLink departmentLink in file.Departments.Where(y => !y.DateEnd.HasValue || y.DateEnd >= DateTime.Today).ToList())
                        {
                            try
                            {
                                string sql = @" INSERT INTO webservices.import_Ecqare_Clienten (ecqareId, FirstName, LastName, GenderCode, BirthDate, DateOfDeath, Nationality, NationalNumber, 
	                                                departmentId, IsMainDepartment, departmentDateStart, departmentDateEnd, moduleId, moduleName, dossierId, fileNumber)
                                                VALUES (@ecqareId, @FirstName, @LastName, @GenderCode, @BirthDate, @DateOfDeath, @Nationality, @NationalNumber, @departmentId, @IsMainDepartment,
	                                                @departmentDateStart, @departmentDateEnd, @moduleId, @moduleName, @dossierId, @fileNumber)";

                                List<DbParam> parameters = new List<DbParam>()
                                {
                                    new DbParam() { Name = "@dossierId", Value = file.Id },
                                    new DbParam() { Name = "@ecqareId", Value = file.Client.Id },
                                    new DbParam() { Name = "@FirstName", Value = file.Client.FirstName },
                                    new DbParam() { Name = "@LastName", Value = file.Client.LastName },
                                    new DbParam() { Name = "@GenderCode", Value = file.Client.GenderCode },
                                    new DbParam() { Name = "@BirthDate", Value = file.Client.BirthDate },
                                    new DbParam() { Name = "@DateOfDeath", Value = file.Client.DateOfDeath },
                                    new DbParam() { Name = "@Nationality", Value = file.Client.Nationality },
                                    new DbParam() { Name = "@departmentId", Value = departmentLink.Department.Id },
                                    new DbParam() { Name = "@departmentDateStart", Value = departmentLink.DateStart },
                                    new DbParam() { Name = "@departmentDateEnd", Value = departmentLink.DateEnd },
                                    new DbParam() { Name = "@fileNumber", Value = file.FileNumber }
                                };
                                if (file.Client.NationalNumber is not null)
                                {
                                    parameters.Add(new DbParam() { Name = "@NationalNumber", Value = file.Client.NationalNumber.Replace("-", "").Replace(".", "") });
                                }
                                else
                                {
                                    parameters.Add(new DbParam() { Name = "@NationalNumber", Value = null });

                                    errors.Add($"Client {file.Client.FirstName} {file.Client.LastName} heeft geen NationalNumber");
                                }
                                if (file.MainDepartment is not null)
                                {
                                    Department maindepartment = file.MainDepartment;

                                    parameters.Add(new DbParam() { Name = "@IsMainDepartment", Value = maindepartment.Id == departmentLink.Department.Id });
                                }
                                else
                                {
                                    parameters.Add(new DbParam() { Name = "@IsMainDepartment", Value = null });

                                    errors.Add($"Client {file.Client.FirstName} {file.Client.LastName} heeft geen MainDepartment");
                                }
                                if (departmentLink.Modules.Count() != 0)
                                {
                                    if (departmentLink.Modules.Where(x => !x.DateEnd.HasValue || x.DateEnd >= DateTime.Today).FirstOrDefault() is not null)
                                    {
                                        Module module = departmentLink.Modules.Where(x => !x.DateEnd.HasValue || x.DateEnd >= DateTime.Today).FirstOrDefault().Modul;

                                        parameters.Add(new DbParam() { Name = "@moduleId", Value = module.Id });
                                        parameters.Add(new DbParam() { Name = "@moduleName", Value = module.Name });
                                    }
                                    else
                                    {
                                        parameters.Add(new DbParam() { Name = "@moduleId", Value = null });
                                        parameters.Add(new DbParam() { Name = "@moduleName", Value = null });

                                        errors.Add($"Client {file.Client.FirstName} {file.Client.LastName} heeft geen geldige Module voor department: {departmentLink.Department.Name}");
                                    }
                                }
                                else
                                {
                                    parameters.Add(new DbParam() { Name = "@moduleId", Value = null });
                                    parameters.Add(new DbParam() { Name = "@moduleName", Value = null });

                                    errors.Add($"Client {file.Client.FirstName} {file.Client.LastName} heeft geen module voor department: {departmentLink.Department.Name}");
                                }

                                commandList.Add(new DbCommand(DbCommandType.crud, sql, parameters.ToArray()));
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }

                        foreach (Attentionpoint attentionpoint in file.attentionPoints)
                        {
                            try
                            {
                                if (attentionpoint.Type.Name.StartsWith("Voedsel:"))
                                {

                                    string sql = @" INSERT INTO webservices.import_Ecqare_Aandachtspunten (bewonerId, AandachtspuntNaam, AandachtspuntParentNaam, AandachtspuntStartdatum, 
                                                        AandachtspuntEinddatum)
                                                    VALUES (@bewonerId, @AandachtspuntNaam, @AandachtspuntParentNaam, @AandachtspuntStartdatum, @AandachtspuntEinddatum)";

                                    string naamAandachtspunt = attentionpoint.Description;
                                    if (attentionpoint.Description != null)
                                    {
                                        int aandachtpuntDivideLocation = naamAandachtspunt.IndexOf('\n');
                                        naamAandachtspunt = naamAandachtspunt.Substring(aandachtpuntDivideLocation + 1);
                                        naamAandachtspunt = naamAandachtspunt.Replace("Geen varkenvlees", "T");
                                    }

                                    List<DbParam> parameters = new List<DbParam>()
                                    {
                                        new DbParam() { Name = "@bewonerId", Value = file.Client.Id },
                                        new DbParam() { Name = "@AandachtspuntNaam", Value = naamAandachtspunt },
                                        new DbParam() { Name = "@AandachtspuntParentNaam", Value = attentionpoint.Type.Name },
                                        new DbParam() { Name = "@AandachtspuntStartdatum", Value = attentionpoint.DateStart },
                                        new DbParam() { Name = "@AandachtspuntEinddatum", Value = attentionpoint.DateEnd }
                                    };

                                    commandList.Add(new DbCommand(DbCommandType.crud, sql, parameters.ToArray()));
                                }
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }
                    }

                    // CREATE UPDATE STATEMENTS FOR INACTIVE CLIENTS
                    foreach (EcqareClientV2File file in inactiveClients)
                    {
                        string updateClient = @"UPDATE bewoner
                                                SET opnameeinde = @opnameeinde
                                                WHERE ecqareId = @ecqareId
                                                    AND isFromEcqare = 1";

                        List<DbParam> parametersUpdateClient = new List<DbParam>()
                        {
                            new DbParam() { Name = "@ecqareId", Value = file.Client.Id },
                            new DbParam() { Name = "@opnameeinde", Value = file.Departments.Max(x => x.DateEnd)}
                        };

                        commandList.Add(new DbCommand(DbCommandType.crud, updateClient, parametersUpdateClient.ToArray()));
                    }

                    // WRITE DOWN ERRORS in long list (for debugging and finding problems with data)
                    string listErrors = "";

                    foreach (string error in errors)
                    {
                        listErrors += "\n" + error;
                    }

                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch (Exception ex)
                {
                    db.RollbackTransaction();
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }

            ws.LastRunErrors.AddRange(errors);
            return Task.FromResult(ws);
        }
        public Task<WebserviceExternal> Save_eCQare_Departments_ToDatabase(WebserviceExternal ws, List<EcqareDepartmentV2> files)
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    List<DbCommand> commandList = new List<DbCommand>();

                    // DELETE ALL ENTRIES FROM TEMP TABLE
                    string sqlDeleteAll = @"DELETE FROM webservices.import_Ecqare_Department; DBCC CHECKIDENT('webservices.import_Ecqare_Department', RESEED, 0);";
                    db.Excecute(new DbCommand(DbCommandType.crud, sqlDeleteAll));

                    // INSERT ALL ROWS FROM ECQARE
                    foreach (EcqareDepartmentV2 file in files)
                    {

                        string sql = @"INSERT INTO webservices.import_Ecqare_Department (Name, Code, Ecqare_Id, Ecqare_ParentId, ActiveFrom, ActiveUntil)
                               VALUES (@name, @code, @ecqareId, @ecqareParentid, @activeFrom, @activeUntil)";

                        List<DbParam> parameters = new List<DbParam>()
                        {
                            new DbParam() { Name = "@name", Value = file.Name },
                            new DbParam() { Name = "@code", Value = file.Code },
                            new DbParam() { Name = "@ecqareId", Value = file.Id },
                            new DbParam() { Name = "@ecqareParentid", Value = file.ParentId },
                            new DbParam() { Name = "@activeFrom", Value = file.ActiveFrom },
                            new DbParam() { Name = "@activeUntil", Value = file.ActiveUntil }
                        };

                        commandList.Add(new DbCommand(DbCommandType.crud, sql, parameters.ToArray()));
                    }
                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }

            return Task.FromResult(ws);
        }
        public Task<WebserviceExternal> Save_eCQare_Mealplans_ToDatabase(WebserviceExternal ws, List<EcqareAlivioMealplanFile> files)
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    List<DbCommand> commandList = new List<DbCommand>();

                    // INSERT ALL ROWS FROM ECQARE
                    foreach (EcqareAlivioMealplanFile file in files)
                    {

                        string sql = @"INSERT INTO webservices.MaaltijdHistoriek (CreationDate, MealDate, ClientIdEcqare, ClientNaam, MealSchedule, Mealchoice, MealchoiceFull, MealLocationName)
                               VALUES (@CreationDate, @MealDate, @ClientIdEcqare, @ClientNaam, @MealSchedule, @Mealchoice, @MealchoiceFull, @MealLocationName)";

                        List<DbParam> parameters = new List<DbParam>()
                        {
                            new DbParam() { Name = "@CreationDate", Value = DateTime.Now },
                            new DbParam() { Name = "@MealDate", Value = file.MealDate },
                            new DbParam() { Name = "@ClientIdEcqare", Value = file.ClientId },
                            new DbParam() { Name = "@ClientNaam", Value = file.Client?.FirstNameLastname },
                            new DbParam() { Name = "@MealSchedule", Value = file.MealScheduleFull.ToString() },
                            new DbParam() { Name = "@Mealchoice", Value = file.MealChoice },
                            new DbParam() { Name = "@MealchoiceFull", Value = file.MealChoiceFull.ToString() },
                            new DbParam() { Name = "@MealLocationName", Value = file.MealLocationName }
                        };

                        commandList.Add(new DbCommand(DbCommandType.crud, sql, parameters.ToArray()));
                    }
                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }

            return Task.FromResult(ws);
        }
        public Task<WebserviceExternal> Save_Acerta_Contracten_ToDatabase(WebserviceExternal ws, List<AcertaContract> files)
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    List<DbCommand> commandList = new List<DbCommand>();
                    DateTime importDate = DateTime.Now;
                    // DELETE ALL ENTRIES FROM TEMP TABLE
                    string sqlDeleteAll = @"DELETE FROM webservices.ImportAcertaContract; DBCC CHECKIDENT('webservices.ImportAcertaContract', RESEED, 0);";
                    db.Excecute(new DbCommand(DbCommandType.crud, sqlDeleteAll));

                    // INSERT ALL ROWS FROM ECQARE  Date, dienstCodeEnd Date
                    foreach (AcertaContract file in files)
                    {

                        string sql = @"INSERT INTO webservices.ImportAcertaContract(
                                        ImportDate,
                                        EmployeeSocialSecurityNumber,
                                        EmploymentStartDate,
                                        EmploymentEndDate,
                                        EmployeeHrmNumberCurrent,
                                        ServiceReceiverCostCenterNumber,
                                        DienstCode,
                                        dienstCodeStart,
                                        dienstCodeEnd,
                                        SequenceNumber
                                    ) VALUES (
                                        @ImportDate,
                                        @EmployeeSocialSecurityNumber,
                                        @EmploymentStartDate,
                                        @EmploymentEndDate,
                                        @EmployeeHrmNumberCurrent,
                                        @ServiceReceiverCostCenterNumber,
                                        @DienstCode,
                                        @dienstCodeStart,
                                        @dienstCodeEnd,
                                        @SequenceNumber
                                    )";

                        List<DbParam> parameters = new List<DbParam>() {
                            new DbParam() { Name = "@ImportDate", Value =importDate },
                            new DbParam() { Name = "@EmployeeSocialSecurityNumber", Value = file.RijksregisterNummer },
                            new DbParam() { Name = "@EmploymentStartDate", Value = file.EmploymentStartDate },
                            new DbParam() { Name = "@EmploymentEndDate", Value = file.EmploymentEndDate },
                            new DbParam() { Name = "@EmployeeHrmNumberCurrent", Value = file.EmployeeHrmNumberCurrent },
                            new DbParam() { Name = "@ServiceReceiverCostCenterNumber", Value = file.ServiceReceiverCostCenterNumber },
                            new DbParam() { Name = "@DienstCode", Value = file.DienstCode },
                            new DbParam() { Name = "@dienstCodeStart", Value = file.ContractCostCenterStartDate },
                            new DbParam() { Name = "@dienstCodeEnd", Value = file.ContractCostCenterEndDate },
                            new DbParam() { Name = "@SequenceNumber", Value = file.SequenceNumber }
                        };

                        commandList.Add(new DbCommand(DbCommandType.crud, sql, parameters.ToArray()));
                    }
                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }
            return Task.FromResult(ws);
        }
        public Task<WebserviceExternal> Save_Acerta_Personen_ToDatabase(WebserviceExternal ws, List<AcertaPersoneel> files)
        {
            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    db.BeginTransaction();
                    List<DbCommand> commandList = new List<DbCommand>();
                    DateTime importDate = DateTime.Now;
                    // DELETE ALL ENTRIES FROM TEMP TABLE
                    string sqlDeleteAll = @"DELETE FROM webservices.ImportAcertaPersoneel; DBCC CHECKIDENT('webservices.ImportAcertaPersoneel', RESEED, 0);";
                    db.Excecute(new DbCommand(DbCommandType.crud, sqlDeleteAll));

                    // INSERT ALL ROWS FROM ECQARE
                    foreach (AcertaPersoneel file in files)
                    {

                        string sql = @"INSERT INTO webservices.ImportAcertaPersoneel (
                    ImportDate,
                    FirstName,
                    LastName,
                    BirthDate,
                    PlaceOfBirthName,
                    CountryOfBirthName,
                    GenderCode,
                    NationalityCode,
                    SocialSecurityNumber,
                    HrmNumber,
                    OfficialLanguageCode,
                    SpokenLanguageCode,
                    CompanyOrganization,
                    IamUserCode,
                    ManagerManualRightsIndicator,
                    RegistrationUserCode,
                    RegistrationTimestamp,
                    MutationUserCode,
                    MutationTimestamp,
                    EmailAdres
                ) VALUES (
                    @ImportDate,
                    @firstName,
                    @lastName,
                    @birthDate,
                    @placeOfBirthName,
                    @countryOfBirthName,
                    @genderCode,
                    @nationalityCode,
                    @socialSecurityNumber,
                    @HrmNumber,
                    @officialLanguageCode,
                    @spokenLanguageCode,
                    @companyOrganization,
                    @iamUserCode,
                    @managerManualRightsIndicator,
                    @registrationUserCode,
                    @registrationTimestamp,
                    @mutationUserCode,
                    @mutationTimestamp,
                    @EmailAdres
                )";

                        List<DbParam> parameters = new List<DbParam>()
{
    new DbParam() { Name = "@ImportDate", Value =importDate },
    new DbParam() { Name = "@firstName", Value = file.FirstName },
    new DbParam() { Name = "@lastName", Value = file.LastName },
    new DbParam() { Name = "@birthDate", Value = file.BirthDate },
    new DbParam() { Name = "@placeOfBirthName", Value = file.PlaceOfBirthName },
    new DbParam() { Name = "@countryOfBirthName", Value = file.CountryOfBirthName },
    new DbParam() { Name = "@genderCode", Value = file.GenderCode },
    new DbParam() { Name = "@nationalityCode", Value = file.NationalityCode },
    new DbParam() { Name = "@socialSecurityNumber", Value = file.SocialSecurityNumber },
    new DbParam() { Name = "@HrmNumber", Value = file.HrmNumber },
    new DbParam() { Name = "@officialLanguageCode", Value = file.OfficialLanguageCode },
    new DbParam() { Name = "@spokenLanguageCode", Value = file.SpokenLanguageCode },
    new DbParam() { Name = "@companyOrganization", Value = file.CompanyOrganization },
    new DbParam() { Name = "@iamUserCode", Value = file.IamUserCode },
    new DbParam() { Name = "@managerManualRightsIndicator", Value = file.ManagerManualRightsIndicator },
    new DbParam() { Name = "@registrationUserCode", Value = file.RegistrationUserCode },
    new DbParam() { Name = "@registrationTimestamp", Value = file.RegistrationTimestamp },
    new DbParam() { Name = "@mutationUserCode", Value = file.MutationUserCode },
    new DbParam() { Name = "@mutationTimestamp", Value = file.MutationTimestamp },
        new DbParam() { Name = "@EmailAdres", Value = file.Email }
};


                        commandList.Add(new DbCommand(DbCommandType.crud, sql, parameters.ToArray()));
                    }
                    db.Excecute(commandList);
                    db.CommitTransaction();
                }
                catch
                {
                    db.RollbackTransaction();
                    throw;
                }
                finally
                {
                    db._sqlConnection.Close();
                }
            }

            return Task.FromResult(ws);
        }
        public Task<WebserviceExternal> StartDatabaseParser(WebserviceExternal ws)
        {

            //call database procedure Webservice_Database_Parser(ws.id);
            return Task.FromResult(ws);
        }
        public async Task UpdateFromAcerta()
        {
            using (SqlConnection conn = new SqlConnection(_connection.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[updateFromAcerta]", conn);
               
                conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        public async Task UpdateFromECQCARE()
        {
            using (SqlConnection conn = new SqlConnection(_connection.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[updateFromECQare]", conn);

                conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
        public List<WebserviceExternal> Get()
        {
            String sql = _sqlManager.GetSQL("webservices.sql");

            try
            {
                _connection.Open();
                var kilometers = _connection.Query<WebserviceExternalDao>(sql);
                return _mapper.Map<List<WebserviceExternal>>(kilometers);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _connection.Close();
            }


        }

        public async Task SendMail(string recipients, string subject, string emailbody)
        {
            using (SqlConnection conn = new SqlConnection(_connection.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("[sendMail]", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.Add(new SqlParameter("@recipients", SqlDbType.NVarChar, 4000) { Value = recipients });
                cmd.Parameters.Add(new SqlParameter("@subject", SqlDbType.NVarChar, 512) { Value = subject });
                cmd.Parameters.Add(new SqlParameter("@body", SqlDbType.Text) { Value = emailbody });

                cmd.ExecuteNonQuery();
            }
        }
    }
}
