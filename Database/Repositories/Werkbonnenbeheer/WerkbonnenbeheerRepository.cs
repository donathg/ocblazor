﻿using AutoMapper;
using Dapper;
using Database.DataAccessObjects;
using Database.Sql;
using DataBase.Services;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Logic.Settings;
using Logic.Werkbonnenbeheer;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Database.Repositories.Werkbonnenbeheer
{
    public class WerkbonnenbeheerRepository : IWerkbonnenbeheerRepository
    {

        private readonly IDbConnection _connection;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;
        private readonly ISQLManager _sqlManager;
        public WerkbonnenbeheerRepository(IDbConnection connection, IOptions<AppSettings> appSettings, IUserService userService, IMapper mapper, ISQLManager sqlManager)
        {
            (_connection, _userService, _appSettings, _mapper, _sqlManager) = (connection, userService, appSettings.Value, mapper, sqlManager);
            _connection.ConnectionString = _appSettings.ConnectionString;
        }
        public List<Werkbon> GetActiveWerkbonnen()
        {
            try
            {
                String sql = "select wo_id, wo_titel from werkopdracht";// _sqlManager.GetSQL("Artikelen.sql");
                List<WerkbonDao> daos = _connection.Query<WerkbonDao>(sql).ToList();


                return _mapper.Map<List<Werkbon>>(daos).ToList();

            }
            catch (Exception ex)
            {
            }
            return new List<Werkbon>();
        }
    }
}
