
drop table [aankoop].[incontinentiemateriaal]
CREATE TABLE [aankoop].[incontinentiemateriaal](
 
  Id int identity(1,1) primary key,
  BewonerId int not null,
  ArtikelId int not null,
  ArtikelPrijs Decimal(10,2) not null,
  Aantal    int not null,
  LeefgroepInfo varchar(1024) not null,
  ArtikelInfo varchar(1024) not null,
  CreationDate Date not null,
  Creator int not null,
  Closer int null,
  ClosedDate Date null,
 )  

 ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (BewonerId) REFERENCES bewoner(id); 

ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (ArtikelId) REFERENCES artikelen(id); 


ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (BewonerId) REFERENCES bewoner(id); 

ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (ArtikelId) REFERENCES artikelen(id); 

create index ix_aankoopim_closeddate on aankoop.incontinentiemateriaal (ClosedDate);

insert into module values ('AANKOOPIM','Aankoop Incontinentiemateriaal')
insert into rechten_functie values ('800','Administratie',null,'administratie','GLOBAL');
insert into rechten_functie values ('810','Aankoop IM App',null,'Aankoop Incontinentiemateriaal App','AANKOOPIM');
insert into rechten_functie values ('820','Aankoop IM afsluit',null,'Aankoop Incontinentiemateriaal afsluit','AANKOOPIM');


drop table [aankoop].[afleverplaats]
CREATE TABLE [aankoop].[afleverplaats](
 
 locatieId integer not null,
 LeefgroepId integer not null,
 Prioriteit integer not null default 1

 )  

 create unique index ix_aankoop_afleverplaats on [aankoop].[afleverplaats] (locatieId,LeefgroepId);

 
 ALTER TABLE [aankoop].[afleverplaats]
ADD FOREIGN KEY (locatieId) REFERENCES locatie(id); 

ALTER TABLE [aankoop].[afleverplaats]
ADD FOREIGN KEY (LeefgroepId) REFERENCES leefgroep(id); 


insert into [aankoop].[afleverplaats] values (588,	98,1)
insert into [aankoop].[afleverplaats] values (588,	99,1)
insert into [aankoop].[afleverplaats] values (588,	112,1)
insert into [aankoop].[afleverplaats] values (98	,91,2)
insert into [aankoop].[afleverplaats] values (98	,96,2)
insert into [aankoop].[afleverplaats] values (98	,138,1)
insert into [aankoop].[afleverplaats] values (98	,100,1)
insert into [aankoop].[afleverplaats] values (98	,102,2)
insert into [aankoop].[afleverplaats] values (98	,103,2)
insert into [aankoop].[afleverplaats] values (98	,104,2)
insert into [aankoop].[afleverplaats] values (98	,105,2)
insert into [aankoop].[afleverplaats] values (98	,107,2)
insert into [aankoop].[afleverplaats] values (98	,108,2)
insert into [aankoop].[afleverplaats] values (98	,134,2)
insert into [aankoop].[afleverplaats] values (98	,115,1)
insert into [aankoop].[afleverplaats] values (98	,116,1)
insert into [aankoop].[afleverplaats] values (98	,117,1)
insert into [aankoop].[afleverplaats] values (98	,118,1)
insert into [aankoop].[afleverplaats] values (98	,119,1)
insert into [aankoop].[afleverplaats] values (98	,120,1)
insert into [aankoop].[afleverplaats] values (98	,121,1)
insert into [aankoop].[afleverplaats] values (98	,122,1)
insert into [aankoop].[afleverplaats] values (98	,128,1)
insert into [aankoop].[afleverplaats] values (98	,129,1)
insert into [aankoop].[afleverplaats] values (98	,133,1)
insert into [aankoop].[afleverplaats] values (98	,130,2)
insert into [aankoop].[afleverplaats] values (98	,131,1)
insert into [aankoop].[afleverplaats] values (98	,132,1)
insert into [aankoop].[afleverplaats] values (98	,137,1)
insert into [aankoop].[afleverplaats] values (496,	95,1)
insert into [aankoop].[afleverplaats] values (496,	97,1)
insert into [aankoop].[afleverplaats] values (496,	101,2)
insert into [aankoop].[afleverplaats] values (496,	135,2)
insert into [aankoop].[afleverplaats] values (496,	136,2)
insert into [aankoop].[afleverplaats] values (496,	109,2)
insert into [aankoop].[afleverplaats] values (496,	110,1)
insert into [aankoop].[afleverplaats] values (496,	111,1)
insert into [aankoop].[afleverplaats] values (496,	113,1)
insert into [aankoop].[afleverplaats] values (496,	114,1)
insert into [aankoop].[afleverplaats] values (496,	123,1)
insert into [aankoop].[afleverplaats] values (496,	124,1)
insert into [aankoop].[afleverplaats] values (496,	125,1)
insert into [aankoop].[afleverplaats] values (496,	126,1)
insert into [aankoop].[afleverplaats] values (496,	127,1)
insert into [aankoop].[afleverplaats] values (608,	92,2)
insert into [aankoop].[afleverplaats] values (608,	93,1)
insert into [aankoop].[afleverplaats] values (608,	94,1)
insert into [aankoop].[afleverplaats] values (608,	106,2)

USE [ClaraFeyLocalDB]
GO
/****** Object:  UserDefinedFunction [dbo].[getAfleverplaatsBestelling]    Script Date: 15/05/2020 8:02:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[getAfleverplaatsBestelling](@bewonerId integer)
RETURNS varchar(500)
BEGIN
    DECLARE @r VARCHAR(500)

select @r =  locatie.naam from aankoop.afleverplaats, bewonerleefgroep,locatie
where bewonerleefgroep.leefgroepid =  aankoop.afleverplaats.leefgroepId
and locatie.id = afleverplaats.locatieid
and bewonerid=@bewonerId order by aankoop.afleverplaats.Prioriteit OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
RETURN @r
END

USE [ClaraFeyLocalDB]
GO

/****** Object:  UserDefinedFunction [dbo].[getAfleverplaatsBestelling]    Script Date: 19/05/2020 7:55:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getAfleverplaatsLeefgroepBestelling](@bewonerId integer)
RETURNS varchar(500)
BEGIN
    DECLARE @r VARCHAR(500)

select @r =  leefgroep.naam from aankoop.afleverplaats, bewonerleefgroep,locatie,leefgroep
where bewonerleefgroep.leefgroepid =  aankoop.afleverplaats.leefgroepId and leefgroep.id = aankoop.afleverplaats.leefgroepId
and locatie.id = afleverplaats.locatieid
and bewonerid=@bewonerId order by aankoop.afleverplaats.Prioriteit OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
RETURN @r
END
GO

select bewonerid,afleverplaats.locatieid, count(*) from aankoop.afleverplaats, bewonerleefgroep
where bewonerleefgroep.leefgroepid =  aankoop.afleverplaats.leefgroepId
and aankoop.afleverplaats.leefgroepid not in (130,96,135,136,91,102,103,104,105,106, 107,108,107,108,101,109,134)
group by bewonerid,afleverplaats.locatieid having count(*)>1  

 

select * from bewoner,bewonerleefgroep where id = 536 
and bewonerleefgroep.bewonerid = bewoner.id 
and leefgroepid not in (130,96,135,136,91,102,103,104,105,106, 107,108,107,108,101,109,134)


--NEW LEEFGROEP

alter TABLE [aankoop].[incontinentiemateriaal] ALTER COLUMN   bewonerId int null
alter TABLE [aankoop].[incontinentiemateriaal] ADD    leefgroepId int null

ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (bewonerId) REFERENCES bewoner(id); 

ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (leefgroepId) REFERENCES leefgroep(id); 

 create  index ix_aankoop_bewoner on [aankoop].[incontinentiemateriaal] (bewonerId);
  create  index ix_aankoop_leefgroep on [aankoop].[incontinentiemateriaal] (leefgroepId);
