﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Sql
{
    public interface ISQLManager
    {
        public string CacheKey => "SQLStatements";
        string GetSQL(string filename);
     
    }

}
