﻿using Database.Sql;
using Logic.Cache;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Database.Sql
{
    public class SQLManager : ISQLManager, ICaching
    {
        public string CacheKey => "SQLStatements";

        private Dictionary<String, String> SQLStatements = new Dictionary<string, string>();


        public SQLManager()
        {
            FillCache();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename">"Locations.sql"</param>
        /// <returns></returns>

        public string GetSQL(string filename)
        {
            if (SQLStatements.ContainsKey(filename))
                return SQLStatements[filename];
            var sql = SQLStatements.Keys.Where(x => x.Contains("." + filename)).FirstOrDefault();
            return SQLStatements[sql];
        }

        public void ClearCache()
        {
            SQLStatements.Clear();
        }

        public void FillCache()
        {
            Log.Information("Loading sqlmanager-cache");

            ClearCache();
            var resources = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            foreach (string resourceName in resources.Where(x => x.Contains(".sql")))
            {
                String[] spl = resourceName.Split('.');
                String fileName = spl[spl.Length - 2] + "." + spl[spl.Length - 1];

                if (!SQLStatements.ContainsKey(fileName))
                {
                    String sql = GetString(Assembly.GetExecutingAssembly(), resourceName);
                    SQLStatements.Add(fileName, sql);
                }
            }
            //_memoryCache.Set(CacheKey, SQLStatements);
        }



        private StreamReader GetStream(System.Reflection.Assembly assembly, string name)
        {
            foreach (string resName in assembly.GetManifestResourceNames())
            {
                if (resName.EndsWith(name))
                {
                    return new System.IO.StreamReader(assembly.GetManifestResourceStream(resName));
                }
            }
            return null;
        }

        private string GetString(System.Reflection.Assembly assembly, string name)
        {
            System.IO.StreamReader sr = EmbeddedResource.GetStream(assembly, name);
            string data = sr.ReadToEnd();
            sr.Close();
            return data;
        }
    }
    public class EmbeddedResource
    {
        private EmbeddedResource()
        {
        }

        public static StreamReader GetStream(System.Reflection.Assembly assembly, string name)
        {
            foreach (string resName in assembly.GetManifestResourceNames())
            {
                if (resName.EndsWith(name))
                {
                    return new System.IO.StreamReader(assembly.GetManifestResourceStream(resName));
                }
            }
            return null;
        }

        public static string GetString(System.Reflection.Assembly assembly, string name)
        {
            System.IO.StreamReader sr = EmbeddedResource.GetStream(assembly, name);
            string data = sr.ReadToEnd();
            sr.Close();
            return data;
        }

        public static string GetString(string name)
        {
            return EmbeddedResource.GetString(typeof(EmbeddedResource).Assembly, name);
        }
    }

}
