﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Logic.ArtikelNs;
using Logic.Clienten;
using Logic.DataBusinessObjects.Aankoop;
using System.Collections.ObjectModel;
using Logic.LeefgroepNS;

namespace Logic.AankoopNs
{
    public class AankoopService : IAankoopService
    {
        private readonly IArtikelenService _artikelService;
        private readonly IClientService _clientService;
        private readonly IAankoopRepository _aankoopRepository;
        private readonly ILeefgroepService _leefgroepService;
        public ObservableCollection<EntiteitAankoop> ClientAankoopList { get; set; }
        public AankoopService(IAankoopRepository aankoopRepository, IArtikelenService artikelenService, IClientService clientService, ILeefgroepService leefgroepService)
        {
            (_artikelService, _clientService, _aankoopRepository, _leefgroepService) = (artikelenService, clientService, aankoopRepository, leefgroepService);
           
        }
        public void Load (int artikelCategorieId)
        {
            if (ClientAankoopList != null)
            {
                ClientAankoopList.Clear();
            }
            ClientAankoopList = Get(artikelCategorieId);
        }
        public void AddClient(Client client)
        {
            EntiteitAankoop aankoop = ClientAankoopList.Where(x => x.Client.Id == client.Id).FirstOrDefault();
            if (aankoop == null)
            {
                 aankoop = new EntiteitAankoop();
                aankoop.Client = client;
                ClientAankoopList.Add(aankoop);
            }
            aankoop.Leefgroep = new Leefgroep();
        }


        public void AddLeefgroep(Leefgroep leefgroep)
        {
            EntiteitAankoop aankoop = ClientAankoopList.Where(x => x.Leefgroep.Id == leefgroep.Id).FirstOrDefault();
            if (aankoop == null)
            {
                aankoop = new EntiteitAankoop();
                aankoop.Leefgroep = leefgroep;
                ClientAankoopList.Add(aankoop);
            }
            aankoop.Client = new Client();
        }

        public void UpdateArtikel(int databaseId, int aantal)
        {
           _aankoopRepository.UpdateArtikel(databaseId, aantal);
        }
        public void DeleteArtikel(Client client, int artikelId)
        {
            
            EntiteitAankoop clientAankoop = ClientAankoopList.Where(x => x.Client.Id == client.Id).FirstOrDefault();
            if (clientAankoop != null)
            {
      
                AankoopArtikel aa = clientAankoop.Artikelen.Where(x => x.ArtikelId == artikelId).FirstOrDefault();
                _aankoopRepository.DeleteArtikel(aa.DatabaseId);
                clientAankoop.Artikelen.Remove(aa);

            }
        }
        public void DeleteArtikel(Leefgroep leefgroep, int artikelId)
        {

            EntiteitAankoop clientAankoop = ClientAankoopList.Where(x => x.Leefgroep.Id == leefgroep.Id).FirstOrDefault();
            if (clientAankoop != null)
            {

                AankoopArtikel aa = clientAankoop.Artikelen.Where(x => x.ArtikelId == artikelId).FirstOrDefault();
                _aankoopRepository.DeleteArtikel(aa.DatabaseId);
                clientAankoop.Artikelen.Remove(aa);

            }
        }
     
        public void AddArtikel(Client client, Artikel artikel)
        {
            EntiteitAankoop clientAankoop =  ClientAankoopList.Where(x => x.Client.Id == client.Id).FirstOrDefault();
            try
            {
                AankoopArtikel aankoopArtikel = new AankoopArtikel(artikel);
                if (!clientAankoop.Artikelen.Any(x => x.ArtikelId == artikel.ArtikelId))
                {


                    _aankoopRepository.InsertClient(client, aankoopArtikel);
                    clientAankoop.Artikelen.Add(aankoopArtikel);
                }
            }
            finally
            {
            }
        }
        public void AddArtikel(Leefgroep leefgroep, Artikel artikel)
        {
            EntiteitAankoop clientAankoop = ClientAankoopList.Where(x => x.Leefgroep.Id == leefgroep.Id).FirstOrDefault();
            try
            {
                AankoopArtikel aankoopArtikel = new AankoopArtikel(artikel);
                if (!clientAankoop.Artikelen.Any(x => x.ArtikelId == artikel.ArtikelId))
                {


                     _aankoopRepository.InsertLeefgroep(leefgroep, aankoopArtikel);
                     clientAankoop.Artikelen.Add(aankoopArtikel);
                }
            }
            finally
            {
            }
        }
        public ObservableCollection<EntiteitAankoop> Get(int artikelCategorieId)
        {
            ObservableCollection<EntiteitAankoop> list = new ObservableCollection<EntiteitAankoop>(_aankoopRepository.Get(artikelCategorieId));
            foreach (EntiteitAankoop ca in list)
            {
                if (ca.IsClient)
                    ca.Client = _clientService.GetClient(ca.Client.Id);
                if (ca.IsLeefgrpep)
                    ca.Leefgroep = _leefgroepService.GetLeefgroepFromId(ca.Leefgroep.Id);
                foreach (AankoopArtikel aa in ca.Artikelen)
                {
                    int aantal = aa.Aantal;
                    Artikel artikel = _artikelService.GetArtikelById(aa.ArtikelId);
                    aa.ArtikelNr = artikel.ArtikelNr;
                    aa.KleurCode = artikel.KleurCode;
                    aa.Maat = artikel.Maat;
                    aa.Name = artikel.Name;
                    aa.Prijs = artikel.Prijs;
                    aa.StuksPerVerpakking = artikel.StuksPerVerpakking;
                    aa.KleurCode = artikel.KleurCode;
                    aa.ArtikelCategorieId = artikel.ArtikelCategorieId;
                    aa.ArtikelCategorieNaam = artikel.ArtikelCategorieNaam;
                }
            }
            return list;
        }
        //public List<Client> GetClienten()
        //{
        //    return  _clientService.GetClientenAndLeefgroepen();
        //}
        //public List<Artikel> GetArtikelen(int artikelCategorieId)
        //{
        //    return  _artikelService.GetArtikelen(artikelCategorieId);
        //}

       
    }
}
