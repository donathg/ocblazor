﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Aankoop;

namespace Logic.ArtikelNs
{
    public interface IAankoopRepository
    {

        List<EntiteitAankoop> Get(int artikelCategorieId);
        void InsertClient(Client client, AankoopArtikel artikel);
        void InsertLeefgroep(Leefgroep leefgroep, AankoopArtikel artikel);
        void Delete(int aankoopArtikelId);
        void UpdateArtikel(int databaseId, int aantal);

        void DeleteArtikel(int databaseId);


    }
}
