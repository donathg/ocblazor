﻿ 
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Aankoop;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.ArtikelNs
{
    public interface IAankoopService
    {

        ObservableCollection<EntiteitAankoop> Get(int artikelCategorieId);
        void AddClient(Client client);

        void AddLeefgroep(Leefgroep leefgroep);
        ObservableCollection<EntiteitAankoop> ClientAankoopList { get; }

       void AddArtikel(Client client, Artikel artikel);

        void AddArtikel(Leefgroep leefgroep, Artikel artikel);

        void UpdateArtikel(int databaseId, int aantal);

        void DeleteArtikel(Client client, int artikelId);

        void DeleteArtikel(Leefgroep leefgroep, int artikelId);
        void Load(int artikelCategorieId);



    }
}
