﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
//using Serilog;
using Common;
using Common.CloneExtensions;

namespace Logic.ArtikelNs
{
    public class ArtikelenService : IArtikelenService, ICaching
    {
        IArtikelRepository _artikelRepository;

        ICache _cache;
        public ArtikelenService( IArtikelRepository artikelRepository, ICache cache)
        {
 
            _artikelRepository = artikelRepository;
            _cache = cache;
      
        }
        public string CacheKey => CacheKeys.Artikelen;
        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        public void FillCache()
        {
        //    Log.Information("Loading artikel-cache");

            List<Artikel> artikelen = _artikelRepository.GetArtikelen(true);
            _cache.Set<List<Artikel>>(CacheKey, artikelen);
        }
        private List<Artikel> GetSetFromCache()
        {

            List<Artikel> lfgsCache = _cache.Get<List<Artikel>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Artikel>>(CacheKey);
        }
        public string GetArtikelCategoryDescription(int artikelCategorieID)
        {
            switch (artikelCategorieID)
            {

                case 1: return "Incontinentiemateriaal";
                case 2: return "Materiaal"; 
                case 3: return "Activiteit";
                   

            }
            return "?";
        }
        public List<Artikel> GetArtikelen(int artikelCategorieID)
        {
            return GetSetFromCache().Where(x => x.ArtikelCategorieId == artikelCategorieID).DeepClone().ToList(); ;
        }
        public Artikel GetArtikelById(int id)
        {
            return GetSetFromCache().Where(x=>x.ArtikelId == id).FirstOrDefault().DeepClone();
        }












    }
}
