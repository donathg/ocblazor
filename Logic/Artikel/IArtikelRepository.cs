﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;

namespace Logic.ArtikelNs
{
    public interface IArtikelRepository
    {
        List<Logic.DataBusinessObjects.Artikel> GetArtikelen(bool aktiv);
 
    }
}
