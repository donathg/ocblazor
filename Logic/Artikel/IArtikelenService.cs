﻿ 
using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.ArtikelNs
{
    public interface IArtikelenService
    {

        List<Artikel> GetArtikelen(int ArtikelCategorie);
        string GetArtikelCategoryDescription(int artikelCategorieID);
        Artikel GetArtikelById(int id);
    }
}
