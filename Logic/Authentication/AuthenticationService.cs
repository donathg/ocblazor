using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataBase.Services;
using Logic.Authentication;
using Logic.DataBusinessObjects;
//using Serilog;

namespace Logic
{

    public class AuthenticationService : IAuthenticationServiceInterface
    {
        IAuthenticationRepository _authenticationRepository;
        public bool LoggedOn { get; set; }
        IUserService _userService;
        public AuthenticationService (IAuthenticationRepository authenticationRepository, IUserService userService)
        {
            _authenticationRepository = authenticationRepository;
            _userService = userService;
        }
     
        public LogonInfoTM LogonInfo { get; set; } = new LogonInfoTM();
       
        public void DoLogOff()
        {
            LogonInfo.LoggedOn = false;
        }
        public async Task<LogonInfoTM> DoLogon(String userName, String Password)
        {
            if (userName == "0000")
                return DoLogonSystem();

            LogonSendTM sendInfo = new LogonSendTM();
            sendInfo.Password = Password;
            sendInfo.UserName = userName;

            try
            {          
                LoggedOnUser tm =  _authenticationRepository.DoLogon(userName, Password);

                LogonInfo.UserId = tm.UserId;
                LogonInfo.Name = tm.Name;
                LoggedOn = true;
                _userService.LoggedOnUser = tm;
                return LogonInfo;
            }
            catch (Exception ex)
            {
                LogonInfo.LoggedOn = false;
                throw;
            }
        }       
        private  LogonInfoTM DoLogonSystem()
        {
            LoggedOnUser tm = new LoggedOnUser
            {
                LogonDate = DateTime.Now,
                LoggedOn = true,
                Name = "System",
                UserId = 0
            };
            LogonInfo.UserId = 0;
            LogonInfo.Name = "System";
            _userService.LoggedOnUser = tm;
            LoggedOn = true;
         //   Log.Information($"DoLogonExternal System  LoggedOn = true");

            return LogonInfo;       
        }
        public async Task<LogonInfoTM> DoLogonExternal( String userName)
        {
            if (userName == "0000")
                return DoLogonSystem();

            LogonSendTM sendInfo = new LogonSendTM
            {
                UserName = userName
            };

            try
            {
             //   Log.Information($"DoLogonExternal {userName}");

                LoggedOnUser tm = _authenticationRepository.DoLogonExternal(userName);

                LogonInfo.UserId = tm.UserId;
                LogonInfo.Name = tm.Name;
                _userService.LoggedOnUser = tm;
                LoggedOn = true;
              //  Log.Information($"DoLogonExternal LoggedOn = true");

                return LogonInfo;
            }
            catch (Exception ex)
            {

              //  Log.Information($"DoLogonExternal Exception {ex.Message}");
                LogonInfo.LoggedOn = false;
                throw;

            }
        }
    }
}
