﻿
 
using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
 

namespace Logic
{
    public interface IAuthenticationServiceInterface 
    {
 
 
        Task<LogonInfoTM> DoLogon(   String userName, String Password);
        Task<LogonInfoTM> DoLogonExternal( String userName);
        bool LoggedOn { get; set; }
        void DoLogOff();
        
    }
}
