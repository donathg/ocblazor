﻿using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Authentication
{
    public interface IAuthenticationRepository
    {
        LoggedOnUser DoLogon(string userName, string password);
        LoggedOnUser DoLogonExternal(String userName);
        Gebruiker GetUserBycode(String code);
    
    }
}
