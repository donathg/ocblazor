﻿
using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataBase.Services
{

    public interface IUserService
    {

        LoggedOnUser LoggedOnUser { get; set; }
    }
    public class UserService : IUserService
    {
        public LoggedOnUser LoggedOnUser { get; set; } = new LoggedOnUser();
        public String DatabaseAliasName { get; set; }
        public String ConnectionString 
        { 
            get 
            { 
                return  GetConnectionString(DatabaseAliasName);
            }
        }
        public int LoggedOnUserId { get; set; } 

        private DataBaseSettings GetDataBaseSettings(String databaseAliasName)
        {
            if (databaseAliasName == "LOCAL")
            {
                return new DataBaseSettings()
                {
                    Server = @"(localdb)\MyLocalDB",
                    Schema = "ClaraFeyLocalDB",
                   // User = "ClaraFeyApp",
                   // Password = "Brecht2960$",
                    IsLocalHost = true,
                    IntegratedSecurity = "true"
                };
            }

            if (databaseAliasName == "DEV")
            {
                return new DataBaseSettings()
                {
                    Server = @"10.118.0.10",
                    Schema = "ClaraFey_DEV",
                    User = "ClaraFeyApp",
                    Password = "Brecht2960$",
                    IsLocalHost = false,
                    IntegratedSecurity = "False"

                };
            }

            if (databaseAliasName == "PROD")
            {
                return new DataBaseSettings()
                {
                    Server = @"10.118.0.10",
                    Schema = "ClaraFey",
                    User = "ClaraFeyApp",
                    Password = "Brecht2960$",
                    IsLocalHost = false,
                    IntegratedSecurity = "False"
                };
            }
            return null;
        }
        private string GetConnectionString(String databaseAliasName)
        {
            DataBaseSettings settings = GetDataBaseSettings(databaseAliasName);
            if (settings != null)
            {
                if (settings.IsLocalHost)
                    return $"Data Source = {settings.Server}; Initial Catalog = {settings.Schema}; Integrated Security = {settings.IntegratedSecurity}";
                else
                    return $"Data Source = {settings.Server}; Initial Catalog = {settings.Schema}; Integrated Security = {settings.IntegratedSecurity};User Id = {settings.User}; Password = {settings.Password}";
            }
            throw new Exception("no connectionstring found for " + databaseAliasName);
        }
    }
}
