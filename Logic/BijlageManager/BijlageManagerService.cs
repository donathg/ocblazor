﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Logic.DataBusinessObjects.BijlageManager;
using System.IO;
using Logic.Modules;

namespace Logic.BijlageManagerServiceNS
{

    
    public class BijlageManagerService : IBijlageManagerService
    {
        IBijlageManagerRepository _bijlageManagerRepository;

        ICache _cache;
        public BijlageManagerService(IBijlageManagerRepository bijlageManagerRepository, ICache cache)
        {
 
            _bijlageManagerRepository = bijlageManagerRepository;
            _cache = cache;
      
        }
        public void DeleteBijlage(ModuleType module, int bijlageId)
        {
            if (module ==  ModuleType.WERKOPDRACHT)
                _bijlageManagerRepository.DeleteWerkopdrachtFile(bijlageId);
            else if (module ==  ModuleType.INVENTARIS)
                _bijlageManagerRepository.DeleteInventarisFile(bijlageId);
            else if (module == ModuleType.GOG)
                _bijlageManagerRepository.DeleteGogFile(bijlageId);
            else if (module == ModuleType.ONKOSTENNOTA)
                _bijlageManagerRepository.DeleteOnkostennotaFile(bijlageId);
        }
        public Bijlage InsertFile(String fileName, Byte[] data, ModuleType module, int modulePrimaryKey, string description)
        {
            Bijlage bijlage = new Bijlage
            {
                FileName = Path.GetFileNameWithoutExtension(fileName),
                FileExtention = Path.GetExtension(fileName).Replace('.', ' ').Trim(),
                Description = description,
                FileData = data,
                ForeignKeyId = modulePrimaryKey
            };

            if (module == ModuleType.WERKOPDRACHT)
            {
                bijlage.Category = "WERKAANVRAAG";
                return _bijlageManagerRepository.AddWerkopdrachtFile(bijlage);
            }
            else if (module == ModuleType.INVENTARIS)
            {
                bijlage.Category = "INVENTARIS";
                return _bijlageManagerRepository.AddInventarisFile(bijlage);
            }
            else if (module == ModuleType.GOG)
            {
                bijlage.Category = "GOG";
                return _bijlageManagerRepository.AddGogFile(bijlage);
            }
            else if (module == ModuleType.ONKOSTENNOTA)
            {
                bijlage.Category = "ONKOSTENNOTA";
                return _bijlageManagerRepository.AddOnkostenNotaFile(bijlage);
            }
            return new Bijlage();
        }
        public List<Bijlage> GetBijlages(ModuleType module, int id)
        {
            if (module ==  ModuleType.WERKOPDRACHT)
                return _bijlageManagerRepository.GetWerkbonnenbeheerBijlages(id);
            else if (module == ModuleType.INVENTARIS)
                return _bijlageManagerRepository.GetInventarisBijlages(id);
            else if (module == ModuleType.GOG)
                return _bijlageManagerRepository.GetGOGBijlages(id);
            else if (module == ModuleType.ONKOSTENNOTA)
                return _bijlageManagerRepository.GetOnkostenNotaFiles(id);
            return new List<Bijlage>();
        }
    }
}
