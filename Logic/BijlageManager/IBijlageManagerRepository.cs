﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.BijlageManager;

namespace Logic.BijlageManagerServiceNS
{
    public interface IBijlageManagerRepository
    {
        List<Bijlage> GetWerkbonnenbeheerBijlages(int id);
        List<Bijlage> GetInventarisBijlages(int id);
        List<Bijlage> GetGOGBijlages(int id);
        List<Bijlage> GetOnkostenNotaFiles(int id);
        
        Bijlage AddWerkopdrachtFile(Bijlage bijlage);
        Bijlage AddInventarisFile(Bijlage bijlage);
        Bijlage AddGogFile(Bijlage bijlage);

        Bijlage AddOnkostenNotaFile(Bijlage bijlage);
        
        void DeleteWerkopdrachtFile(int id);  
        void DeleteInventarisFile( int id);
        void DeleteGogFile(int id);
        void DeleteOnkostennotaFile(int id);

        
    }
}
