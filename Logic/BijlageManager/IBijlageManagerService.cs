﻿ 
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.BijlageManager;
using Logic.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.BijlageManagerServiceNS
{
    public interface IBijlageManagerService
    {
  
        List<Bijlage> GetBijlages(ModuleType module, int id);
        
        Bijlage InsertFile(String fileName, Byte[] data, ModuleType module, int modulePrimaryKey, string description);
        void DeleteBijlage(ModuleType module, int bijlageId);
    }
}
