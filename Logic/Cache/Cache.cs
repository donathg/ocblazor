using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Logic.Cache
{




    public class Cache : ICache
    {
        private readonly IMemoryCache _memoryCache;

       // private static NLog.Logger _loggerCache = NLog.LogManager.GetLogger("cache");
        public ConcurrentDictionary<string, CachedObjectInfo> CacheInfos { get; set; } = new ConcurrentDictionary<string, CachedObjectInfo>();

        public Cache(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public void ClearAll()
        {
            try
            {
                foreach (var cacheKey in CacheInfos.Keys)
                {
                    _memoryCache.Remove(cacheKey);
                }
                CacheInfos.Clear();
             //   _loggerCache.Info("Cleared all cache entries.");
            }
            catch (Exception ex)
            {
               // _loggerCache.Error(ex, "Error clearing all cache entries.");
            }
        }
        public void Clear(string cacheKey)
        {

            try
            {
                _memoryCache.Remove(cacheKey);

                bool removed = CacheInfos.TryRemove(cacheKey, out CachedObjectInfo _);

                if (removed)
                {
                   // _loggerCache.Info($"Cache Clear {cacheKey}");
                }
                else
                {
                 //   _loggerCache.Warn($"Cache entry {cacheKey} not found or could not be removed.");
                }
            }
            catch (Exception ex)
            {
             //   _loggerCache.Error(ex, $"Error clearing cache entry {cacheKey}.");
            }

        }

        public T Get<T>(string cacheKey)
        {
            try
            {
                return _memoryCache.TryGetValue(cacheKey, out T value) ? value : default;

            }
            catch (Exception ex)
            {
              //  _loggerCache.Error(ex, $"Error retrieving cache entry {cacheKey}.");
                return default;
            }
        }

        public void Set<T>(string cacheKey, T objectToCache, DateTime absoluteExpirationDate)
        {
            try
            {
                //Clear(cacheKey);
                DateTimeOffset offset = new DateTimeOffset(absoluteExpirationDate);
                SetCacheInfo(cacheKey, new ObjectCachedAbsoluteExpirationDate() { CreationDate = DateTime.Now, ExpirationDateOffset = offset, ExpirationDate = absoluteExpirationDate });
                _memoryCache.Set<T>(cacheKey, objectToCache, new MemoryCacheEntryOptions() { AbsoluteExpiration = offset });
              //  _loggerCache.Info($"Set {cacheKey} - Absolute Expiration Date : {absoluteExpirationDate.ToLongDateString()} ");
            }
            catch (Exception ex)
            {
               // _loggerCache.Error(ex, $"Error setting cache entry {cacheKey}.");
            }
        }
        public void Set<T>(string cacheKey, T objectToCache, TimeSpan relativeExpirationDate)
        {
            try
            {
                //Clear(cacheKey);
                SetCacheInfo(cacheKey, new ObjectCachedRelativeExpirationDate() { CreationDate = DateTime.Now, ExpirationDateTimeSpan = relativeExpirationDate });
                _memoryCache.Set<T>(cacheKey, objectToCache, new MemoryCacheEntryOptions() { AbsoluteExpirationRelativeToNow = relativeExpirationDate });
               // _loggerCache.Info($"Set {cacheKey} - Relative Expiration Date : {relativeExpirationDate.TotalMinutes} minutes ");
            }
            catch (Exception ex)
            {
               // _loggerCache.Error(ex, $"Error setting cache entry {cacheKey}.");
            }
        }

        public void Set<T>(string cacheKey, T objectToCache)
        {
            try
            {
                //Clear(cacheKey);
                SetCacheInfo(cacheKey, new ObjectCachedNoExpirationDate() { CreationDate = DateTime.Now });
                _memoryCache.Set<T>(cacheKey, objectToCache);
               // _loggerCache.Info($"Set {cacheKey}");
            }
            catch (Exception ex)
            {
              //  _loggerCache.Error(ex, $"Error setting cache entry {cacheKey}.");
            }

        }
        public void Set<T>(string cacheKey, T objectToCache, CacheItemPriority priority)
        {
            try
            {
                //Clear(cacheKey);
                SetCacheInfo(cacheKey, new ObjectCachedNoExpirationDate() { CreationDate = DateTime.Now, priority = priority });
                _memoryCache.Set<T>(cacheKey, objectToCache, new MemoryCacheEntryOptions { Priority = priority });
              //  _loggerCache.Info($"Set {cacheKey} with priority {priority.ToString()} ");
            }
            catch (Exception ex)
            {
                //_loggerCache.Error(ex, $"Error setting cache entry {cacheKey}.");
            }
        }

        private void SetCacheInfo(string cacheKey, CachedObjectInfo info)
        {
            CacheInfos.AddOrUpdate(cacheKey, info, (key, existingVal) => info);
        }
        public CachedObjectInfo GetCacheInfo(string cacheKey)
        {
            try
            {
                //Cached memory can Expire
                if (!_memoryCache.TryGetValue(cacheKey, out var memoryCache))
                {
                    Clear(cacheKey);
                }

                if (CacheInfos.TryGetValue(cacheKey, out var cachedObjectInfo))
                {
                    //if (cachedObjectInfo is ObjectCachedAbsoluteExpirationDate exp)
                    //{
                    //    if (exp.IsExpired)
                    //        return new ObjectNotCached();
                    //}
                    //if (cachedObjectInfo is ObjectCachedRelativeExpirationDate rel)
                    //{
                    //    if (rel.IsExpired)
                    //        return new ObjectNotCached();
                    //}
                    return cachedObjectInfo;
                }
                else return new ObjectNotCached();
            }

            catch (Exception ex)
            {
               /// _loggerCache.Error(ex, $"Error retrieving cache info for {cacheKey}.");
                return new ObjectNotCached();
            }
        }
    }
}

