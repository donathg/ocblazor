﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Cache
{
    public static class CacheKeys
    {
        public static String Gebruikers => "gebruikers";
        public static String Leefgroepen => "leefgroepen";
        public static String Artikelen => "artikelen";
        public static String Locaties => "locaties";
        public static String Clienten => "clienten";
        public static String KilometerTypes => "kilometerTypes";
        public static String Wagens => "wagens";
        public static String Kampen => "kampen";
        public static String MultiSelect => "multiselect";
        public static String StandaardPrijzen => "standaardPrijzen";
    }
}
