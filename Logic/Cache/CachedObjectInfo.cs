using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Cache
{
    public abstract class CachedObjectInfo
    {
    }
    public class ObjectNotCached : CachedObjectInfo
    {
    }
    public class ObjectCached : CachedObjectInfo
    {
        public DateTime CreationDate { get; set; }
        public CacheItemPriority priority { get; set; }
    }
    public class ObjectCachedNoExpirationDate : ObjectCached
    {

    }
    public class ObjectCachedAbsoluteExpirationDate : ObjectCached
    {

        public DateTimeOffset ExpirationDateOffset { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
    public class ObjectCachedRelativeExpirationDate : ObjectCached
    {

        public TimeSpan ExpirationDateTimeSpan { get; set; }
    }
}
