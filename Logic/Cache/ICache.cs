using Microsoft.Extensions.Caching.Memory;
using System;

namespace Logic.Cache
{
    public interface ICache
    {
        void ClearAll();
        void Clear(string cacheKey);
        T Get<T>(string cacheKey);
        void Set<T>(string cacheKey, T objectToCache);
        void Set<T>(string cacheKey, T objectToCache, DateTime absoluteExpirationDate);
        void Set<T>(string cacheKey, T objectToCache, TimeSpan relativeExpirationDate);

        void Set<T>(string cacheKey, T objectToCache, CacheItemPriority priority);

        CachedObjectInfo GetCacheInfo(string cacheKey);


    }
}