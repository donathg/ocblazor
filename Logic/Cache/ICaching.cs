﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Cache
{
    public interface ICaching
    {

        void ClearCache();
        void FillCache();
        String CacheKey { get; }

    }
}
