
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Common.CloneExtensions;
using Logic.Cache;
using Logic.DataBusinessObjects;
using Microsoft.Extensions.Caching.Memory;
//using Serilog;

namespace Logic.Clienten
{
    public class ClientService : IClientService, ICaching
    {
        private readonly IClientRepository _clientRepository;
        private readonly ICache _cache;

        public ClientService(IClientRepository clientRepository,ICache cache)
        {
            _clientRepository = clientRepository;
            _cache = cache;
        }
        public string CacheKey => CacheKeys.Clienten;

        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }

        private List<Client> GetSetFromCache()
        {
            List<Client> lfgsCache = _cache.Get<List<Client>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Client>>(CacheKey);
        }
        public  void FillCache()
        {
      //      Log.Information("Loading Client-cache");

            List<Client> lfgs = _clientRepository.GetAllClienten();
            _cache.Set<List<Client>>(CacheKey, lfgs);
        }

        public Client GetClient(int id)
        {
            return GetSetFromCache().Where(x=>x.Id == id).FirstOrDefault().DeepClone();
        }
        public Client GetClient(Guid ecqareId)
        {
            List<Client> tempList = GetSetFromCache();
            return tempList.Where(x => x.EcqareId.ToString() == ecqareId.ToString()).FirstOrDefault().DeepClone();
        }
        public Client GetClientByDossierId(Guid dossierId)
        {
            List<Client> tempList = GetSetFromCache(); 
            return tempList.Where(x => x.DossierId.Equals(dossierId)).FirstOrDefault().DeepClone();
            //return tempList.Where(x => x.DossierId.ToString() == dossierId.ToString()).FirstOrDefault().DeepClone();
        }
        public List<Client> GetClienten()
        {
            return GetSetFromCache().DeepClone();
        }
        public List<Client> GetActiveClienten()
        {
            return GetSetFromCache().Where(x => x.Actif).ToList().DeepClone();
        }
         public List<Client> GetClientenForLeefgroep(int leefgroepId)
         {
            List<Client> clienten = GetSetFromCache().DeepClone();
            List<Client> list = new List<Client>();
            foreach (Client client in clienten)
            {
                foreach (Leefgroep leefgroep in client.Leefgroep)
                {
                    if (leefgroep.Id == leefgroepId && list.Contains(client) == false)
                        list.Add(client);
                }
            }
            return list;
        }
        public List<Client> GetAllClientenForLeefgroepen(List<Leefgroep> leefgroepenList)
        {
            List<Client> clienten = GetSetFromCache().DeepClone();
            List<Client> list = new List<Client>();

            foreach (Client client in clienten)
            {
                foreach (Leefgroep leefgroep in client.Leefgroep)
                {
                    if (leefgroepenList.Any(x => x.Id == leefgroep.Id) && !list.Contains(client))
                        list.Add(client);
                }
            }
            return list;
        }
        public List<Client> GetClientenAndLeefgroepen()
        {
            return GetSetFromCache().DeepClone();
        }

        public Dictionary<int, string> GetAttentionPointsForGebruiker(DateTime startDate, DateTime endDate)
        {
            return _clientRepository.GetAttentionPointsForGebruiker(startDate, endDate);
        }
        public Dictionary<int, string> GetExtrasForGebruiker(DateTime startDate, DateTime endDate)
        {
            return _clientRepository.GetExtrasForGebruiker(startDate, endDate);
        }
    }
}
