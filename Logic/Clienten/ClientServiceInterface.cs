﻿using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.Clienten
{
    public interface IClientService
    {

       Client GetClient(int id);
        Client GetClient(Guid ecqareId);
        Client GetClientByDossierId(Guid dossierId);
        List<Client> GetClienten();
        List<Client> GetActiveClienten();
        List<Client> GetClientenForLeefgroep(int leefgroepId);
        List<Client> GetAllClientenForLeefgroepen(List<Leefgroep> leefgroepenList);
        List<Client> GetClientenAndLeefgroepen();

        Dictionary<int, string> GetAttentionPointsForGebruiker(DateTime startDate, DateTime endDate);
        Dictionary<int, string> GetExtrasForGebruiker(DateTime startDate, DateTime endDate);

    }
   
}
