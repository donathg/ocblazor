﻿using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Clienten
{
    public interface IClientRepository
    {
        List<Client> GetAllClienten();
        Dictionary<int, string> GetAttentionPointsForGebruiker(DateTime startDate, DateTime endDate);
        Dictionary<int, string> GetExtrasForGebruiker(DateTime startDate, DateTime endDate);
    }
}
