﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects.Aankoop
{
    public class AankoopArtikel : Artikel
    {
        public AankoopArtikel()
        {

        }
        public AankoopArtikel(Artikel artikel)
        {
            this.ArtikelNr = artikel.ArtikelNr;
            this.Aantal = 1;
            this.ArtikelId = artikel.ArtikelId;
            this.KleurCode = artikel.KleurCode;
            this.Maat = artikel.Maat;
            this.Name = artikel.Name;
            this.StuksPerVerpakking = artikel.StuksPerVerpakking;
            this.Prijs = artikel.Prijs;
        }


        public int DatabaseId { get; set; }
        public int Aantal { get; set; }

    }
}
