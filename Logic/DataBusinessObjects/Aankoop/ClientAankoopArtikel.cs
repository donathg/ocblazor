﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Logic.DataBusinessObjects.Aankoop
{

      
   
   public class EntiteitAankoop
   {

      

        public bool IsClient { get { return Client != null && Client.Id > 0; } }
        public bool IsLeefgrpep { get { return !IsClient; } }
        public Leefgroep Leefgroep { get; set; } = new Leefgroep();
        public Client Client { get; set; } = new Client();
        public ObservableCollection<AankoopArtikel> Artikelen { get; set; } = new ObservableCollection<AankoopArtikel>();
        public bool Afgesloten { get; set; } = false;

        
    }
}
