﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects
{
    public class Artikel
    {

        public int ArtikelId { get; set; }

        public String Name { get; set; }
        public Double Prijs { get; set; }

        public String Maat { get; set; }

        public String StuksPerVerpakking { get; set; }

        public String ArtikelNr { get; set; }

        public String KleurCode { get; set; }

        public int ArtikelCategorieId { get; set; }
        public String ArtikelCategorieNaam { get; set; }

    }
}
