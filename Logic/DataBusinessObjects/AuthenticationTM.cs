﻿ 
using System;
 

namespace Logic.DataBusinessObjects
{

 
    public class LogonSendTM
    {
   
        public string UserName { get; set; }
 
        public string Password { get; set; }
 
        public string DatabaseAliasName { get; set; }
    }

 
    public class LogonInfoTM
    {
 
        public bool LoggedOn { get; set; }
 
        public String ErrorMessage { get; set; }
 
        public bool NeedNewPassword { get; set; }
  
        public int UserId { get; set; }
    
        public String Name { get; set; }
 
        public String Email { get; set; }
 
        public String AuthorisationToken { get; set; }
 
        public int CompanyId { get; set; }

 
        public Byte[] CompanyLogo { get; set; }
 
        public string DatabaseAliasName { get; set; }

 
        public DateTime? LogonDate { get; set; }

        public LogonInfoTM()
        {

        }
    }
}
