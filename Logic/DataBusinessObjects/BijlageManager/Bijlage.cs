﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects.BijlageManager
{
    public class Bijlage
    {

        public int Id { get; set; }

        /// <summary>
        /// wo_id, inv_id
        /// </summary>
        public int ForeignKeyId { get; set; }

        public String FileName { get; set; }

        public String FileExtention { get; set; }
    public int CreatorId { get; set; }
        public String Creator { get; set; }

        public DateTime Creationdate { get; set; }
        public String Description { get; set; }
  public String Category { get; set; }
        public byte[] FileData { get; set; }
        
    }
}
