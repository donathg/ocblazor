﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects
{
    [Serializable]
    public class Client
    {
        public Client()
        {

        }
        public int Id { get; set; }
        public Guid EcqareId { get; set; }

        public String Code { get; set; }
        public String Rijksregisternr { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String FirstNameLastname => FirstName + " " + LastName;
        public bool Actif { get; set; }
        public string Ondersteuningsvorm { get; set; }

        public DateTime? OpenameEinde { get; set; }
        public List<Leefgroep> Leefgroep { get; set; } = new List<Leefgroep>();

        public String LeefgroepenString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (Leefgroep lfg in Leefgroep)

                    if (lfg.Id == Leefgroep[0].Id)
                        sb.Append(lfg.Name);
                    else
                        sb.Append(", " + lfg.Name);

                return sb.ToString();
            }

        }

        public Guid DossierId { get; set; }

        public override string ToString()
        {
            return FirstNameLastname;
        }
    }
}
