﻿using System;
using System.Collections.Generic;
using System.Text;
using Logic.DataBusinessObjects;

namespace Database.DataAccessObjects
{
    public class KasbeheerTransactieClientInfo
    {
        public int Id { get; set; }
        public String Code { get; set; }
        public String FirstName { get; set; }

        public String LastName { get; set; }
        public String Rijksregisternr { get; set; }
        public Decimal Bedrag
        {
            get;
            set;
        }
    }

    public class KasbeheerTransactie
    {


        
        public int Id { get; set; }
        public KasbeheerRekening Rekening { get; set; } = new KasbeheerRekening();
        public KasbeheerTransactieType TransactieType { get; set; } = new KasbeheerTransactieType();
        public KasbeheerTransactieCodering TransactieCode { get; set; } = new KasbeheerTransactieCodering() { Kode = String.Empty };
        public Leefgroep Leefgroep { get; set; } = new Leefgroep();
        public DateTime TransactieDatum { get; set; } = DateTime.Now;

        public bool Afgerekend { get; set; }
        public List<KasbeheerTransactieClientInfo> Clienten { get; set; } = new List<KasbeheerTransactieClientInfo>();




        public void AddClient(Client c)
        {
            KasbeheerTransactieClientInfo cm = new KasbeheerTransactieClientInfo();
            cm.Code = c.Code;
            cm.Id = c.Id;
            cm.FirstName = c.FirstName;
            cm.LastName = c.LastName;
            cm.Rijksregisternr = c.Rijksregisternr;
            Clienten.Add(cm);
        }


        public Decimal TransactieBedrag { get; set; }
        public String TransactieOmschrijving { get; set; }
        public Gebruiker Modifier { get; set; } = new Gebruiker();
        public void Validate()
        {




            if (this.Rekening == null || this.Rekening.Id == 0)
                throw new Exception("Gelieve een rekening te kiezen");

            if (this.TransactieType == null || this.TransactieType.Id == 0)
                throw new Exception("Gelieve een type te kiezen");

            if (this.TransactieCode == null || String.IsNullOrWhiteSpace(this.TransactieCode.Kode))
                throw new Exception("Gelieve een code uit te kiezen");



            if (this.TransactieBedrag == 0)
                throw new Exception("Gelieve een bedrag in te geven");

            if (String.IsNullOrEmpty(this.TransactieOmschrijving))
                throw new Exception("Gelieve een omschrijving in te geven.");

            if (TransactieType.IsClientBased)
            {
                if (this.Clienten == null || this.Clienten.Count == 0)
                    throw new Exception("Gelieve min. 1 cliënt in te geven.");

                Decimal sum = GetClientenSum();
                if (sum != Math.Abs(this.TransactieBedrag))
                    throw new Exception($"De som van de cliënten moet {Math.Abs(this.TransactieBedrag)} euro zijn.");
            }
        }
        public Decimal GetClientenSum()
        {
            Decimal sum = 0;
            foreach (KasbeheerTransactieClientInfo i in this.Clienten)
            {
                sum = sum + i.Bedrag;

            }
            return sum;
        }
        public Decimal GetClientenAmountLeftToAssign()
        {
            return Math.Abs(TransactieBedrag) - GetClientenSum();
        }

    }
    public class KasbeheerRekening
    {
        public int Id { get; set; }
        public String Name { get; set; }

        public Decimal Bedrag { get; set; }
        public String BedragEuro
        {
            get
            {


                return Bedrag.ToString("N") + "€";
            }
        }


    }

    public class KasbeheerTransactieType
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public bool IsClientBased { get; set; }


    }
    public class KasbeheerTransactieCodering
    {
        public String Kode { get; set; }
        public String Name { get; set; }
        public bool IsClientBased { get; set; }


    }
}







