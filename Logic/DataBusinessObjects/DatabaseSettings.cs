﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects
{
    public class DataBaseSettings
    {
        public String Server { get; set; }
        public String Schema { get; set; }
        public String User { get; set; }
        public String Password { get; set; }
        public bool IsLocalHost { get; set; } //remote of local, belangrijk voor oa email settings
        /// <summary>
        /// True or False
        /// </summary>
        public String IntegratedSecurity { get; set; }

    }
}
