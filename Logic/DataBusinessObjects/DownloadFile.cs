﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects
{
    public class DownloadFilePDF : DownloadFile
    {
        public DownloadFilePDF(string fileName, string extension) : base("application/pdf", fileName, extension)
        {


        }
        public DownloadFilePDF(string fileName, string extension, Byte[] Data) : base("application/pdf", fileName, extension, Data)
        {


        }

    }
    public class DownloadFileExcel : DownloadFile
    {
        public DownloadFileExcel(string fileName, string extension) : base("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName, extension)
        {


        }
        public DownloadFileExcel(string fileName, string extension, Byte[] Data) : base("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName, extension, Data)
        {


        }

    }
    /// <summary>
    /// To be tested
    /// </summary>
    public class DownloadFileCSV : DownloadFile
    {
        public DownloadFileCSV(string fileName, string extension) : base("application/octet-stream", fileName, extension)
        {


        }
        public DownloadFileCSV(string fileName, string extension, Byte[] Data) : base("application/octet-stream", fileName, extension, Data)
        {


        }

    }
    public abstract class DownloadFile
    {
        public String FileName { get; set; }
        public String Extension { get; set; }
        public String ContentType { get; set; }
        public String FileNameExtension { get { return $"{FileName}.{Extension}"; } }

        public Byte[] Data { get; set; }

        public DownloadFile(String contentType)
        {
            this.ContentType = contentType;
        }
        public DownloadFile(String contentType, string fileName, string extension)
        {
            this.ContentType = contentType;
            this.FileName = fileName;
            this.Extension = extension;

        }
        public DownloadFile(String contentType, string fileName, string extension, Byte[] data)
        {
            this.Data = data;
            this.ContentType = contentType;
            this.FileName = fileName;
            this.Extension = extension;

        }
    }
}
