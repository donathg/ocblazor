﻿using Logic.DataBusinessObjects.MultiSelectNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.ExitFormulier
{
    public class ExitFormItem
    {
        public int Id { get; set; }
        public List<MultiSelectItemResult> SelectedMultiSelectItemsResults { get; set; } = new List<MultiSelectItemResult>();
        #region Status
        public int? PreviousStatusId = null;
        private int? _statusId;
        public int? StatusId
        {
            get
            {
                return _statusId;
            }
            set
            {
                if (PreviousStatusId < StatusIdNotNull)
                    PreviousStatusId = _statusId;
                _statusId = value;
            }
        }
        public int StatusIdNotNull => StatusId.GetValueOrDefault();

        public void SetStatusFormValidatedAndCreated()
        {
            if (StatusId.HasValue == false)
            {
                StatusId = 1;
            }
        }
        #endregion
    }
}
