﻿using Common.DateTimeExtensions;
using System;

namespace Logic.DataBusinessObjects.ExternalDashboard
{
    


    public class OrbisDates
    {


      

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


        public OrbisDates()
        {

    }


    }

    public class OrbisListDates : OrbisDates
    {

 


        public OrbisListDates()
        {

            StartDate = DateTime.Now.GetFirstDayOfMonth().Date;
            EndDate = DateTime.Now.GetLastDayOfMonth().Date;

        }
 

    }
    public class OrbisChartDates : OrbisDates
    {


    


        public OrbisChartDates()
        {


            StartDate = DateTime.Now.GetFirstDayOfYear().Date;
            EndDate = DateTime.Now.GetLastDayOfMonth().Date;
        }


    }
}
