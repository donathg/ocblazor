﻿using Force.DeepCloner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.ExternalDashboard
{
    public class ExternalDashboardMedewerkers
    {

        public String GroepCode { get; set; }   
        public int Voltijds { get; set; }
        public int Deeltijds { get; set; }
        public int Aanwerving { get; set; }
        public int UitDienst { get; set; }
        public double Ziekte => Math.Round(ZiekteMinderJaar + ZiekteMeerMaand + ZiekteMeerJaar + ZiekteZonderBriefje, 2);
        public double ZiekteMinderJaar { get; set; }
        public double ZiekteMeerMaand { get; set; }
        public double ZiekteMeerJaar { get; set; }
        public double ZiekteZonderBriefje { get; set; }
        public double Arbeidsongeval { get; set; }
        public int Werkhervatting { get; set; }
        public double VormingsurenExtern { get; set; }
    }
    public class ExternalDashboardMedewerkersGraph 
    {
        public DateTime DayDate { get; set; } = new DateTime();
        public ExternalDashboardMedewerkers ExternalDashboardMedewerkers { get; set; } = new ExternalDashboardMedewerkers();
    }
}
