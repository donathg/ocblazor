﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.GOG
{
    public  class GOGIncidentSpline : INotifyPropertyChanged
    {

        public DateTime IncidentDatum { get; set; } = DateTime.Now;
        public int Id { get; set; }
        public Leefgroep Leefgroep { get; set; } = new Leefgroep();
        
        public int AantalIncidenten { get; set; }      = 0;

        public GOGIncidentSpline() { }

        public event PropertyChangedEventHandler PropertyChanged;

        
    }
}
