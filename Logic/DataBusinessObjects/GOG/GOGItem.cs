﻿using Logic.DataBusinessObjects.MultiSelectNS;
using Org.BouncyCastle.Crypto.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.GOG
{
    public class GOGItem
    {
        public DateTime? DatumIncident { get; set; }
        public int Id { get; set; }
        public Gebruiker Creator { get; set; }
        public DateTime CreationDate { get; set; }
        public int? Modifier { get; set; }
        public string ModifierVoornaamAchternaam { get; set; }
        public DateTime? ModificationDate { get; set; }
        public Leefgroep Leefgroep { get; set; }
        public List<Gebruiker> BetrokkenBegeleiders { get; set; } = new List<Gebruiker>();
        public List<Client> BetrokkenBewoners { get; set; } = new List<Client>();
        public string BetrokkenBegeleidersString
        {
            get
            {
                string tempString = "";
                BetrokkenBegeleiders.ForEach((Gebruiker geb) =>
                {
                    if (!string.IsNullOrWhiteSpace(tempString))
                        tempString += ", ";

                    tempString += geb.Name;
                });

                return tempString;
            }
        }
        public string BetrokkenBewonersString
        {
            get
            {
                string tempString = "";
                if (BetrokkenBewoners.Count == 0 || BetrokkenBewoners[0] == null)
                {
                    return "";
                }
                else
                {
                    BetrokkenBewoners.ForEach((Client cli) =>
                    {
                        if (!string.IsNullOrWhiteSpace(tempString))
                            tempString += ", ";

                        tempString += cli.FirstNameLastname;
                    });
                }

                return tempString;
            }
        }
        public List<MultiSelectItemResult> SelectedMultiSelectItemsResults { get; set; } = new List<MultiSelectItemResult>();
        public int AantalBijlagen { get; set; }
        public String AardVanIncident { get; set; }
        public int? PreviousStatusId = null;
        private int? _statusId;
        public int? StatusId
        {
            get
            {
                return _statusId;
            }
            set
            {
                if (PreviousStatusId < StatusIdNotNull)
                    PreviousStatusId = _statusId;
                _statusId = value;
            }
        }
        public int StatusIdNotNull => StatusId.GetValueOrDefault();  

        public void SetStatusFormValidatedAndCreated()
        {
            if (StatusId.HasValue == false)
            {
                StatusId = 1;
            }
        }
    }
}
