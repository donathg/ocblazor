﻿using Logic.DataBusinessObjects.MultiSelectNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.GOG
{
    public class GogReport
    {
        public DateTime DateTime { get; set; } = DateTime.Now;
        public int Id { get; set; } 
        public Leefgroep Leefgroep { get; set; } = new Leefgroep();
        public string BetrokkenBegeleidersString { get; set; } = "";
        public string BetrokkenClientenString { get; set; } = "";
        public List<GogReportItem> Items { get; set; }  = new List<GogReportItem>();
    }

    public class GogReportItem  
    {
        public String Title { get; set; } = String.Empty;
        public String Result { get; set; } = String.Empty;
    }
}
