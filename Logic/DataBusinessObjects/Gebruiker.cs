﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects
{
    public class Gebruiker
    {
        public Gebruiker()
        {
        }
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string InternalPhoneNumber { get; set; }
        public string InternGroupPhoneNumber { get; set; }
        public string Function { get; set; }
        public string Workplace { get; set; }

        public byte[] Foto { get; set; }
        public bool Active { get; set; }

        public string FotoString
        {
            get
            {
                if (Foto != null)
                {

                    return "data:image/jpeg;charset=utf-8;base64, " + Convert.ToBase64String(Foto);
                }
                else return String.Empty;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
