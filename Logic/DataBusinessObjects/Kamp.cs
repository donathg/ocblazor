﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects
{

    [Serializable]
    public class Kamp
    {
        public int Id { get; set; }

        public string Nr { get; set; }

        public string Name { get; set; }


        public bool Active { get; set; }

        public string Bestemming { get; set; }
        public string Wie { get; set; }

        public DateTime Van { get; set; }

        public DateTime Tot { get; set; }
    }
}
