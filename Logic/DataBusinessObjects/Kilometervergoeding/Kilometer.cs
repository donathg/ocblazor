﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects.Kilometervergoeding
{
    [Serializable]
    public class Kilometer
    {


        public void CopyProperties (Kilometer source)
        {
            this.Id = source.Id;
            this.DatumIngave = source.DatumIngave;
            this.Kilometers = source.Kilometers;
            this.Bestuurder = source.Bestuurder;
            this.Reden = source.Reden;
            this.Bedrag = source.Bedrag;
            this.Beginstand = source.Beginstand;
            this.Eindstand = source.Eindstand;
            this.Clienten = source.Clienten;
            this.Kampen = source.Kampen;
            this.Leefgroepen = source.Leefgroepen;
            this.Reden = source.Reden;
            this.Reisroute = source.Reisroute;
            this.Wagen = source.Wagen;
            this.KilometerType = source.KilometerType;
        }

        public int? Id { get; set; }
        public DateTime DatumIngave { get; set; }

        public string Bestuurder { get; set; }

        public String Reden { get; set; } = String.Empty;
        public Double Bedrag { get; set; }

        private int? _beginstand;
        private int? _eindstand;
 
        public bool IsBeginEindStand()
        {
            return Wagen.Id != 1 && Wagen.Id != 5;

        }
        public  int? Eindstand { get { return _eindstand; } set { _eindstand = value; UpdateKilometers(); } }

        private void UpdateKilometers()
        {
            if (!IsBeginEindStand())
                return;
            if (Eindstand.GetValueOrDefault() > 0 && Beginstand.GetValueOrDefault() > 0)
                Kilometers = Eindstand.GetValueOrDefault() - Beginstand.GetValueOrDefault();
            else Kilometers = 0;
        }


        public void SetBeginEindstandNoUpdate(int? beginstand, int? eindstand)
        {
            this._eindstand = eindstand;
            this._beginstand = beginstand;
        }
        public int? Beginstand { get { return _beginstand; } set { _beginstand = value; UpdateKilometers(); } }

        private Double? _kilometers;
        public Double? Kilometers { get {

                if (_kilometers.GetValueOrDefault() == 0)
                    return null;
                return _kilometers;
            
            } set { _kilometers = value; } }
        public DateTime DatumRit{ get; set; }

        public KilometerType KilometerType { get; set; } = new KilometerType();



        public Wagen Wagen {
            get; 
            
            set; 
        } = new Wagen();
        public bool AfgeslotenDoorPersoneelsdienst { get; set; }


        public List<Client> Clienten { get; set; } = new List<Client>();
        public List<Leefgroep> Leefgroepen { get; set; } = new List<Leefgroep>();

        public List<Kamp> Kampen { get; set; } = new List<Kamp>();
        public String Reisroute { get; set; } = String.Empty;




    }
}
