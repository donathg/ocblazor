﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects.Kilometervergoeding
{

    [Serializable]
    public class KilometerType
    {

        public String Code { get; set; } = String.Empty;

        public String Name { get; set; } = String.Empty;
    }
}
