﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Kilometervergoeding
{
    public class VerklaringItem
    {

        public int Id { get; set; }
        public String Text { get; set; }
    }

    public class VerklaringResult
    {
        public string BatchNr { get; set; }
        public int GebruikerId { get; set; }
        private int? _verklaringItemId;
        public int? VerklaringItemId
        {
            get
            {
                return _verklaringItemId;
            }
            set
            {
                _verklaringItemId = value;
                AfstandEnkeleReisFiets = null;
                AfstandEnkeleReisWagen = null;
            }
        }
        public DateTime ModificationDate { get; set; }
        public double? AfstandEnkeleReisFiets { get; set; }
        public double? AfstandEnkeleReisWagen { get; set; }
        public bool VerklaringOndertekend { get; set; }
    }
}
