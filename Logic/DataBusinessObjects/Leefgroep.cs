﻿using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects
{
    [Serializable]
    public class Leefgroep
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Actief { get; set; }
        public int Id { get; set; }
        public bool NietVerbondenAanSchool { get; set; }
        public string DisplayNaam { get; set; }
        public DateTime StartDatum { get; set; }
        public DateTime EindDatum { get; set; }
        public string Ondersteuningsvorm { get; set; }

        // ecqare info
        public Guid EcqareId { get; set; }
        public Guid EcqareParentId { get; set; }
        public string EcqareCode { get; set; }
        public bool IsFromEcQare => !String.IsNullOrWhiteSpace(EcqareCode);
        public bool IsEcqareLowestLevel { get; set; }

        // nodig voor maaltijden keuken
        public bool HeeftMaaltijdLevering { get; set; }
        public int MaaltijdlijstenOrderNum { get; set; }
        public string MaaltijdlijstenGroepering { get; set; }
        public bool MaaltijdlijstenVacuumVerpakking { get; set; }

        // nodig voor maaltijden administratie
        public bool IsMaaltijdAdministratieBudgetVervangend { get; set; }
        public bool IsMaaltijdAdministratieGeenLeveringen { get; set; }
        public string MaaltijdlijstenGroeperingAdministratie { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
