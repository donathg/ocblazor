﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects
{
    public class Locatie
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public bool IsRoot
        {
            get
            {
                return !ParentId.HasValue;
            }
        }
        public bool IsCampus
        {
            get
            {
                if (Parent == null)
                    return false;
                if (!Parent.ParentId.HasValue)
                    return true;

                return false;
            }
        }
        public Locatie Parent { get; set; }

        public List<Locatie> Children { get; set; } = new List<Locatie>();
        public String Naam { get; set; }

    }
}
