﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace Logic.DataBusinessObjects
{
    public class Module
    {
        public string Code { get; set; }
        public string Descr { get; set; }
    }
    public enum AccessType
    {
        none = 0,
        access = 1
    }
    public class AccessCode
    {
        public string Groep { get; set; }
        public bool FromAcerta { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Module { get; set; }
        public Module ModuleObject { get; set; }
        public AccessType AccesType { get; set; }

        public override string ToString()
        {
            return AccesType.ToString() + " " + Code + " - " + Description + " - " + ModuleObject.Descr;
        }
    }

    public class UserSettings 
    {
        public int PageSizeGOG { get; set; } = 15;
    }
    public class LoggedOnUser
    {

        public UserSettings UserSettings { get; set; } = new UserSettings();
        public bool LoggedOn { get; set; }
        public String ErrorMessage { get; set; }
        public bool NeedNewPassword { get; set; }
        public int UserId { get; set; } 
        public String Name { get; set; }
        public String Email { get; set; } 
        public String AuthorisationToken { get; set; }
        public int CompanyId { get; set; }
        public string FunctionDescription { get; set; }
        public Byte[] CompanyLogo { get; set; }
        public string DatabaseAliasName { get; set; }
        public DateTime? LogonDate { get; set; }

        public  Dictionary<String, AccessCode> AccesCodes = new Dictionary<string, AccessCode>();
        public Dictionary<string, List<int>> AccesLeefgroepenModule = new Dictionary<string, List<int>>();//moduleCode, Leefgroep
        public Dictionary<string, string> AppSettings = new Dictionary<string, string>();
        public List<int> AccesBewonersModule = new List<int>();

        public List<int>GetLeefgroepen(string moduleCode)
        {
            return  AccesLeefgroepenModule[moduleCode];
        }
        public List<int> GetLeefgroepen(params string[] moduleCodes)
        {
            List<int> lfgs = new List<int>();
            foreach (string mod in moduleCodes)
            {
                if (AccesLeefgroepenModule.ContainsKey(mod))
                  lfgs.AddRange(AccesLeefgroepenModule[mod]);
            }
            return lfgs.Distinct().ToList();
        }
        public List<int> GetBewonerIds()
        {
            return AccesBewonersModule;
        }
        public LoggedOnUser()
        {


        }
        public bool HasAccess(String code)
        {
            return GetAccesType(code) != AccessType.none;
        }
        public AccessType GetAccesType(String code)
        {

            if (String.IsNullOrWhiteSpace(code))
                return AccessType.none;

            int iCode = Int32.Parse(code);




            if (AccesCodes.ContainsKey("1"))
                return AccessType.access;


            if (iCode >= 1000 && iCode < 2000)//Clientdossier
            {
                if (AccesCodes.ContainsKey("1001"))
                    return AccessType.access;
            }


            if (AccesCodes.ContainsKey(code))
                return AccesCodes[code].AccesType;
            return AccessType.none;//geen rechten
        }

        /// <summary>
        /// Kijkt na af de gebruiker rechten heeft binnen een hoofdgroep. Bijvoorbeel 4(00) voor kalender
        /// </summary>
        /// <param name="firstNum"></param>
        /// <returns></returns>
        public bool HasAccessToGroup(int firstNum, int lastNum)
        {


            if (AccesCodes.ContainsKey("1"))
                return true;


            if (firstNum >= 1000 && firstNum < 2000)//Clientdossier
            {
                if (AccesCodes.ContainsKey("1001"))
                    return true;
            }

            foreach (KeyValuePair<string, AccessCode> kvp in AccesCodes)
            {
                int k = Int32.Parse(kvp.Key.ToString());
                if (k >= firstNum && k <= lastNum && kvp.Value.AccesType != AccessType.none)
                    return true;

            }
            return false;
        }
    }
}
