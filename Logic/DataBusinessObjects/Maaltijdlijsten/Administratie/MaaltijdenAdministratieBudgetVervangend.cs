﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Maaltijdlijsten.Administratie
{
    public class MaaltijdenAdministratieBudgetVervangendRow
    {
        public string LeefgroepNaam { get; set; }
        public string Type { get; set; }
        public int Aantal { get; set; }
        public decimal Prijs { get; set; }
        public decimal TotaalLijn => Aantal * Prijs;
        public decimal TotaalLeefgroep => TotaalLijn;
        public string Boeking { get; set; }
    }

    public class MaaltijdenAdministratieBudgetVervangend
    {
        public string ReportTitle { get; set; } = "";
        public DateTime DateFrom { get; set; } = DateTime.Now;
        public DateTime DateTo { get; set; } = DateTime.Now;

        public List<MaaltijdenAdministratieBudgetVervangendRow> Rows { get; set; } = new List<MaaltijdenAdministratieBudgetVervangendRow>();
    }
}
