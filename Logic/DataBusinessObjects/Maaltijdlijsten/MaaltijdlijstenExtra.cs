﻿using Logic.DataBusinessObjects.Webservices.eCQare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Maaltijdlijsten
{
    public class MaaltijdlijstenExtraRow
    {
        public String Leefgroep { get; set; }
        public String Extras { get; set; }
    }
    public class MaaltijdlijstenExtra
    {

        public string ReportTitle { get; set; } = "";
        public DateTime DateFrom { get; set; } = DateTime.Now;
        public DateTime DateTo { get; set; } = DateTime.Now;

        public List<MaaltijdlijstenExtraRow> Rows { get; set; } = new List<MaaltijdlijstenExtraRow>();


    }
}
