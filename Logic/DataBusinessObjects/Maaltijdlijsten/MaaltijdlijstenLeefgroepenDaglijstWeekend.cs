﻿using Logic.DataBusinessObjects.Webservices.eCQare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Maaltijdlijsten
{
    public class MaaltijdlijstenLeefgroepenDaglijstWeekendRow
    {
        public string Leefgroep { get; set; }
        public int Ontbijt { get; set; } = 0;
        public string MiddagWarm { get; set; }
        public int MiddagKoud { get; set; } = 0;
        public string AvondWarm { get; set; }
        public int AvondKoud { get; set; } = 0;
    }
    public class MaaltijdlijstenLeefgroepenDaglijstWeekend
    {

        public string ReportTitle { get; set; } = "";
        public DateTime DateFrom { get; set; } = DateTime.Now;
        public DateTime DateTo { get; set; } = DateTime.Now;

        public List<MaaltijdlijstenLeefgroepenDaglijstWeekendRow> Rows { get; set; } = new List<MaaltijdlijstenLeefgroepenDaglijstWeekendRow>();


    }
}
