﻿using Force.DeepCloner;
using Logic.DataBusinessObjects.Webservices.eCQare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Maaltijdlijsten
{


    public class MaaltijdlijstenLeefgroepenTotalenRow
    {
        public String Bijzonderheid { get; set; }   
        public MealChoiceEnum MealChoiceFull { get; set; } = MealChoiceEnum.None;
        public MealScheduleEnum MealScheduleFull { get; set; } = MealScheduleEnum.None;
        public int Aantal { get; set; } = 0;


      
    }
    public class MaaltijdlijstenLeefgroepenTotalen
    {
        public string ReportTitle { get; set; } = "";
        public DateTime DateFrom { get; set; } = DateTime.Now;
        public DateTime DateTo { get; set; } = DateTime.Now;

        public List<MaaltijdlijstenLeefgroepenTotalenRow> Rows { get; set; } = new List<MaaltijdlijstenLeefgroepenTotalenRow>();

        public void Convert(List<EcqareAlivioMealplanFile> fileItems, List<Leefgroep> leefgoepen)
        {
            List<EcqareAlivioMealplanFile> fileItems2 = fileItems.Where(x => x.MealScheduleFull == MealScheduleEnum.Ochtend ||
                ((x.MealScheduleFull == MealScheduleEnum.Middag || x.MealScheduleFull == MealScheduleEnum.Avond) &&
                (x.MealChoiceFull == MealChoiceEnum.Warm || x.MealChoiceFull == MealChoiceEnum.Koud))).ToList();

            foreach (EcqareAlivioMealplanFile fileItem in fileItems2)
            {
                EcqareAlivioMealplanFile fileItemCopy = fileItem.DeepClone();

                if (leefgoepen.SingleOrDefault(x => x.DisplayNaam.Equals(fileItemCopy.MealLocationName, StringComparison.CurrentCultureIgnoreCase)) != null)
                {
                    if (fileItemCopy.MealScheduleFull == MealScheduleEnum.Ochtend || fileItemCopy.MealChoiceFull == MealChoiceEnum.Koud) // voor koud en ochtend alles optellen. Geen dieëten
                        fileItemCopy.MealAttentionpoints = "Regulier";

                    MaaltijdlijstenLeefgroepenTotalenRow foundItem = Rows.FirstOrDefault(x => x.MealChoiceFull == fileItemCopy.MealChoiceFull
                        && x.MealScheduleFull == fileItemCopy.MealScheduleFull
                        && x.Bijzonderheid == fileItemCopy.MealAttentionpoints);

                    if (foundItem == null)
                    {
                        Rows.Add(new MaaltijdlijstenLeefgroepenTotalenRow()
                        {
                            MealChoiceFull = fileItemCopy.MealChoiceFull,
                            MealScheduleFull = fileItemCopy.MealScheduleFull,
                            Bijzonderheid = fileItemCopy.MealAttentionpoints,
                            Aantal = 1
                        });
                    }
                    else
                        foundItem.Aantal++;
                }
            }
        }
    }
}
