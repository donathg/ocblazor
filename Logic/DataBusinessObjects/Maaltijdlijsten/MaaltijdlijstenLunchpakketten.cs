﻿using Logic.DataBusinessObjects.Webservices.eCQare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Maaltijdlijsten
{
    public class MaaltijdlijstenLunchpakkettenRow
    {
        public String Leefgroep { get; set; }
        public int AantalLunchpakketten { get; set; } = 0;
    }
    public class MaaltijdlijstenLunchpakketten
    {
        public string ReportTitle { get; set; } = "";
        public DateTime DateFrom { get; set; } = DateTime.Now;
        public DateTime DateTo { get; set; } = DateTime.Now;

        public List<MaaltijdlijstenLunchpakkettenRow> Rows { get; set; } = new List<MaaltijdlijstenLunchpakkettenRow>();
    }
}
