﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Maaltijdlijsten
{
    public class MaaltijdLijstenReportItem
    {
        public int Id { get; set; }
        public string Naam { get; set; }

        public string ReportTitle { get; set; }
    }
    public class MaaltijdLijstenReport
    {
        public List<MaaltijdLijstenReportItem> Items { get; set; } = new List<MaaltijdLijstenReportItem>();
        public MaaltijdLijstenReport(bool isAdministratie)
        {
            if (isAdministratie)
            {
                MaaltijdLijstenReportItem i = new MaaltijdLijstenReportItem
                {
                    Id = 1,
                    Naam = "Budget Vervangende Maaltijd",
                    ReportTitle = "Budget Vervangende Maaltijd : "
                };
                Items.Add(i);

                i = new MaaltijdLijstenReportItem
                {
                    Id = 2,
                    Naam = "Budget Vervangende Maaltijd - Geen Levering",
                    ReportTitle = "Budget Vervangende Maaltijd - Geen Levering : "
                };
                Items.Add(i);
            }
            else
            {
                MaaltijdLijstenReportItem i = new MaaltijdLijstenReportItem
                {
                    Id = 1,
                    Naam = "Leefgroep Totalen",
                    ReportTitle = "Totale bestellingen voor leefgroepen voor : "
                };
                Items.Add(i);

                i = new MaaltijdLijstenReportItem
                {
                    Id = 2,
                    Naam = "Leefgroep Daglijst",
                    ReportTitle = "Bestellingen leefgroepen : "
                };
                Items.Add(i);

                i = new MaaltijdLijstenReportItem
                {
                    Id = 3,
                    Naam = "Leefgroep Daglijst Weekend - Vakantie",
                    ReportTitle = "Bestellingen leefgroepen weekend - vakantie : "
                };
                Items.Add(i);

                i = new MaaltijdLijstenReportItem
                {
                    Id = 4,
                    Naam = "Vacuumverpakte maaltijden totalen",
                    ReportTitle = "Totale bestellingen vacuumverpakte maaltijden"
                };
                Items.Add(i);

                i = new MaaltijdLijstenReportItem
                {
                    Id = 5,
                    Naam = "Vacuumverpakte maaltijden daglijst",
                    ReportTitle = "Bestellingen vacuumverpakte maaltijden"
                };
                Items.Add(i);

                i = new MaaltijdLijstenReportItem
                {
                    Id = 6,
                    Naam = "Extra's leefgroepen",
                    ReportTitle = "Extra's leefgroepen"
                };
                Items.Add(i);

                i = new MaaltijdLijstenReportItem
                {
                    Id = 7,
                    Naam = "Lunchpakketten",
                    ReportTitle = "Bestellingen Lunchpakketten"
                };
                Items.Add(i);
            }
        }
    }
}
