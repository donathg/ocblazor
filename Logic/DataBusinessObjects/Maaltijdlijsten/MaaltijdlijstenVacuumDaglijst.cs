﻿using Logic.DataBusinessObjects.Webservices.eCQare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Maaltijdlijsten
{
    public class MaaltijdlijstenVacuumDaglijstRow
    {

        public DateTime DayDate { get; set; }
        public String Leefgroep { get; set; }
        public int Ontbijt { get; set; }
        public String Warm { get; set; }
        public String Koud { get; set; }
    }
    public class MaaltijdlijstenVacuumDaglijst
    {

        public string ReportTitle { get; set; } = "";
        public DateTime DateFrom { get; set; } = DateTime.Now;
        public DateTime DateTo { get; set; } = DateTime.Now;

        public List<MaaltijdlijstenVacuumDaglijstRow> Rows { get; set; } = new List<MaaltijdlijstenVacuumDaglijstRow>();


    }
}
