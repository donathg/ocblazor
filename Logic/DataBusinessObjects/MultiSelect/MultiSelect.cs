﻿using Org.BouncyCastle.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
 
using System.Net.Mail;
using MailKit.Search;
using System.Diagnostics;
using Newtonsoft.Json;
using Logic.EmailNS;
using Microsoft.Win32.SafeHandles;

namespace Logic.DataBusinessObjects.MultiSelectNS
{
    public class MultiSelect : IDisposable
    {
        public int Id { get; set; }

        public string ModuleCode { get; set; }
        public string Title { get; set; }

        public string ExtraInfo { get; set; }

        public string ChoiceType { get; set; }

        public bool IsMandatory { get; set; }
        public bool IsVisible { get; set; } = true;
        public int OrderNumReport { get; set; }

        private static Dictionary<string, string> _defaultErrorMessage = new Dictionary<string, string>() {
            { "EMAIL","Gelieve een geldig emailadres aan in te geven." },
            { "DEFAULT","Gelieve dit veld in te vullen." }
        };
        public bool Validate()
        {
            if (IsVisible)
            {
                if (ChoiceType == "DATE" || ChoiceType == "TIME")
                {
                    DateTime? dateValue = Items[0].Result.DateValue;
                    if (!dateValue.HasValue && IsMandatory)
                    {
                        SetErrorDefault();
                        return false;
                    }
                }
                else if (ChoiceType == "EMAIL")
                {
                    String stringValue = this.Items[0].Result.StringValue;
                    if (String.IsNullOrWhiteSpace(stringValue) || !Common.Email.IsValid(stringValue) && IsMandatory)
                    {
                        SetErrorDefault();
                        return false;
                    }
                }
                else if (ChoiceType == "TEXT" || ChoiceType == "TEXTAREA")
                {
                    String stringValue = this.Items[0].Result.StringValue;
                    if (String.IsNullOrWhiteSpace(stringValue) && IsMandatory)
                    {
                        SetErrorDefault();
                        return false;
                    }
                }
                else if (ChoiceType == "COMBO" || ChoiceType == "RADIO" || ChoiceType == "CHECK")
                {
                    var results = this.Items.Where(d => d.Result.IsSelected).ToList();
                    if (IsMandatory && results.Count == 0)
                    {
                        SetErrorDefault();
                        return false;
                    }
                    foreach (MultiSelectItem item in this.Items)
                    {
                        if (item.Result.IsSelected && item.IsOther && String.IsNullOrWhiteSpace(item.Result.StringValue))
                        {
                            SetError("Gelieve een text in te geven voor keuze '" + item.Description + "' ? ");
                           return false;
                        }
                    }
                }
            }

            ClearError();
            return true;
        }
        public void SetError(string errorMessage)
        {
            this.ErrorMessage = errorMessage;
        }
        public void SetErrorDefault()
        {
            if (!_defaultErrorMessage.ContainsKey(ChoiceType)  )
                this.ErrorMessage = _defaultErrorMessage["DEFAULT"];
            else
            this.ErrorMessage = _defaultErrorMessage[ChoiceType];
        }
        public void ClearError()
        {
            this.ErrorMessage = null;
        }
        public string ErrorMessage { get; set; } = null;

        public List<MultiSelectItem> Items { get; set; } = new List<MultiSelectItem>();


        public MultiSelect Clone()
        {
            MultiSelect clone = new MultiSelect
            {
                Id = Id,
                Title = Title,
                ModuleCode = ModuleCode,
                ExtraInfo = ExtraInfo,
                ChoiceType = ChoiceType,
                IsMandatory = IsMandatory,
                IsVisible = IsVisible,
                OrderNumReport = OrderNumReport
            };
            clone.ClearError();
           // clone._defaultErrorMessage = _defaultErrorMessage;

            foreach (MultiSelectItem i in Items)
            {
                MultiSelectItem cloned = i.Clone(clone);
                clone.Items.Add(cloned);
                cloned.Result = i.Result;
            }
            return clone;

        }
        //TEXT, RADIO, COMBO, CHECK, DATE, TIME, TEXTAREA,TEXT,EMAIL
        public string GetResultFormatted()
        {

            StringBuilder sb = new StringBuilder();
            foreach (MultiSelectItem item in Items)
            {
                if (item.Result.IsSelected)
                {
                    string formattedValue = GetReultTextItemFormatted(item);
                    if (String.IsNullOrWhiteSpace(formattedValue)==false)
                         sb.AppendLine(formattedValue);

                }
            }
            return sb.ToString();
        }
        private string GetReultTextItemFormatted (MultiSelectItem item)
        {
            if (!item.Result.IsSelected)
                return String.Empty;

            String text = "";

            //if (item.IsOther)
            //{
            //    text = "Andere : ";
            //}
            switch (item.Parent.ChoiceType)
            {
                case "TEXT": text =  text+item.Result.StringValue;  break;
                case "RADIO": text = text + item.Description; break;
                case "COMBO": text = text + item.Description; break;
                case "CHECK":
                    if (item.Parent.Items.Count(x => x.Result.IsSelected) > 1)
                    { text = text + "- " + item.Description; }
                    else
                    { text = text + item.Description; }
                    break;
                   
                case "DATE": text = text + item.Result.DateValue.GetValueOrDefault().ToString("dd/MM/yyyy");  break;
                case "TIME": text = text +  item.Result.DateValue.GetValueOrDefault().ToString("HH:mm"); break;
                case "TEXTAREA": text = text + item.Result.StringValue; break;
                case "EMAIL": text = text + item.Result.StringValue; break;
                default: text = text + item.Result.StringValue; break;
            }
            if (item.IsOther)
            {
                text += " : " + item.Result.StringValue;
            }
            return text;    

        }
        public void Reset()
        {
            foreach (MultiSelectItem item in Items)
            {
                item.Reset();
            }
         //   _defaultErrorMessage.Clear();
        }
        
        public void Dispose()
        {
            Reset();
            foreach (MultiSelectItem item in Items)
            {
                item.Dispose();
                
            }
            if (Items != null)
            {
                this.Items.Clear();
                this.Items = null;
            }
        }
    }


    public class MultiSelectItem : IDisposable
    {
        public MultiSelect Parent { get; set; } = new MultiSelect();
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsDefaultSelected { get; set; }
        public bool IsOther { get; set; }

        public int Ordernum { get; set; }

        public bool Active { get; set; } //Databasefield
        public bool ActiveInDev { get; set; }

        public bool Show
        {
            get
            {
                #if DEBUG
                    return (ActiveInDev || this.Result.IsSelected);
                #else
                    return (Active || this.Result.IsSelected);
                #endif
            }
        }


        public MultiSelectItemResult Result { get; set; }

        public MultiSelectItem()
        {
            Result = new MultiSelectItemResult() { MultiSelectItemId = Id };
        }
    
        public MultiSelectItem Clone(MultiSelect parent)
        {
            MultiSelectItem clone = new MultiSelectItem();
            clone.Id = Id;
            clone.Description = Description;
            clone.IsDefaultSelected = IsDefaultSelected;
            clone.IsOther = IsOther;
            clone.Ordernum = Ordernum;
            clone.Parent = parent;
            clone.Active = Active;
            clone.ActiveInDev = ActiveInDev;
            clone.Result = new MultiSelectItemResult();
          
            return clone;   
        }
        public void Reset()
        {            
            this.Result.Reset();
        }      
        public void Dispose()
        {
            this.Result.Reset();
            this.Parent = null;
        }
    }

    public class MultiSelectItemResult
    {
        public int MultiSelectItemId { get; set; }

        public string ErrorMessage { get; set; } = null;
        public bool IsSelected { get; set; }

        public string? StringValue { get; set; }
        public int? IntValue { get; set; }

        public Double? DoubleValue { get; set; }

        public DateTime? DateValue { get; set; }

        public void Reset()
        {
            IsSelected = false;
            ErrorMessage = null;
            StringValue = null;
            IntValue = null;
            DoubleValue = null;
            DateValue = null;
        }
    }
    
    public class MultiSelectTreeItem
    {

        public Object SourceItem { get; set; }
        public int Id { get; set; }
        public String Name { get; set; }

        public List<MultiSelectTreeItem> Children { get; set; } = new List<MultiSelectTreeItem>();

    }
}
