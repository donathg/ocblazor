﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Onkostennota
{
    public class OnkostennotaCategorie
    {

        public int Id { get; set; }

        public string Naam { get; set; }
        public string Beschrijving { get; set; }

        public bool Actief { get; set; }

    }
}
