﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Onkostennota
{

    public class MaaltijdAandachtspuntenItem
    {

        public int Id { get; set; }

        public string Naam { get; set; }
    }



    public class OnkostennotaCategorieItem
    {

        public int Id { get; set; }

        public string Naam { get; set; }


        public string Code { get; set; }

        public string ModuleCode { get; set; }

    }

  

     public class OnkostennotaItem
    {



        public int? LeefgroepId { get; set; }
        public String LeefgroepName { get; set; }

        public int Id { get; set; } 

        public int GebruikerId { get; set; }  
        public DateTime CreationDate { get; set; } = DateTime.Now;

        public int? AandachtspuntenEcqareId { get; set; }
        public String AandachtspuntenEcqareNaam { get; set; }
        
        public String ClientenString
        {
            get
            {
                if (Clienten == null)
                    return "";
                String t = "";
                foreach (Client c in Clienten)
                {
                    if (t != "")
                    {
                        t = t + ", ";
                    }
                    t = t + c.FirstNameLastname;
                }

                return t;
            }
            set { }
        }
        public List<Client> Clienten { get; set; } = new List<Client>();



        private int _categorieId;
        public int CategorieId { get { return _categorieId; } set { _categorieId = value; CheckBedrag(); } }


        public void CheckBedrag()
        {


        }

        public String CategorieNaam { get; set; }

        public DateTime OnkostenNotaDate { get; set; } = DateTime.Now;

      

        public String Naam { get; set; } = String.Empty;

        public String Beschrijving { get; set; } = String.Empty;

        public double Bedrag { get; set; } = 0;

        public bool IsAfgerekend { get; set; }

        public bool AkkoordVerklaring { get; set; }

        public string ClientenStringFromDB { get; set; }

        public string Validate()
        {

            if (OnkostenNotaDate.Date > DateTime.Now.Date)
                return "Datum mag niet in de toekomst liggen.";
            if (CategorieId == 0)
                return "Gelieve een type te selecteren.";

            if (String.IsNullOrWhiteSpace(Beschrijving)) return "Gelieve een beknopte beschrijving in te geven.";

            if (Bedrag==0)
                return "Gelieve een bedrag in te geven.";
        

            return String.Empty;

        }
    }
}
