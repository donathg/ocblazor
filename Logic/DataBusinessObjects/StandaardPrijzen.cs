﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects
{
    public class StandaardPrijzen
    {
        public int Id { get; set; }

        public string ModuleCode { get; set; }
        public string SubType_1 { get; set; }
        public string SubType_2 { get; set; }
        public decimal Bedrag { get; set; }
        public DateTime StartDatum { get; set; }
        public DateTime? EindDatum { get; set; }
    }
}
