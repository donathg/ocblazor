﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects
{

    [Serializable]
    public class Wagen 
    {
        public int Id { get; set; }
        public String Naam { get; set; }

        public String Nummerplaat { get; set; }

        public String Merk { get; set; }


    }
}
