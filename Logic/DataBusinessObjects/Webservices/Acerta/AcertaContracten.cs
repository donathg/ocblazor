﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Logic.DataBusinessObjects.Webservices.Acerta
{
    public class AcertaContract
    {

        public string RijksregisterNummer { get; set; }

        public string EmployeeHrmNumberCurrent { get; set; }


        /// <summary>
        /// Dienstcode  positie 10,11
        /// </summary>
        public string ServiceReceiverCostCenterNumber { get; set; } = String.Empty;
        public string SequenceNumber { get; set; } = String.Empty;
        
        public string DienstCode { get { 
            
            if (ServiceReceiverCostCenterNumber?.Length>11)
                {

                    return ServiceReceiverCostCenterNumber.Substring(9, 2);
                }
                return String.Empty;
            
            }  }
        public DateTime? EmploymentStartDate { get; set; }

        public DateTime? EmploymentEndDate { get; set; }


        public DateTime? ContractCostCenterStartDate { get; set; }

        public DateTime? ContractCostCenterEndDate { get; set; }


        // Constructor
        public AcertaContract(string rijksregisterNummer, string employeeHrmNumberCurrent, XElement contractCostCenter, XElement contract)
        {
            this.RijksregisterNummer = rijksregisterNummer;
            this.EmployeeHrmNumberCurrent = employeeHrmNumberCurrent;



            this.ServiceReceiverCostCenterNumber = contractCostCenter?.Element("ServiceReceiverCostCenterNumber")?.Value;
            this.SequenceNumber = contractCostCenter?.Element("SequenceNumber")?.Value;
            var historyFromDateString = contractCostCenter.Element("HistoryFromDate")?.Value;
            if (!string.IsNullOrEmpty(historyFromDateString))
            {
                ContractCostCenterStartDate = DateTime.ParseExact(historyFromDateString, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            }
            var historyUntilDateString = contractCostCenter.Element("HistoryUntilDate")?.Value;
            if (!string.IsNullOrEmpty(historyUntilDateString))
            {
                ContractCostCenterEndDate = DateTime.ParseExact(historyUntilDateString, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            }







            var stringDate =  contract.Element("EmploymentStartDate")?.Value;
            if (!string.IsNullOrEmpty(stringDate)) 
            { 
                EmploymentStartDate = DateTime.ParseExact(stringDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            }

            stringDate = contract.Element("EmploymentEndDate")?.Value;
            if (!string.IsNullOrEmpty(stringDate))
            {
                EmploymentEndDate = DateTime.ParseExact(stringDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            }
        }
    }
}
 
