﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Logic.DataBusinessObjects.Webservices.Acerta
{
    public class AcertaPersoneel
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string PlaceOfBirthName { get; set; }
        public string CountryOfBirthName { get; set; }
        public string GenderCode { get; set; }
        public string NationalityCode { get; set; }
        public string SocialSecurityNumber { get; set; }

        public string Email { get; set; }

        public string HrmNumber { get; set; }
        public string OfficialLanguageCode { get; set; }
        public string SpokenLanguageCode { get; set; }
        public string CompanyOrganization { get; set; }
        public string IamUserCode { get; set; }
        public string ManagerManualRightsIndicator { get; set; }
        public string RegistrationUserCode { get; set; }
        public string RegistrationTimestamp { get; set; }
        public string MutationUserCode { get; set; }
        public string MutationTimestamp { get; set; }

        public List<string> Errors { get; set; } = new List<string>();


        public AcertaPersoneel(XElement employeeInfo, XElement employeeContact)
        {
            FirstName = employeeInfo.Element("FirstName").Value;
            LastName = employeeInfo.Element("LastName").Value;

            if (employeeContact != null)
            {
                var x = employeeContact.Element("WorkE-Mail");
                if (x != null)
                    Email = employeeContact.Element("WorkE-Mail").Value;
            }
            var stringDate = employeeInfo.Element("BirthDate")?.Value;
            if (!string.IsNullOrEmpty(stringDate))
            {
                BirthDate = DateTime.ParseExact(employeeInfo.Element("BirthDate").Value, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            }
            PlaceOfBirthName = employeeInfo.Element("PlaceOfBirthName")?.Value;
            CountryOfBirthName = employeeInfo.Element("CountryOfBirthName")?.Value;
            GenderCode = employeeInfo.Element("GenderCode")?.Value;
            NationalityCode = employeeInfo.Element("NationalityCode")?.Value;
            SocialSecurityNumber = employeeInfo.Element("SocialSecurityNumber").Value;
            if (employeeInfo.Parent?.Attribute("HrmNumber") != null)
                HrmNumber = employeeInfo.Parent?.Attribute("HrmNumber").Value.ToString();
            else
                Errors.Add($"Geen HRM-Nummer voor {FirstName} {LastName}  met RRNR {SocialSecurityNumber}.");

            OfficialLanguageCode = employeeInfo.Element("OfficialLanguageCode")?.Value;
            SpokenLanguageCode = employeeInfo.Element("SpokenLanguageCode")?.Value;
            CompanyOrganization = employeeInfo.Element("CompanyOrganization")?.Value;
            IamUserCode = employeeInfo.Element("IamUserCode")?.Value;
            ManagerManualRightsIndicator = employeeInfo.Element("ManagerManualRightsIndicator")?.Value;
            RegistrationUserCode = employeeInfo.Element("RegistrationUserCode")?.Value;
            RegistrationTimestamp = employeeInfo.Element("RegistrationTimestamp")?.Value;
            MutationUserCode = employeeInfo.Element("MutationUserCode")?.Value;
            MutationTimestamp = employeeInfo.Element("MutationTimestamp")?.Value;
    
        }
    }
}
 
