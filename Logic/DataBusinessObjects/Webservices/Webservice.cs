﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Webservices
{ 
    public class WebserviceExternal
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string ApiName { get; set; }
        public string ApiLocation { get; set; }
        public string ApiContent { get; set; }
        public string ApiDescription { get; set; }
        public DateTime? LastRunDate { get; set; }
        public string LastRunStatus { get; set; }
        public List<string> LastRunErrors { get; set; }
        public string LastRunErrorString
        {
            get
            {
                string tempString = "";
                if (LastRunErrors is not null && LastRunErrors.Count != 0)
                {
                    foreach (string error in LastRunErrors)
                    {
                        tempString += "\n" + error;
                    }
                }
                return tempString;
            }
        }
        public String FileLocation { get; set; }      
    }  
}
