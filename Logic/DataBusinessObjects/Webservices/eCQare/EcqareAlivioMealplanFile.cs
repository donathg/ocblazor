﻿using Logic.DataBusinessObjects.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.DataBusinessObjects.Webservices.eCQare
{
    /// <summary>
    /// gebruikt voor:
    ///     - Leefgroepen > daglijst
    ///     - Leefgroepen > daglijst weekend
    ///     - Vacuum > daglijst
    ///     - Extra's
    ///     - Lunchpakketten
    /// </summary>
    public class EcqareAlivioMealplanByLeefgroep
    {
        public DateTime MealDay { get; set; }
        public string LeefgroepName {get;set;}
        public Leefgroep LeefgroepObject { get; set; }

        /// <summary>
        /// gebruikt voor:
        ///     - Leefgroepen > daglijst
        ///     - Leefgroepen > daglijst weekend
        ///     - Vacuum > daglijst
        /// </summary>
        public int AantalOntbijt { get; set; }
        /// <summary>
        /// gebruikt voor:
        ///     - Leefgroepen > daglijst
        ///     - Leefgroepen > daglijst weekend
        /// </summary>
        public int AantalMiddagKoud { get; set; }
        /// <summary>
        /// gebruikt voor:
        ///     - Leefgroepen > daglijst
        ///     - Leefgroepen > daglijst weekend
        /// </summary>
        public int AantalAvondKoud { get; set; }
        /// <summary>
        /// gebruikt voor:
        ///     - Leefgroepen > daglijst
        ///     - Leefgroepen > daglijst weekend
        /// </summary>
        public string BijzonderhedenMiddagWarmString => ParseDietenList(BijzonderhedenMiddagWarmList);
        /// <summary>
        /// gebruikt voor:
        ///     - Leefgroepen > daglijst
        ///     - Leefgroepen > daglijst weekend
        /// </summary>
        public string BijzonderhedenAvondWarmString => ParseDietenList(BijzonderhedenAvondWarmList);
        /// <summary>
        /// gebruikt voor:
        ///     - Vacuum > daglijst
        /// </summary>
        public string BijzonderhedenWarmString => ParseDietenList(BijzonderhedenAllWarmList);
        /// <summary>
        /// gebruikt voor:
        ///     - Vacuum > daglijst
        /// </summary>
        public string BijzonderhedenKoudString => ParseDietenList(BijzonderhedenAllKoudList);
        /// <summary>
        /// gebruikt voor:
        ///     - Extra's
        /// </summary>
        public string ExtrasString { get; set; }
        /// <summary>
        /// gebruikt voor:
        ///     - Lunchpakketten
        /// </summary>
        public int Lunchpakketten { get; set; }

        private List<WarmDetails> BijzonderhedenMiddagWarmList { get; set; } = new List<WarmDetails>();
        private List<WarmDetails> BijzonderhedenAvondWarmList { get; set; } = new List<WarmDetails>();
        private List<WarmDetails> BijzonderhedenAllWarmList { get; set; } = new List<WarmDetails>();
        private List<WarmDetails> BijzonderhedenAllKoudList { get; set; } = new List<WarmDetails>();

        private static string ParseDietenList(List<WarmDetails> bijzonderheden)
        {
            // first element is regulier
            WarmDetails regulier = bijzonderheden.SingleOrDefault(x => x.AttentionpointName == "Regulier");
            string tempString = regulier != null ? regulier.Aantal.ToString() : string.Empty;

            // add the rest
            foreach (WarmDetails bijzonderheid in bijzonderheden)
            {
                if (bijzonderheid.AttentionpointName != "Regulier") // already added before
                {
                    if (!string.IsNullOrEmpty(tempString)) // first one doesn't need a +
                    {
                        tempString += " + ";
                    }
                    tempString += bijzonderheid.ToString();
                }
            }

            return tempString;
        }

        public static List<EcqareAlivioMealplanByLeefgroep> ConvertLeefgroepenForLeefgroepenDaglijst(List<EcqareAlivioMealplanFile> mealPlans, List<Leefgroep> Leefgroepen, bool isWeekendRapport)
        {
            List<EcqareAlivioMealplanByLeefgroep> tempMealplanByLeefgroepList = new List<EcqareAlivioMealplanByLeefgroep>();
            // filter alle leefgroepen naar lijst van leefgroepen voor de daglijst (niet alle leefgroepen op die manier weergegeven)
            List<Leefgroep> shownLeefgroepen = Leefgroepen.Where(x => !x.MaaltijdlijstenVacuumVerpakking).ToList();

            // filter shownLeefgroepen voor Weekend levering
            if (isWeekendRapport)
            {
                shownLeefgroepen = shownLeefgroepen.Where(x => !string.IsNullOrEmpty(x.MaaltijdlijstenGroepering)).DistinctBy(x => x.MaaltijdlijstenGroepering).ToList();
            }

            // maak basis daglijstlijnen aan voor verder gebruik
            foreach (Leefgroep item in shownLeefgroepen)
            {
                tempMealplanByLeefgroepList.Add(new EcqareAlivioMealplanByLeefgroep()
                {
                    MealDay = mealPlans.FirstOrDefault().MealDate,
                    LeefgroepName = isWeekendRapport ? item.MaaltijdlijstenGroepering : item.DisplayNaam,
                    LeefgroepObject = item
                });
            }

            // doorloop de lijnen uit Ecqare
            foreach (EcqareAlivioMealplanFile mealPlan in mealPlans)
            {
                // test check voor debugging
                if (mealPlan.MealLocationName.Contains("CKK"))
                {
                    var a = 1;
                }
                Leefgroep foundLeefgroep;

                // vind en link de leefgroep aan bestaand leefgroepen. 2 manieren: 1 voor weekend en 1 voor weekdag
                if (isWeekendRapport)
                    foundLeefgroep = shownLeefgroepen.SingleOrDefault(y => mealPlan.MealLocationName.Contains(y.MaaltijdlijstenGroepering)); // groepering naam moet dezelfde zijn als leefgroepnaam ecare of een deel ervan!!! Geen andere manier om dit te doen. LocationId is waardeloos en linkt naar niks
                else
                    foundLeefgroep = shownLeefgroepen.SingleOrDefault(y => y.DisplayNaam.Equals(mealPlan.MealLocationName, StringComparison.CurrentCultureIgnoreCase));

                // check of leefgroep is gevonden
                if (foundLeefgroep != null)
                {
                    EcqareAlivioMealplanByLeefgroep foundLeefgroepItem;

                    // vind en link de leefgroep aan de corresponderende daglijstlijn. 2 manieren: 1 voor weekend en 1 voor weekdag
                    if (isWeekendRapport)
                        foundLeefgroepItem = tempMealplanByLeefgroepList.SingleOrDefault(x => x.LeefgroepName.Equals(foundLeefgroep.MaaltijdlijstenGroepering, StringComparison.CurrentCultureIgnoreCase));
                    else
                        foundLeefgroepItem = tempMealplanByLeefgroepList.SingleOrDefault(x => x.LeefgroepName.Equals(foundLeefgroep.DisplayNaam, StringComparison.CurrentCultureIgnoreCase));

                    // check of basis daglijstlijn is gevonden
                    if (foundLeefgroepItem != null)
                    {
                        /** vul de daglijstlijn aan met data */

                        if (mealPlan.MealChoiceFull == MealChoiceEnum.Ontbijt)
                        {
                            foundLeefgroepItem.AantalOntbijt++;
                        }
                        else if (mealPlan.MealChoiceFull == MealChoiceEnum.Koud)
                        {
                            if (mealPlan.MealScheduleFull == MealScheduleEnum.Middag)
                            {
                                foundLeefgroepItem.AantalMiddagKoud++;
                            }
                            else if (mealPlan.MealScheduleFull == MealScheduleEnum.Avond)
                            {
                                foundLeefgroepItem.AantalAvondKoud++;
                            }
                        }
                        else if (mealPlan.MealChoiceFull == MealChoiceEnum.Warm)
                        {
                            if (mealPlan.MealScheduleFull == MealScheduleEnum.Middag)
                            {
                                WarmDetails foundWarmDetails = foundLeefgroepItem.BijzonderhedenMiddagWarmList.FirstOrDefault(x => x.AttentionpointName == mealPlan.MealAttentionpoints);

                                if (foundWarmDetails == null)
                                    foundLeefgroepItem.BijzonderhedenMiddagWarmList.Add(new WarmDetails() { AttentionpointName = mealPlan.MealAttentionpoints, Aantal = 1 });
                                else
                                    foundWarmDetails.Aantal++;
                            }
                            else if (mealPlan.MealScheduleFull == MealScheduleEnum.Avond)
                            {
                                WarmDetails foundWarmDetails = foundLeefgroepItem.BijzonderhedenAvondWarmList.FirstOrDefault(x => x.AttentionpointName == mealPlan.MealAttentionpoints);

                                if (foundWarmDetails == null)
                                    foundLeefgroepItem.BijzonderhedenAvondWarmList.Add(new WarmDetails() { AttentionpointName = mealPlan.MealAttentionpoints, Aantal = 1 });
                                else
                                    foundWarmDetails.Aantal++;
                            }
                        }
                    }
                }
            }

            // return lijst van daglijstlijnen
            return tempMealplanByLeefgroepList.OrderBy(x => x.LeefgroepObject.MaaltijdlijstenOrderNum).ToList();
        }
        public static List<EcqareAlivioMealplanByLeefgroep> ConvertLeefgroepenForVacuumDaglijst(List<EcqareAlivioMealplanFile> mealPlans, List<Leefgroep> Leefgroepen)
        {
            List<EcqareAlivioMealplanByLeefgroep> tempList = new List<EcqareAlivioMealplanByLeefgroep>();
            List<Leefgroep> shownLeefgroepen = Leefgroepen.Where(x => x.MaaltijdlijstenVacuumVerpakking).ToList();
            List<DateTime> dayList = mealPlans.Select(item => item.MealDate).Distinct().ToList();

            foreach (Leefgroep item in shownLeefgroepen)
            {
                foreach (DateTime dag in dayList)
                {
                    tempList.Add(new EcqareAlivioMealplanByLeefgroep()
                    {
                        MealDay = dag,
                        LeefgroepName = item.DisplayNaam,
                        LeefgroepObject = item
                    });
                }
            }

            foreach (EcqareAlivioMealplanFile mealPlan in mealPlans)
            {
                EcqareAlivioMealplanByLeefgroep foundLeefgroepItem = tempList.SingleOrDefault(x => x.LeefgroepName.Equals(mealPlan.MealLocationName, StringComparison.CurrentCultureIgnoreCase) && x.MealDay == mealPlan.MealDate.Date);

                if (foundLeefgroepItem != null)
                {
                    if (mealPlan.MealChoiceFull == MealChoiceEnum.Ontbijt)
                    {
                        foundLeefgroepItem.AantalOntbijt++;
                    }
                    else if (mealPlan.MealChoiceFull == MealChoiceEnum.Koud)
                    {
                        WarmDetails foundWarmDetails = foundLeefgroepItem.BijzonderhedenAllKoudList.FirstOrDefault(x => x.AttentionpointName == mealPlan.MealAttentionpoints);

                        if (foundWarmDetails == null)
                            foundLeefgroepItem.BijzonderhedenAllKoudList.Add(new WarmDetails() { AttentionpointName = mealPlan.MealAttentionpoints, Aantal = 1 });
                        else
                            foundWarmDetails.Aantal++;
                    }
                    else if (mealPlan.MealChoiceFull == MealChoiceEnum.Warm)
                    {
                        WarmDetails foundWarmDetails = foundLeefgroepItem.BijzonderhedenAllWarmList.FirstOrDefault(x => x.AttentionpointName == mealPlan.MealAttentionpoints);

                        if (foundWarmDetails == null)
                            foundLeefgroepItem.BijzonderhedenAllWarmList.Add(new WarmDetails() { AttentionpointName = mealPlan.MealAttentionpoints, Aantal = 1 });
                        else
                            foundWarmDetails.Aantal++;
                    }
                }
            }

            return tempList.OrderBy(x => x.LeefgroepObject.MaaltijdlijstenOrderNum).ToList();
        }
        public static List<EcqareAlivioMealplanByLeefgroep> ConvertLeefgroepenForExtras(List<EcqareAlivioMealplanFile> mealPlans, List<Leefgroep> Leefgroepen)
        {
            List<EcqareAlivioMealplanByLeefgroep> tempList = new List<EcqareAlivioMealplanByLeefgroep>();
            List<Leefgroep> shownLeefgroepen = Leefgroepen.Where(x => x.IsFromEcQare && x.IsEcqareLowestLevel && !x.MaaltijdlijstenVacuumVerpakking).ToList();

            foreach (Leefgroep item in shownLeefgroepen)
            {
                tempList.Add(new EcqareAlivioMealplanByLeefgroep()
                {
                    MealDay = mealPlans.FirstOrDefault().MealDate,
                    LeefgroepName = item.DisplayNaam,
                    LeefgroepObject = item,
                    ExtrasString = ""
                });
            }

            List<EcqareAlivioMealplanFile> filteredMealplans = mealPlans.Where(x => !string.IsNullOrEmpty(x.MealExtras)).DistinctBy(x => x.ClientId).ToList();

            foreach (EcqareAlivioMealplanFile mealPlan in filteredMealplans)
            {
                EcqareAlivioMealplanByLeefgroep foundLeefgroepItem = tempList.SingleOrDefault(x => x.LeefgroepName.Equals(mealPlan.MealLocationName, StringComparison.CurrentCultureIgnoreCase));

                if (foundLeefgroepItem != null)
                {
                    if (!string.IsNullOrEmpty(foundLeefgroepItem.ExtrasString))
                        foundLeefgroepItem.ExtrasString += "\n";

                    foundLeefgroepItem.ExtrasString += mealPlan.MealExtras;
                }
            }

            return tempList.OrderBy(x => x.LeefgroepObject.MaaltijdlijstenOrderNum).ToList();
        }
        public static List<EcqareAlivioMealplanByLeefgroep> ConvertLeefgroepenForLunchpakketten(List<EcqareAlivioMealplanFile> mealPlans, List<Leefgroep> Leefgroepen)
        {
            List<EcqareAlivioMealplanByLeefgroep> tempList = new List<EcqareAlivioMealplanByLeefgroep>();
            List<Leefgroep> shownLeefgroepen = Leefgroepen.Where(x => x.IsFromEcQare && x.IsEcqareLowestLevel && !x.MaaltijdlijstenVacuumVerpakking).ToList();

            foreach (Leefgroep item in shownLeefgroepen)
            {
                tempList.Add(new EcqareAlivioMealplanByLeefgroep()
                {
                    MealDay = mealPlans.FirstOrDefault().MealDate,
                    LeefgroepName = item.DisplayNaam,
                    LeefgroepObject = item,
                    Lunchpakketten = 0
                });
            }

            foreach (EcqareAlivioMealplanFile mealPlan in mealPlans)
            {
                if (mealPlan.MealChoiceFull == MealChoiceEnum.Lunchpakket)
                {
                    EcqareAlivioMealplanByLeefgroep foundLeefgroepItem = tempList.SingleOrDefault(x => x.LeefgroepName.Equals(mealPlan.MealLocationName, StringComparison.CurrentCultureIgnoreCase));

                    if (foundLeefgroepItem != null)
                    {
                        foundLeefgroepItem.Lunchpakketten++;
                    }
                }
            }

            return tempList.OrderBy(x => x.LeefgroepObject.MaaltijdlijstenOrderNum).ToList();
        }
    }
    public class WarmDetails
    {
        public string AttentionpointName { get; set; }
        public int Aantal { get; set; }

        public override string ToString()
        {
            return Aantal.ToString() + " " + AttentionpointName;
        }
    }

    public class BudgetVervangdeMaaltijdenByLeefgroep
    {
        public DateTime? Mealdate { get; set; }
        public string MealMonth { get; set; }
        public string LeefgroepNaam { get; set; }
        public Leefgroep LeefgroepObject { get; set; }
        public string Ondersteuningsvorm { get; set; }
        public string MaaltijdTypeAlfabet { get; set; }
        public string MaaltijdType { get; set; }
        public int Aantal { get; set; }
        public int AantalOntbijt { get; set; }
        public int AantalKoud { get; set; }
        public int AantalWarm { get; set; }

        public static List<BudgetVervangdeMaaltijdenByLeefgroep> ConvertBudgetVervangdeMaaltijden(List<EcqareAlivioMealplanFile> mealPlans, List<Leefgroep> Leefgroepen)
        {
            List<BudgetVervangdeMaaltijdenByLeefgroep> tempMealplanByLeefgroepList = new List<BudgetVervangdeMaaltijdenByLeefgroep>();
            // filter alle leefgroepen naar lijst van leefgroepen voor de Budget Vervangde Maaltijden
            List<Leefgroep> shownLeefgroepen = Leefgroepen.Where(x => !string.IsNullOrEmpty(x.MaaltijdlijstenGroeperingAdministratie)).DistinctBy(x => x.MaaltijdlijstenGroeperingAdministratie).ToList();

            // maak basis maaltijdlijnen administratie aan voor verder gebruik
            foreach (Leefgroep item in shownLeefgroepen)
            {
                DateTime mealTime = mealPlans.FirstOrDefault().MealDate;
                tempMealplanByLeefgroepList.Add(new BudgetVervangdeMaaltijdenByLeefgroep()
                {
                    MealMonth = mealTime.ToString("MMMM", new CultureInfo("nl-BE")) + " " + mealTime.Year,
                    LeefgroepNaam = item.MaaltijdlijstenGroeperingAdministratie,
                    Ondersteuningsvorm = item.Ondersteuningsvorm,
                    LeefgroepObject = item
                });
            }

            // doorloop de lijnen uit Ecqare
            foreach (EcqareAlivioMealplanFile mealPlan in mealPlans)
            {
                // test check voor debugging
                if (mealPlan.MealDate <= new DateTime(2024, 11, 2))
                {
                    var a = 1;
                }
                // test check voor debugging
                if (mealPlan.MealLocationName.Contains("Villa"))
                {
                    var a = 1;
                }
                else if (mealPlan.MealLocationName.Contains("Pioen"))
                {
                    var a = 1;
                }
                Leefgroep foundLeefgroep = shownLeefgroepen.SingleOrDefault(y => mealPlan.MealLocationName.Contains(y.MaaltijdlijstenGroeperingAdministratie)); // groepering naam moet dezelfde zijn als leefgroepnaam ecare of een deel ervan!!! Geen andere manier om dit te doen. LocationId is waardeloos en linkt naar niks
                if (foundLeefgroep == null)
                {
                    foundLeefgroep = shownLeefgroepen.SingleOrDefault(y => y.MaaltijdlijstenGroeperingAdministratie.Contains(mealPlan.MealLocationName));
                }
                // check of leefgroep is gevonden
                if (foundLeefgroep != null)
                {
                    BudgetVervangdeMaaltijdenByLeefgroep foundLeefgroepItem = tempMealplanByLeefgroepList.SingleOrDefault(x => x.LeefgroepNaam.Equals(foundLeefgroep.MaaltijdlijstenGroeperingAdministratie, StringComparison.CurrentCultureIgnoreCase));

                    // check of basis daglijstlijn is gevonden
                    if (foundLeefgroepItem != null)
                    {
                        if (foundLeefgroep.IsMaaltijdAdministratieBudgetVervangend && mealPlan.MealChoiceFull == MealChoiceEnum.Kookactiviteit)
                        {
                            foundLeefgroepItem.Mealdate = foundLeefgroepItem.Mealdate != null ? foundLeefgroepItem.Mealdate : mealPlan.MealDate;
                            foundLeefgroepItem.Aantal++;
                        }
                        else if (foundLeefgroep.IsMaaltijdAdministratieGeenLeveringen)
                        {
                            if (mealPlan.MealChoiceFull == MealChoiceEnum.Ontbijt)
                            {
                                foundLeefgroepItem.Mealdate = foundLeefgroepItem.Mealdate != null ? foundLeefgroepItem.Mealdate : mealPlan.MealDate;
                                foundLeefgroepItem.AantalOntbijt++;
                            }
                            else if (mealPlan.MealChoiceFull == MealChoiceEnum.Koud)
                            {
                                foundLeefgroepItem.Mealdate = foundLeefgroepItem.Mealdate != null ? foundLeefgroepItem.Mealdate : mealPlan.MealDate;
                                foundLeefgroepItem.AantalKoud++;
                            }
                            else if (mealPlan.MealChoiceFull == MealChoiceEnum.Warm)
                            {
                                foundLeefgroepItem.Mealdate = foundLeefgroepItem.Mealdate != null ? foundLeefgroepItem.Mealdate : mealPlan.MealDate;
                                foundLeefgroepItem.AantalWarm++;
                            }
                        }
                    }
                    else
                        throw new Exception("Leefgroep not found: " + mealPlan.MealLocationName);
                }
            }

            // return lijst van daglijstlijnen
            return tempMealplanByLeefgroepList.OrderBy(x => x.LeefgroepObject.MaaltijdlijstenOrderNum).ToList();
        }

    }

    public class EcqareAlivioMealplanFile
    {
        public Guid ClientId { get; set; }
        public DateTime MealDate { get; set; }
        public string MealSchedule { get; set; }
        public MealScheduleEnum MealScheduleFull
        {
            get
            {
                if (MealChoiceFull == MealChoiceEnum.Ontbijt)
                    return MealScheduleEnum.Ochtend;
                else
                {
                    return MealSchedule.ToUpper() switch
                    {
                        "OCH" => MealScheduleEnum.Ochtend,
                        "MID" => MealScheduleEnum.Middag,
                        "AVO" => MealScheduleEnum.Avond,
                        _ => MealScheduleEnum.None
                    };
                }
            }
        }
        public bool IsHomeDelivery { get; set; }
        public string MealChoice { get; set; }
        public MealChoiceEnum MealChoiceFull
        {
            get
            {
                if (!string.IsNullOrEmpty(MealChoice))
                {
                    // idem klassieke switch
                    return MealChoice.ToUpper() switch
                    {
                        "A" => MealChoiceEnum.Warm,
                        "B" => MealChoiceEnum.Koud,
                        "C" => MealChoiceEnum.Lunchpakket,
                        "D" => MealChoiceEnum.Ontbijt,
                        "E" => MealChoiceEnum.Kookactiviteit,
                        _ => MealChoiceEnum.None,
                    };
                }
                else
                    return MealChoiceEnum.None;
            }
        }
        public Guid MealLocationId { get; set; } // ecqareId leefgroep
        public string MealLocationName { get; set; } // leefgroep

        // Data from other sources
        public Logic.DataBusinessObjects.Client Client { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
        public string MealAttentionpoints { get; set; }
        public string MealExtras { get; set; }
    }
    public enum MealChoiceEnum
    {
        None,
        Warm,
        Koud,
        Lunchpakket,
        Ontbijt,
        Kookactiviteit
    }
    public enum MealScheduleEnum
    {
        None,
        Ochtend,
        Middag,
        Avond
    }
}