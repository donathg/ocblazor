﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Logic.DataBusinessObjects.Webservices.eCQare;

public class JsonDateTimeConverter : System.Text.Json.Serialization.JsonConverter<DateTime?>
{
    public override DateTime? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType != JsonTokenType.String)
        {
            return null;
        }

        var dateString = reader.GetString();
        if (DateTime.TryParseExact(dateString, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
        {
            return dateTime;
        }

        return null;
    }

    public override void Write(Utf8JsonWriter writer, DateTime? value, JsonSerializerOptions options)
    {
        if (value.HasValue)
        {
            writer.WriteStringValue(value.Value.ToString("yyyy-MM-ddTHH:mm:ss"));
        }
        else
        {
            writer.WriteNullValue(); // Write null if the DateTime value is null
        }
    }
}

public class EcqareClientV2File
{
    public string Id { get; set; }
    public string FileNumber { get; set; }
    public string Status { get; set; }
    public Client Client { get; set; }
    public string ClientAccountancyNumber { get; set; }
    public Department MainDepartment { get; set; }
    public DepartmentLink[] Departments { get; set; }
    public Serviceagreement[] ServiceAgreements { get; set; }
    public Attentionpoint[] attentionPoints { get; set; }
}

public class Client
{

    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string LastNameFirstName { get; set; }
    public string FirstNameLastName { get; set; }
    public string Nationality { get; set; }
    public string GenderCode { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? BirthDate { get; set; }
    public string NationalNumber { get; set; }
    public bool? IsDeceased { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateOfDeath { get; set; }
    public string MotherTongue { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Mobile { get; set; }
    public string Fax { get; set; }
    public Domicileaddress DomicileAddress { get; set; }
    public Residenceaddress ResidenceAddress { get; set; }
}

public class Domicileaddress
{
    public string Street { get; set; }
    public string Number { get; set; }
    public string ZipCode { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
}

public class Residenceaddress
{
    public string Street { get; set; }
    public string Number { get; set; }
    public string ZipCode { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
}

public class Department
{
    public string Id { get; set; }
    public string ParentId { get; set; }
    public string Name { get; set; }
    public string Code { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? ActiveFrom { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? ActiveUntil { get; set; }
}

public class DepartmentLink
{
    public string Id { get; set; }
    public Department Department { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateStart { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateEnd { get; set; }
    public ModuleLink[] Modules { get; set; }
}

public class ModuleLink
{
    public string Id { get; set; }
    [JsonPropertyName("module")]
    public Module Modul { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateStart { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateEnd { get; set; }
}

public class Module
{
    public string Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public Typemodule TypeModule { get; set; }
}

public class Typemodule
{
    public string Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
}

public class Serviceagreement
{
    public string Id { get; set; }
    public Careform CareForm { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateStart { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateEnd { get; set; }
    public string Status { get; set; }
    public Module[] Modules { get; set; }
}

public class Careform
{
    public string Id { get; set; }
    public string Name { get; set; }
    public bool IsActive { get; set; }
}

public class Attentionpoint
{
    public string Id { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateStart { get; set; }
    [Newtonsoft.Json.JsonConverter(typeof(JsonDateTimeConverter))]
    public DateTime? DateEnd { get; set; }
    public string Description { get; set; } // name attentionpoint (at end of string)
    public AttentionpointType Type { get; set; }
}

public class AttentionpointType
{
    public string Id { get; set; }
    public string Name { get; set; } // parentname
    public bool IsFoodRelated { get; set; }
    public bool IsActive { get; set; }
}