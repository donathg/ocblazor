﻿using System;

namespace Logic.DataBusinessObjects.Webservices.eCQare;

public class EcqareDepartmentV2
{
    public Guid? Id { get; set; }
    public Guid? ParentId { get; set; }
    public string Name { get; set; }
    public string Code { get; set; }
    public DateTime? ActiveFrom { get; set; }
    public DateTime? ActiveUntil { get; set; }
}