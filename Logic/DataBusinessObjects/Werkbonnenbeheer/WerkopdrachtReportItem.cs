﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.DataBusinessObjects.Werkbonnenbeheer
{
    public class WerkopdrachtReportItem
    {
        public int Id { get; set; }

        public string Nummer { get; set; }

        public string Dienst { get; set; }
        public string Locatie { get; set; }

        public string BeschrijvingKort { get; set; }
        public string BeschrijvingLang { get; set; }

        public string Invetaris { get; set; }

        public string Type { get; set; }

        public string Prioriteit { get; set; }

        public DateTime BeginDatum { get; set; }

        public DateTime EindDatum { get; set; }

        public String Groepen { get; set; }

        public String Medewerkers { get; set; }


        public String AangevraagdDoor { get; set; }

        public String Status { get; set; }

        public String StatusOpmerking { get; set; }
    }

}
