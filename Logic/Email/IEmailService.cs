﻿ 
using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.EmailNS
{
    public interface IEmailService
    {
        void SendEmail(String[] to, String from, String subject, String body);
    }
}
