﻿using DataBase.Services;
using Logic.DataBusinessObjects.ExitFormulier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.ExitFormulier
{
    public class ExitFormService : IExitFromService
    {
        private readonly IExitFormRepository _exitFromRepository;

        public ExitFormService(IExitFormRepository exitFormRepository)
        {
            _exitFromRepository = exitFormRepository;
        }

        public void SaveExitFormItem(ExitFormItem selectedExitForm)
        {
            _exitFromRepository.CreateExitFormItem(selectedExitForm);
        }
    }
}
