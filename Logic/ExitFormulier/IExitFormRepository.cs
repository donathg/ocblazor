﻿using Logic.DataBusinessObjects.ExitFormulier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.ExitFormulier
{
    public interface IExitFormRepository
    {
        void CreateExitFormItem(ExitFormItem selectedExitForm);
    }
}
