 
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Common.CloneExtensions;
using Logic.Cache;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.ExternalDashboard;
using Microsoft.Extensions.Caching.Memory;
//using Serilog;

namespace Logic.ExternalDashboard
{
    public class ExternalDashboardService : IExternalDashboardService
    {
        private readonly IExternalDashboardRepository _externalDashboardRepository;
        private readonly ICache _cache;

        public ExternalDashboardService(IExternalDashboardRepository externalDashboardRepository)
        {
            _externalDashboardRepository = externalDashboardRepository;
        }
        public List<ExternalDashboardMedewerkers> GetExternalDashboardMedewerkers(OrbisDates dates)
        {
            return JoinGroups(_externalDashboardRepository.GetExternalDashboardMedewerkers(dates));
        }
        private static List<ExternalDashboardMedewerkers> JoinGroups(List<ExternalDashboardMedewerkers> groepen)
        {
            AddDataGroepAToGroepB(groepen, "AT - Personeel", "AT - Teamcoach");
            AddDataGroepAToGroepB(groepen, "BMW/STAF", "Dienst zinzorg en pastoraat");
            AddDataGroepAToGroepB(groepen, "BMW/STAF", "Preventieadviseur");
            AddDataGroepAToGroepB(groepen, "KK Dagbesteding", "TC - Dagondersteuning");
            AddDataGroepAToGroepB(groepen, "Co�rdinatoren/verantw. zorg", "Weerbaarheidscoach");
            AddDataGroepAToGroepB(groepen, "IO Team 1", "IO Team PA");

            return groepen;
        }
        private static void AddDataGroepAToGroepB(List<ExternalDashboardMedewerkers> lijstGroepen, string stringGroepToKeep, string stringGroepToRemove)
        {
            ExternalDashboardMedewerkers groepToKeep = lijstGroepen.Find(x => x.GroepCode == stringGroepToKeep);
            ExternalDashboardMedewerkers groepToRemove = lijstGroepen.Find(x => x.GroepCode == stringGroepToRemove);

            groepToKeep.Voltijds += groepToRemove.Voltijds;
            groepToKeep.Deeltijds += groepToRemove.Deeltijds;
            groepToKeep.Aanwerving += groepToRemove.Aanwerving;
            groepToKeep.UitDienst += groepToRemove.UitDienst;
            groepToKeep.ZiekteMinderJaar += groepToRemove.ZiekteMinderJaar;
            groepToKeep.ZiekteMeerMaand += groepToRemove.ZiekteMeerMaand;
            groepToKeep.ZiekteMeerJaar += groepToRemove.ZiekteMeerJaar;
            groepToKeep.ZiekteZonderBriefje += groepToRemove.ZiekteZonderBriefje;
            groepToKeep.Arbeidsongeval += groepToRemove.Arbeidsongeval;
            groepToKeep.Werkhervatting += groepToRemove.Werkhervatting;
            groepToKeep.VormingsurenExtern += groepToRemove.VormingsurenExtern;

            lijstGroepen.Remove(groepToRemove);
        }
    }
}
