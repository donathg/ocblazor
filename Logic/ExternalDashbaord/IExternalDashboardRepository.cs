﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.ExternalDashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.ExternalDashboard
{
    public interface IExternalDashboardRepository
    {
        List<ExternalDashboardMedewerkers> GetExternalDashboardMedewerkers(OrbisDates filter);
    }
}
