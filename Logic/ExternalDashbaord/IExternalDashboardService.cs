﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.ExternalDashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.ExternalDashboard
{
    public interface IExternalDashboardService
    {
        List<ExternalDashboardMedewerkers> GetExternalDashboardMedewerkers(OrbisDates dates);
    }   
}
