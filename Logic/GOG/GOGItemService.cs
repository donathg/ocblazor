﻿using DataBase.Services;
using Logic.ArtikelNs;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Aankoop;
using Logic.DataBusinessObjects.GOG;
using Logic.LeefgroepNS;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.GOG
{
    public class GOGItemService : IGOGItemService
    {
        private readonly IGOGItemRepository _gogRepository;
        private readonly IUserService _userService;
        private readonly ILeefgroepService _leefgroepService;

        public GOGItemService(IGOGItemRepository gogRepository, IUserService userService, ILeefgroepService leefgroepService)
        {
            _userService = userService;
            _gogRepository = gogRepository;
            _leefgroepService = leefgroepService;   
        }

        public List<GOGItem> GetGOGItemsForLeefgroepenAndOwn(List<Leefgroep> mijnLeefgroepen, DateTime startDate, DateTime EndDate  )
        {
            return _gogRepository.SelectAllGOGItemsForLeefgroepenAndOwn(mijnLeefgroepen.Select(x=>x.Id).ToList(),startDate,EndDate);
        }
        public List<GOGItem> GetGOGItemsForBewonersAndOwn(List<int> mijnClienten, DateTime startDate, DateTime EndDate  )
        {
            return _gogRepository.SelectAllGOGItemsForBewonersAndOwn(mijnClienten, startDate, EndDate);
        }
        //public List<GOGItem> GetAllGOGItems()
        //{
        //    return _gogRepository.SelectAllGOGItems();
        //}
        public GOGItem GetGOGItemFromId(int Id)
        {
            return _gogRepository.SelectGOGItemFromId(Id);
        }
        public void SaveGOGItem(GOGItem item)
        {
            _gogRepository.SaveGogItem(item);
        }
        public List<GOGItem> GetAllCrisisgroepItemsItems()
        {
            return _gogRepository.SelectAllCrisisgroepItemsItems();
        }
    }
}
