﻿using Logic.DataBusinessObjects.GOG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.GOG
{
    public interface IGOGItemRepository
    {

        List<GOGItem> SelectAllGOGItemsForLeefgroepenAndOwn(List<int> leefgroepIds, DateTime startDate, DateTime EndDate        );
        List<GOGItem> SelectAllGOGItemsForBewonersAndOwn(List<int> bewonersIds, DateTime startDate, DateTime EndDate    );
        //List<GOGItem> SelectAllGOGItems();
        List<GOGItem> SelectAllCrisisgroepItemsItems();
        GOGItem SelectGOGItemFromId(int id);
        void SaveGogItem(GOGItem item);
    }
}
