﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.GOG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.GOG
{
    public interface IGOGItemService
    {
        //List<GOGItem> GetAllGOGItems();
        List<GOGItem> GetGOGItemsForLeefgroepenAndOwn(List<Leefgroep> mijnLeefgroepen, DateTime startDate, DateTime EndDate);
        List<GOGItem> GetGOGItemsForBewonersAndOwn(List<int> mijnClienten, DateTime startDate, DateTime EndDate);
        List<GOGItem> GetAllCrisisgroepItemsItems();
        GOGItem GetGOGItemFromId(int Id);
        void SaveGOGItem(GOGItem item);
    }
}
