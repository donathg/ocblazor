﻿using Logic.DataBusinessObjects.GOG;
using Logic.DataBusinessObjects.MultiSelectNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.GOG
{
    public interface IGOGReportService
    {
        GogReport GetGogReport(GOGItem gogItem, List<MultiSelect> AllMultiSelect);
    }
}
