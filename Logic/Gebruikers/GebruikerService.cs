 
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Common.CloneExtensions;
using Logic.Cache;
using Logic.DataBusinessObjects;
using Microsoft.Extensions.Caching.Memory;
//using Serilog;

namespace Logic.Gebruikers
{
    public class GebruikerService : IGebruikerService, ICaching
    {
        private readonly IGebruikerRepository _gebruikerRepository;
        private readonly ICache _cache;

        public GebruikerService(IGebruikerRepository gebruikerRepository, ICache cache)
        {
            _gebruikerRepository = gebruikerRepository;
            _cache = cache;

        }
        public string CacheKey => CacheKeys.Gebruikers;

        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        private List<Gebruiker> GetSetFromCache()
        {
            List<Gebruiker> lfgsCache = _cache.Get<List<Gebruiker>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Gebruiker>>(CacheKey);
        }
        public  void FillCache()
        {
          //  Log.Information("Loading gebruiker-cache");

            List<Gebruiker> lfgs = _gebruikerRepository.GetAllGebruikers();
            _cache.Set(CacheKey, lfgs);
        }

        public Gebruiker GetGebruikerFromId(int id)
        {
            List<Gebruiker> gebruikers = GetSetFromCache();
            return gebruikers.Where(x=>x.Id==id).FirstOrDefault().DeepClone();
     
        }
        public List<Gebruiker> GetAllGebruikers()
        {
            List<Gebruiker> gebruikers = GetSetFromCache();
            return gebruikers.DeepClone();
        }
        public List<Gebruiker> GetAllActiveGebruikers()
        {
            List<Gebruiker> gebruikers = GetSetFromCache();
            return gebruikers.Where(x => x.Active).DeepClone().ToList();
        }
    }
}
