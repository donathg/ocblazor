﻿using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Gebruikers
{
    public interface IGebruikerRepository
    {
        List<Gebruiker> GetAllGebruikers();
    }
}
