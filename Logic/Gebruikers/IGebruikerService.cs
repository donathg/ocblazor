﻿using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.Gebruikers
{
    public interface IGebruikerService
    {
        Gebruiker GetGebruikerFromId(int id);
        List<Gebruiker> GetAllGebruikers();
        List<Gebruiker> GetAllActiveGebruikers();       
    }   
}
