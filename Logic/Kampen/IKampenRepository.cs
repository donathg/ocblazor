﻿using System;
using System.Collections.Generic;
using System.Text;
using Logic.DataBusinessObjects;

namespace Logic.Kampen
{
    public interface IKampenRepository 
    {
        List<Kamp> GetKampen();
    }
}
