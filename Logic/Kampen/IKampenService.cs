﻿using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace Logic.Kampen
{
    public interface IKampenService
    {

        List<Kamp> GetKampenActief();
        List<Kamp> GetKampen();
        Kamp GetKampenById(int id);
    }
   
}
