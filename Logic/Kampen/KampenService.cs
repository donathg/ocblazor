 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Data;
using Logic.WagenNS;
using Database.DataAccessObjects;
using Microsoft.Extensions.Caching.Memory;
using Logic.Cache;
using Logic.DataBusinessObjects;
//using Serilog;
using Common;
using Common.CloneExtensions;

namespace Logic.Kampen
{
    public class KampenService : ICaching, IKampenService
    {
        private const string ApiUrlBase = "kampen";
       private IKampenRepository _kampenRepository;
        private readonly ICache _cache;
        public string CacheKey => CacheKeys.Kampen;
        public KampenService(IKampenRepository kampenRepository, ICache cache)
        {
            _kampenRepository = kampenRepository;
            _cache = cache;
        }

       

 

        public void FillCache()
        {
         //   Log.Information("Loading kampen-cache");

            List<Kamp> kampen = _kampenRepository.GetKampen();
            _cache.Set<List<Kamp>>(CacheKey, kampen);
        }

       
        public void ClearCache()
        {
           _cache.Clear(CacheKey);
        }
        private List<Kamp> GetSetFromCache()
        {

            List<Kamp> kampenCache = _cache.Get<List<Kamp>>(CacheKey);
            if (kampenCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Kamp>>(CacheKey);
        }

        public List<Kamp> GetKampen()
        {
            return GetSetFromCache().DeepClone();
        }
        public List<Kamp> GetKampenActief()
        {
            return GetSetFromCache().Where(x=>x.Active).DeepClone().ToList();
        }
        public Kamp GetKampenById(int id)
        {
            return GetSetFromCache().Where(x => x.Id==id).FirstOrDefault().DeepClone() ;
        }
       
    }

     
}
