﻿using Database.DataAccessObjects;
using System;
using System.Collections.Generic;
using System.Text;
using Logic.DataBusinessObjects;
namespace Logic.Kasbeheer
{
    public interface IKasbeheerRepository
    {
        List<KasbeheerTransactie> GetTransacties(int leefgroepId);
        List<Leefgroep> GetLeefgroepRechten(int userId);
        List<Leefgroep> GetLeefgroepTesting();
        KasbeheerTransactie GetTransactie(int transactieId);

        KasbeheerTransactie SaveTransactie(int leefgroepId, KasbeheerTransactie transactie);
       
        void DeleteTransactie(int transactieId);
        List<KasbeheerTransactieCodering> GetKasbeheerTransactieCoderingen();
         
        List<KasbeheerTransactieType> GetKasbeheerTransactieTypes();
        List<KasbeheerRekening> GetRekeningen(int leefgroepId);
        List<KasbeheerTransactieClientInfo> GetSelectedClientenForTransactie(int transactieId);
        List<KasbeheerTransactieClientInfo> GetPreviousUsedClientsForLeefgroepTransactie(int transactieId);
        
    }
}
