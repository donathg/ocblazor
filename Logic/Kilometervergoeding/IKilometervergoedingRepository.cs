﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.DataBusinessObjects.Werkbonnenbeheer;

namespace Logic.Werkbonnenbeheer
{
    public interface IKilometervergoedingRepository
    {
        List<Kilometer> GetKilometers(int userId);
        void SaveKilometers(Kilometer km);
        List<Client> GetClienten(int kmId);
        List<Kamp> GetKampen(int kmId);
        List<Leefgroep> GetLeefgroepen(int kmId);
        void DeleteKilometers(int kmId);
        List<String> GetRedenSuggestions(int userId, int wagenId, String typeCode);
        List<String> GetRouteSuggestions(int userId, int wagenId, String typeCode);
        VerklaringResult GetVerklaringResult(int gebruikerId, string batchNummer);
        void SaveVerklaringResult(VerklaringResult result);
    }
}
