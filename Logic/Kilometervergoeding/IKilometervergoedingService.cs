﻿ 
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.Kilometervergoeding
{
    public interface IKilometervergoedingService
    {
       Task<List<Kilometer>> GetKilometers();
        void SaveKilometers(Kilometer km);

        Task<List<Client>> GetClienten(int kmId);

        Task<List<Leefgroep>> GetLeefgroepen(int kmId);

        Task<List<Kamp>> GetKampen(int kmId);

        void DeleteKilometers(int kmId);
        List<String> GetRedenSuggestions(int wagenId, String typeCode);
        List<String> GetRouteSuggestions( int wagenId, String typeCode);

        List<VerklaringItem> GetVerklaringItems();

        VerklaringResult GetVerklaringResult(int gebruikerId, string batchNummer);
        void SaveVerklaringResult(VerklaringResult result);
    }
}
