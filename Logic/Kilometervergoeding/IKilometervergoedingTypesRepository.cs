﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.DataBusinessObjects.Werkbonnenbeheer;

namespace Logic.Werkbonnenbeheer
{
    public interface IKilometervergoedingTypesRepository
    {
        List<KilometerType> GetKilometerTypes();
 
    }
}
