﻿ 
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.Kilometervergoeding
{
    public interface IKilometervergoedingTypesService
    {
       List<KilometerType> GetKilometersTypes();
       KilometerType GetKilometerByCode(String code);
    }
}
