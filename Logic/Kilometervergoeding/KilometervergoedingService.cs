﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Logic.ArtikelNs;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.Werkbonnenbeheer;
using DataBase.Services;

namespace Logic.Kilometervergoeding 
{
    public class KilometervergoedingService : IKilometervergoedingService
    {
        IKilometervergoedingRepository _kilometerRepository;
        IUserService _userService;

        ICache _cache;
        public KilometervergoedingService(IKilometervergoedingRepository _kilometerRepository, ICache cache, IUserService _userService) 
        {
            this._userService = _userService;
            this._kilometerRepository = _kilometerRepository;
            _cache = cache;
        }

        public void SaveKilometers(Kilometer km)
        {
            _kilometerRepository.SaveKilometers(km);
        }
        public async Task<List<Kilometer>> GetKilometers()
        {
            return await Task.FromResult(_kilometerRepository.GetKilometers(_userService.LoggedOnUser.UserId));
        }
        public async Task<List<Client>> GetClienten(int kmId)
        {
            return await Task.FromResult(_kilometerRepository.GetClienten(kmId));
        }
        public async Task<List<Leefgroep>> GetLeefgroepen(int kmId)
        {
            return await Task.FromResult(_kilometerRepository.GetLeefgroepen(kmId));
        }
        public async Task<List<Kamp>> GetKampen(int kmId)
        {
            return await Task.FromResult(_kilometerRepository.GetKampen(kmId));
        }
        public void DeleteKilometers(int kmId)
        {
            _kilometerRepository.DeleteKilometers(kmId);
        }
              
        public List<String> GetRedenSuggestions( int wagenId, String typeCode)
        {
            return _kilometerRepository.GetRedenSuggestions(_userService.LoggedOnUser.UserId,  wagenId,  typeCode);
        }
        public List<String> GetRouteSuggestions(int wagenId, String typeCode)
        {
            return _kilometerRepository.GetRouteSuggestions(_userService.LoggedOnUser.UserId, wagenId, typeCode);
        }

        public List<VerklaringItem> GetVerklaringItems()
        {
            List<VerklaringItem> list = new List<VerklaringItem>
            {
                new VerklaringItem() { Id = 1, Text = "Ik kom 100% met de wagen" },
                new VerklaringItem() { Id = 2, Text = "Ik kom 100% met de fiets" },
                new VerklaringItem() { Id = 3, Text = "Combinatie wagen – fiets : ik kom 80% of meer met de wagen (ik ontvang geen fietsvergoeding)" },
                new VerklaringItem() { Id = 4, Text = "Combinatie 80% of meer met de fiets (ik ontvang enkel fietsvergoeding)" },
                new VerklaringItem() { Id = 5, Text = "Combinatie meer dan 20% maar minder dan 80% met de fiets = 50/50 regeling" },
                new VerklaringItem() { Id = 0, Text = "Ik heb een combinatie abonnement openbaar vervoer : neem contact op met Personeelsdienst" }
            };

            return list;
        }
        public VerklaringResult GetVerklaringResult(int gebruikerId, string batchNummer)
        {
            return _kilometerRepository.GetVerklaringResult( gebruikerId,  batchNummer);
        }
        public void SaveVerklaringResult(VerklaringResult result)
        {
            result.ModificationDate =  DateTime.Now;    
            _kilometerRepository.SaveVerklaringResult(result);
        }
    }
}
