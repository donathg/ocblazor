﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Logic.ArtikelNs;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.Werkbonnenbeheer;
using Serilog;

namespace Logic.Kilometervergoeding 
{
    public class KilometervergoedingTypeService : IKilometervergoedingTypesService
    {
        IKilometervergoedingTypesRepository _kilometerTypeRepository;
 
        ICache _cache;
        public KilometervergoedingTypeService(IKilometervergoedingTypesRepository _kilometerRepository, ICache cache)
        {

            this._kilometerTypeRepository = _kilometerRepository;
            _cache = cache;
        }

        public String CacheKey
        {

            get { return CacheKeys.KilometerTypes; }
        }

        public   KilometerType GetKilometerByCode(String code)
        {
            return GetKilometersTypes().Where(x => x.Code == code).FirstOrDefault();
        }
        
        public List<KilometerType> GetKilometersTypes()
        {
            return GetSetFromCache();
        }
       
        public void FillCache()
        {
         //   Log.Information("Loading kilometer-cache");
            List<KilometerType> types = _kilometerTypeRepository.GetKilometerTypes();
            _cache.Set<List<KilometerType>>(CacheKey, types);
        }
        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        public List<KilometerType> GetSetFromCache()
        {

            List<KilometerType> lfgsCache = _cache.Get<List<KilometerType>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<KilometerType>>(CacheKey);
        }

       
    }
}
