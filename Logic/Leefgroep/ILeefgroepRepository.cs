﻿using System;
using System.Collections.Generic;
using System.Text;
using Logic.DataBusinessObjects;

namespace Logic.DataBusinessObjects
{
    public interface ILeefgroepRepository 
    {
    List<Leefgroep> GetLeefgroepen();
    }
}
