﻿using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace Logic.LeefgroepNS
{
    public interface ILeefgroepService
    {
        List<Leefgroep> GetAllActiveLeefgroepen(bool onlyLowestLevel);
        List<Leefgroep> GetAllLeefgroepen();
        List<Leefgroep> GetLeefgroepenLoggedOnUser(string moduleCode);
        Leefgroep  GetLeefgroepFromId(int id);

        List<Leefgroep> GetLeefgroepenMaaltijdlijstenKeuken();
        List<Leefgroep> GetLeefgroepenMaaltijdlijstenAdministratie();
    }
   
}
