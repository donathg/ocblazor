 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Data;

using Logic.DataBusinessObjects;
 

using Database.DataAccessObjects;
using Microsoft.Extensions.Caching.Memory;
using Logic.Cache;
using DataBase.Services;
//using Serilog;
using Common;
using Common.CloneExtensions;

namespace Logic.LeefgroepNS
{
    public class LeefgroepService : ILeefgroepService, ICaching
    {
        private const string ApiUrlBase = "leefgroep";
        private ILeefgroepRepository _leefgroepRepository;
        private readonly ICache _cache;
        public string CacheKey => CacheKeys.Leefgroepen;

        private readonly IUserService _userService;

        public LeefgroepService( ILeefgroepRepository leefgroepRepository, ICache cache, IUserService userService)
        {
            _leefgroepRepository = leefgroepRepository;
            _userService = userService;
            _cache = cache;
        }

        public Leefgroep  GetLeefgroepFromId(int id)
        {
            return GetSetFromCache().Where(x => x.Id == id).FirstOrDefault().DeepClone();
        }
        public List<Leefgroep> GetLeefgroepenMaaltijdlijstenKeuken()
        { 
            List<Leefgroep> lfgs = GetSetFromCache();
            return lfgs.Where(x => x.IsFromEcQare && x.IsEcqareLowestLevel && x.HeeftMaaltijdLevering && x.Actief).ToList();
        }
        public List<Leefgroep> GetLeefgroepenMaaltijdlijstenAdministratie()
        {
            List<Leefgroep> lfgs = GetSetFromCache();
            return lfgs.Where(x => x.IsFromEcQare && x.IsEcqareLowestLevel && (x.IsMaaltijdAdministratieBudgetVervangend || x.IsMaaltijdAdministratieGeenLeveringen) && x.Actief).ToList();
        }
        public List<Leefgroep> GetLeefgroepenLoggedOnUser(string moduleCode)
        {
            if (moduleCode == "GOG" && _userService.LoggedOnUser.HasAccess("4000"))
                return GetAllLeefgroepen();

            List<Leefgroep> lfgs =  GetSetFromCache();
            List<int> leefgroepIds = this._userService.LoggedOnUser.GetLeefgroepen(moduleCode,"GLOBAL");
            return lfgs.Where(x => leefgroepIds.Contains(x.Id)).OrderBy(x => x.Name).DeepClone().ToList(); 
        }
        public  List<Leefgroep> GetAllLeefgroepen()
        {
            return GetSetFromCache().DeepClone();
        }
        public List<Leefgroep> GetAllActiveLeefgroepen(bool onlyLowestLevel)
        {
            return GetSetFromCache().Where(x => x.Actief && x.IsEcqareLowestLevel == onlyLowestLevel).ToList().DeepClone();
        }
        public void FillCache()
        {
            List<Leefgroep> lfgs = _leefgroepRepository.GetLeefgroepen();
            _cache.Set<List<Leefgroep>>(CacheKey, lfgs);
        }
        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        private List<Leefgroep> GetSetFromCache()
        {
            List<Leefgroep> lfgsCache = _cache.Get<List<Leefgroep>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Leefgroep>>(CacheKey);
        }

    }
}
