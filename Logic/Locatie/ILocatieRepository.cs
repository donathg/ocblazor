﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;

namespace Logic.LocatieNs
{
    public interface ILocatieRepository
    {
        List<Logic.DataBusinessObjects.Locatie> GetLocaties();
 
    }
}
