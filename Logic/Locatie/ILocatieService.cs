﻿ 
using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.LocatieNs
{
    public interface ILocatieService
    {


        List<Locatie> GetLocatiesAsFlatList();

        Locatie GetLocatiesAsTree();

        Locatie GetLocatieById(int id);
    }
}
