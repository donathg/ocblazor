﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
//using Serilog;
using Common;
using Common.CloneExtensions;

namespace Logic.LocatieNs
{
    public class LocatieService : ILocatieService, ICaching
    {
        ILocatieRepository _artikelRepository;

        ICache _cache;
        public LocatieService( ILocatieRepository artikelRepository, ICache cache)
        {
 
            _artikelRepository = artikelRepository;
            _cache = cache;
      
        }
        public string CacheKey => CacheKeys.Locaties;
        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        public void FillCache()
        {
         //   Log.Information("Loading locatie-cache");
            List<Locatie> locaties = _artikelRepository.GetLocaties();
            _cache.Set<List<Locatie>>(CacheKey, locaties);
        }
        private List<Locatie> GetSetFromCache()
        {
            List<Locatie> lfgsCache = _cache.Get<List<Locatie>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Locatie>>(CacheKey);
        }
        public List<Locatie> GetLocatiesAsFlatList()
        {
          return GetSetFromCache().DeepClone();
        }

    public Locatie GetLocatiesAsTree()
        {
            return GetSetFromCache().DeepClone()[0];
        }

        public Locatie GetLocatieById(int id)
        {
            return GetSetFromCache().Where(x=>x.Id == id).DeepClone().FirstOrDefault();
        }
  

  











    }
}
