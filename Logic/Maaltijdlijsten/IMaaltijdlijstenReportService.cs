﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.GOG;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Maaltijdlijsten.Administratie;
using Logic.DataBusinessObjects.MultiSelectNS;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Prijzen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Maaltijdlijsten
{
    public interface IMaaltijdlijstenReportService
    {
        public MaaltijdlijstenExtra GetMaaltijdlijstenExtra(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList);
        public MaaltijdlijstenLeefgroepenDaglijst GetMaaltijdlijstenLeefgroepenDaglijstReportData(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList);
        public MaaltijdlijstenLeefgroepenDaglijstWeekend GetMaaltijdlijstenLeefgroepenDaglijstWeekendReportData(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList);
        public MaaltijdlijstenLeefgroepenTotalen GetLeefgroepenTotalenReportData(List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanFile> mealPlanList);
        public MaaltijdlijstenLunchpakketten GetMaaltijdlijstenLunchpakketten(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList);
        public MaaltijdlijstenVacuumDaglijst GetMaaltijdlijstenVacuumDaglijst(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList);
        public MaaltijdlijstenVacuumTotalen GetMaaltijdlijstenVacuumTotalen(List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanFile> mealPlanList);
        public MaaltijdenAdministratieBudgetVervangend GetMaaltijdenBudgetVervangendData(List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem reportItem, DateTime startDatum, DateTime eindDatum, List<BudgetVervangdeMaaltijdenByLeefgroep> ecqareAlivioMealplanFiles, IPrijzenService _prijzenService);
        public MaaltijdenAdministratieGeenLevering GetMaaltijdenGeenLevering(List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem reportItem, DateTime startDatum, DateTime eindDatum, List<BudgetVervangdeMaaltijdenByLeefgroep> ecqareAlivioMealplanFiles, IPrijzenService _prijzenService);
    }
}
