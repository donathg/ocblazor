﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Modules
{
    public enum ModuleType
    {
        WERKOPDRACHT,
        INVENTARIS,
        MAALTIJDLIJSTEN,
        GOG,
        WEBSERVICES_ACERTA_PERSONEEL_XML,
        ONKOSTENNOTA
    }
}
