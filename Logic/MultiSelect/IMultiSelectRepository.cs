﻿using System;
using System.Collections.Generic;
using System.Text;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.MultiSelectNS;

namespace Logic.DataBusinessObjects
{
    public interface IMultiSelectRepository
    {
        List<MultiSelect> GetAll();
    }
}
