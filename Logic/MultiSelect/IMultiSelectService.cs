﻿using Database.DataAccessObjects;
using Logic.DataBusinessObjects;

using Logic.DataBusinessObjects.MultiSelectNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace Logic.MultiSelectNS
{
    public interface IMultiSelectService
    {


       // MultiSelect GetClone(int id);
        List<MultiSelect> GetAll(String moduleCode);
        // List<MultiSelectTreeItem> GetAllCloneAsTree(String moduleCode);
        List<MultiSelectTreeItem> ConvertToTree(List<MultiSelect> multiSelectList);
    }
   
}
