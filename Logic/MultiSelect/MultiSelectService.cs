 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Data;

using Logic.DataBusinessObjects;
 

using Database.DataAccessObjects;
using Microsoft.Extensions.Caching.Memory;
using Logic.Cache;
using Logic.LeefgroepNS;
using Logic.DataBusinessObjects.MultiSelectNS;
using Common;
using Logic.Prijzen;

namespace Logic.MultiSelectNS
{
    public class MultiSelectService : IMultiSelectService, ICaching
    {
        private const string ApiUrlBase = "multiselect";

        private IMultiSelectRepository _multiSelectRepository;
        private readonly ICache _cache;
        public string CacheKey => CacheKeys.MultiSelect;

        public MultiSelectService(IMultiSelectRepository multiSelectRepository, ICache cache)
        {
            _multiSelectRepository = multiSelectRepository;
            _cache = cache;
        }


        #region Cache Logic
        public void FillCache()
        {
            List<MultiSelect> lfgs = _multiSelectRepository.GetAll();
            _cache.Set<List<MultiSelect>>(CacheKey, lfgs);
        }
        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        private List<MultiSelect> GetSetFromCache()
        {
            List<MultiSelect> lfgsCache = _cache.Get<List<MultiSelect>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<MultiSelect>>(CacheKey);
        }
        #endregion
        public List<MultiSelect> GetAll(String moduleCode)
        {
            return GetSetFromCache().Where(x => x.ModuleCode == moduleCode).ToList();
        }
        public List<MultiSelectTreeItem> ConvertToTree(List<MultiSelect> multiSelectList)
        {
           // List<MultiSelect> MultiSelectList = GetSetFromCache().Where(x => x.ModuleCode == moduleCode).Select(x => x.Clone()).ToList();
            List<MultiSelectTreeItem> TreeItems = new List<MultiSelectTreeItem>();
            foreach (var item in multiSelectList)
            {

                MultiSelectTreeItem p = new MultiSelectTreeItem() { Id = item.Id, Name = item.Title, SourceItem = item };
                TreeItems.Add(p);
                foreach (var childItem in item.Items)
                {
                    MultiSelectTreeItem c = new MultiSelectTreeItem() { Id = childItem.Id, Name = childItem.Description, SourceItem = childItem };
                    p.Children.Add(c);
                }
            }
            return TreeItems;
        }
        //public List<MultiSelectTreeItem> GetAllCloneAsTree(String moduleCode)
        //{
        //    List<MultiSelect> MultiSelectList = GetSetFromCache().Where(x => x.ModuleCode == moduleCode).Select(x => x.Clone()).ToList();
        //    List<MultiSelectTreeItem> TreeItems = new List<MultiSelectTreeItem>();
        //    foreach (var item in MultiSelectList)
        //    {

        //        MultiSelectTreeItem p = new MultiSelectTreeItem() { Id = item.Id, Name = item.Title, SourceItem = item };
        //        TreeItems.Add(p);
        //        foreach (var childItem in item.Items)
        //        {
        //            MultiSelectTreeItem c = new MultiSelectTreeItem() { Id = childItem.Id, Name = childItem.Description, SourceItem = childItem };
        //            p.Children.Add(c);  
        //        }
        //    }
        //    return TreeItems;
        //}

        //public void FillCache()
        //{
        //    List<MultiSelect> items = _multiSelectRepository.GetAll();
        //    _cache.Set<List<MultiSelect>>(CacheKey, items);
        //}
        //public void ClearCache()
        //{
        //    _cache.Clear(CacheKey);
        //}
        //private List<MultiSelect> GetSetFromCache()
        //{
        //    List<MultiSelect> cachedObjects = _cache.Get<List<MultiSelect>>(CacheKey);
        //    if (cachedObjects == null)
        //    {
        //        FillCache();
        //    }
        //    return _cache.Get<List<MultiSelect>>(CacheKey);
        //}
    }

     
}
