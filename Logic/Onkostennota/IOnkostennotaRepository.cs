﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Aankoop;
using Logic.DataBusinessObjects.Onkostennota;

namespace Logic.ArtikelNs
{
    public interface IOnkostennotaRepository
    {

        List<OnkostennotaItem> LoadOnkostenNotas();

        List<OnkostennotaItem> LoadMaaltijdregistraties();
        List<MaaltijdAandachtspuntenItem> LoadAandachtspunten();


        List<OnkostennotaCategorieItem> GetCategorien();
        OnkostennotaItem InsertOnkostenNota(OnkostennotaItem item, string moduleCode);
        OnkostennotaItem UpdateOnkostenNotaMetAkkoordMetVoorwaarden(OnkostennotaItem item);

        void DeleteOnkostenNota(OnkostennotaItem item);

    }
}
