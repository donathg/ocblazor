﻿ 
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Aankoop;
using Logic.DataBusinessObjects.Onkostennota;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.ArtikelNs
{
    public interface IOnkostennotaService 
    {

        bool IsNietBegeleidendPesoneel(int categorieId);
        bool IsBegeleidendPesoneel(int categorieId);
        List<OnkostennotaItem> LoadOnkostenNotas();
        List<OnkostennotaItem> LoadMaaltijdregistraties();

        List<MaaltijdAandachtspuntenItem> LoadAandachtspunten();

        List<OnkostennotaCategorieItem> GetCategorienOnkostenNota();

        List<OnkostennotaCategorieItem> GetCategorienMaaltijdRegistraties();

        OnkostennotaItem InsertOnkostenNota(OnkostennotaItem item, string moduleCode);
        OnkostennotaItem UpdateOnkostenNota(OnkostennotaItem item);
        void DeleteOnkostenNota(OnkostennotaItem item);
        



    }
}
