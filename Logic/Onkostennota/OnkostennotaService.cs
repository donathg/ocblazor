﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Logic.ArtikelNs;
using Logic.Clienten;
using Logic.DataBusinessObjects.Aankoop;
using System.Collections.ObjectModel;
using Logic.LeefgroepNS;
using Logic.DataBusinessObjects.Onkostennota;

namespace Logic.AankoopNs
{
    public class OnkostennotaService : IOnkostennotaService
    {
        IOnkostennotaRepository _onkostennotaRepository = null;
        public OnkostennotaService(IOnkostennotaRepository onkostennotaRepository)
        {
            (_onkostennotaRepository) = (onkostennotaRepository);

        }



       public bool IsNietBegeleidendPesoneel(int categorieId)
       {
            
                return categorieId == 7;
       }
       public bool IsBegeleidendPesoneel(int categorieId)
       {
            return categorieId == 5;
        }

        public List<OnkostennotaItem> LoadOnkostenNotas()
        {
           return _onkostennotaRepository.LoadOnkostenNotas();
        }

        public List<OnkostennotaItem> LoadMaaltijdregistraties()
        {
            return _onkostennotaRepository.LoadMaaltijdregistraties();
        }
        public List<MaaltijdAandachtspuntenItem> LoadAandachtspunten()
        {
            return _onkostennotaRepository.LoadAandachtspunten();
        }
 
        public List<OnkostennotaCategorieItem> GetCategorienOnkostenNota()
        {
            return _onkostennotaRepository.GetCategorien().Where(x=>x.ModuleCode== "ONKOSTENNOTA").ToList();
        }
        public List<OnkostennotaCategorieItem> GetCategorienMaaltijdRegistraties()
        {
            return _onkostennotaRepository.GetCategorien().Where(x => x.ModuleCode == "MAALTIJDREG").ToList();
        }

        public OnkostennotaItem UpdateOnkostenNota(OnkostennotaItem item)
        {
            return _onkostennotaRepository.UpdateOnkostenNotaMetAkkoordMetVoorwaarden(item);
        }
        public OnkostennotaItem InsertOnkostenNota(OnkostennotaItem item, string moduleCode)
        {
            return _onkostennotaRepository.InsertOnkostenNota(item, moduleCode);
        }
        public void DeleteOnkostenNota(OnkostennotaItem item)
        {
             _onkostennotaRepository.DeleteOnkostenNota(item);
        }

        



    }
}
