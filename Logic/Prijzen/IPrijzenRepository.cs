﻿using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Prijzen
{
    public interface IPrijzenRepository
    {
        List<StandaardPrijzen> SelectAllPrijzen();
    }
}
