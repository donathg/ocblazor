﻿using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Prijzen
{
    public interface IPrijzenService
    {
        List<StandaardPrijzen> GetAllPrijzenByDatum(DateTime date);
        List<StandaardPrijzen> GetPrijzenByModuleCode_Datum(string moduleCode, DateTime date);
        StandaardPrijzen GetPrijzenByModuleCode_Datum_Subtypes(string moduleCode, DateTime date, string subType1, string subType2);
        decimal GetBedragByModuleCode_Datum_Subtypes(string moduleCode, DateTime date, string subType1, string subType2);
    }
}
