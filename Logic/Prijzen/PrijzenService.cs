﻿using Logic.Cache;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Prijzen
{
    public class PrijzenService : IPrijzenService, ICaching
    {
        private readonly IPrijzenRepository _prijzenRepository;
        private readonly ICache _cache;
        public string CacheKey => CacheKeys.StandaardPrijzen;

        public PrijzenService(IPrijzenRepository prijzenRepository, ICache cache)
        {
            _prijzenRepository = prijzenRepository;
            _cache = cache;
        }

        #region Cache Logic
        public void FillCache()
        {
            List<StandaardPrijzen> lfgs = _prijzenRepository.SelectAllPrijzen();
            _cache.Set<List<StandaardPrijzen>>(CacheKey, lfgs);
        }
        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        private List<StandaardPrijzen> GetSetFromCache()
        {
            List<StandaardPrijzen> lfgsCache = _cache.Get<List<StandaardPrijzen>>(CacheKey);
            if (lfgsCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<StandaardPrijzen>>(CacheKey);
        }
        #endregion
        public List<StandaardPrijzen> GetAllPrijzenByDatum(DateTime date)
        {
            return GetSetFromCache().Where(x => x.StartDatum <= date && (!x.EindDatum.HasValue || x.EindDatum.Value >= date)).ToList();
        }
        public List<StandaardPrijzen> GetPrijzenByModuleCode_Datum(string moduleCode, DateTime date)
        {
            return GetAllPrijzenByDatum(date).Where(x => x.ModuleCode == moduleCode).ToList();
        }
        public StandaardPrijzen GetPrijzenByModuleCode_Datum_Subtypes(string moduleCode, DateTime date, string subType1, string subType2)
        {
            return GetPrijzenByModuleCode_Datum(moduleCode, date).Where(x => x.SubType_1 == subType1 && x.SubType_2 == subType2).FirstOrDefault();
        }
        public decimal GetBedragByModuleCode_Datum_Subtypes(string moduleCode, DateTime date, string subType1, string subType2)
        {
            StandaardPrijzen tempPrijsItem = GetPrijzenByModuleCode_Datum_Subtypes(moduleCode, date, subType1, subType2);

            if (tempPrijsItem != null)
                return tempPrijsItem.Bedrag;
            else
                return 0;
        }
    }
}
