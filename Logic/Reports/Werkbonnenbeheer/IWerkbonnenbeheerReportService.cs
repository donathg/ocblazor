﻿using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Reports
{
    public interface IWerkbonnenbeheerReportService
    {
        DownloadFilePDF GetPDF(int id);
    }
}
