﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Settings
{
    public class AppSettings
    {
        public String ConnectionString { get; set; }

        public EmailConfiguration EmailConfiguration { get; set; }
    }


    public struct eCQareAuthentication
    {

      
    }

    public class eCQare
    {
        public String APIBaseAddress { get; set; }

        public string Tenant { get; set; }



        public String TokenEndpoint { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }
        public string Scope { get; set; }
    }
    public class EmailConfiguration
    {

        public String From { get; set; }

        public String SmtpServer { get; set; }

        public String Port { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }

    }
}
