﻿using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Telefoonboek
{
    public interface ITelefoonboekRepository
    {
        List<Gebruiker> GetTelefoonboek();

    }
}
