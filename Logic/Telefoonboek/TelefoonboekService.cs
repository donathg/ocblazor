 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Data;
using AutoMapper;
using Logic.DataBusinessObjects;
using Logic.Telefoonboek;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Serilog;

namespace Logic
{
    public class TelefoonboekService : ITelefoonboekServiceInterface, ICaching
    {
        private const string ApiUrlBase = "Telefoonboek";
      
        private readonly ITelefoonboekRepository _telefoonboekRepository;
        private readonly ICache _cache;
        public TelefoonboekService( ITelefoonboekRepository telefoonboekRepository, ICache cache)
        {
            ( _telefoonboekRepository, _cache) = (telefoonboekRepository, cache);
     
        }

        public string CacheKey => CacheKeys.Gebruikers;

        public void ClearCache()
        {
            _cache.Clear(CacheKey);
        }
        public  void FillCache()
        {
            Log.Information("Loading telefoonboek-cache");
            List<Gebruiker> gebruiker = _telefoonboekRepository.GetTelefoonboek();
            _cache.Set<List<Gebruiker>>(CacheKey, gebruiker);
        }
        public List<Gebruiker> GetSetFromCache()
        {
            List<Gebruiker> gebruikersFromCache = _cache.Get<List<Gebruiker>>(CacheKey);
            if (gebruikersFromCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Gebruiker>>(CacheKey);


        }
        public List<Gebruiker> GetTelefoonboek()
        {
            return GetSetFromCache();
        }
    }
}
