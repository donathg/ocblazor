﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
using System.Data;

using Logic.DataBusinessObjects;

namespace Logic
{
    public interface ITelefoonboekServiceInterface
    {

       List<Gebruiker> GetTelefoonboek();


    }
}
