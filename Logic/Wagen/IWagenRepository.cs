﻿using System;
using System.Collections.Generic;
using System.Text;
using Logic.DataBusinessObjects;

namespace Logic.WagenNS
{
    public interface IWagenRepository 
    {
        List<Wagen> GetWagens();
    }
}
