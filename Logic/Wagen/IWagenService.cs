﻿using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace Logic.WagenNS
{
    public interface IWagenService
    {


        List<Wagen> GetWagens();
        Wagen GetWagenById(int id);
    }
   
}
