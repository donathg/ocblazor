 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Data;
using Logic.WagenNS;
using Database.DataAccessObjects;
using Microsoft.Extensions.Caching.Memory;
using Logic.Cache;
using Logic.DataBusinessObjects;
//using Serilog;
using Common;
using Common.CloneExtensions;

namespace Logic.WagenNS
{
    public class WagenService : ICaching, IWagenService
    {
        private const string ApiUrlBase = "wagen";
       private IWagenRepository _wagenRepository;
        private readonly ICache _cache;
        public string CacheKey => CacheKeys.Wagens;
        public WagenService(IWagenRepository wagenRepository, ICache cache)
        {
          _wagenRepository = wagenRepository;
            _cache = cache;
        }

       

 

        public void FillCache()
        {
         //   Log.Information("Loading wagen-cache");
            List<Wagen> wagens = _wagenRepository.GetWagens();
            _cache.Set<List<Wagen>>(CacheKey, wagens);
        }

       
        public void ClearCache()
        {
           _cache.Clear(CacheKey);
        }
        private List<Wagen> GetSetFromCache()
        {

            List<Wagen> wagenCache = _cache.Get<List<Wagen>>(CacheKey);
            if (wagenCache == null)
            {
                FillCache();
            }
            return _cache.Get<List<Wagen>>(CacheKey);
        }

        public List<Wagen> GetWagens()
        {
            return GetSetFromCache().DeepClone();
        }
        public Wagen GetWagenById(int id)
        {
            return GetSetFromCache().Where(x => x.Id==id).FirstOrDefault().DeepClone() ;
        }
       
    }

     
}
