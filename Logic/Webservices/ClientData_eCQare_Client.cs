﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.Json;
using Logic.DataBusinessObjects;
using Logic.Maaltijdlijsten;
using Logic.Clienten;
using Logic.Settings;

namespace Logic.Webservices
{
    public class ClientData_eCQare_Client
    {
        private readonly HttpClient httpClient;
        private readonly IClientService clientService;

        public ClientData_eCQare_Client(HttpClient httpClient, IClientService clientService)
        {
            this.httpClient = httpClient;
            this.clientService = clientService;
        }

        public async Task<EcqareClientV2File[]> Get_eCQareClientenV2Files(string tenant)
        {
            var uri = QueryHelpers.AddQueryString($"api/{Uri.EscapeDataString(tenant)}/clientdata/v2/files",
                new Dictionary<string, string>
                {
                    { "includeNetworkContacts", false.ToString() },
                    { "includeNationalNumber", true.ToString() },
                    { "includeAttentionPoints", true.ToString() },
                });

            var options = new JsonSerializerOptions
            {
                Converters = { new JsonDateTimeConverter() },
                PropertyNameCaseInsensitive = true
            };

            return await httpClient.GetFromJsonAsync<EcqareClientV2File[]>(uri, options) ?? Array.Empty<EcqareClientV2File>();
        }
        public async Task<EcqareDepartmentV2[]> Get_eCQareDepartmentsV2Files(string tenant)
        {
            var uriString = Uri.EscapeDataString(tenant);
            var uri = QueryHelpers.AddQueryString($"api/{Uri.EscapeDataString(tenant)}/clientdata/v2/Departments", new Dictionary<string, string>{ });

            return await httpClient.GetFromJsonAsync<EcqareDepartmentV2[]>(uri) ?? Array.Empty<EcqareDepartmentV2>();
        }
        /// <summary>
        /// Max.1 month allowed by eCQare
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public async Task<List<EcqareAlivioMealplanFile>> Get_eCQareAlivioMealplansFiles(string tenant, DateTime startDate, DateTime endDate)
        {
            try
            {
                string uri = QueryHelpers.AddQueryString($"api/{Uri.EscapeDataString(tenant)}/alivio/v1/MealPlans",
                    new Dictionary<string, string>
                    {
                        { "startDate", startDate.ToString("yyyy-MM-dd") },
                        { "endDate", endDate.ToString("yyyy-MM-dd") }
                    });

                var options = new JsonSerializerOptions
                {
                    Converters = { new JsonDateTimeConverter() },
                    PropertyNameCaseInsensitive = true
                };

                List<EcqareAlivioMealplanFile> tempFiles = (await httpClient.GetFromJsonAsync<EcqareAlivioMealplanFile[]>(uri, options) ?? Array.Empty<EcqareAlivioMealplanFile>()).ToList();
                Dictionary<int, string> bewonerAttentionpoints = clientService.GetAttentionPointsForGebruiker(startDate, endDate);
                Dictionary<int, string> bewonerExtras = clientService.GetExtrasForGebruiker(startDate, endDate);

                // add extra data from DB
                foreach (EcqareAlivioMealplanFile file in tempFiles)
                {
                    file.Client = clientService.GetClientByDossierId(file.ClientId);

                    if (file.Client != null && bewonerAttentionpoints.ContainsKey(file.Client.Id))
                    {
                        file.MealAttentionpoints = bewonerAttentionpoints[file.Client.Id];
                    }
                    else
                        file.MealAttentionpoints = "Regulier"; // Wanneer aanpassen ook aanpassen de string check in EcqareAlivioMealplanByLeefgroep.ParseDietenList() & MaaltijdlijstenVacuumTotalen.Convert() !!!

                    if (file.Client != null && bewonerExtras.ContainsKey(file.Client.Id))
                    {
                        file.MealExtras = bewonerExtras[file.Client.Id];
                    }
                }

                return tempFiles.Where(x => x.MealChoiceFull != MealChoiceEnum.None).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<EcqareClientV2File[]> GetFile(string tenant, string nationalNumber, bool includeNetworkContacts = false, bool includeNationalNumber = true)
        {
            var uri = QueryHelpers.AddQueryString($"api/{Uri.EscapeDataString(tenant)}/clientdata/v2/files/{Uri.EscapeDataString(nationalNumber)}",
                new Dictionary<string, string>
                {
                    { "includeNetworkContacts", includeNetworkContacts.ToString() },
                    { "includeNationalNumber", includeNationalNumber.ToString() }
                });

            return await httpClient.GetFromJsonAsync<EcqareClientV2File[]>(uri) ?? Array.Empty<EcqareClientV2File>();
        }

        public async Task<EcqareClientV2File[]> GetFile(string tenant, Guid fileId, bool includeNetworkContacts = false, bool includeNationalNumber = false)
        {
            var uri = QueryHelpers.AddQueryString($"api/{Uri.EscapeDataString(tenant)}/clientdata/v2/files/{fileId}",
                new Dictionary<string, string>
                {
                    { "includeNetworkContacts", includeNetworkContacts.ToString() },
                    { "includeNationalNumber", includeNationalNumber.ToString() }
                });

            return await httpClient.GetFromJsonAsync<EcqareClientV2File[]>(uri) ?? Array.Empty<EcqareClientV2File>();
        }
    }
}
