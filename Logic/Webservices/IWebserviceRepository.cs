﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects.Webservices;
using Logic.DataBusinessObjects.Webservices.Acerta;
using Logic.DataBusinessObjects.Webservices.eCQare;

namespace Logic.WagenNS
{
    public interface IWebserviceRepository 
    {

        Task<WebserviceExternal> Save_Acerta_Personen_ToDatabase(WebserviceExternal ws, List<AcertaPersoneel> files);
        Task<WebserviceExternal> Save_Acerta_Contracten_ToDatabase(WebserviceExternal ws, List<AcertaContract> files);
        
        Task<WebserviceExternal> Save_eCQare_Departments_ToDatabase(WebserviceExternal ws, List<EcqareDepartmentV2> files);
        Task<WebserviceExternal> Save_eCQare_Clienten_ToDatabase(WebserviceExternal ws, List<EcqareClientV2File> files);
        Task<WebserviceExternal> StartDatabaseParser(WebserviceExternal ws);
        List<WebserviceExternal> Get();
        Task UpdateFromAcerta();

        Task UpdateFromECQCARE();
        Task<WebserviceExternal> Save_eCQare_Mealplans_ToDatabase(WebserviceExternal ws, List<EcqareAlivioMealplanFile> files);
        Task SendMail(string recipients, string subject, string emailbody);
    }
}
