﻿using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Webservices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
 
namespace Logic.WagenNS
{
    public interface IWebservicesService
    {



        List<WebserviceExternal> Get();
        Task<WebserviceExternal> Run(WebserviceExternal ws);
        Task<List<string>> Parse_XML_Acerta_And_Save(WebserviceExternal ws, MemoryStream memoryStream);
    }
   
}
