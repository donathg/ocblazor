 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Data;
using Logic.WagenNS;
using Database.DataAccessObjects;
using Microsoft.Extensions.Caching.Memory;
using Logic.Cache;
using Logic.DataBusinessObjects;
//using Serilog;
using Common;
using Common.CloneExtensions;
using Logic.DataBusinessObjects.Webservices;
using Logic.Settings;
using Microsoft.Extensions.Options;
using Logic.Webservices;
using System.Net.Http.Headers;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Logic.DataBusinessObjects.Webservices.Acerta;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;


namespace Logic.WagenNS
{
    public class WebserviceService :  IWebservicesService
    {
        IWebserviceRepository _webserviceRepository;
        private readonly AppSettings _appSettings;
        private readonly eCQare _eCQare;
        private readonly ClientData_eCQare_Client _client;
        private readonly IServiceProvider _serviceProvider;

        public WebserviceService(IWebserviceRepository webserviceRepository, ClientData_eCQare_Client client, IOptions<AppSettings> appSettings, IOptions<eCQare> eCQare, IServiceProvider serviceProvider)
        {

            _eCQare = eCQare.Value;
            _appSettings = appSettings.Value;
            _webserviceRepository = webserviceRepository;
            _client = client;
            _serviceProvider = serviceProvider; 
        }
        public async Task<WebserviceExternal> Run(WebserviceExternal ws)
        {
            try
            {
                ws.LastRunErrors = new List<string>();

                //API Call
                switch (ws.CustomerName + "-" + ws.ApiName)
                {
                    case "eCQare-Alles":
                        await FetchData_eCQare_Departments_And_Save(ws);
                        await FetchData_eCQare_Clienten_And_Save(ws);
                        break;
                    case "eCQare-MaaltijdenHistoriek":
                        await FetchData_eCQare_Maaltijden_And_Save(ws);
                        break;
                    case "Acerta-Personeel":
                        await FetchData_Acerta_And_Save(ws);
                        break;
                    case "Blazor-Caches":
                        await FetchData_Blazor_CacheClear(ws);
                        break;
                    default: throw new Exception("Webservice unkown");
                }

                // await _webserviceRepository.StartDatabaseParser(ws);
                ws.LastRunDate = DateTime.Now;
                ws.LastRunStatus = "ok";
                ws.LastRunErrors.Clear();    
            }
            catch (Exception  ex) 
            {
                ws.LastRunStatus = "error";
                ws.LastRunErrors.Add(ex.Message + " \nInner Exception: " + ex.InnerException);
            }
            return await Task.FromResult(ws);
        }

        private async Task FetchData_eCQare_Maaltijden_And_Save(WebserviceExternal ws)
        {
            var files = await _client.Get_eCQareAlivioMealplansFiles(_eCQare.Tenant, DateTime.Today, DateTime.Today);
            await _webserviceRepository.Save_eCQare_Mealplans_ToDatabase(ws, files.ToList());
        }

        private async Task FetchData_eCQare_Departments_And_Save(WebserviceExternal ws)
        {
            var files = await _client.Get_eCQareDepartmentsV2Files(_eCQare.Tenant);
            await _webserviceRepository.Save_eCQare_Departments_ToDatabase(ws, files.ToList());
        }
        private async Task FetchData_eCQare_Clienten_And_Save(WebserviceExternal ws)
        {
            var files = await _client.Get_eCQareClientenV2Files(_eCQare.Tenant);
            await _webserviceRepository.Save_eCQare_Clienten_ToDatabase(ws, files.ToList());
            await _webserviceRepository.UpdateFromECQCARE();
        }

        private async Task FetchData_Acerta_And_Save(WebserviceExternal ws)
        {
            while (await FetchData_Acerta_And_Save_Oldest_PERSandAGRM_Pair(ws) == true) ;
        }
        private async Task FetchData_Blazor_CacheClear(WebserviceExternal ws)
        {
            string emailbody = "";
            // Get all types in the current assembly
            var types = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => typeof(ICaching).IsAssignableFrom(t) && !t.IsAbstract)
                .ToList();

            foreach (var type in types)
            {
                // If the class requires dependency injection, use the DI container to resolve it
                var cachingInstance = (ICaching)_serviceProvider.GetService(type);

                if (cachingInstance != null)
                {
                    cachingInstance.ClearCache();
                    emailbody += $"Cache cleared for: {type.Name}";
                }
                else
                {
                    emailbody += $"Failed to resolve service for {type.Name}";
                }
            }

            await _webserviceRepository.SendMail("sonny.vankerchove@clarafey.broedersvanliefde.be", "Cache Blazor Cleared", emailbody);
        }


        private async Task<bool> FetchData_Acerta_And_Save_Oldest_PERSandAGRM_Pair(WebserviceExternal ws)
        {
            string xmlFilePath = ws.FileLocation;
            List<FileSystemInfo> fileInfo = new List<FileSystemInfo>();
            FileSystemInfo fileInfoFirstPERS = new DirectoryInfo(xmlFilePath).GetFileSystemInfos("*PERS*.xml").OrderBy(fi => fi.LastWriteTime).FirstOrDefault();
            FileSystemInfo fileInfoFirstAGRM = new DirectoryInfo(xmlFilePath).GetFileSystemInfos("*AGRM*.xml").OrderBy(fi => fi.LastWriteTime).FirstOrDefault();

            if (fileInfoFirstPERS != null && fileInfoFirstAGRM != null)
            {
                fileInfo.Add(fileInfoFirstPERS);
                fileInfo.Add(fileInfoFirstAGRM);
            }
            else
                return false;

            foreach (FileSystemInfo info in fileInfo)
            {
                try
                {
                    MemoryStream memoryStream = new MemoryStream();
                    using (FileStream fileStream = new FileStream(info.FullName, FileMode.Open))
                    {
                        fileStream.CopyTo(memoryStream);
                    }
                    memoryStream.Position = 0; ;
                    ws.LastRunErrors = await Parse_XML_Acerta_And_Save(ws, memoryStream);
                    File.Move(info.FullName, Path.Combine(xmlFilePath + @"\processed", info.Name),true);
                }
                catch
                {
                    File.Move(info.FullName, Path.Combine(xmlFilePath + @"\error", info.Name),true);
                }

            }
            await _webserviceRepository.UpdateFromAcerta();
            return true;
        }

        public async Task<List<string>> Parse_XML_Acerta_And_Save(WebserviceExternal ws, MemoryStream memoryStream)
        {
            List<string> errors = new List<string>();
            try
            {

                List<AcertaPersoneel> personeelList = new List<AcertaPersoneel>();
                List<AcertaContract> contractList = new List<AcertaContract>();
                memoryStream.Position = 0;
                StreamReader sr = new StreamReader(memoryStream);
                var xml = XDocument.Load(sr);
                var employees = xml.Descendants().Where(p => p.Name.LocalName == "EMPLOYEE").ToList();
               
                foreach (var employee in employees)
                {
                    var employeeInfo = employee.Descendants().Where(p => p.Name.LocalName == "EMPLOYEEINFO").ToList();
                    var employeeContact = employee.Descendants().Where(p => p.Name.LocalName == "EMPLOYEECONTACT").ToList();
                    AcertaPersoneel p = new AcertaPersoneel(employeeInfo.FirstOrDefault(), employeeContact.FirstOrDefault());
                    if (p.FirstName == "TAMARA")
                    {
                        int i = 0;
                    }
                    personeelList.Add(p);
                    if (p.Errors.Count > 0)
                    {
                        errors.AddRange(p.Errors);
                    }
                }
                if (personeelList.Count > 0)
                    await _webserviceRepository.Save_Acerta_Personen_ToDatabase(ws, personeelList);


                var contractInfo = xml.Descendants().Where(p => p.Name.LocalName == "CONTRACT").ToList();
                foreach (var i in contractInfo)
                {
                    var employement = i.Descendants().Where(p => p.Name.LocalName == "CONTRACTEMPLOYMENT").ToList();
                    var contractCostCenter = i.Descendants().Where(p => p.Name.LocalName == "CONTRACTCOSTCENTER");

                    foreach (var e in employement)
                    {
                        foreach (var c in contractCostCenter)
                        {
                            AcertaContract p = new AcertaContract(i.Attribute("EmployeeSocialSecurityNumber")?.Value, i.Attribute("ContractExternalReferenceNumber")?.Value, c, e);
                            contractList.Add(p);
                        }
                    }
                }
                if (contractList.Count > 0)
                    await _webserviceRepository.Save_Acerta_Contracten_ToDatabase(ws, contractList);



            }
            catch (Exception ex)
            {

            }
            return errors;
        }
        public List<WebserviceExternal> Get()
        {
            return _webserviceRepository.Get();
        }
    }     
}
