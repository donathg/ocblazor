﻿ 
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Logic.Werkbonnenbeheer
{
    public interface IWerkbonnenbeheerService
    {
       Task<List<Werkbon>> GetActiveWerkbonnen();

    }
}
