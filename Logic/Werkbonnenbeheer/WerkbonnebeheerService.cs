﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Logic.ArtikelNs;
using Logic.DataBusinessObjects.Werkbonnenbeheer;

namespace Logic.Werkbonnenbeheer
{
    public class WerkbonnenbeheerService : IWerkbonnenbeheerService 
    {
        IWerkbonnenbeheerRepository _werkbonnenbeheerRepository;
        IDbConnection _dbConnection;
        ICache _cache;
        public WerkbonnenbeheerService( IWerkbonnenbeheerRepository artikelRepository, ICache cache)
        {

            _werkbonnenbeheerRepository = artikelRepository;
            _cache = cache;
        }

        public async Task<List<Werkbon>> GetActiveWerkbonnen()
        {
           return await Task.FromResult(_werkbonnenbeheerRepository.GetActiveWerkbonnen());
        }
    }
}
