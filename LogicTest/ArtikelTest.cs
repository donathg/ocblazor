using DataBase.Services;
using Logic;
using Logic.Cache;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.Telefoonboek;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xunit;

using OCBlazor.Services;
using Logic.LeefgroepNS;

using Logic.ArtikelNs;
using DataBase.Repositories.Artikelen;
using Database.Sql;

namespace LogicTest
{
    public class ArtikelTest
    {
        [Fact]
        public async void ArtikelService()
        {
            IDbConnection iDBConnection = new SqlConnection();
            IUserService userService = new TestUSerService();
            ICache cache = new Cache(ServiceFactory.GetMemoryCache());
            IArtikelRepository rep = new ArtikelRepository(iDBConnection, ServiceFactory.GetAppSettings(), userService, ServiceFactory.GetMapper(), new SQLManager());
            Logic.ArtikelNs.IArtikelenService artikelService = new ArtikelenService(rep,cache);
            List<Artikel> artikelen = artikelService.GetArtikelen(1);
            Assert.True(artikelen.Count > 0);
            var artikelenFromCache = cache.Get<List<Artikel>>(((ICaching)artikelService).CacheKey);
            Assert.True(artikelenFromCache.Count > 0);
            ((ICaching)artikelService).ClearCache();
            Assert.Null(cache.Get<List<Leefgroep>>(((ICaching)artikelService).CacheKey));
        }
    }
}
