using DataBase.Services;
using Logic;
using Logic.Cache;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.Telefoonboek;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xunit;
using OCBlazor.Services;
using Logic.LeefgroepNS;
using DataBase.Repositories.Clienten;
using Database.Sql;

namespace LogicTest
{
    public class ClientTest
    {
        [Fact]
        public async void ClientService()
        {
            IDbConnection iDBConnection = new SqlConnection();
            IUserService userService = new TestUSerService();

            ICache cache = new Cache(ServiceFactory.GetMemoryCache());
            IClientRepository rep = new ClientRepository(iDBConnection, ServiceFactory.GetAppSettings(), userService, ServiceFactory.GetMapper(),new SQLManager()) ;
            IClientService clientService = new ClientService( rep, cache);

            List<Client> clienten = clientService.GetClienten();

            Assert.True(clienten.Count > 0);

            var clientenFromCache = cache.Get<List<Client>>(((ICaching)clientService).CacheKey);
            Assert.True(clientenFromCache.Count > 0);

            List<Client> clientenLfg = clientService.GetClientenForLeefgroep(105);
            Assert.True(clientenLfg.Count > 0);
            Assert.True(clientenLfg.Count < clienten.Count);


            List<Client> clientenActive  = clientService.GetActiveClienten();
            Assert.True(clientenActive.Count>0);
            Assert.True(clientenActive.Count < clienten.Count);

            List<Client> clientenLfgOverview = clientService.GetClientenAndLeefgroepen();
            Assert.True(clientenLfgOverview.Count > 0);

            ((ICaching)clientService).ClearCache();
            Assert.Null(cache.Get<List<Client>>(((ICaching)clientService).CacheKey));



        }
       
    }
}
