using DataBase.Services;
using Logic;
using Logic.Cache;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.Telefoonboek;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xunit;

using OCBlazor.Services;
using Logic.LeefgroepNS;

using Logic.ArtikelNs;
using DataBase.Repositories.Artikelen;
using Database.Sql;
using Logic.LocatieNs;
using DataBase.Repositories.Leefgroepen;
using DataBase.Repositories.LocatieNs;

namespace LogicTest
{
    public class LocatieTest
    {
        [Fact]
        public async void LocatieService()
        {
            Database.InitializeMappings.Initialize();
            IDbConnection iDBConnection = new SqlConnection();
            IUserService userService = new TestUSerService();
            ICache cache = new Cache(ServiceFactory.GetMemoryCache());
            ILocatieRepository rep = new LocatieRepository(iDBConnection, ServiceFactory.GetAppSettings(), userService, ServiceFactory.GetMapper(), new SQLManager());
            ILocatieService locatieService = new LocatieService(rep,cache);
            List<Locatie> locaties = locatieService.GetLocatiesAsFlatList();
            Assert.True(locaties.Count > 0);
            var LocatieFromCache = cache.Get<List<Locatie>>(((ICaching)locatieService).CacheKey);
            Assert.True(LocatieFromCache.Count > 0);
            ((ICaching)locatieService).ClearCache();
            Assert.Null(cache.Get<List<Locatie>>(((ICaching)locatieService).CacheKey));
        }
    }
}
