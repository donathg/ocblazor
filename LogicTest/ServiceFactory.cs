﻿using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using OCBlazor;
using Logic.Settings;
using Microsoft.Extensions.Options;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace LogicTest
{
    public static class ServiceFactory
    {

        public static  IMapper GetMapper ()
        {
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
          return mappingConfig.CreateMapper();
        }
        public static IMemoryCache GetMemoryCache()
        {
            return (IMemoryCache)new MemoryCache( new MemoryCacheOptions());
        }
        public static IOptions<AppSettings> GetAppSettings()
        {
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
            var jsonString = File.ReadAllText("appsettings.json");
            var jsonModel = JsonSerializer.Deserialize<AppSettings>(jsonString, options);
            var modelJson = JsonSerializer.Serialize(jsonModel, options);
            Console.WriteLine(modelJson);
            var root = JObject.Parse(jsonString);
            var appSettingsValues = root["AppSettings"].ToObject<AppSettings>();
            AppSettings s = new AppSettings();
            s.ConnectionString =  appSettingsValues.ConnectionString;
            return Options.Create(s);
        }

       
    }
}
