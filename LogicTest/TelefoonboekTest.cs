using DataBase.Services;
using Logic;
using Logic.Cache;
using Logic.Clienten;
using Logic.DataBusinessObjects;
using Logic.Telefoonboek;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xunit;
using DataBase.Repositories.Telefoonboek;
using Database.Sql;

namespace LogicTest
{
    public class TelefoonboekTest
    {
        
        [Fact]
        public  void Telefoonboek()
        {
            IDbConnection iDBConnection = new SqlConnection();
            IUserService userService = new TestUSerService();
            ICache cache = new Cache(ServiceFactory.GetMemoryCache());
            ITelefoonboekRepository rep = new TelefoonboekRepository(iDBConnection, ServiceFactory.GetAppSettings(), userService,ServiceFactory.GetMapper(), new SQLManager());
           
            TelefoonboekService telefoonboekService = new TelefoonboekService(rep, cache);

            List<Gebruiker> gebruikers = telefoonboekService.GetTelefoonboek();

            Assert.True(gebruikers.Count > 0);
            
            Assert.NotNull(cache.Get<List<Gebruiker>>(CacheKeys.Gebruikers));
            
            telefoonboekService.ClearCache();
            
            Assert.Null(cache.Get<List<Gebruiker>>(CacheKeys.Gebruikers));
        }

        
    }
}
