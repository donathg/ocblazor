using DataBase.Services;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xunit;
using Logic.Werkbonnenbeheer;
using Database.Repositories.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Database.Sql;
using Logic.Cache;

namespace LogicTest
{
    public class WerkbonTest
    {
        public WerkbonTest()
        {
            Database.InitializeMappings.Initialize();
        }

        [Fact]
        public async void GetActiveWerkbonnen()
        {
            IDbConnection iDBConnection = new SqlConnection();
            IUserService userService = new TestUSerService();
            ICache cache = new Cache(ServiceFactory.GetMemoryCache());
            IWerkbonnenbeheerRepository werkbonnenbeheerRepository = new WerkbonnenbeheerRepository(iDBConnection, ServiceFactory.GetAppSettings(), userService, ServiceFactory.GetMapper(), new SQLManager());
            IWerkbonnenbeheerService werkbonnenbeheerService = new WerkbonnenbeheerService( werkbonnenbeheerRepository, cache);
            List<Werkbon> werkbonnen = await werkbonnenbeheerService.GetActiveWerkbonnen();
            Assert.True(werkbonnen.Count > 0);
            Assert.True(werkbonnen[0].Id > 0);

        }
        [Fact]
        public async void GetHistorischeWerkbonnen()
        {
            IDbConnection iDBConnection = new SqlConnection();
            IUserService userService = new TestUSerService();
            ICache cache = new Cache(ServiceFactory.GetMemoryCache());
            IWerkbonnenbeheerRepository werkbonnenbeheerRepository = new WerkbonnenbeheerRepository(iDBConnection, ServiceFactory.GetAppSettings(), userService, ServiceFactory.GetMapper(), new SQLManager());
            IWerkbonnenbeheerService werkbonnenbeheerService = new WerkbonnenbeheerService( werkbonnenbeheerRepository, cache);

            List<Werkbon> werkbonnen = await werkbonnenbeheerService.GetActiveWerkbonnen();
            Assert.True(werkbonnen.Count > 0);

            Assert.True(werkbonnen[0].Id > 0);


            //   Assert.True(werkbonnen[0].Afgesloten);

        }
    }
}
