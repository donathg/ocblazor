﻿ 
using Microsoft.AspNetCore.Components;
 
using System;
using System.Collections.Generic;
 
using Logic;
 
using DevExpress.Blazor;
using Logic.BijlageManagerServiceNS;
using Logic.DataBusinessObjects.BijlageManager;
using Microsoft.JSInterop;
using Logic.Modules;
using Sotsera.Blazor.Toaster;
using System.Threading.Tasks;
//using Serilog;

namespace OCBlazor.Components
{


    public class BijlageManagerModel : ComponentBase,IDisposable
    {
        [Inject]
        public IToaster _toaster { get; set; }

        public String PageTitle { get; set; } = "Bijlagemanager";

        [Inject]
        NavigationManager _navigationManager { get; set; }

        [Inject]
        IJSRuntime js { get; set; }

        [Parameter]
        public int Id { get; set; }


        

        [Parameter]
        public ModuleType Module { get; set; }
        [Parameter]
        public bool CanDelete { get; set; } = true;

        [Parameter]
        public int SelectedTreeId { get; set; }
        [Inject]
        public IBijlageManagerService _bijlageManagerService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }



        private Bijlage _selectedBijlage;
        public Bijlage SelectedBijlage { get { return _selectedBijlage; } set { _selectedBijlage = value; StateHasChanged(); } }


        public List<Bijlage> Bijlages { get; set; } = new List<Bijlage>();

        [Parameter] public EventCallback<int> AantalBijlagesChanged { get; set; }

        protected override async Task OnParametersSetAsync()
        {
           await Refresh();
        }
         

        private async Task Refresh()
        {

            Bijlages = null;
            Bijlages = _bijlageManagerService.GetBijlages(Module, Id);
            PageTitle = "Bijlagemanager";
         
         

          await  InvokeAsync(StateHasChanged);
           // StateHasChanged();


        }
        public void DownloadFile(Bijlage bijlage)
        {
            SaveAs($"{bijlage.FileName}.{bijlage.FileExtention}", bijlage.FileData);
        }
        public void SaveAs(string filename, byte[] data)
        {
            js.InvokeAsync<object>(
   "myLib.saveAsFile",
   filename,
   Convert.ToBase64String(data));
        }
 
        protected void OnFileUploaded(FileUploadEventArgs args)
        {
            _toaster.Success("Het bestand is geüpload.", "OK");
       
            Refresh();
            AantalBijlagesChanged.InvokeAsync(Bijlages.Count);
        }



        public string GetUploadUrl(string url)
        {
            String path = _navigationManager.ToAbsoluteUri(url).AbsoluteUri + $"?module={Module.ToString()}&modulePrimaryKey={Id}";
            return path;
        }



        #region delete 
        public String DeleteQuestionText { get; set; }
        public bool DeleteQuestionPopupVisible { get; set; } = false;
        private Bijlage _bijlageMarkedForDelete = null;

        public void DeleteBijlage_OpenQuestionPopup(Bijlage bijlage)
        {
            DeleteQuestionText = $"Wil u bijlage {bijlage.FileName}.{bijlage.FileExtention} verwijderen ?";
            _bijlageMarkedForDelete = bijlage;
            DeleteQuestionPopupVisible = true;
            StateHasChanged();            
        }        
        public void OnQuestionDeleteYes()
        {
            if (_bijlageMarkedForDelete != null)
            {
                _toaster.Success("Het bestand is verwijderd.", "OK");
                _bijlageManagerService.DeleteBijlage(this.Module, _bijlageMarkedForDelete.Id);
                _bijlageMarkedForDelete = null;
                DeleteQuestionPopupVisible = false;
             
                Refresh();
                AantalBijlagesChanged.InvokeAsync(Bijlages.Count);
            }
        }
        public void OnQuestionDeleteNo()
        {
            _bijlageMarkedForDelete = null;
            DeleteQuestionPopupVisible = false;
            StateHasChanged();
        }
        public void Dispose()
        {
            //   Log.Information("Dispose LocatieBoomModel");

            //   GC.SuppressFinalize(this);
            if (Bijlages != null)
            {
                Bijlages.Clear();
                Bijlages = null;
            }
        }
        ~BijlageManagerModel()
        {
            Dispose();
        }

        #endregion


    }

}
