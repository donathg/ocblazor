﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.LocatieNs;
using DevExpress.Blazor;
using Logic.DataBusinessObjects.MultiSelectNS;
//using Serilog;

namespace OCBlazor.Components
{

 
    public class LocatieBoomModel : ComponentBase,IDisposable
    {



        private string _filterText;
        public String FilterText { get { return _filterText; }  set { _filterText = value; DoFilter(); } }

        private void DoFilter()
        {

            InvokeAsync(StateHasChanged);
        }

        [Parameter]
        public bool Enabled { get; set; } = true;

        [Parameter]
        public Action<Locatie>  OnSelectedTreeItemChanged{ get; set; }


        [Parameter]
        public int SelectedTreeId { get; set; }

        [Inject]
        public ILocatieService _locatieService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }



        private Locatie _selectedLocatie;
        public Locatie SelectedLocatie { get { return _selectedLocatie; } set { _selectedLocatie = value; StateHasChanged(); } }

        public List<Locatie> AllLocaties { get; set; } = new List<Locatie>();
        public List<Locatie> Locaties { get; set; } = new List<Locatie>();


        protected override void OnParametersSet()
        {
            this.SelectedLocatie = Locaties.Where(x => x.Id == SelectedTreeId).FirstOrDefault();
            base.OnParametersSet();
        }
        protected override void OnInitialized()
        {
            if (AllLocaties == null || AllLocaties.Count == 0)
                AllLocaties.Add ( _locatieService.GetLocatiesAsTree());
        }



        protected void TreeNodeSelectionChanged(TreeViewNodeEventArgs e)
        {
            if (e.NodeInfo.DataItem is Locatie locatie)
            {
                this.SelectedLocatie = locatie;
                this.SelectedTreeId = locatie.Id;
                if (OnSelectedTreeItemChanged != null)
                    OnSelectedTreeItemChanged(SelectedLocatie);
        
                InvokeAsync(StateHasChanged);

            }
        }

        public void Dispose()
        {
            //   Log.Information("Dispose LocatieBoomModel");

            //   GC.SuppressFinalize(this);
            if (AllLocaties != null)
            {
                AllLocaties.Clear();
                AllLocaties = null;
            }
        }
        ~LocatieBoomModel()
        {
            Dispose();
        }

    }

}
