﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.LocatieNs;
using DevExpress.Blazor;
using Logic.BijlageManagerServiceNS;
using Logic.DataBusinessObjects.BijlageManager;
using Microsoft.JSInterop;
using Logic.Modules;
using Logic.MultiSelectNS;
using Logic.DataBusinessObjects.MultiSelectNS;
using System.Net.Mail;
using Sotsera.Blazor.Toaster;
//using Serilog;

namespace OCBlazor.Components
{
    public class MultiSelectComponentModel : ComponentBase/*, IDisposable*/
    {
        public class BeoordelingsKolom
        {
            public int MultiselectItemId { get; set; }
            public string Naam { get; set; }
            public bool Uitstekend { get; set; }
            public bool Goed { get; set; }
            public bool NietGoedNietSlecht { get; set; }
            public bool Slecht { get; set; }
            public bool HeelSlecht { get; set; }

            public string GetResultaatLijn()
            {
                if (Uitstekend)
                    return nameof(Uitstekend);
                else if (Goed)
                    return nameof(Goed);
                else if (NietGoedNietSlecht)
                    return nameof(NietGoedNietSlecht);
                else if (Slecht)
                    return nameof(Slecht);
                else if (HeelSlecht)
                    return nameof(HeelSlecht);
                else
                    return null;
            }
            public void RemovePreviousChoice(string choice)
            {
                if (nameof(Uitstekend) == choice)
                    Uitstekend = false;
                else if (nameof(Goed) == choice)
                    Goed = false;
                else if (nameof(NietGoedNietSlecht) == choice)
                    NietGoedNietSlecht = false;
                else if (nameof(Slecht) == choice)
                    Slecht = false;
                else if (nameof(HeelSlecht) == choice)
                    HeelSlecht = false;
            }
            public bool HasMultipleChoicesSelected()
            {
                // Count how many properties are true
                int trueCount = 0;

                if (Uitstekend) trueCount++;
                if (Goed) trueCount++;
                if (NietGoedNietSlecht) trueCount++;
                if (Slecht) trueCount++;
                if (HeelSlecht) trueCount++;

                // Return true if there's more than one true value
                return trueCount > 1;
            }
        }
        public class RangschikKolom
        {
            public int MultiselectItemId { get; set; }
            public string Naam { get; set; }
            public int? Rang { get; set; }
        }

        [Inject]
        public IToaster _toaster { get; set; }
        [Inject]
        NavigationManager _navigationManager { get; set; }
        [Inject]
        IJSRuntime js { get; set; }
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }
        
        [Parameter]
        public EventCallback<MultiSelectItem> OnMultiSelectChanged { get; set; }
        [Parameter]
        public MultiSelect MultiSelect { get; set; }
        [Parameter]
        public ModuleType Module { get; set; }
        [Parameter]
        public bool CanDelete { get; set; } = true;
        [Parameter]
        public int SelectedTreeId { get; set; }
        [Parameter]
        public DateTime? MaxDate { get; set; }
        [Parameter]
        public DateTime? MinDate { get; set; }
        [Parameter]
        public bool IsEditable { get; set; }

        public List<BeoordelingsKolom> BeoordelingsKolommen { get; set; }
        public List<RangschikKolom> RangschikKolommen { get; set; }

        public List<int> AllNumberOptions = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        private List<int> SelectedNumberOptions = new List<int>();
        public List<int> AvailableNumberOptions;
        public MultiSelectItem RangschikOtherItem { get; set; }
        public bool ShowRangschikOther { get; set; }

        protected readonly string EmailRegex = @"(\w|[.-])+@(\w|-)+\.(\w|-){2,4}";


        public MultiSelectItem SelectMultiSelectItem { get; set; } = new MultiSelectItem();

        protected override void OnInitialized()
        {
            if (MultiSelect.ChoiceType == "RANGSCHIK")
            {
                AvailableNumberOptions = new List<int>(AllNumberOptions);
                RangschikKolommen = new List<RangschikKolom>();

                foreach (MultiSelectItem item in MultiSelect.Items)
                {
                    RangschikKolommen.Add(new RangschikKolom()
                    {
                        MultiselectItemId = item.Id,
                        Naam = item.Description,
                        Rang = null
                    });
                }
            }
            else if (MultiSelect.ChoiceType == "WAARDE")
            {
                BeoordelingsKolommen = new List<BeoordelingsKolom>();

                foreach (MultiSelectItem item in MultiSelect.Items)
                {
                    BeoordelingsKolommen.Add(new BeoordelingsKolom()
                    {
                        MultiselectItemId = item.Id,
                        Naam = item.Description,
                        Uitstekend = false,
                        Goed = false,
                        NietGoedNietSlecht = false,
                        Slecht = false,
                        HeelSlecht = false
                    });
                }
            }
        }
        protected override void OnParametersSet()
        {
            //Bij editeren bestaand formulier => Hier moet het geselecteerde Item uit de databank worden gezet voor Comboboxen
            if (MultiSelect.ChoiceType == "RADIO" || MultiSelect.ChoiceType == "COMBO")
                SelectMultiSelectItem = MultiSelect.Items.Where(x => x.Result.IsSelected).FirstOrDefault();
        }
        public async void OnTextChangedAndere(string textItem, MultiSelectItem item)
        {
            item.Result.StringValue = textItem;
            item.Parent.Validate();
            await OnMultiSelectChanged.InvokeAsync(item);
        }
        public async void OnDateChanged(DateTime dateTime, MultiSelectItem item)
        {
            MultiSelect.ErrorMessage = "";
            item.Result.DateValue = dateTime;
            
            if (item.Parent.Validate())
            {
                item.Result.IsSelected = true;
            }
            else
                item.Result.IsSelected = false;

            StateHasChanged();
            await OnMultiSelectChanged.InvokeAsync(item);
        }
        public async Task RangschikGrid_Saving(GridEditModelSavingEventArgs e)
        {
            if (e.EditModel is RangschikKolom newItem)
            {
                MultiSelectItem tempItem = MultiSelect.Items.FirstOrDefault(x => x.Id == newItem.MultiselectItemId);

                if (tempItem.Result.IntValue.HasValue) // vorige optie verwijderen uit opties-lijsten + ordenen
                {
                    SelectedNumberOptions.Remove(tempItem.Result.IntValue.Value);
                    AvailableNumberOptions.Add(tempItem.Result.IntValue.Value);
                    AvailableNumberOptions = AvailableNumberOptions.Order().ToList(); // reorder list. Anders word op einde toegevoegd
                }

                if (newItem.Rang != null)
                {
                    // set multiselect with new value
                    tempItem.Result.IntValue = newItem.Rang;
                    RangschikKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Rang = newItem.Rang; // nodig voor weergeven selectie

                    if (newItem.Rang.HasValue) // nieuwe optie toevoegen aan optie-lijsten
                    {
                        SelectedNumberOptions.Add(newItem.Rang.Value);
                        AvailableNumberOptions.Remove(newItem.Rang.Value);
                    }
                }
                else
                {
                    // set multiselect is null
                    tempItem.Result.IntValue = null;
                    RangschikKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Rang = newItem.Rang; // nodig voor weergeven selectie
                }

                if (tempItem.IsOther)
                {
                    ShowRangschikOther = true;
                    RangschikOtherItem = tempItem;
                }
                else
                {
                    ShowRangschikOther = false;
                    RangschikOtherItem = null;
                }

                await OnMultiSelectChanged.InvokeAsync(tempItem);
            }
        }
        public async Task WaardeGrid_Saving(GridEditModelSavingEventArgs e)
        {
            if (e.EditModel is BeoordelingsKolom newItem)
            {
                MultiSelectItem tempItem = MultiSelect.Items.FirstOrDefault(x => x.Id == newItem.MultiselectItemId);
                newItem.RemovePreviousChoice(tempItem.Result.StringValue);

                if (newItem.HasMultipleChoicesSelected())
                {
                    _toaster.Error("Gelieve maar 1 selectie aan te duiden");
                    return;
                }
                
                tempItem.Result.StringValue = newItem.GetResultaatLijn();

                if (newItem.Uitstekend)
                {
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Uitstekend = true;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Goed = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).NietGoedNietSlecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Slecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).HeelSlecht = false;
                }
                else if (newItem.Goed)
                {
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Uitstekend = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Goed = true;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).NietGoedNietSlecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Slecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).HeelSlecht = false;
                }
                else if (newItem.NietGoedNietSlecht)
                {
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Uitstekend = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Goed = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).NietGoedNietSlecht = true;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Slecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).HeelSlecht = false;
                }
                else if (newItem.Slecht)
                {
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Uitstekend = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Goed = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).NietGoedNietSlecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Slecht = true;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).HeelSlecht = false;
                }
                else if (newItem.HeelSlecht)
                {
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Uitstekend = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Goed = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).NietGoedNietSlecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).Slecht = false;
                    BeoordelingsKolommen.FirstOrDefault(x => x.Naam == newItem.Naam).HeelSlecht = true;
                }

                await OnMultiSelectChanged.InvokeAsync(tempItem);
            }
        }

        public async void OnTextOrEmailLostFocus(string textItem, MultiSelectItem item)
        {
            MultiSelect.ErrorMessage = "";
            item.Result.StringValue = textItem;
            if (item.Parent.Validate())
            {
                item.Result.IsSelected = true;
            }
            else
                item.Result.IsSelected = false;

            StateHasChanged();
            await OnMultiSelectChanged.InvokeAsync(item);
        }
        public async void OnCheckboxChanged(bool isChecked, MultiSelectItem item)
        {
            item.Result.IsSelected = isChecked;
            item.Parent.Validate();
            await OnMultiSelectChanged.InvokeAsync(item);
        }
        public async void OnComboBoxChanged(MultiSelectItem item)
        {
            foreach (MultiSelectItem i in item.Parent.Items)
                i.Result.Reset();
            this.SelectMultiSelectItem = item;
            item.Result.IsSelected = true;
            item.Parent.Validate();
            StateHasChanged();
            await OnMultiSelectChanged.InvokeAsync(item);
        }
        //public void Dispose()
        //{
        //    //Log.Information("Dispose MultiSelectComponentModel");

        //    //   GC.SuppressFinalize(this);
        //    OnMultiSelectChanged = new EventCallback<MultiSelectItem>();
        //    MultiSelect = null;


        //}
        //~MultiSelectComponentModel()
        //{
        //    Dispose();
        //}
    }
}
