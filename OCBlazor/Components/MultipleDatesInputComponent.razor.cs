﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.LocatieNs;
using DevExpress.Blazor;
using Logic.BijlageManagerServiceNS;
using Logic.DataBusinessObjects.BijlageManager;
using Microsoft.JSInterop;
using Logic.Modules;
using Logic.MultiSelectNS;
using Logic.DataBusinessObjects.MultiSelectNS;
using System.Net.Mail;
using Sotsera.Blazor.Toaster;
using DevExpress.Xpo.DB;
using DevExpress.XtraGauges.Core.Styles;
using System.ComponentModel;
using System.Net.NetworkInformation;
//using Serilog;

namespace OCBlazor.Components
{
    public class MultipleDatesInputComponentModel : ComponentBase/*, IDisposable*/
    {


        [Inject]
        public IToaster _toaster { get; set; }
        [Inject]
        NavigationManager _navigationManager { get; set; }
        [Inject]
        IJSRuntime js { get; set; }
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        [Parameter]
        public List<DateTime> DisabledDates { get; set; } = new List<DateTime>();

        [Parameter]
        public DateTime MinDate { get; set; }
        [Parameter]
        public DateTime MaxDate { get; set; }
        [Parameter]
        public List<DateTime?> SelectedDates { get; set; }

       public DateTime? SelectedDate1
       {
            get => SelectedDates[0];
            set => SelectedDates[0] = value;
       }
        public DateTime? SelectedDate2
        {
            get => SelectedDates[1];
            set => SelectedDates[1] = value;
        }
        public DateTime? SelectedDate3
        {
            get => SelectedDates[2];
            set => SelectedDates[2] = value;
        }
        public DateTime? SelectedDate4
        {
            get => SelectedDates[3];
            set => SelectedDates[3] = value;
        }
        public DateTime? SelectedDate5
        {
            get => SelectedDates[4];
            set => SelectedDates[4] = value;
        }
        public DateTime? SelectedDate6
        {
            get => SelectedDates[5];
            set => SelectedDates[5] = value;
        }



        public void OnCustomDisabledDate(CalendarCustomDisabledDateEventArgs args)
        {
            if (DisabledDates == null)
                return;
            args.IsDisabled = args.Date < DateTime.Today.AddDays(-20)
                || DisabledDates.Exists(d => DaysEqual(d, args.Date));
        }
        public bool DaysEqual(DateTime date1, DateTime date2)
        {
            return (date1.Year == date2.Year && date1.DayOfYear == date2.DayOfYear);
        }




        protected override Task OnParametersSetAsync()
        {
          
            return base.OnParametersSetAsync();
        }

        protected override Task OnInitializedAsync()
        {
            SelectedDates.Clear();
            SelectedDates.Add(null);
            SelectedDates.Add(null);
            SelectedDates.Add(null);
            SelectedDates.Add(null);
            SelectedDates.Add(null);
            SelectedDates.Add(null);
            return base.OnInitializedAsync();
        }

        
    }
}
