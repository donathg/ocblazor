﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Logic.BijlageManagerServiceNS;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.BijlageManager;
using Logic.Modules;
using Logic.Reports;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OCBlazor.Controller.Files
{
    [Route("api/[controller]/[action]")]

    public class DownloadController : ControllerBase
    {
        IWerkbonnenbeheerReportService _werkbonnenbeheerReportService;
        public DownloadController(IWerkbonnenbeheerReportService werkbonnenbeheerReportService)
        {
            _werkbonnenbeheerReportService = werkbonnenbeheerReportService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetWerkon(int id)
        {

            DownloadFilePDF pdf = _werkbonnenbeheerReportService.GetPDF(9999);
            return File(pdf.Data, pdf.ContentType, pdf.FileNameExtension);
        }
    }
    }
 
