﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DevExpress.Web.Internal;
using Logic.BijlageManagerServiceNS;
using Logic.DataBusinessObjects.BijlageManager;
using Logic.DataBusinessObjects.Webservices;
using Logic.Modules;
using Logic.WagenNS;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OCBlazor.Controller.Files
{
    [Route("api/[controller]/[action]")]

    public class UploadController : ControllerBase
    {
        IBijlageManagerService _bijlageManagerService;
        IWebservicesService _webservicesService;
        public UploadController (IBijlageManagerService bijlageManagerService, IWebservicesService webservicesService)
        {
            _bijlageManagerService = bijlageManagerService;
            _webservicesService = webservicesService;
        }

        [HttpPost]
        public async Task<ActionResult<Object>> UploadFile(IFormFile myFile, ModuleType module, int modulePrimaryKey/*wo_id,inv_id*/)
        {
            try
            {


                MemoryStream memoryStream = new MemoryStream();
                myFile.CopyTo(memoryStream);

                if (module == ModuleType.WEBSERVICES_ACERTA_PERSONEEL_XML)
                {
                    WebserviceExternal ws = new WebserviceExternal() { Id = 2, ApiName = "Personeel", CustomerName = "Acerta" };
                    List<string> errors = await _webservicesService.Parse_XML_Acerta_And_Save(ws, memoryStream);
                    if (errors.Count > 0)
                    {
                        Response.StatusCode = 207; //Multple status
                        return BadRequest(String.Join(String.Empty, errors.ToArray()));
                    }
                    else Ok("OK");
                }
                else
                {
                    Bijlage b =  _bijlageManagerService.InsertFile(myFile.FileName, memoryStream.ToArray(), module, modulePrimaryKey, "");
                    return Ok(b);
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;

            }

            return new EmptyResult();
        }
        [HttpGet]
        public String Test()
        {
            return "TestX";
        }
    }
}
