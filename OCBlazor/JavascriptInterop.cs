﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor
{
    public static class DotNetJavascriptReciever
    {

        public static Action<string> OnKeyUpHandlerAction { get; set; }
        [JSInvokable]
        public static void OnKeyUpHandler(string value)
        {
            OnKeyUpHandlerAction?.Invoke(value);
        }

        public static Action<int> OnSwipeLeftReceived { get; set; }
        [JSInvokable]
        public static void SwipeLeft(String id)
        {
            OnSwipeLeftReceived?.Invoke(Int32.Parse(id));
        }
    }
    public class DotNetJavascriptCaller
    {

        private readonly IJSRuntime _jsRuntime;

        public DotNetJavascriptCaller(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public void WriteToLocalStorage (String key, Object value)
        {
            _jsRuntime.InvokeAsync<string>("myLib.WriteStorage", key,value);
        }
        public ValueTask<object> ReadFromLocalStorage(String key)
        {
            return _jsRuntime.InvokeAsync<object>("myLib.ReadStorage", key);
        }

        public ValueTask<object> Focus(ElementReference elementRef)
        {
            return _jsRuntime.InvokeAsync<object>("myLib.focusElement", elementRef);
        }

        public ValueTask<object> StartClock(ElementReference elementRef)
        {
            return _jsRuntime.InvokeAsync<object>("myLib.startClock", elementRef);
        }

        public ValueTask<object> ToggleFullScreen()
        {
            return _jsRuntime.InvokeAsync<object>("myLib.toggleFullScreen");

        }
        public ValueTask<object> SetFullScreen()
        {
            return _jsRuntime.InvokeAsync<object>("myLib.setFullScreen");

        }
        public ValueTask<object> ScrollToTop()
        {
            return _jsRuntime.InvokeAsync<object>("myLib.scrollToTop");

        }
        public ValueTask<object> BringActiveItemIntoView()
        {
            return _jsRuntime.InvokeAsync<object>("myLib.BringActiveItemIntoView");

        }
        public void ShowAlert(String message)
        {
            _jsRuntime.InvokeAsync<String>("myLib.showPrompt", message);
        }
        public void HardRefreshF5()
        {
            _jsRuntime.InvokeAsync<string>("myLib.hardRefreshF5", null);
        }

    }
}
