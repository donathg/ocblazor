﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Database.DataAccesObjects;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.BijlageManager;
using Logic.DataBusinessObjects.ExternalDashboard;
using Logic.DataBusinessObjects.GOG;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Onkostennota;
using Logic.DataBusinessObjects.Webservices;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using OCBlazor.Models;
using OCBlazor.PageModels;

namespace OCBlazor
{
    public class MappingProfile : Profile   
    {
        public MappingProfile()
        {
            CreateMap<KasbeheerTransactieCM, KasbeheerTransactie>();
            CreateMap<KasbeheerTransactie, KasbeheerTransactieCM>();
            CreateMap<GebruikerDao, Gebruiker>();
            CreateMap<Gebruiker, GebruikerDao>();
            CreateMap<ClientDao, Client>();
            CreateMap<Client, ClientDao>();
            CreateMap<LeefgroepDao, Leefgroep>();
            CreateMap<Artikel, ArtikelDao>();
            CreateMap<ArtikelDao, Artikel>();
            CreateMap<Locatie, LocatieDao>();
            CreateMap<LocatieDao, Locatie>();
            CreateMap<Werkbon,WerkbonDao>();
            CreateMap<WerkbonDao, Werkbon>();
            CreateMap<BijlageDao, Bijlage>();
            CreateMap<KilometerDao, Kilometer>();
            CreateMap<KilometerTypeDao, KilometerType>();
            CreateMap<WagenDao, Wagen>();
            CreateMap<KampDao, Kamp>();
            CreateMap<GOGItemDAO, GOGItem>();
            CreateMap<ExternalDashboardMedewerkersDao, ExternalDashboardMedewerkers>();
            CreateMap<WebserviceExternalDao, WebserviceExternal>();

            CreateMap<OnkostennotaCategorieItemDao, OnkostennotaCategorieItem>();
            CreateMap<OnkostennotaCategorieItem, OnkostennotaCategorieItemDao>();

            CreateMap<OnkostennotaItemDao, OnkostennotaItem>();
            CreateMap<OnkostennotaItem, OnkostennotaItemDao>();
            CreateMap<MaaltijdAandachtspuntenDao, MaaltijdAandachtspuntenItem>();
            

            CreateMap<StandaardPrijzenDAO, StandaardPrijzen>();
            CreateMap<StandaardPrijzen, StandaardPrijzenDAO>();
        }
    }
}
