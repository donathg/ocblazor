﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Middlewares
{
    public class ExceptionMiddleware
    {
        public readonly RequestDelegate _next;
        string _path;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(ILogger<ExceptionMiddleware> logger, RequestDelegate next, string Path)
        {
            _next = next;
            _path = Path;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"RequsetPath: {context.Request.Path}", default);
                context.Response.Redirect(_path);

            }
        }

    }
}
