﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Middlewares
{
    public static class ExceptionMiddlewareExtension
    {
        public static IApplicationBuilder UsemycustomException(
        this IApplicationBuilder builder, string path)
        {
            return builder.UseMiddleware<ExceptionMiddleware>(path);
        }
    }
}
