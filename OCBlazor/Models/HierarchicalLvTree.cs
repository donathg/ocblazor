﻿using Database;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Models
{
    public class LvTreeConverter
    {

        //    public static LvTreeItem LvListToLvTreeConverter(List<LvRecord> LvList)
        //    {
        //        LvTreeItem Root = new LvTreeItem();

        //        var rootList = LvList.Where(x => x.TnParentId == null).ToList();

        //        foreach (var root in rootList)
        //        {
        //            LvTreeItem lvTreeItem1 = new LvTreeItem(root);

        //            if (!string.IsNullOrWhiteSpace(root.Massnahme))
        //            {
        //                lvTreeItem1.TreeLevelDepth = -1;
        //                lvTreeItem1.Descr = root.Massnahme;
        //                lvTreeItem1.DisplayText = root.Massnahme;
        //                lvTreeItem1.Icon = "oi oi-box";

        //                LvTreeItem lvTreeItem2 = new LvTreeItem(root);
        //                lvTreeItem2.TreeLevelDepth = 0;
        //                lvTreeItem2.Icon = (lvTreeItem2.TreeLevelDepth < lvTreeItem2.CalcLevel) ? "oi oi-folder" : "oi oi-document";
        //                lvTreeItem2.IsSelected = false;

        //                lvTreeItem1.Children.Add(lvTreeItem2);

        //                addChildren(lvTreeItem2, LvList, 1);
        //            }
        //            else
        //            {
        //                lvTreeItem1.TreeLevelDepth = 0;
        //                lvTreeItem1.Icon = (lvTreeItem1.TreeLevelDepth < lvTreeItem1.CalcLevel) ? "oi oi-folder" : "oi oi-document";
        //                lvTreeItem1.IsSelected = false;

        //                addChildren(lvTreeItem1, LvList, 1);
        //            }

        //            Root.Children.Add(lvTreeItem1);
        //        }

        //        return Root;


        //    }
        //    private static void addChildren(LvTreeItem parentHierarchicalLvTree, List<LvRecord> lvList, int TreeLevel)
        //    {
        //        var childList = lvList.Where(x => x.TnParentId == parentHierarchicalLvTree.PaplvdTnId).ToList();

        //        if (childList.Count() == 0)
        //        {
        //            //parentHierarchicalLvTree.Icon = "oi oi-target";
        //        }

        //        foreach (var child in childList)
        //        {
        //            LvTreeItem lvTreeItem = new LvTreeItem(child);
        //            lvTreeItem.TreeLevelDepth = TreeLevel;
        //            lvTreeItem.Icon = (lvTreeItem.TreeLevelDepth < lvTreeItem.CalcLevel) ? "oi oi-folder" : "oi oi-document";
        //            lvTreeItem.IsSelected = false;

        //            addChildren(lvTreeItem, lvList, (++TreeLevel));
        //            parentHierarchicalLvTree.Children.Add(lvTreeItem);
        //        }
        //    }


        //    public static HierarchicalLvTree FlatLvToHierarchicalLvTreeConverter(List<LvRecord> lvList)
        //    {
        //        HierarchicalLvTree Root = new HierarchicalLvTree();

        //        var rootList = lvList.Where(x => x.TnParentId == null).ToList();

        //        foreach (var root in rootList)
        //        {
        //            HierarchicalLvTree hierarchicalLvTree = new HierarchicalLvTree(root);

        //            if (!string.IsNullOrWhiteSpace(root.Massnahme))
        //            {
        //                hierarchicalLvTree.TreeLevelDepth = -1;
        //                hierarchicalLvTree.Descr = root.Massnahme;
        //                hierarchicalLvTree.Icon = "oi oi-box";

        //                HierarchicalLvTree hierarchicalLvTree2 = new HierarchicalLvTree(root);
        //                hierarchicalLvTree2.TreeLevelDepth = 0;
        //                hierarchicalLvTree2.Icon = (hierarchicalLvTree2.TreeLevelDepth < hierarchicalLvTree2.CalcLevel) ? "oi oi-folder" : "oi oi-document";
        //                hierarchicalLvTree2.IsSelected = false;

        //                hierarchicalLvTree.Children.Add(hierarchicalLvTree2);

        //                addChildren(hierarchicalLvTree2, lvList, 1);
        //            }
        //            else
        //            {
        //                hierarchicalLvTree.TreeLevelDepth = 0;
        //                hierarchicalLvTree.Icon = (hierarchicalLvTree.TreeLevelDepth < hierarchicalLvTree.CalcLevel) ? "oi oi-folder" : "oi oi-document";
        //                hierarchicalLvTree.IsSelected = false;

        //                addChildren(hierarchicalLvTree, lvList, 1);
        //            }

        //            Root.Children.Add(hierarchicalLvTree);
        //        }

        //        return Root;
        //    }

        //    private static void addChildren(HierarchicalLvTree parentHierarchicalLvTree, List<LvRecord> lvList, int TreeLevel)
        //    {
        //        var childList = lvList.Where(x => x.TnParentId == parentHierarchicalLvTree.PaplvdTnId).ToList();

        //        if (childList.Count() == 0)
        //        {
        //            //parentHierarchicalLvTree.Icon = "oi oi-target";
        //        }

        //        foreach (var child in childList)
        //        {
        //            HierarchicalLvTree hierarchicalLvTree = new HierarchicalLvTree(child);
        //            hierarchicalLvTree.TreeLevelDepth = TreeLevel;
        //            hierarchicalLvTree.Icon = (hierarchicalLvTree.TreeLevelDepth < hierarchicalLvTree.CalcLevel) ? "oi oi-folder" : "oi oi-document";
        //            hierarchicalLvTree.IsSelected = false;

        //            addChildren(hierarchicalLvTree, lvList, (++TreeLevel));
        //            parentHierarchicalLvTree.Children.Add(hierarchicalLvTree);
        //        }
        //    }
        //}


        //public class HierarchicalLvTree
        //{
        //    public HierarchicalLvTree() { }
        //    public HierarchicalLvTree(LvRecord lvTree)
        //    {
        //        this.TnParentId = lvTree.TnParentId;
        //        this.PaplvdTnId = lvTree.PaplvdTnId;
        //        this.PaplvdPapId = lvTree.PaplvdPapId;
        //        this.CalcLevel = lvTree.CalcLevel;
        //        this.Massnahme = lvTree.Massnahme;
        //        this.MassnahmeGuid = lvTree.MassnahmeGuid;
        //        this.Descr = lvTree.Descr;
        //    }

        //    public Decimal? PaplvdPapId { get; set; }
        //    public Decimal? PaplvdTnId { get; set; }
        //    public Decimal? TnParentId { get; set; }
        //    public bool IsSelected { get; set; } = false;
        //    public String Descr { get; set; }
        //    public int CalcLevel { get; set; }
        //    public string Massnahme { get; set; }
        //    public string MassnahmeGuid { get; set; }

        //    public List<HierarchicalLvTree> Children { get; set; } = new List<HierarchicalLvTree>();

        //    public string Icon { get; set; }
        //    public int TreeLevelDepth { get; set; }
        //    public string BackgroundColor { get { return GetBackgroundColor(); } }

        //    public string GetBackgroundColor()
        //    {
        //        if (IsSelected) return "SelectedBgColor";
        //        if (TreeLevelDepth < CalcLevel) return "LeistungBgColorBeforeCalcLevel";
        //        if (TreeLevelDepth == CalcLevel) return "LeistungBgColorCalcLevel";
        //        if (TreeLevelDepth > CalcLevel) return "LeistungBgColorAfterCalcLevel";

        //        return "LeistungBgColorBeforeCalcLevel";
        //    }

        //    private int _colorwidth = 30;

        //    public string ColorWidth { get { return GetColorWidth(); } }

        //    public string GetColorWidth()
        //    {
        //        var treeDepth = TreeLevelDepth + 1;
        //        return $"{_colorwidth - (treeDepth * 2) }em";
        //    }
        //}
    }
}

