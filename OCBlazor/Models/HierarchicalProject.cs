﻿ 

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OCBlazor.Models
{

    //public class TestCreator
    //{

    //    public HierarchicalProject HierarchicalProjectCreator2()
    //    {
    //        HierarchicalProject Root = new HierarchicalProject();
    //        Root.Name = "Root";
    //        int siblings = 20;
    //        int treedepth = 4;
    //        CreateChildren(Root, 1, treedepth, siblings);
    //        return Root;
    //    }

    //    private void CreateChildren(HierarchicalProject parent, int d, int depth, int siblings)
    //    {
    //        var increasingSiblings = (int)((double)siblings * ((double)d / (double)depth));
    //        if (increasingSiblings < 1) increasingSiblings = 1;
    //        for (int s = 1; s < increasingSiblings; s++)
    //        {
    //            HierarchicalProject hp = new HierarchicalProject();
    //            hp.Name = $"{d}-{s} : My depth is {d}, and I am sibling number {s}.";
    //            if (d < depth)
    //            {
    //                CreateChildren(hp, d + 1, depth, siblings);
    //            }
    //            parent.Children.Add(hp);
    //        }
    //    }

    //    public HierarchicalProject HierarchicalProjectCreator()
    //    {
    //        HierarchicalProject A1 = new HierarchicalProject();
    //        A1.Name = "A1 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö,";
    //        HierarchicalProject B1 = new HierarchicalProject();
    //        B1.Name = "B1 - Ridiculous long name for a tree item";
    //        HierarchicalProject B2 = new HierarchicalProject();
    //        B2.Name = "B2 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö,";
    //        A1.Children.Add(B1);
    //        A1.Children.Add(B2);

    //        HierarchicalProject C1 = new HierarchicalProject();
    //        C1.Name = "C1 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö,";
    //        HierarchicalProject C2 = new HierarchicalProject();
    //        C2.Name = "C2 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö,";
    //        B1.Children.Add(C1);
    //        B1.Children.Add(C2);

    //        HierarchicalProject C3 = new HierarchicalProject();
    //        C3.Name = "C3 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö,";
    //        HierarchicalProject C4 = new HierarchicalProject();
    //        C4.Name = "C4 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö,";
    //        B2.Children.Add(C3);
    //        B2.Children.Add(C4);

    //        HierarchicalProject D1 = new HierarchicalProject();
    //        D1.Name = "D1 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, KP";
    //        HierarchicalProject D2 = new HierarchicalProject();
    //        D2.Name = "D2 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, KP";
    //        C1.Children.Add(D1);
    //        C1.Children.Add(D2);

    //        HierarchicalProject E1 = new HierarchicalProject();
    //        E1.Name = "E1 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, KA";
    //        HierarchicalProject E2 = new HierarchicalProject();
    //        E2.Name = "E2 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, KA";
    //        D1.Children.Add(E1);
    //        D1.Children.Add(E2);

    //        HierarchicalProject F1 = new HierarchicalProject();
    //        F1.Name = "F1 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, HA";
    //        HierarchicalProject F2 = new HierarchicalProject();
    //        F2.Name = "F2 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, HA";
    //        E1.Children.Add(F1);
    //        E1.Children.Add(F2);

    //        HierarchicalProject G1 = new HierarchicalProject();
    //        G1.Name = "G1 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G2 = new HierarchicalProject();
    //        G2.Name = "G2 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G3 = new HierarchicalProject();
    //        G3.Name = "G3 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G4 = new HierarchicalProject();
    //        G4.Name = "G4 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G5 = new HierarchicalProject();
    //        G5.Name = "G5 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G6 = new HierarchicalProject();
    //        G6.Name = "G6 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G7 = new HierarchicalProject();
    //        G7.Name = "G7 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G8 = new HierarchicalProject();
    //        G8.Name = "G8 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G9 = new HierarchicalProject();
    //        G9.Name = "G9 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G10 = new HierarchicalProject();
    //        G10.Name = "G10 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G11 = new HierarchicalProject();
    //        G11.Name = "G11 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G12 = new HierarchicalProject();
    //        G12.Name = "G12 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G13 = new HierarchicalProject();
    //        G13.Name = "G13 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G14 = new HierarchicalProject();
    //        G14.Name = "G14 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G15 = new HierarchicalProject();
    //        G15.Name = "G15 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G16 = new HierarchicalProject();
    //        G16.Name = "G16 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G17 = new HierarchicalProject();
    //        G17.Name = "G17 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        HierarchicalProject G18 = new HierarchicalProject();
    //        G18.Name = "G18 - Ridiculous long name for a tree item, with German characters ü, Ü, ä, Ä, ö, Ö, UA";
    //        F1.Children.Add(G1);
    //        F1.Children.Add(G2);
    //        F1.Children.Add(G3);
    //        F1.Children.Add(G4);
    //        F1.Children.Add(G5);
    //        F1.Children.Add(G6);
    //        F1.Children.Add(G7);
    //        F1.Children.Add(G8);
    //        F1.Children.Add(G9);
    //        F1.Children.Add(G10);
    //        F1.Children.Add(G11);
    //        F1.Children.Add(G12);
    //        F1.Children.Add(G13);
    //        F1.Children.Add(G14);
    //        F1.Children.Add(G15);
    //        F1.Children.Add(G16);
    //        F1.Children.Add(G17);
    //        F1.Children.Add(G18);




    //        return A1;
    //    }

    //    public Test1 Create()
    //    {
    //        Test1 A1 = new Test1();
    //        A1.Name = "A1";
    //        Test1 B1 = new Test1();
    //        B1.Name = "B1";
    //        Test1 B2 = new Test1();
    //        B2.Name = "B2";
    //        A1.Children.Add(B1);
    //        A1.Children.Add(B2);

    //        Test1 C1 = new Test1();
    //        C1.Name = "C1";
    //        Test1 C2 = new Test1();
    //        C2.Name = "C2";
    //        B1.Children.Add(C1);
    //        B1.Children.Add(C2);

    //        Test1 C3 = new Test1();
    //        C3.Name = "C3";
    //        Test1 C4 = new Test1();
    //        C4.Name = "C4";
    //        B2.Children.Add(C3);
    //        B2.Children.Add(C4);

    //        Test1 D1 = new Test1();
    //        D1.Name = "D1";
    //        Test1 D2 = new Test1();
    //        D2.Name = "D2";
    //        C1.Children.Add(D1);
    //        C1.Children.Add(D2);

    //        Test1 E1 = new Test1();
    //        E1.Name = "E1";
    //        Test1 E2 = new Test1();
    //        E2.Name = "E2";
    //        D1.Children.Add(E1);
    //        D1.Children.Add(E2);

    //        return A1;
    //    }
    //}

    //public class Test1
    //{
    //    public string Name { get; set; }
    //    public List<Test1> Children { get; set; } = new List<Test1>();
    //}

    
    //public class ProjectConverter
    //{

    //    public static string IconFunction(object o)
    //    {
    //        if (o is HierarchicalProject hp)
    //        {
    //            if (hp.ProjectLevel == ProjectLevel.KP) return "oi oi-box";
    //            if (hp.ProjectLevel == ProjectLevel.KA) return "oi oi-folder";
    //            if (hp.ProjectLevel == ProjectLevel.HA) return "oi oi-file"; //oi-paperclip
    //            if (hp.ProjectLevel == ProjectLevel.UA) return "oi oi-document";
    //        }

    //        if (o is ProjectTreeItem pti)
    //        {
    //            if (pti.ProjectLevel == ProjectLevel.KP) return "oi oi-box";
    //            if (pti.ProjectLevel == ProjectLevel.KA) return "oi oi-folder";
    //            if (pti.ProjectLevel == ProjectLevel.HA) return "oi oi-file"; //oi-paperclip
    //            if (pti.ProjectLevel == ProjectLevel.UA) return "oi oi-document";
    //        }
    //        return "";
    //    }


    //    public static String RemoveDiacritics(String s)
    //    {
    //        String normalizedString = s.Normalize(NormalizationForm.FormD);
    //        StringBuilder stringBuilder = new StringBuilder();

    //        for (int i = 0; i < normalizedString.Length; i++)
    //        {
    //            Char c = normalizedString[i];
    //            if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
    //                stringBuilder.Append(c);
    //        }

    //        return stringBuilder.ToString();
    //    }

        
    //    public static ProjectTreeItem ProjectListToProjectTreeConverter(List<Project> projectList)
    //    {
    //        ProjectTreeItem Root = new ProjectTreeItem();
    //        ProjectTreeItem KP = null;
    //        ProjectTreeItem KA = null;
    //        ProjectTreeItem HA = null;
    //        ProjectTreeItem UA = null;
    //        ProjectLevel projectLevel;
    //        foreach (var project in projectList)
    //        {
    //            //= Remove german Characters                                                    Only 10 Chars.
    //            //string displayValue = RemoveDiacritics($"{project.PrjEbene} - {project.PrjCode} - {project.PrjName}").Substring(0, 10);
    //            string displayValue = $"{project.PrjEbene} - {project.PrjCode} - {project.PrjName}";
    //            string auftragsnr = project.PrjAuftragsnr;

    //            switch (project.PrjEbene)
    //            {
    //                case "KP":
    //                    projectLevel = ProjectLevel.KP;
    //                    KP = new ProjectTreeItem();
    //                    KP.Project = project;
    //                    KP.DisplayText = displayValue;
    //                    KP.ProjectLevel = projectLevel;
    //                    KP.Auftragsnr = auftragsnr;
    //                    KP.IconCssClassExpression = x => IconFunction(x);
    //                    KP.Icon = "oi oi-box";
    //                    break;
    //                case "KA":
    //                    projectLevel = ProjectLevel.KA;
    //                    KA = new ProjectTreeItem();
    //                    KA.Project = project;
    //                    KA.DisplayText = displayValue;
    //                    KA.ProjectLevel = projectLevel;
    //                    KA.Auftragsnr = auftragsnr;
    //                    KA.IconCssClassExpression = x => IconFunction(x);
    //                    KA.Icon = "oi oi-folder";
    //                    if (KP != null) KP.Children.Add(KA);
    //                    break;
    //                case "HA":
    //                    projectLevel = ProjectLevel.HA;
    //                    HA = new ProjectTreeItem();
    //                    HA.Project = project;
    //                    HA.DisplayText = displayValue;
    //                    HA.ProjectLevel = projectLevel;
    //                    HA.Auftragsnr = auftragsnr;
    //                    HA.IconCssClassExpression = x => IconFunction(x);
    //                    HA.Icon = "oi oi-file";
    //                    if (KA != null) KA.Children.Add(HA);
    //                    break;
    //                case "UA":
    //                    projectLevel = ProjectLevel.UA;
    //                    UA = new ProjectTreeItem();
    //                    UA.Project = project;
    //                    UA.DisplayText = displayValue;
    //                    UA.ProjectLevel = projectLevel;
    //                    UA.Auftragsnr = auftragsnr;
    //                    UA.IconCssClassExpression = x => IconFunction(x);
    //                    UA.Icon = "oi oi-document";
    //                    if (HA != null) HA.Children.Add(UA);
    //                    break;
    //            }
    //        }
    //        Root.Children.Add(KP);
    //        return Root;
    //    }

    //        public static HierarchicalProject ProjectToHierarchicalProjectConverter(List<Project> projectList)
    //    {
    //        HierarchicalProject Root = new HierarchicalProject();
    //        HierarchicalProject KP = null;
    //        HierarchicalProject KA = null;
    //        HierarchicalProject HA = null;
    //        HierarchicalProject UA = null;
    //        ProjectLevel projectLevel;
    //        foreach (var project in projectList)
    //        {
    //            //= Remove german Characters                                                    Only 10 Chars.
    //            //string displayValue = RemoveDiacritics($"{project.PrjEbene} - {project.PrjCode} - {project.PrjName}").Substring(0, 10);
    //            string displayValue = $"{project.PrjEbene} - {project.PrjCode} - {project.PrjName}";
    //            string auftragsnr = project.PrjAuftragsnr;

    //            switch (project.PrjEbene)
    //            {
    //                case "KP":
    //                    projectLevel = ProjectLevel.KP;
    //                    KP = new HierarchicalProject();
    //                    KP.Name = displayValue;
    //                    KP.ProjectLevel = projectLevel;
    //                    KP.AuftragsNr = auftragsnr;
    //                    KP.IconCssClassExpression = x => IconFunction(x);
    //                    KP.Icon = "oi oi-box";
    //                    break;
    //                case "KA":
    //                    projectLevel = ProjectLevel.KA;
    //                    KA = new HierarchicalProject();
    //                    KA.Name = displayValue;
    //                    KA.ProjectLevel = projectLevel;
    //                    KA.AuftragsNr = auftragsnr;
    //                    KA.IconCssClassExpression = x => IconFunction(x);
    //                    KA.Icon = "oi oi-folder";
    //                    if (KP != null) KP.Children.Add(KA);
    //                    break;
    //                case "HA":
    //                    projectLevel = ProjectLevel.HA;
    //                    HA = new HierarchicalProject();
    //                    HA.Name = displayValue;
    //                    HA.ProjectLevel = projectLevel;
    //                    HA.AuftragsNr = auftragsnr;
    //                    HA.IconCssClassExpression = x => IconFunction(x);
    //                    HA.Icon = "oi oi-file";
    //                    if (KA != null) KA.Children.Add(HA);
    //                    break;
    //                case "UA":
    //                    projectLevel = ProjectLevel.UA;
    //                    UA = new HierarchicalProject();
    //                    UA.Name = displayValue;
    //                    UA.ProjectLevel = projectLevel;
    //                    UA.AuftragsNr = auftragsnr;
    //                    UA.IconCssClassExpression = x => IconFunction(x);
    //                    UA.Icon = "oi oi-document";
    //                    if (HA != null) HA.Children.Add(UA);
    //                    break;
    //            }
    //        }
    //        Root.Children.Add(KP);
    //        return Root;
    //    }
    //}

    //public class DxTreeData
    //{
    //    public List<HierarchicalProject> Children { get; set; } = new List<HierarchicalProject>();

    //}

    //[Serializable]
    //public class ProjectTreeItem : TreeData
    //{
    //    public ProjectLevel ProjectLevel { get; set; } = ProjectLevel.KP;
    //    public Project Project { get; set; } = new Project();
    //    public string Auftragsnr { get; set; } = "";
    //    public override string BackgroundColor
    //    {
    //        get
    //        {
    //            if (this.IsSelected)
    //            {
    //                return "DefaultTreeSelectedBackgroundColor";
    //            }
    //            else
    //            {
    //                return "DefaultTreeBackgroundColor";
    //            }
    //        }
    //    }
    //}
    //[Serializable]
    //public class LvTreeItem : TreeData
    //{
    //    public string Auftragsnr { get; set; } = "";
    //    public Decimal? PaplvdPapId { get; set; }
    //    public Decimal? PaplvdTnId { get; set; }
    //    public Decimal? TnParentId { get; set; }
    //    public String Descr { get; set; } = "";
    //    public int CalcLevel { get; set; } = 0;
    //    public string Massnahme { get; set; } = "";
    //    public String MassnahmeGuid { get; set; } = "";

    //    public override string DisplayText { get => Descr; set => base.DisplayText = value; }

    //    public override string BackgroundColor
    //    {
    //        get
    //        {
    //            if (this.IsSelected)
    //            {
    //                return "DefaultTreeSelectedBackgroundColor";
    //            }
    //            else
    //            {
    //                return "DefaultTreeBackgroundColor";
    //            }
    //        }
    //    }

    //    public LvTreeItem() { }
    //    public LvTreeItem(LvRecord lvRecord)
    //    {
    //        this.TnParentId = lvRecord.TnParentId;
    //        this.PaplvdTnId = lvRecord.PaplvdTnId;
    //        this.PaplvdPapId = lvRecord.PaplvdPapId;
    //        this.CalcLevel = lvRecord.CalcLevel;
    //        this.Massnahme = lvRecord.Massnahme;
    //        this.MassnahmeGuid = lvRecord.MassnahmeGuid;
    //        this.Descr = lvRecord.Descr;
    //    }
    //    public LvRecord ToLvTreeRecord()
    //    {
    //        LvRecord newLvTreeRecord = new LvRecord();

    //        newLvTreeRecord.TnParentId = this.TnParentId;
    //        newLvTreeRecord.PaplvdTnId = this.PaplvdTnId;
    //        newLvTreeRecord.PaplvdPapId = this.PaplvdPapId;
    //        newLvTreeRecord.CalcLevel = this.CalcLevel;
    //        newLvTreeRecord.Massnahme = this.Massnahme;
    //        newLvTreeRecord.MassnahmeGuid = this.MassnahmeGuid;
    //        newLvTreeRecord.Descr = this.Descr;
    //        return newLvTreeRecord;
    //    }
    //}

    //public class HierarchicalProject 
    //{
    //    public string Name { get; set; }

    //    public ProjectLevel ProjectLevel { get; set; }

    //    public Expression<Func<object, string>> IconCssClassExpression { get; set; }

    //    public string AuftragsNr { get; set; }

    //    public List<HierarchicalProject> Children { get; set; } = new List<HierarchicalProject>();

    //    public string Icon { get; set; }

    //    private int _colorwidth = 30;

    //    public string ColorWidth { get { return GetColorWidth();  } }

    //    public bool IsSelected { get; set; } = false;

    //    public string GetColorWidth()
    //    {
    //        switch (ProjectLevel)
    //        {
    //            case ProjectLevel.KP: return $"{_colorwidth}em";
    //            case ProjectLevel.KA: return $"{_colorwidth - 2}em";
    //            case ProjectLevel.HA: return $"{_colorwidth - 4}em";
    //            case ProjectLevel.UA: return $"{_colorwidth - 6}em";
    //        }
    //        return "0em";
    //    }

    //    public string Color { get { return GetColor(); } }
    //    public string BackgroundColor { get { return GetBackgroundColor(); } }

    //    public string GetColor()
    //    {
    //        switch (ProjectLevel)
    //        {
    //            case ProjectLevel.KP: return "KpColor";
    //            case ProjectLevel.KA: return "KaColor";
    //            case ProjectLevel.HA: return "HaColor";
    //            case ProjectLevel.UA: return "UaColor";
    //        }
    //        return "";
    //    }

    //    public string GetBackgroundColor()
    //    {
    //        if (IsSelected) return "SelectedBgColor";
    //        switch (ProjectLevel)
    //        {
    //            case ProjectLevel.KP: return "KpBgColor";
    //            case ProjectLevel.KA: return "KaBgColor";
    //            case ProjectLevel.HA: return "HaBgColor";
    //            case ProjectLevel.UA: return "UaBgColor";
    //        }
    //        return "";
    //    }
        

    //}





    //public enum ProjectLevel
    //{
    //    KP, KA, HA, UA
    //}
}
