﻿using AutoMapper;
using Database.DataAccessObjects;
using MimeKit.Cryptography;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading.Tasks;


namespace OCBlazor.PageModels
{
    public partial class KasbeheerTransactieCM : KasbeheerTransactie
    {


        public String UniqueNr { get; set; } = "";
        public DateTime TransactieDatumDag
        {

            get
            {

                return TransactieDatum.Date;
                   
            }

        }
        public String TransactieTypeName

        {

            get
            {

                return TransactieType.Name;

            }

        }
      

        public KasbeheerTransactieCM()
        {

        }
    }

   

    public partial class KasbeheerInputCM  
    {

        

        public Decimal OldTransactieBedrag { get; set; }


        private Decimal _transactieBedrag;
        public Decimal TransactieBedrag
        {
            get
            {
                return Math.Abs(_transactieBedrag);
            }
            set {

                _transactieBedrag = value;
                FillBedrag();
            }       
              
            
        } 

        private String _StortenOfAfhalen;
        public String StortenOfAfhalen
        {
            get
            {
                return _StortenOfAfhalen;
            }
            set
            {

                _StortenOfAfhalen = value;
                FillBedrag();

            }
        }

        private void FillBedrag()
        {
            if (StortenOfAfhalen == "uitgaven")
                this.Transactie.TransactieBedrag = 0 - _transactieBedrag;
            else
                this.Transactie.TransactieBedrag = 0 + _transactieBedrag;

        }

        public KasbeheerTransactieCM Transactie { get; set; }

        public KasbeheerInputCM(KasbeheerTransactieCM transactie)  
        {
            this.OldTransactieBedrag = transactie.TransactieBedrag;
            this.Transactie = transactie;
            Init();

        }
        public KasbeheerInputCM()
        {
            this.OldTransactieBedrag = 0;
            this.Transactie = new KasbeheerTransactieCM() { TransactieDatum = DateTime.Now };
            Init();

        }

        private void Init()
        {
            _transactieBedrag = Math.Abs(Transactie.TransactieBedrag);
            if (Transactie.TransactieBedrag <= 0)
                StortenOfAfhalen = "uitgaven";
            else StortenOfAfhalen = "inkomsten";
        }


        public void OnClientChoosenAction (ClientDao client)
        {


        }

    }


}
