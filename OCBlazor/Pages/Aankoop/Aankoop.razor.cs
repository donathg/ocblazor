﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using OCBlazor.Models;
using OCBlazor.PageModels;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic;
using Logic.Clienten;
using Logic.ArtikelNs;
using Logic.DataBusinessObjects.Aankoop;
using Database.DataAccessObjects;
using DevExpress.Blazor;
using Sotsera.Blazor.Toaster;
//using Serilog;

namespace OCBlazor.Pages.Aankoop
{
    public class AankoopPageModel : ComponentBase, IDisposable
    {
        [Parameter]
        public int ArtikelCategorieID { get; set; } = 1;

        [Inject]
        public IArtikelenService _artikelService { get; set; }
        [Inject]
        public IAankoopService _aankoopService { get; set; }
        [Inject]
        protected IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }
        [Inject]
        protected IToaster _toaster { get; set; }
        [Inject]
        protected IJSRuntime _jsRuntime { get; set; }

        public DxDataGrid<AankoopArtikel> GridAankoopArtikel { get; set; }
        public ObservableCollection<EntiteitAankoop> EntiteitAankoopList { get { return _aankoopService.ClientAankoopList; } }
        public EntiteitAankoop SelectedEntiteitAankoop { get { return _selectedEntiteitAankoop; } set { _selectedEntiteitAankoop = value; StateHasChanged(); } }
        private EntiteitAankoop _selectedEntiteitAankoop { get; set; } = new EntiteitAankoop();
        //   public bool LocatiePopupIsVisible { get; set; } = false;
        public bool ClientenPopupIsVisible = false;
        public bool ArtikelPopupIsVisible = false;
        public bool LeefgroepPopupIsVisible = false;
        public string WindowTitle { get; set; } = "";

        public void OnActionOnCloseClientPopupMulti(List<Client> clienten)
        {
            if (clienten != null && clienten.Count > 0)
            {
                foreach (Client client in clienten)
                    _aankoopService.AddClient(client);
                this.SelectedEntiteitAankoop = this.EntiteitAankoopList.Where(x => x.Client.Id == clienten[0].Id).FirstOrDefault();
            }
            ClosePopups();
        }
        public void OnActionOnCloseLeefgroepPopupMulti(List<Leefgroep> leefgroepen)
        {
            if (leefgroepen != null && leefgroepen.Count > 0)
            {
                foreach (Leefgroep leefgroep in leefgroepen)
                    _aankoopService.AddLeefgroep(leefgroep);
                this.SelectedEntiteitAankoop = this.EntiteitAankoopList.Where(x => x.Leefgroep.Id == leefgroepen[0].Id).FirstOrDefault();
            }
            ClosePopups();
        }
        public void OnActionOnCloseLocatiePopup(Locatie locatie)
        {
            ClosePopups();
        }
        public void OnActionOnCloseArtikelPopupMulti(List<Artikel> artikelen)
        {
            if (artikelen != null)
            {
                foreach (Artikel artikel in artikelen)
                {
                    try
                    {
                        if (this.SelectedEntiteitAankoop.IsClient)
                            _aankoopService.AddArtikel(this.SelectedEntiteitAankoop.Client, artikel);
                        else
                            _aankoopService.AddArtikel(this.SelectedEntiteitAankoop.Leefgroep, artikel);
                    }
                    catch (Exception ex)
                    {
                        _toaster.Error(ex.Message, "Fout");
                    }
                }
            }
            ClosePopups();
        }
        protected override void OnParametersSet()
        {
            _aankoopService.Load(ArtikelCategorieID);
            this.WindowTitle = _artikelService.GetArtikelCategoryDescription(ArtikelCategorieID);
            base.OnParametersSet();
        }
        public void AddArtikelClick()
        {
            //  LocatiePopupIsVisible = true;
            ArtikelPopupIsVisible = true;
            StateHasChanged();
        }
        public void AddClientClick()
        {
            ClientenPopupIsVisible = true;
            StateHasChanged();
        }
        public void AddLeefgroepClick()
        {
            LeefgroepPopupIsVisible = true;
            StateHasChanged();
        }
        private void ClosePopups()
        {
            ArtikelPopupIsVisible = false;
            ClientenPopupIsVisible = false;
            LeefgroepPopupIsVisible = false;

            StateHasChanged();
        }
        public void OnRowUpdating(AankoopArtikel artikel, Dictionary<string, object> newValue)
        {
            if (newValue.ContainsKey("Aantal"))
            {
                try
                {
                    int newAantal;
                    if (Int32.TryParse(newValue["Aantal"].ToString(), out newAantal))
                    {
                        if (newAantal > 0)
                        {
                            _aankoopService.UpdateArtikel(artikel.DatabaseId, newAantal);
                            artikel.Aantal = newAantal;
                        }
                        else
                        {
                            if (this.SelectedEntiteitAankoop.Client == null || this.SelectedEntiteitAankoop.Client.Id == 0)
                                _aankoopService.DeleteArtikel(this.SelectedEntiteitAankoop.Leefgroep, artikel.ArtikelId);
                            else
                                _aankoopService.DeleteArtikel(this.SelectedEntiteitAankoop.Client, artikel.ArtikelId);
                        }
                    }
                    else
                        throw new Exception("Gelieve een geldig aantal in te geven (geheel getal).");
                }
                catch (Exception ex)
                {
                    _toaster.Error(ex.Message, "Fout");
                }
            }
        }
        public Task OnInitNewRow(Dictionary<string, object> values)
        {

            ArtikelPopupIsVisible = true;
            StateHasChanged();
            return Task.CompletedTask;
        }
        public void Dispose()
        {
         //   Log.Information("Dispose Aankoop");

            //   GC.SuppressFinalize(this);
            
            if (GridAankoopArtikel != null)
                GridAankoopArtikel = null;
            if (EntiteitAankoopList != null)
            {
                EntiteitAankoopList.Clear();
            }
        
            SelectedEntiteitAankoop = null;
        }
        ~AankoopPageModel()
        {
            Dispose();
        }
    }
}
