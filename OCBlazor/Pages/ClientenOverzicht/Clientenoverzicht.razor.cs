﻿using Database.DataAccessObjects;
using Logic;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.Clienten;

namespace OCBlazor.Pages
{

    public class ClientOverzichtClass : Client
    {
        public string LeefgroepNaam { get; set; }

    }
    public class ClientenoverzichtModelModel : ComponentBase
    {

        [Inject]
        public IClientService _clientenService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        public List<ClientOverzichtClass> ClientenVisual
        {
            get
            {
                return Clienten.OrderBy(o => o.LeefgroepNaam).ThenBy(o => o.FirstName).ToList();
            }
        }
        public List<ClientOverzichtClass> Clienten { get; set; } = new List<ClientOverzichtClass>();


        protected override void OnInitialized()
        {
            ClientenVisual.Clear();
            List<Client> allClienten = _clientenService.GetClientenAndLeefgroepen();
            foreach (Client c in allClienten)
            {
                if (c.FirstName == "HARRY")
                {

                }
                if ((c.OpenameEinde.HasValue && c.OpenameEinde.GetValueOrDefault().Date >= DateTime.Now.Date) || c.OpenameEinde.HasValue == false)
                {
                    foreach (Leefgroep lfg in c.Leefgroep)
                    {
                        if (lfg.Name == "Disney")
                            continue;
                        ClientOverzichtClass n = new ClientOverzichtClass();
                        n.FirstName = c.FirstName;
                        n.LastName = c.LastName;
                        n.LeefgroepNaam = lfg.Name;
                        n.OpenameEinde = c.OpenameEinde;
                        Clienten.Add(n);
                    }
                }
            }
        }
    }
}
