﻿using AutoMapper;
using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using OCBlazor.PageModels;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using OCBlazor.Models;
using Logic;
using Logic.DataBusinessObjects.MultiSelectNS;
using Sotsera.Blazor.Toaster;
using DataBase.Services;
using Logic.MultiSelectNS;
using Logic.ExitFormulier;
using System.ComponentModel;
using Logic.DataBusinessObjects.ExitFormulier;
//using Serilog;

namespace OCBlazor.Pages
{
    public class ExitEnqueteModel : ComponentBase
    {
        /** INJECTIONS */
        [Inject]
        public IMultiSelectService _multiSelectService { get; set; }
        [Inject]
        public IExitFromService _exitFormItemService { get; set; }
        [Inject]
        public IToaster _toaster { get; set; }
        [Inject]
        IJSRuntime js { get; set; }

        /** FIELDS */
        BackgroundWorker backGroundWorker = new BackgroundWorker();
        public List<MultiSelect> AllMultiSelect { get; set; } = new List<MultiSelect>();
        public ExitFormItem SelectedExitForm { get; set; }


        /** METHODS */
        // OVERRIDES
        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            Console.WriteLine("Exitenquete afterRender");
        }
        protected override void OnInitialized()
        {
            SelectedExitForm = new ExitFormItem();
            AllMultiSelect = _multiSelectService.GetAll("EXITFORM");

            ClearResultsFromMultiselects();
        }
        protected override void OnParametersSet()
        {
            base.OnParametersSet();
            //Editeren formulier
            SetMultiSelectItemResults();
        }
        // REGULAR METHODS
        protected void SetMultiSelectItemResults()
        {
            if (SelectedExitForm != null)
            {
                // ExitForm-melding uit DB invullen in the AllMultiSelect;
                foreach (MultiSelect component in AllMultiSelect)
                {
                    component.Items.ForEach((MultiSelectItem item) =>
                    {
                        if (SelectedExitForm.SelectedMultiSelectItemsResults.FirstOrDefault(x => x.MultiSelectItemId == item.Id) is MultiSelectItemResult res)
                        {
                            item.Result = res;
                        }
                    });
                }
            }
            StateHasChanged();
        }
        protected void ClearResultsFromMultiselects()
        {
            foreach (MultiSelect component in AllMultiSelect)
            {
                component.Items.ForEach((MultiSelectItem item) =>
                {
                    item.Parent.ErrorMessage = null;
                    item.Result.Reset();
                });
            }
        }
        public void OnChangedMultiSelect(MultiSelectItem multiselectItem)
        {
            StateHasChanged();
        }
        #region Buttons
        public void OKButton()
        {
            List<String> MultiSelectWithErrorIds = new List<String>();
            try
            {
                String errorMessage = "";

                // validate content
                foreach (MultiSelect m in this.AllMultiSelect)
                {
                    if (!m.Validate())
                    {
                        MultiSelectWithErrorIds.Add(m.Id.ToString());
                        errorMessage = $"Gelieve vraag: {m.Title} in te vullen. Deze vraag is verplicht.";
                    }
                }
                if (!String.IsNullOrWhiteSpace(errorMessage))
                {
                    throw new Exception(errorMessage);
                }
                // clear previous results and add new results
                this.SelectedExitForm.SelectedMultiSelectItemsResults.Clear();
                foreach (MultiSelect ms in this.AllMultiSelect)
                {
                    foreach (MultiSelectItem item in ms.Items.Where(x => x.Result.IsSelected))
                    {
                        this.SelectedExitForm.SelectedMultiSelectItemsResults.Add(item.Result);
                    }
                }

                // extra check on error + if not: save item + show message             
                SelectedExitForm.SetStatusFormValidatedAndCreated();

                // _exitFormItemService.SaveExitFormItem(this.SelectedExitForm);
                _toaster.Success("Alle gegevens zijn opgeslagen.", "Opgeslagen");
            }
            catch (Exception ex)
            {
                if (MultiSelectWithErrorIds.Count > 0)

                    js.InvokeAsync<object>(
                       "myLib.BlazorScrollToId",
                       "Anchor" + MultiSelectWithErrorIds[0]);

                _toaster.Error(ex.Message, "Fout");
            }
            finally
            {
                StateHasChanged();
            }
        }
        public void CancelButton()
        {
            _toaster.Warning("De gegevens zijn niet opgeslagen", "Geannuleerd");
            //Editmode_ChangeToListPage();
        }
        #endregion
    }
}
 