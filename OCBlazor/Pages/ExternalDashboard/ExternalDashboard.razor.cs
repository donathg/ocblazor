﻿using Database.DataAccessObjects;
using Logic;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.DataBusinessObjects.ExternalDashboard;
using Logic.ExternalDashboard;
using OCBlazor.Pages.ExternalDashboard;
using DevExpress.CodeParser;
using OCBlazor.Pages.GOG;
using DevExpress.Blazor;
using Logic.DataBusinessObjects.GOG;
using static System.Runtime.InteropServices.JavaScript.JSType;
using Common.DateTimeExtensions;
using Common.CloneExtensions;
using Sotsera.Blazor.Toaster;
using DataBase.Services;
using Microsoft.Extensions.Options;
using DevExpress.Blazor.Internal.Grid;
using DevExpress.DataAccess.DataFederation;
using Database.DatabaseParsers.Helper;

namespace OCBlazor.Pages
{

    public class ExternalDashboardModel : ComponentBase
    {


       
        [Inject]
        public IExternalDashboardService _externalDashboardService { get; set; }
        [Inject]
        protected IUserService _userService { get; set; }
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }
        [Inject]
        public IToaster _toaster { get; set; }

        public bool IsChartLoading { get; set; } = false;

        public  bool IsGeneratingChart { get; set; }
        //

        private int _activeTabIndex = 0;
        public int ActiveTabIndex { get {return _activeTabIndex; }

            set { _activeTabIndex = value; ResetParams(value); }
        }

      

        public bool IsDashboardOrbisColumnChooserVisible => ActiveTabIndex == 0;
        public bool IsDashboardOrbisExportExcelVisible => ActiveTabIndex == 0;
       
        public List<OrbisChartResult> OrbisChartResultList { get; set; } = new List<OrbisChartResult>();
        public OrbisChartParameters OrbisChartParameters { get; set; } = new OrbisChartParameters();
        public OrbisChartParameters OrbisChartParametersFilter { get; set; } = new OrbisChartParameters();
        public IGrid MyGrid { get; set; }
        public OrbisListDates OrbisListDates { get; set; } = new OrbisListDates();
        public OrbisChartDates OrbisChartDates { get; set; } = new OrbisChartDates();
        public List<ExternalDashboardMedewerkers> ExternalDashboardMedewerkersList { get; set; } = new List<ExternalDashboardMedewerkers>();
   

        public ExternalDashboardMedewerkers SelectedExternalDashboardMedewerkers { get; set; }
        public bool _documentatieFlyoutIsVisible { get; set; }

        public void GroepCheckedChanged(TreeViewCheckedChangedEventArgs e)
        {
            int count = e.CheckedItems.Count;
            OrbisChartParametersFilter.OrbisGroep.Clear();
            for (int i = 0; i < e.CheckedItems.Count; i++)
            {
                string x = ((ExternalDashboardMedewerkers)e.CheckedItems[i].DataItem).GroepCode;


                OrbisChartParametersFilter.OrbisGroep.Add(x);
            } 
        }
      

        protected override void OnInitialized()
        {
        }


        private void ResetParams(int tabIndex)
        {
            if (tabIndex==1)
            {
                OrbisChartParametersFilter.OrbisGroep.Clear();
                OrbisChartResultList.Clear();
            }
        }
        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
               LoadData();
            }

            return base.OnAfterRenderAsync(firstRender);
        }

        public bool IsLoadinggData { get; set; }
        public async Task LoadData()
        {

            IsLoadinggData = true;
            await Task.Delay(500);


            ExternalDashboardMedewerkersList = _externalDashboardService.GetExternalDashboardMedewerkers(OrbisListDates);


            IsLoadinggData = false;
            StateHasChanged();
         
        }
        public void OnStartOrbisListDateChanged(DateTime newValue)
        {
            OrbisListDates.StartDate = newValue.GetFirstDayOfMonth();
            
        }
        public void OnEndOrbisListDateChanged(DateTime newValue)
        {
            OrbisListDates.EndDate = newValue.GetLastDayOfMonth();
            
        }
        public void OnStartOrbisChartDateChanged(DateTime newValue)
        {
            OrbisChartDates.StartDate = newValue.GetFirstDayOfMonth();

        }
        public void OnEndOrbisChartDateChanged(DateTime newValue)
        {
            OrbisChartDates.EndDate = newValue.GetLastDayOfMonth();

        }
        public void ColumnChooserButton_Click()
        {
            MyGrid.ShowColumnChooser(".column-chooser-button");
        }
        public async Task ExportXlsx_Click()
        {


            System.String filename = $"{"Dashboard"}_{OrbisListDates.StartDate.Date.Year}_{OrbisListDates.StartDate.Date.Month:D2}_{OrbisListDates.StartDate.Date.Day:D2}_tot_{OrbisListDates.EndDate.Date.Year}_{OrbisListDates.EndDate.Date.Month:D2}_{OrbisListDates.EndDate.Date.Day:D2}";
            await MyGrid.ExportToXlsxAsync(filename, new GridXlExportOptions()
            {

            });
        }
        public void Grid_CustomizeElement(GridCustomizeElementEventArgs e)
        {
            if (e.ElementType == GridElementType.DataRow && e.VisibleIndex % 2 == 1)
            {
                e.CssClass = "alt-item";
            }
            if (e.ElementType == GridElementType.HeaderCell)
            {
                e.Style = "background-color: rgba(0, 0, 0, 0.08)";
                e.CssClass = "header-bold";
            }
        }
        public void Grid_CustomizeCellDisplayText(GridCustomizeCellDisplayTextEventArgs e)
        {
            if (e.Value is int val && val == 0)
            {
                e.DisplayText = "";
            }
            if ((e.FieldName == nameof(ExternalDashboardMedewerkers.Ziekte) || e.FieldName == nameof(ExternalDashboardMedewerkers.Arbeidsongeval) || e.FieldName == nameof(ExternalDashboardMedewerkers.VormingsurenExtern)) 
                && e.Value is double val2)
            {
                if (val2 == 0.00)
                {
                    e.DisplayText = "";
                }
                else
                {
                    e.DisplayText = val2 + " uren";
                }
            }
        }
        public void ChartOnloadedData()
        {

        }
        public async Task GenerateReportIncident()
        {
            try
            {

                OrbisChartResultList = new List<OrbisChartResult>();
              

                OrbisChartParameters = new OrbisChartParameters(OrbisChartParametersFilter);
                if (OrbisChartParameters.OrbisGroep.Count() == 0)
                    throw new Exception("Gelieve min. 1 groep uit te kiezen.");

                _toaster.Success("Grafiek wordt gegenereerd. Even geduld aub.");
                IsGeneratingChart = true;
                await Task.Delay(100);

                List<ExternalDashboardMedewerkersGraph> dbData = new List<ExternalDashboardMedewerkersGraph>();
                OrbisDates monthFlter = new OrbisDates();


                for (DateTime m = OrbisChartDates.StartDate; m <= OrbisChartDates.EndDate.GetFirstDayOfMonth(); m = m.AddMonths(1))
                {
                    monthFlter.StartDate = m.GetFirstDayOfMonth();
                    monthFlter.EndDate = m.GetLastDayOfMonth();
                    List<ExternalDashboardMedewerkers> dbDataMonth = _externalDashboardService.GetExternalDashboardMedewerkers(monthFlter);
                    foreach (ExternalDashboardMedewerkers i in dbDataMonth)
                    {
                        ExternalDashboardMedewerkersGraph g = new ExternalDashboardMedewerkersGraph();
                        g.ExternalDashboardMedewerkers = i;
                        g.DayDate = m;
                        dbData.Add(g);
                    }
                }
                OrbisChartResultList = OrbisChartLogic.GenerateChartValues(OrbisChartParameters, dbData);
                _toaster.Success("Grafiek is gegenereerd.");
            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
            finally
            {
                IsGeneratingChart = false;
            
            }
        }
        public void SubGrid_CustomizeCellDisplayText(GridCustomizeCellDisplayTextEventArgs e)
        {
            if (e.Value is double val2)
            {
                if (val2 == 0.00)
                {
                    e.DisplayText = "";
                }
                else
                {
                    e.DisplayText = val2 + " uren";
                }
            }
        }
        public void DocumentatieButton_Click()
        {
            _documentatieFlyoutIsVisible = true;
            StateHasChanged();
        }



        public DxTreeView treeView;
        protected override void OnAfterRender(bool firstRender)
        {
           /* if (treeView !=null)
             treeView.SetNodeChecked((n) => n.Text == "AT - Onthaal", true);*/
 
            base.OnAfterRender(firstRender);
        }

    }
}
