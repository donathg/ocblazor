﻿using DevExpress.Office.Utils;
using Force.DeepCloner;
using Logic.DataBusinessObjects.ExternalDashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using static DevExpress.Xpo.Helpers.AssociatedCollectionCriteriaHelper;

namespace OCBlazor.Pages.ExternalDashboard
{

    [Serializable]
    public enum OrbisChartValues
    {
        Voltijds = 0,
        Deeltijds = 1,
        Aanwerving = 3,
        UitDienst = 4,
        Ziekte = 5,
        Arbeidsongeval = 6,
        Werkhervatting = 7
   

    }

    [Serializable]
    public class OrbisChartParameters
    {

        public OrbisChartParameters()
        {
        }
            public OrbisChartParameters (OrbisChartParameters p)
        {

            this.FromDate = p.FromDate; 
            this.ToDate = p.ToDate;
            this.OrbisChartType = p.OrbisChartType; 
            this.OrbisChartValue = p.OrbisChartValue;
            this.OrbisGroep = new List<string>();
            foreach (string s in p.OrbisGroep)
                this.OrbisGroep.Add(s);
            this.ShowLabels = p.ShowLabels;
            this.ResultSummed = p.ResultSummed;
        }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public OrbisChartValues OrbisChartValue { get; set; }

        public String OrbisChartType { get; set; } = "Line";

        public bool ResultSummed { get; set; } = false;

        public List<string> OrbisGroepToChart { get; set; } = new List<string>();
        public List<string> OrbisGroep { get; set; } = new List<string>();
        public bool ShowLabels { get; set; } = true;

    }
    public class OrbisChartResult
    {

        public String OrbisGroep { get; set; } 

        public DateTime DayDate { get; set; }

        public double Aantal { get; set; }

    }

    public static class OrbisChartLogic
    {


   /*     public static List<OrbisChartResult> GenerateChartValues(OrbisChartParameters orbisChartParameters, List<ExternalDashboardMedewerkersGraph> externalDashboardMedewerkersList)
        {
           


          orbisChartParameters.OrbisGroepToChart = orbisChartParameters.OrbisGroep;
            List<OrbisChartResult> result = new List<OrbisChartResult>();
            String chartOption = "";
            foreach (ExternalDashboardMedewerkersGraph e in externalDashboardMedewerkersList)
            {
               
             
                    OrbisChartResult r = new OrbisChartResult();
                    r.OrbisGroep = e.ExternalDashboardMedewerkers.GroepCode;
                    switch (Enum.GetName(orbisChartParameters.OrbisChartValue))
                    {
                       

                        case "Voltijds":
                            r.Aantal = e.ExternalDashboardMedewerkers.Voltijds;
                            break;
                        case "Deeltijds":
                            r.Aantal = e.ExternalDashboardMedewerkers.Deeltijds;
                            break;
                        case "Ziekte":
                            r.Aantal = e.ExternalDashboardMedewerkers.Ziekte;
                            break;
                        case "Aanwerving":
                            r.Aantal = e.ExternalDashboardMedewerkers.Aanwerving;
                            break;
                        case "UitDienst":
                            r.Aantal = e.ExternalDashboardMedewerkers.UitDienst;
                            break;
                        case "Arbeidsongeval":
                            r.Aantal = e.ExternalDashboardMedewerkers.Arbeidsongeval;
                            break;
                        case "Werkhervatting":
                            r.Aantal = e.ExternalDashboardMedewerkers.Werkhervatting;
                            break;
               

                    }

               
                   
                    r.DayDate = e.DayDate;
                    result.Add(r);
                }
            
   
            return result;

        }*/
   
        
       

        public static List<OrbisChartResult> GenerateChartValues(OrbisChartParameters orbisChartParameters, List<ExternalDashboardMedewerkersGraph> externalDashboardMedewerkersList)
        {
            OrbisChartParameters orbisChartParametersClone = new OrbisChartParameters(orbisChartParameters);
            orbisChartParameters.OrbisGroepToChart = orbisChartParameters.OrbisGroep;
            if (orbisChartParametersClone.ResultSummed)
            {
                orbisChartParameters.OrbisGroepToChart.Clear();
                orbisChartParameters.OrbisGroepToChart.Add("Gesommeerd");

                foreach (ExternalDashboardMedewerkersGraph e in externalDashboardMedewerkersList)
                {
                    if (orbisChartParametersClone.OrbisGroep.Contains(e.ExternalDashboardMedewerkers.GroepCode))
                    {
                        e.ExternalDashboardMedewerkers.GroepCode = "Gesommeerd";
                    }
                }
              
            }

            List<OrbisChartResult> result = new List<OrbisChartResult>();
            String chartOption = "";
            foreach (ExternalDashboardMedewerkersGraph e in externalDashboardMedewerkersList)
            {


                OrbisChartResult r = new OrbisChartResult();
                r.OrbisGroep = e.ExternalDashboardMedewerkers.GroepCode;
                switch (Enum.GetName(orbisChartParametersClone.OrbisChartValue))
                {


                    case "Voltijds":
                        r.Aantal = e.ExternalDashboardMedewerkers.Voltijds;
                        break;
                    case "Deeltijds":
                        r.Aantal = e.ExternalDashboardMedewerkers.Deeltijds;
                        break;
                    case "Ziekte":
                        r.Aantal = e.ExternalDashboardMedewerkers.Ziekte;
                        break;
                    case "Aanwerving":
                        r.Aantal = e.ExternalDashboardMedewerkers.Aanwerving;
                        break;
                    case "UitDienst":
                        r.Aantal = e.ExternalDashboardMedewerkers.UitDienst;
                        break;
                    case "Arbeidsongeval":
                        r.Aantal = e.ExternalDashboardMedewerkers.Arbeidsongeval;
                        break;
                    case "Werkhervatting":
                        r.Aantal = e.ExternalDashboardMedewerkers.Werkhervatting;
                        break;


                }



                r.DayDate = e.DayDate;
                result.Add(r);
            }


            return result;

        }
    }
}
