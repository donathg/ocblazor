﻿using Common.DateTimeExtensions;
using System;

namespace OCBlazor.Pages.GOG
{
   
    public class GOGMainFilter
    {

        private DateTime _startDate { get; set; }
        private DateTime _endDate { get; set; }

        public DateTime StartDate { get => _startDate; set { _startDate = value; } }
        public DateTime EndDate { get => _endDate; set { _endDate = value; } }


        public GOGMainFilter()
        {

            _startDate = DateTime.Now.AddDays(-90).GetFirstDayOfMonth().Date;
            _endDate = DateTime.Now.GetLastDayOfMonth().Date;
        }
    }
}
