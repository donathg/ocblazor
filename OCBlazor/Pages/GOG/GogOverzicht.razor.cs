﻿using Microsoft.AspNetCore.Components;
using OCBlazor.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using OCBlazor.Models;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.Kilometervergoeding;
using Logic.WagenNS;
using System;
using DevExpress.Blazor;
using DataBase.Services;
using System.Linq;
using Logic.DataBusinessObjects.MultiSelectNS;
using Logic.MultiSelectNS;
using Logic.DataBusinessObjects.GOG;
using Logic.GOG;
using Logic.Clienten;
using Logic.LeefgroepNS;
using Sotsera.Blazor.Toaster;
using Microsoft.JSInterop;
using Reporting.Maaltijdlijsten;
using DevExpress.ClipboardSource.SpreadsheetML;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports;
using System.IO;
using Force.DeepCloner;
//using Serilog;
using System.Threading;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Collections;
using Common.DateTimeExtensions;
using MailKit.Net.Imap;

namespace OCBlazor.Pages.GOG
{

    public class GogOverzichtPageModel : ComponentBase/*, IDisposable*/
    {
        /** PARAMETERS */
        [Parameter]
        public int? Id { get; set; } // waarom? kies het uit de lijst. Zo niet, creëer dan een GOGDetail component (idem aan GOGList) en roep dat dan op
        [Parameter]
        public GOGItem SelectedGOG { get; set; }
        [Parameter]
        public int PageSize { get; set; }

        /** INJECTIONS */
        [Inject]
        protected IUserService _userService { get; set; }
        [Inject]
        public IMultiSelectService _multiSelectService { get; set; }
        [Inject]
        public IGOGItemService _gogItemService { get; set; }
        [Inject]
        public IClientService _clientService { get; set; }
        [Inject]
        public ILeefgroepService _leefgroepService { get; set; }
        [Inject]
        public IGOGReportService _gOGReportService { get; set; }
        [Inject]
        public IToaster _toaster { get; set; }
        [Inject]
        IJSRuntime js { get; set; }

        /** FIELDS */
        public List<MultiSelect> AllMultiSelect { get; set; } = new List<MultiSelect>();
 
        public List<GOGItem> GOGLijst { get; set; } = new List<GOGItem>();
        public List<Client> MyClienten { get; set; } = new List<Client>();
        public List<Client> MyDoorgeefClienten { 
            get
            {
                if (MyClienten != null && MyClienten.Count != 0)
                    return MyClienten;

                if (MyClientenRechten != null && MyClientenRechten.Count != 0)
                {
                    List<Client> tempListClienten = new List<Client>();
                    foreach (int clientId in MyClientenRechten)
                    {
                        tempListClienten.Add(_clientService.GetClient(clientId));
                    }

                    return tempListClienten;
                }
                else
                    return new List<Client>();
            }
        }
        public List<Client> AllMyClienten { get; set; } = new List<Client>();
        public List<Leefgroep> MyLeefgroepen { get; set; } = new List<Leefgroep>();
        public List<Leefgroep> MyDoorgeefLeefgroepen { get
            {
                if(MyLeefgroepen != null && MyLeefgroepen.Count != 0)
                    return MyLeefgroepen;

                if (SelectedGOG.BetrokkenBewoners != null && SelectedGOG.BetrokkenBewoners.Count > 0)
                {
                    List<Leefgroep> tempList = new List<Leefgroep>();

                    foreach (Client client in SelectedGOG.BetrokkenBewoners)
                    {
                        tempList.AddRange(client.Leefgroep);
                    }

                    return tempList.DistinctBy(x => x.Id).ToList();
                }
                else
                    return new List<Leefgroep>();
            }
        }  
        public List<Gebruiker> SelectedGebruikers { get; set; } = new List<Gebruiker>();
        public List<Client> SelectedClienten { get; set; } = new List<Client>();
        public List<int> MyClientenRechten { get; set; } = new List<int>();

        #region Static Lists of GOG & SGOG (Change to flag in DB + load)
        private readonly List<int> GOGItemsList = new List<int>() { 1, 2, 3, 8, 10, 9, 20, 21, 25, 28, 29, 30, 31, 32, 33, 34, 36, 38, 39, 40, 42, 48, 50, 51, 55, 56 };
        private readonly List<int> SGOGItemsList = new List<int>() { 1, 2, 3, 8, 10, 13, 14, 16, 17, 18, 19 };
        private readonly List<int> VandalismeItemsList = new List<int>() { 1, 2, 3, 8, 10, 59, 61, 62, 63 };
        private readonly List<int> WegloopgedragItemsList = new List<int>() { 1, 2, 3, 8, 10, 59, 61, 62, 63 };
        private readonly List<int> BetredenGebouwItemsList = new List<int>() { 1, 2, 3, 8, 10, 59, 61, 62, 63 };
        private readonly List<int> SamenscholingItemsList = new List<int>() { 1, 2, 3, 8, 10, 59, 61, 62, 63 };
        private readonly List<int> GlobalItemsList = new List<int>() { 1, 2, 3, 8, 10 }; // not used. Just info
        #endregion
        BackgroundWorker backGroundWorker = new BackgroundWorker();
        protected bool _editMode = false;
        protected bool IsGogItemEditable { get; set; }
        public bool ShowSpinner { get; set; } = true;
        #region Visibility popups
        protected bool _leefgroepPopupIsVisible = false;
        protected bool _clientenPopupIsVisible = false;
        protected bool _gebruikerPopupIsVisible = false;
        protected bool _savePopupIsVisible = false;
        protected bool _statusPopupIsVisible = false;
        #endregion
        #region Visibility Multiselects

        public GOGMainFilter MainFilter = new GOGMainFilter();
        protected bool IsGOGSelected
        {
            get
            {
                if (AllMultiSelect.FirstOrDefault(x => x.Id == 10) is MultiSelect select &&
                    select.Items.Find(x => x.Result.MultiSelectItemId == 21 && x.Result.IsSelected) != null)
                {
                    AllMultiSelect.ForEach((MultiSelect select) =>
                    {
                        if (GOGItemsList.Contains(select.Id))
                            select.IsVisible = true;
                        else
                            select.IsVisible = false;
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected bool IsSGOGSelected
        {
            get
            {
                if (AllMultiSelect.FirstOrDefault(x => x.Id == 10) is MultiSelect select &&
                    select.Items.Find(x => x.Result.MultiSelectItemId == 22 && x.Result.IsSelected) != null)
                {
                    AllMultiSelect.ForEach((MultiSelect select) =>
                    {
                        if (SGOGItemsList.Contains(select.Id))
                            select.IsVisible = true;
                        else
                            select.IsVisible = false;
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected bool IsVandalisme
        {
            get
            {
                if (AllMultiSelect.FirstOrDefault(x => x.Id == 10) is MultiSelect select &&
                    select.Items.Find(x => x.Result.MultiSelectItemId == 134 && x.Result.IsSelected) != null)
                {
                    AllMultiSelect.ForEach((MultiSelect select) =>
                    {
                        if (VandalismeItemsList.Contains(select.Id))
                            select.IsVisible = true;
                        else
                            select.IsVisible = false;
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected bool IsWegloopgedrag
        {
            get
            {
                if (AllMultiSelect.FirstOrDefault(x => x.Id == 10) is MultiSelect select &&
                    select.Items.Find(x => x.Result.MultiSelectItemId == 135 && x.Result.IsSelected) != null)
                {
                    AllMultiSelect.ForEach((MultiSelect select) =>
                    {
                        if (WegloopgedragItemsList.Contains(select.Id))
                            select.IsVisible = true;
                        else
                            select.IsVisible = false;
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected bool IsBetredenGebouwen
        {
            get
            {
                if (AllMultiSelect.FirstOrDefault(x => x.Id == 10) is MultiSelect select &&
                    select.Items.Find(x => x.Result.MultiSelectItemId == 136 && x.Result.IsSelected) != null)
                {
                    AllMultiSelect.ForEach((MultiSelect select) =>
                    {
                        if (BetredenGebouwItemsList.Contains(select.Id))
                            select.IsVisible = true;
                        else
                            select.IsVisible = false;
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected bool IsSamenscholing
        {
            get
            {
                if (AllMultiSelect.FirstOrDefault(x => x.Id == 10) is MultiSelect select &&
                    select.Items.Find(x => x.Result.MultiSelectItemId == 137 && x.Result.IsSelected) != null)
                {
                    AllMultiSelect.ForEach((MultiSelect select) =>
                    {
                        if (SamenscholingItemsList.Contains(select.Id))
                            select.IsVisible = true;
                        else
                            select.IsVisible = false;
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        // 2 nodig bij is context ingelicht anders is er altijd 1 visible
        protected bool IsContextIngelicht
        {
            get
            {
                MultiSelect jaSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 43);
                MultiSelect neeSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 24);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 21) is MultiSelect mainSelect && mainSelect.Items.Find(x => x.Result.MultiSelectItemId == 48 &&
                    x.Result.IsSelected) != null)
                {
                    neeSelect.IsVisible = false;
                    neeSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    jaSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    jaSelect.IsVisible = false;
                    jaSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    neeSelect.IsVisible = true;

                    return false;
                }
            }
        }
        protected bool IsContextNietIngelicht
        {
            get
            {
                MultiSelect jaSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 43);
                MultiSelect neeSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 24);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 21) is MultiSelect mainSelect && mainSelect.Items.Find(x => x.Result.MultiSelectItemId == 49 &&
                    x.Result.IsSelected) != null)
                {
                    jaSelect.IsVisible = false;
                    jaSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    neeSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    neeSelect.IsVisible = false;
                    neeSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    jaSelect.IsVisible = true;

                    return false;
                }
            }
        }
        // 2 nodig bij is cliënt aangesproken anders is er altijd 1 visible
        protected bool IsClientAangesproken
        {
            get
            {
                MultiSelect jaSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 61);
                MultiSelect neeSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 62);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 59) is MultiSelect mainSelect && mainSelect.Items.Find(x => x.Result.MultiSelectItemId == 139 &&
                    x.Result.IsSelected) != null)
                {
                    neeSelect.IsVisible = false;
                    neeSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    jaSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    jaSelect.IsVisible = false;
                    jaSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    neeSelect.IsVisible = true;

                    return false;
                }
            }
        }
        protected bool IsClientNietaangesproken
        {
            get
            {
                MultiSelect jaSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 61);
                MultiSelect neeSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 62);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 59) is MultiSelect mainSelect && mainSelect.Items.Find(x => x.Result.MultiSelectItemId == 140 &&
                    x.Result.IsSelected) != null)
                {
                    jaSelect.IsVisible = false;
                    jaSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    neeSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    neeSelect.IsVisible = false;
                    neeSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    jaSelect.IsVisible = true;

                    return false;
                }
            }
        }
        protected bool ISLeefgroepSelected
        {
            get
            {
                if (SelectedGOG.Leefgroep != null && SelectedGOG.Leefgroep.Id != 0)
                    return true;
                else
                    return false;
            }
        }
        protected bool IsLichamelijkeSchadeClienten
        {
            get
            {
                MultiSelect subSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 44);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 28) is MultiSelect tempSelect && tempSelect.Items.Find(x => x.Result.MultiSelectItemId == 53 && x.Result.IsSelected) != null)
                {
                    subSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    subSelect.IsVisible = false;
                    subSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });

                    return false;
                }
            }
        }
        protected bool IsLichamelijkeSchadeBegeleiding
        {
            get
            {
                MultiSelect subSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 45);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 30) is MultiSelect tempSelect && tempSelect.Items.Find(x => x.Result.MultiSelectItemId == 57 && x.Result.IsSelected) != null)
                {
                    subSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    subSelect.IsVisible = false;
                    subSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });

                    return false;
                }
            }
        }
        protected bool IsLichamelijkeSchadeMedebewoners
        {
            get
            {
                MultiSelect subSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 54);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 52) is MultiSelect tempSelect && tempSelect.Items.Find(x => x.Result.MultiSelectItemId == 124 && x.Result.IsSelected) != null)
                {
                    subSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    subSelect.IsVisible = false;
                    subSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });

                    return false;
                }
            }
        }
        protected bool IsMaterieleSchade
        {
            get
            {
                MultiSelect subSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 46);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 25) is MultiSelect tempSelect && tempSelect.Items.Find(x => x.Result.MultiSelectItemId == 51 && x.Result.IsSelected) != null)
                {
                    subSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    subSelect.IsVisible = false;
                    subSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });

                    return false;
                }
            }
        }
        protected bool IsBeschijfVBMVisible
        {
            get
            {
                MultiSelect subSelect1 = AllMultiSelect.FirstOrDefault(x => x.Id == 38);
                MultiSelect subSelect2 = AllMultiSelect.FirstOrDefault(x => x.Id == 42);
                MultiSelect subSelect3 = AllMultiSelect.FirstOrDefault(x => x.Id == 47);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 36) is MultiSelect tempSelect && tempSelect.Items.Any(x => x.Result.MultiSelectItemId == 96 && x.Result.IsSelected))
                {
                    subSelect1.IsVisible = true;
                    subSelect2.IsVisible = true;
                    subSelect3.IsVisible = true;

                    return true;
                }
                else
                {
                    subSelect1.IsVisible = false;
                    subSelect1.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    subSelect2.IsVisible = false;
                    subSelect2.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });
                    subSelect3.IsVisible = false;
                    subSelect3.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });

                    return false;
                }
            }
        }
        protected bool IsBetrokkenWerkingVisible
        {
            get
            {
                if (AllMultiSelect.FirstOrDefault(x => x.Id == 8) is MultiSelect tempSelect && tempSelect.Items.Find(x => x.Result.MultiSelectItemId == 15 && x.Result.IsSelected) != null)
                {
                    SelectedGOG.Leefgroep = null;
                    SelectedGOG.BetrokkenBewoners = null;
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        protected bool LoggedOnUserHasLeefgroepRecht => MyLeefgroepen.Count != 0;
        protected bool IsPersonenalarmVisible
        {
            get
            {
                MultiSelect subSelect = AllMultiSelect.FirstOrDefault(x => x.Id == 58);

                if (AllMultiSelect.FirstOrDefault(x => x.Id == 55) is MultiSelect tempSelect && tempSelect.Items.Find(x => x.Result.MultiSelectItemId == 129 && x.Result.IsSelected) != null)
                {
                    subSelect.IsVisible = true;

                    return true;
                }
                else
                {
                    subSelect.IsVisible = false;
                    subSelect.Items.ForEach((MultiSelectItem item) =>
                    {
                        item.Result.Reset();
                    });

                    return false;
                }
            }
        }
        #endregion
        #region Errormessages
        public string ErrorMessageLeefgroep { get; set; } = "";
        public string ErrorMessageClient { get; set; } = "";
        public string ErrorMessageAnchorBetrokkenBegeleiders { get; set; } = "";
        #endregion

        /** EVENTS */
        private void BackGroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((bool)e.Result == true)
                _toaster.Success("Gegevens zijn opgeslagen.", "AutoSave");
            else
                _toaster.Error("Gegevens zijn niet opgeslagen (Auto-Save).", "AutoSave");
        }
        private void BackGroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (AutoSave())
                e.Result = true;
            else
                e.Result = false;
        }

        /** METHODS */
        // OVERRIDES
        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            Console.WriteLine("gogOverzicht afterRender");
        }
        protected override void OnInitialized()
        {
            LoadInitialData();

            backGroundWorker.DoWork += BackGroundWorker_DoWork;
            backGroundWorker.RunWorkerCompleted += BackGroundWorker_RunWorkerCompleted;
        }
        protected override void OnParametersSet()
        {
            base.OnParametersSet();
            //Editeren formulier
            SetMultiSelectItemResults();
        }
        // REGULAR METHODS
        public async void AutoSaveTask()
        {
            if (backGroundWorker.IsBusy) return;

            if (this.SelectedGOG.StatusId.HasValue) return;

            await Task.Run(() => backGroundWorker.RunWorkerAsync());
         
        }
        protected bool AutoSave()
        {
            if (IsGOGSelected && (SelectedGOG.Leefgroep == null || SelectedGOG.Leefgroep.Id == 0) && (SelectedGOG.BetrokkenBewoners == null || SelectedGOG.BetrokkenBewoners.Count == 0))
                return false;

            this.SelectedGOG.SelectedMultiSelectItemsResults.Clear();
            foreach (MultiSelect ms in this.AllMultiSelect)
            {
                foreach (MultiSelectItem item in ms.Items.Where(x => x.Result.IsSelected))
                {
                    this.SelectedGOG.SelectedMultiSelectItemsResults.Add(item.Result);
                }
            }
            _gogItemService.SaveGOGItem(this.SelectedGOG);
            return true;
        }
        protected void LoadInitialData()
        {
            MyLeefgroepen = _leefgroepService.GetLeefgroepenLoggedOnUser("GOG");
            MyClientenRechten = _userService.LoggedOnUser.GetBewonerIds();
            AllMultiSelect = _multiSelectService.GetAll("GOG");

            ClearResultsFromMultiselects();
           // RefreshGogList();
        }
        public void Editmode_ChangeToListPage()
        {
            int findObjectId = SelectedGOG.Id;       

            ClearResultsFromMultiselects();
            RefreshGogList(this.MainFilter);

            GOGItem objectGOG = GOGLijst.Where(x => x.Id == findObjectId).FirstOrDefault();
            if (objectGOG != null)
                SelectedGOG = objectGOG;

            _editMode = false;

            StateHasChanged();
        }
        public void DoFilterGogList(GOGMainFilter mainFilter)
        {
            this.MainFilter = mainFilter;
            RefreshGogList(mainFilter);

            GOGItem i = GOGLijst.Where(x => x.StatusId.HasValue == false).FirstOrDefault();
            if (i != null)
            {
                _toaster.Info("Gelieve GOG" + i.Id + " compleet in te vullen.", "Onvolledige GOG", options =>
                 {
                     options.VisibleStateDuration = 5000;
                     options.Onclick = toast =>
                     {
                         SelectedGOG = i;
                         StateHasChanged();
                         return Task.CompletedTask;
                     };
                 }
                );
            }
            ShowSpinner = false;
           // StateHasChanged();
        }
        public void Editmode_ChangeToEditPage(GOGItem selectedItem, bool IsEditable)
        {
            ClearResultsFromMultiselects();

            if (selectedItem != null)
            {
                if (selectedItem.Id != 0)
                    SelectedGOG = _gogItemService.GetGOGItemFromId(selectedItem.Id);
                else
                    SelectedGOG = selectedItem;

                _editMode = true;
                IsGogItemEditable = IsEditable;
                SetMultiSelectItemResults();
                FillPossbibleClientenPool();
            }
            else
            {
                SelectedGOG = new GOGItem();
                _editMode = false;
                RefreshGogList(this.MainFilter);
            }
        }
        protected void ClearResultsFromMultiselects()
        {
            foreach (MultiSelect component in AllMultiSelect)
            {
                component.Items.ForEach((MultiSelectItem item) =>
                {
                    item.Parent.ErrorMessage = null;
                    item.Result.Reset();
                });
            }
            ErrorMessageLeefgroep = String.Empty;
            ErrorMessageClient = String.Empty;
            ErrorMessageAnchorBetrokkenBegeleiders = String.Empty;
        }      
        protected void SetMultiSelectItemResults()
        {
            if (_editMode && SelectedGOG != null)
            {
                // GOG-meding uit DB invullen in the AllMultiSelect;
                foreach (MultiSelect component in AllMultiSelect)
                {
                    component.Items.ForEach((MultiSelectItem item) =>
                    {
                        if (SelectedGOG.SelectedMultiSelectItemsResults.FirstOrDefault(x => x.MultiSelectItemId == item.Id) is MultiSelectItemResult res)
                        {
                            item.Result = res;
                        }
                    });
                }
            }
            StateHasChanged();
        }
        protected void RefreshGogList(GOGMainFilter mainFilter)
        {
            if (MyLeefgroepen.Count != 0 && GOGLijst != null) // gebruikers met rechten op leefgroepen
                GOGLijst = _gogItemService.GetGOGItemsForLeefgroepenAndOwn(MyLeefgroepen,mainFilter.StartDate,mainFilter.EndDate);
            if (MyClientenRechten.Count != 0 && GOGLijst != null) // gebruikers met rechten op gebruikers
            {
                List<GOGItem> tempList = _gogItemService.GetGOGItemsForBewonersAndOwn(MyClientenRechten,mainFilter.StartDate, mainFilter.EndDate);

                if (GOGLijst.Count == 0)
                {
                    GOGLijst = tempList;
                }
                else
                {
                    foreach (GOGItem item in tempList)
                    {
                        if (!GOGLijst.Select(x => x.Id).Contains(item.Id))
                            GOGLijst.Add(item);
                    }
                }
            }
            if (_userService.LoggedOnUser.HasAccess("1") || _userService.LoggedOnUser.HasAccess("4000") || _userService.LoggedOnUser.HasAccess("4003")) // gebruikers met rechten op vandalisme, enz.
            {
                List<GOGItem> tempListCrisisItems = _gogItemService.GetAllCrisisgroepItemsItems(); // find all niet-gog items
                tempListCrisisItems.RemoveAll(x => GOGLijst.Exists(y => y.Id == x.Id)); // remove the doubles
                GOGLijst.AddRange(tempListCrisisItems); // add to itemsList
                GOGLijst = GOGLijst.OrderByDescending(x => x.Id).ToList(); // reorder the itemsList
            }

            StateHasChanged();
        }       
        public void OnChangedMultiSelect(MultiSelectItem multiselectItem)
        {
            if (multiselectItem.Parent.ChoiceType == "TEXT" || multiselectItem.Parent.ChoiceType == "TEXTAREA" || multiselectItem.IsOther)
            {
                AutoSaveTask(); 
            }
            StateHasChanged();
        }
        public void OnSelectLeefGroep()
        {
            _leefgroepPopupIsVisible = true;
            
            StateHasChanged();
        }
        public void OnSelectStatus()
        {
            _statusPopupIsVisible = true;

            StateHasChanged();
        }
        protected void GogStatusWijzigen(int newStatusId)
        {
            SelectedGOG.StatusId = newStatusId;

            _statusPopupIsVisible = false;
            StateHasChanged();
        }
        public void OnSelectClient()
        {
            if (SelectedGOG.BetrokkenBewoners != null)
                SelectedClienten = SelectedGOG.BetrokkenBewoners;
            _clientenPopupIsVisible = true;
           
            StateHasChanged();
        }
        public void OnSelectGebruiker()
        {
            if (SelectedGOG.BetrokkenBegeleiders != null)
                SelectedGebruikers = SelectedGOG.BetrokkenBegeleiders;
            _gebruikerPopupIsVisible = true;

            StateHasChanged();
        }
        public void ClosePopups()
        {
            _clientenPopupIsVisible = false;
            _leefgroepPopupIsVisible = false;
            _gebruikerPopupIsVisible = false;
            _savePopupIsVisible = false;
            _statusPopupIsVisible = false;

            StateHasChanged();
        }
        private void FillPossbibleClientenPool()
        {
            if (SelectedGOG != null && SelectedGOG.Leefgroep != null && SelectedGOG.Leefgroep.Id != 0)
                MyClienten = _clientService.GetClientenForLeefgroep(SelectedGOG.Leefgroep.Id);

            if (MyLeefgroepen != null && MyLeefgroepen.Count != 0)
                AllMyClienten = _clientService.GetAllClientenForLeefgroepen(MyLeefgroepen);
        }
        public void CancelButton()
        {
            Editmode_ChangeToListPage();
        }
        public void OKButton()
        {
            List<String> MultiSelectWithErrorIds = new List<String>();
            try
            {
                ErrorMessageLeefgroep  = String.Empty;
                ErrorMessageClient = String.Empty;
                ErrorMessageAnchorBetrokkenBegeleiders = String.Empty;
 
                String errorMessage = "";
                // validate content
                if (IsBetrokkenWerkingVisible) // bij bepaalde selectie multiselect (id = 8) => leefgroep niet verplicht => bewoner niet verplicht
                {
                    if (SelectedGOG.Leefgroep == null || SelectedGOG.Leefgroep.Id == 0)
                    {
                        ErrorMessageLeefgroep =  errorMessage = "Gelieve een leefgroep te kiezen.";
                        MultiSelectWithErrorIds.Add("Leefgroep");
                    }
                    else if (SelectedGOG.BetrokkenBewoners == null || SelectedGOG.BetrokkenBewoners.Count == 0)
                    {
                        ErrorMessageClient = errorMessage = "Gelieve minstens 1 cliënt te kiezen.";
                        MultiSelectWithErrorIds.Add("Client");
                    }
                }

                if (!String.IsNullOrWhiteSpace(errorMessage))
                {
                    AutoSaveTask();//autoSave
                    throw new Exception(errorMessage);
                }
                foreach (MultiSelect m in this.AllMultiSelect)
                {
                    if (!m.Validate())
                    {
                        MultiSelectWithErrorIds.Add(m.Id.ToString());
                        errorMessage = "Gelieve all velden in te vullen.";
                    }
                }
                if (!String.IsNullOrWhiteSpace(errorMessage))
                {
                    AutoSaveTask();//autoSave
                    throw new Exception(errorMessage);
                }
                // clear previous results and add new results
                this.SelectedGOG.SelectedMultiSelectItemsResults.Clear();
                foreach (MultiSelect ms in this.AllMultiSelect)
                {
                    foreach (MultiSelectItem item in ms.Items.Where(x => x.Result.IsSelected))
                    {
                        this.SelectedGOG.SelectedMultiSelectItemsResults.Add(item.Result);
                    }
                }

                // extra check on error + if not: save item + show message             
                 SelectedGOG.SetStatusFormValidatedAndCreated();

                _gogItemService.SaveGOGItem(this.SelectedGOG);
                _toaster.Success("Alle gegevens zijn opgeslagen.", "Opgeslagen");

                Editmode_ChangeToListPage();
            }
            catch (Exception ex )
            {
                if (MultiSelectWithErrorIds.Count>0)
                               
                    js.InvokeAsync<object>(
                       "myLib.BlazorScrollToId",
                       "Anchor" + MultiSelectWithErrorIds[0]);
                 
                _toaster.Error(ex.Message, "Fout");
            }
            finally
            {
                StateHasChanged();
            }
        }
        #region PDF
        public void PrintPDF(GOGItem gOGItem)
        {
            if (gOGItem == null)
                return;

            DownloadFilePDF pdf = GetReportGOG(_gogItemService.GetGOGItemFromId(gOGItem.Id));
            SaveAs($"{pdf.FileName}.{pdf.Extension}", pdf.Data);
        } 
        private DownloadFilePDF GetReportGOG(GOGItem GOGItem)
        {
            GogReport reportData = _gOGReportService.GetGogReport(GOGItem, this.AllMultiSelect.DeepClone());

            DateTime date = DateTime.Now;
            XtraReport r = new GOGReport(reportData);
     
            MemoryStream memoryStream = new MemoryStream();
            r.ExportToPdf(memoryStream);
            String filename = "";
  
            filename = $"{"GOG_Melding_nr"}_{GOGItem.Id}_{date.Date.Year}_{date.Date.Month:D2}_{date.Date.Day:D2}";
            DownloadFilePDF file = new DownloadFilePDF(filename, "pdf", memoryStream.ToArray());
            return file;
        }
        public void SaveAs(string filename, byte[] data)
        {
            js.InvokeAsync<object>(
               "myLib.saveAsFile",
               filename,
               Convert.ToBase64String(data));
        }
        #endregion
        #region Dispose
       /* public void Dispose()
        {
            if (AllMultiSelect != null)
            {
                foreach (MultiSelect select in AllMultiSelect)
                {
                    select.Dispose();
                }
                AllMultiSelect.Clear();
                AllMultiSelect = null;
            }
            if (GOGLijst != null)
            {
                GOGLijst.Clear();
                GOGLijst = null;
            }
            if (MyClienten != null)
            {
                MyClienten.Clear();
                MyClienten = null;
            }
            if (MyLeefgroepen != null)
            {
                MyLeefgroepen.Clear();
                MyLeefgroepen = null;
            }
            if (SelectedGebruikers != null)
            {
                SelectedGebruikers.Clear();
                SelectedGebruikers = null;
            }
            if (SelectedClienten != null)
            {
                SelectedClienten.Clear();
                SelectedClienten = null;
            }
            if (MyClientenRechten != null)
            {
                MyClientenRechten.Clear();
                MyClientenRechten = null;
            }

        //    GC.SuppressFinalize(this);
        }*/
        ~GogOverzichtPageModel()
        {
             //Dispose();
        }
        #endregion
        // CALLBACKS
        /// <summary>
        /// Callback. This Method is called from Popup with a result
        /// </summary>
        public void OnActionOnCloseClientenPopupMulti(List<Client> clienten)
        {
            if (clienten != null)
            {
                try
                {
                    SelectedGOG.BetrokkenBewoners?.Clear();

                    SelectedGOG.BetrokkenBewoners = clienten;
                    ErrorMessageClient = String.Empty;
                }
                catch (Exception ex)
                {
                    _toaster.Error(ex.Message, "Fout DoorrekenenNaarClientPopup sluiten");
                }
            }

            ClosePopups();
        }
        /// <summary>
        /// Callback. This Method is called from Popup with a result
        /// </summary>
        public void OnActionOnCloseLeefgroepPopup(Leefgroep leefgroep)
        {
            if (leefgroep != null && SelectedGOG.Leefgroep != leefgroep)
            {
                this.SelectedGOG.Leefgroep = leefgroep;
                if (LoggedOnUserHasLeefgroepRecht)
                {
                    this.SelectedGOG.BetrokkenBewoners = new List<Client>();
                    FillPossbibleClientenPool();
                }
                ErrorMessageLeefgroep = String.Empty;       
            }
            ClosePopups();
        }
        /// <summary>
        /// Callback. This Method is called from Popup with a result
        /// </summary>
        public void OnActionOnCloseGebruikerPopupMulti(List<Gebruiker> gebruikers)
        {
            if (gebruikers != null)
            {
                try
                {
                    SelectedGOG.BetrokkenBegeleiders.Clear();

                    SelectedGOG.BetrokkenBegeleiders = gebruikers;

             
                    ErrorMessageAnchorBetrokkenBegeleiders = String.Empty;
                }
                catch (Exception ex)
                {
                    _toaster.Error(ex.Message, "Fout Close GebruikerPopup sluiten");
                }
            }

            ClosePopups();
        }
    }
}
