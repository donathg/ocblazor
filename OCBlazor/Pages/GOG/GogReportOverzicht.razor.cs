﻿using Microsoft.AspNetCore.Components;
using OCBlazor.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using OCBlazor.Models;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.Kilometervergoeding;
using Logic.WagenNS;
using System;
using DevExpress.Blazor;
using DataBase.Services;
using System.Linq;
using Logic.DataBusinessObjects.MultiSelectNS;
using Logic.MultiSelectNS;
using Logic.DataBusinessObjects.GOG;
using Logic.GOG;
using Logic.Clienten;
using Logic.LeefgroepNS;
using Sotsera.Blazor.Toaster;
using Microsoft.JSInterop;
using Reporting.Maaltijdlijsten;
using DevExpress.ClipboardSource.SpreadsheetML;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports;
using System.IO;
 
//using Serilog;
using System.Threading;
using System.ComponentModel;
using DevExpress.CodeParser;
using Common;
using DevExpress.XtraRichEdit.Import.OpenXml;
using System.Diagnostics;
using Common.CloneExtensions;
using Common.DateTimeExtensions;

namespace OCBlazor.Pages.GOG
{
    public class GogReportOverzichtPageModel : ComponentBase, IDisposable
    {
        [Parameter]
        public int? Id { get; set; } // waarom? kies het uit de lijst. Zo niet, creëer dan een GOGDetail component (idem aan GOGList) en roep dat dan op
       
        [Parameter]
        public int PageSize { get; set; }        

        [Inject]
        protected IUserService _userService { get; set; }
        [Inject]
        public IMultiSelectService _multiSelectService { get; set; }
        [Inject]
        public IGOGItemService _gogItemService { get; set; }
        [Inject]
        public IClientService _clientService { get; set; }
        [Inject]
        public ILeefgroepService _leefgroepService { get; set; }
        [Inject]
        public IGOGReportService _gOGReportService { get; set; }        
        [Inject]
        public IToaster _toaster { get; set; }
        [Inject]
        IJSRuntime js { get; set; }
        public List<MultiSelect> AllMultiSelect { get; set; } = new List<MultiSelect>();



        public List<MultiSelectTreeItem> AllMultiSelectTree { get; set; } = new List<MultiSelectTreeItem>();
        public IEnumerable<MultiSelectTreeItem> AllMultiSelectTreeValues { get; set; } = new List<MultiSelectTreeItem>();


        public IEnumerable<MultiSelectTreeItem> AllMultiSelectTreeChilds { get; set; } = new List<MultiSelectTreeItem>();
        public IEnumerable<MultiSelectTreeItem> AllMultiSelectTreeChildValues { get; set; } = new List<MultiSelectTreeItem>();

        public List<GOGItem> GOGLijst { get; set; } = new List<GOGItem>();
        public List<GOGIncidentSpline> GOGIncidentSplineResult { get; set; } = new List<GOGIncidentSpline>();
        public List<Client> MyClienten { get; set; } = new List<Client>();
        public List<Leefgroep> MyLeefgroepen { get; set; } = new List<Leefgroep>();
        public List<Gebruiker> SelectedGebruikers { get; set; } = new List<Gebruiker>();
        public List<Client> SelectedClienten { get; set; } = new List<Client>();
        public List<Leefgroep> SelectedLeefgroepen { get; set; } = new List<Leefgroep>();

        public DateTime BeginDatum { get; set; } = DateTime.Now.GetFirstDayOfMonth();
        public DateTime EindDatum { get; set; } = DateTime.Now.GetLastDayOfMonth();

        public DxChart<GOGIncidentSpline> GOGIncidentSplineChart;


        private bool _forceRerender = true;
        public void MultiSelectSelectedItemsChanged(IEnumerable<MultiSelectTreeItem> values)
        {
            AllMultiSelectTreeChilds = values.First().Children;
        }
        public String SelectedLeefgroepenString { 
            
            get
            {

                String t = String.Empty;
                foreach (Leefgroep lfg in SelectedLeefgroepen)
                {

                    t = t + lfg.Name + ", ";
                }
                return t;
            }
            set { }
        }
        public String SelectedClientenString
        {
            get
            {

                String t = String.Empty;
                foreach (Client c in SelectedClienten)
                {

                    t = t + c.FirstNameLastname + ", ";
                }
                return t;
            }
            set { }
        }

        
        public List<int> MyClientenRechten { get; set; } = new List<int>();
        public List<Client> AllMyClienten { get; set; } = new List<Client>();
        protected bool LoggedOnUserHasLeefgroepRecht => MyLeefgroepen.Count != 0;
        public List<Client> MyDoorgeefClienten
        {
            get
            {
                if (MyClienten != null && MyClienten.Count != 0)
                    return MyClienten;

                if (MyClientenRechten != null && MyClientenRechten.Count != 0)
                {
                    List<Client> tempListClienten = new List<Client>();
                    foreach (int clientId in MyClientenRechten)
                    {
                        tempListClienten.Add(_clientService.GetClient(clientId));
                    }

                    return tempListClienten;
                }
                else
                    return new List<Client>();
            }
        }
        public List<Leefgroep> MyDoorgeefLeefgroepen
        {
            get
            {
                if (MyLeefgroepen != null && MyLeefgroepen.Count != 0)
                    return MyLeefgroepen;

                if (SelectedClienten != null && SelectedClienten.Count > 0)
                {
                    List<Leefgroep> tempList = new List<Leefgroep>();

                    foreach (Client client in SelectedClienten)
                    {
                        tempList.AddRange(client.Leefgroep);
                    }

                    return tempList.DistinctBy(x => x.Id).ToList();
                }
                else
                    return new List<Leefgroep>();
            }
        }
        public String ErrorMessageLeefgroep { get; set; } = "";
        public String ErrorMessageClient { get; set; } = "";
        public String ErrorMessageAnchorBetrokkenBegeleiders { get; set; } = "";
        protected bool _editMode = false;
        protected bool _leefgroepPopupIsVisible = false;
        protected bool _clientenPopupIsVisible = false;
        protected bool _gebruikerPopupIsVisible = false; 
        protected bool _savePopupIsVisible = false;
        private readonly List<int> GOGItemsList = new List<int>() { 33, 34, 36, 38, 39, 40, 42, 48, 50, 51, 55, 58};
        private readonly List<int> SGOGItemsList = new List<int>() { 13, 14, 16, 17, 18, 19 };
        protected bool IsGogItemEditable { get; set; }

        public bool ShowSpinner { get; set; } = true;

        BackgroundWorker backGroundWorker = new BackgroundWorker();

        private Action ActionAutoSave;

        protected override  async Task OnInitializedAsync()
        {
            LoadInitialData();
        }
        void LoadInitialData()
        {
            MyLeefgroepen = _leefgroepService.GetLeefgroepenLoggedOnUser("GOG");
            MyClientenRechten = _userService.LoggedOnUser.GetBewonerIds();
            AllMultiSelect = _multiSelectService.GetAll("GOG");
            AllMultiSelectTree = _multiSelectService.ConvertToTree(AllMultiSelect);
            FillPossbibleClientenPool();
            ClearResultsFromMultiselects();
            RefreshGogList();
            ShowSpinner = false;
            StateHasChanged();
        }
        protected override void OnParametersSet()
        {
            base.OnParametersSet();
            SetMultiSelectItemResults();
        }
      
        protected void ClearResultsFromMultiselects()
        {
            foreach (MultiSelect component in AllMultiSelect)
            {
                component.Items.ForEach((MultiSelectItem item) =>
                {
                    item.Parent.ErrorMessage = null;
                    item.Result.Reset();
                });
            }
            ErrorMessageLeefgroep = String.Empty;
            ErrorMessageClient = String.Empty;
            ErrorMessageAnchorBetrokkenBegeleiders = String.Empty;
        }      
        protected void SetMultiSelectItemResults()
        {
            foreach (GOGItem gogItem in this.GOGLijst)
            {
                // GOG-meding uit DB invullen in the AllMultiSelect;
                foreach (MultiSelect component in AllMultiSelect)
                {
                    component.Items.ForEach((MultiSelectItem item) =>
                    {
                        if (gogItem.SelectedMultiSelectItemsResults.FirstOrDefault(x => x.MultiSelectItemId == item.Id) is MultiSelectItemResult res)
                        {
                            item.Result = res;
                        }
                    });
                }
            }
 
        }
        protected void RefreshGogList()
        {
            if (MyLeefgroepen.Count != 0 && GOGLijst != null)
                GOGLijst = _gogItemService.GetGOGItemsForLeefgroepenAndOwn(MyLeefgroepen,this.BeginDatum,this.EindDatum);
            if (MyClientenRechten.Count != 0 && GOGLijst != null)
            {
                List<GOGItem> tempList = _gogItemService.GetGOGItemsForBewonersAndOwn(MyClientenRechten, this.BeginDatum, this.EindDatum);

                if (GOGLijst.Count == 0)
                {
                    GOGLijst = tempList;
                }
                else
                {
                    foreach (GOGItem item in tempList)
                    {
                        if (!GOGLijst.Select(x => x.Id).Contains(item.Id))
                            GOGLijst.Add(item);
                    }
                }
            }
            SetMultiSelectItemResults();
            StateHasChanged();
        }
        public void OnSelectLeefGroep()
        {
            _leefgroepPopupIsVisible = true;

            StateHasChanged();
        }
        public void OnSelectClient()
        {
            _clientenPopupIsVisible = true;

            StateHasChanged();
        }
        public void OnSelectGebruiker()
        {
            _gebruikerPopupIsVisible = true;

            StateHasChanged();
        }
        public void ClosePopups()
        {
            _clientenPopupIsVisible = false;
            _leefgroepPopupIsVisible = false;
            _gebruikerPopupIsVisible = false;
            _savePopupIsVisible = false;
            StateHasChanged();
        }
        private void FillPossbibleClientenPool()
        {
            if (SelectedLeefgroepen != null && SelectedLeefgroepen.Count>0)
                MyClienten = _clientService.GetAllClientenForLeefgroepen(SelectedLeefgroepen);

            if (MyLeefgroepen != null && MyLeefgroepen.Count != 0)
                AllMyClienten = _clientService.GetAllClientenForLeefgroepen(MyLeefgroepen);
        }

        public void OnActionOnCloseClientenPopupMulti(List<Client> clienten)
        {
            SelectedClienten.Clear();
            if (clienten != null)
            {
                SelectedClienten.AddRange(clienten);
            }
            ClosePopups();
        }
        /// <summary>
        /// Callback. This Method is called from Popup with a result
        /// </summary>
        public void OnActionOnCloseLeefgroepPopup(List<Leefgroep> leefgroepen)
        {
            if (leefgroepen != null)
            {
                this.SelectedLeefgroepen.Clear();
                this.SelectedLeefgroepen.AddRange(leefgroepen);
                if (LoggedOnUserHasLeefgroepRecht)
                {
                   this.SelectedClienten = new List<Client>();
                    FillPossbibleClientenPool();
                }
                ErrorMessageLeefgroep = String.Empty;

            }
            ClosePopups();

      
        }
        /// <summary>
        /// Callback. This Method is called from Popup with a result
        /// </summary>
        public void OnActionOnCloseGebruikerPopupMulti(List<Gebruiker> gebruikers)
        {
            ClosePopups();
        }
        private GOGIncidentSpline ConvertToGOGIncidentSpline(GOGItem item)
        {

            GOGIncidentSpline result = new GOGIncidentSpline();
            result.AantalIncidenten = 1;
            result.IncidentDatum = item.DatumIncident.GetValueOrDefault();
            result.Leefgroep = item.Leefgroep;
            return result;
        }
        public enum ClientIncident
        {
            Client,
            Incident

        }
        public void GenerateReportIncident( )
        {
            bool alleModus = false;
            if ((SelectedClienten == null || SelectedClienten.Count == 0) && (SelectedLeefgroepen == null || SelectedLeefgroepen.Count == 0))
            {
                alleModus = true;
            }
            GOGIncidentSplineResult = null;
            List<GOGIncidentSpline> result  = new List<GOGIncidentSpline>();
           foreach (GOGItem gogItem in GOGLijst.Where(x=>x.StatusId.GetValueOrDefault()>0))
           {
                if (gogItem.Id == 382)
                    Debugger.Break();
                
              DateTime? datumIncident = gogItem.DatumIncident.GetValueOrDefault();
                if (!datumIncident.HasValue)
                    {
                        continue;
                    }
              if (datumIncident.GetValueOrDefault().Date >= BeginDatum.GetFirstDayOfMonth() && datumIncident.GetValueOrDefault().Date <= EindDatum.GetLastDayOfMonth())
              {
                  if (alleModus)
                    {
                       
                            result.Add(ConvertToGOGIncidentSpline(gogItem));
                       
                       continue;
                  }
              }
              else continue;
              
              if (SelectedClienten != null && SelectedClienten.Count > 0)
              {
                   List<int> selectedClientenIds = new List<int>();
                   selectedClientenIds = SelectedClienten.Select(x => x.Id).ToList();
                   foreach (int id in selectedClientenIds)
                    {
                        
                        if (gogItem.BetrokkenBewoners.Select(x=>x.Id).Contains(id))
                        {
                            result.Add(ConvertToGOGIncidentSpline(gogItem));
                            continue;
                        }
                   }
              }
              else
              {
                 if (SelectedLeefgroepen != null && SelectedLeefgroepen.Count > 0)
                 {
                    List<int> selectedLeefgroepIds = new List<int>();
                    selectedLeefgroepIds = SelectedLeefgroepen.Select(x => x.Id).ToList();
                    if (selectedLeefgroepIds.Contains(gogItem.Leefgroep.Id))
                    {
                            result.Add(ConvertToGOGIncidentSpline(gogItem));
                            continue;
                    }
                 }
              }
                
            }
            if (result.Count == 0)
            {
                _toaster.Error("Geen GOG's met de gekozen criteria gevonden.");
            }
            GOGIncidentSplineResult = null;
          

            GOGIncidentSplineResult = result;
        
            GOGIncidentSplineChart.RefreshData();
             Task.Delay(150);
            _forceRerender = true;
            StateHasChanged();
        }
        protected override bool ShouldRender()
        {
            if (_forceRerender)
            {
                _forceRerender = false;
                return true;
            }
            return base.ShouldRender();
        }
        public void Dispose()
        {

        }
        ~GogReportOverzichtPageModel()
        {
            Dispose();
        }
        #region PDF
        public void PrintPDF(GOGItem gOGItem)
        {
            if (gOGItem == null)
                return;

            DownloadFilePDF pdf = GetReportGOG(gOGItem.DeepClone());
            SaveAs($"{pdf.FileName}.{pdf.Extension}", pdf.Data);
        }

 
        private DownloadFilePDF GetReportGOG(GOGItem GOGItem)
        {
            GogReport reportData = _gOGReportService.GetGogReport(GOGItem, this.AllMultiSelect.DeepClone());

            DateTime date = DateTime.Now;
            XtraReport r = new GOGReport(reportData);
     
            MemoryStream memoryStream = new MemoryStream();
            r.ExportToPdf(memoryStream);
            String filename = "";
  
            filename = $"{"GOG_Melding_nr"}_{GOGItem.Id}_{date.Date.Year}_{date.Date.Month:D2}_{date.Date.Day:D2}";
            DownloadFilePDF file = new DownloadFilePDF(filename, "pdf", memoryStream.ToArray());
            return file;
        }
        public void SaveAs(string filename, byte[] data)
        {
            js.InvokeAsync<object>(
       "myLib.saveAsFile",
       filename,
       Convert.ToBase64String(data));
        }
        #endregion
       
    }
  
}
