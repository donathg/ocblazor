﻿using AutoMapper;
using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using OCBlazor.PageModels;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using OCBlazor.Models;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.LeefgroepNS;
using Logic.DataBusinessObjects.MultiSelectNS;
using OCBlazor.Pages.GOG;
//using Serilog;

namespace OCBlazor.Pages
{
    public class KasBeheerPageModel : ComponentBase/*, IDisposable*/
    {
        int activeTabIndex = 0;
        public int ActiveTabIndex
        {
            get => activeTabIndex;
            set { activeTabIndex = value;
                InvokeAsync(StateHasChanged); }
        }

        [Parameter]
        public int? LeefgroepId { get; set; }

        DateTime datePickerValue = DateTime.Now;

        public bool EditMode { get; set; } = false;

        public List<Client> AllClientsForLeefGroep = new List<Client>();

        public List<KasbeheerTransactieType> TransactieTypes { get; set; } = new List<KasbeheerTransactieType>();
        [Inject]
        protected IKasbeheerServiceInterface _kasBeheerService { get; set; }


        [Inject]
        protected IMapper _mapper { get; set; }

        [Inject]
        protected ILeefgroepService _leefgroepService { get; set; }

        [Inject]
        protected IClientService _clientService { get; set; }

        [Inject]
        protected LocalStorageService _localStorage { get; set; }
        [Inject]
        protected IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }
        [Inject]
        Sotsera.Blazor.Toaster.IToaster _toaster { get; set; }
        [Inject]
        IJSRuntime _jsRuntime { get; set; }
        public List<KasbeheerTransactieCodering> TransactieCoderingenVisual
        {
            get
            {
                if (this.InputItem.Transactie.TransactieType.Id == 0)
                    return new List<KasbeheerTransactieCodering>();
                return KasbeheerTransactieCoderingen;
            }
        }
        public List<KasbeheerTransactieCM> TransactieItemsVisual { get; set; } = new List<KasbeheerTransactieCM>();
        public List<KasbeheerTransactie> TransactieItems = new List<KasbeheerTransactie>();
        public ObservableCollection<KasbeheerRekening> KasbeheerRekeningen = new ObservableCollection<KasbeheerRekening>();

        public List<KasbeheerTransactieType> KasbeheerTransactieTypes = new List<KasbeheerTransactieType>();
        public List<KasbeheerTransactieCodering> KasbeheerTransactieCoderingen = new List<KasbeheerTransactieCodering>();
        public List<String> KasbeheerStortenOfAfhalen = new List<String>();
        public List<Leefgroep> LeefgroepItemsVisual = new List<Leefgroep>();
        private int _selectedLeefgroepId;


        public String SelectedLeefgroepAndTotalAmountAllAccounts
        {

            get
            {
                if (KasbeheerRekeningen != null)
                {
                    Decimal sum = this.KasbeheerRekeningen.Sum(x => x.Bedrag);
                    return SelectedLeefgroep.Name + " : " + sum.ToString("C2");
                }
                return "";
            }
        }

        public Leefgroep SelectedLeefgroep {

            get
            {
                return LeefgroepItemsVisual.Where(x => x.Id == SelectedLeefgroepId).FirstOrDefault();
            }
        }

        public int SelectedLeefgroepId
        {
            get { return _selectedLeefgroepId; }
            set
            {
                _selectedLeefgroepId = value;
                RefreshIntern();
            }
        }

        public KasbeheerRekening SelectedRekening
        {
            get
            {
                return KasbeheerRekeningen[ActiveTabIndex];



            }
        }

        public KasbeheerInputCM InputItem
        {
            get;
            set;
        } = new KasbeheerInputCM();
        public List<KasbeheerTransactieClientInfo> InputItemClienten
        {
            get
            {
                return InputItem.Transactie.Clienten.ToList();
            }
        }
        public KasbeheerTransactieCM SelectedItem
        {
            get;
            set;
        } = new KasbeheerTransactieCM();

        public bool dialogIsOpen { get; set; }
        protected override async void OnInitialized()
        {
            if (KasbeheerTransactieTypes == null || KasbeheerTransactieTypes.Count==0)
                KasbeheerTransactieTypes = await _kasBeheerService.GetTransactieTypes();
         
            if (KasbeheerTransactieCoderingen == null || KasbeheerTransactieCoderingen.Count == 0)
                KasbeheerTransactieCoderingen = await _kasBeheerService.GetTransactieCoderingen();
           
            if (LeefgroepItemsVisual == null || LeefgroepItemsVisual.Count == 0)
                LeefgroepItemsVisual = await _kasBeheerService.GetLeefgroepRechten();
        } 
        protected override void OnParametersSet()
        {
            if (LeefgroepId.HasValue)
                this._selectedLeefgroepId = LeefgroepId.GetValueOrDefault();
            else
            {
                if (LeefgroepItemsVisual.Count > 0)
                    this._selectedLeefgroepId = LeefgroepItemsVisual[0].Id;
            }
            RefreshIntern();
            base.OnParametersSet();
        }
        public void RefreshIntern()
        {
            Task.FromResult(Refresh());
        }
        public async Task<bool> Refresh()
        {
            try
            {
                if (this.SelectedLeefgroepId >= 0)
                {
                    if (this.AllClientsForLeefGroep !=null)
                     this.AllClientsForLeefGroep.Clear();
                    List<Client> clienten = _clientService.GetClientenForLeefgroep(this.SelectedLeefgroepId);
                    this.AllClientsForLeefGroep = clienten;
                    if (TransactieItems != null)
                        TransactieItems.Clear();
                    TransactieItems = await _kasBeheerService.GetTransacties( SelectedLeefgroepId);
                    this.KasbeheerRekeningen.Clear();
                    var KasbeheerRekeningenDB = await _kasBeheerService.GetRekeningen( SelectedLeefgroepId);
                    foreach (KasbeheerRekening rek in KasbeheerRekeningenDB)
                    {
                        this.KasbeheerRekeningen.Add(rek);
                    }
                    CopyToVisual();
                    StateHasChanged();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }
        public void CopyToVisual()
        {
            TransactieItemsVisual.Clear();
            TransactieItemsVisual = _mapper.Map<List<KasbeheerTransactieCM>>(TransactieItems);

            AddUniqueNumbering(TransactieItemsVisual);

        }

        private void AddUniqueNumbering(List<KasbeheerTransactieCM> transactieItemsVisual)
        {
            int nr = 1;
            foreach (KasbeheerRekening rek in KasbeheerRekeningen)
            {
                nr = 1;

            
                foreach (KasbeheerTransactieCM c in  transactieItemsVisual.Where(x=>x.Rekening.Id == rek.Id).OrderBy(x=>x.Id))
                {
               
                        c.UniqueNr = rek.Name[0].ToString() + nr++;
                }
            }
        }

        public void AddNewTransaction()
        {
            this.SelectedItem = new KasbeheerTransactieCM();
            KasbeheerRekening rek = KasbeheerRekeningen.Where(x => x.Id == SelectedRekening.Id).FirstOrDefault();
            SelectedItem.Rekening = rek;
            OpenEditWindow(SelectedItem);
        }

        public void UpdateSelectedTransaction(KasbeheerTransactieCM t)
        {
            this.SelectedItem = t;
            OpenEditWindow(SelectedItem);
        }
        public async void OpenEditWindow(KasbeheerTransactieCM selected)
        {

            try
            {
                ClosePopups();

                //copy contents into a new object with automapper
                KasbeheerTransactieCM cm = _mapper.Map<KasbeheerTransactieCM>(selected);
                this.InputItem.Transactie.Clienten.Clear();
                List<KasbeheerTransactieClientInfo> clienten = await _kasBeheerService.GetSelectedClientenForTransactie(selected.Id);
                this.InputItem = new KasbeheerInputCM(cm);

                foreach (KasbeheerTransactieClientInfo c in clienten)
                    this.InputItem.Transactie.Clienten.Add(c);


                /*OKT 2020 => New Martine/Debby => Laatst gebruikte cliënten ook toevoegen aan lijst om werk te vergemakkelijken */
                List<KasbeheerTransactieClientInfo> previousUsedClienten = await _kasBeheerService.GetPreviousUsedClientsForLeefgroepTransactie(selected.Id);
                foreach (KasbeheerTransactieClientInfo c in previousUsedClienten)
                {
                    var existingClient = this.InputItem.Transactie.Clienten.Where(x => x.Id == c.Id).FirstOrDefault();
                    if (existingClient == null)
                    {
                        this.InputItem.Transactie.Clienten.Add(c);
                   }
                }
                /*OKT 2020 END*/




                foreach (Client c in AllClientsForLeefGroep)
                {
                    var existingClient = this.InputItem.Transactie.Clienten.Where(x => x.Id == c.Id).FirstOrDefault();
                    if (existingClient == null)
                    {
                         this.InputItem.Transactie.AddClient(c);
                    }
                }

                FillSelectStortenOfAfhalen(selected);

                EditMode = true;
            }
            catch (Exception ex)
            {


            }
            StateHasChanged();

        }

        /// <summary>
        /// de item dat geselecteerd moet worden ,moet eerst in de lijst komen (tijdelijke hack)
        /// </summary>
        public void FillSelectStortenOfAfhalen(KasbeheerTransactieCM selected)
        {
            if (selected.TransactieBedrag > 0)
                KasbeheerStortenOfAfhalen = new List<String>() { "inkomsten", "uitgaven" };
            else if (selected.TransactieBedrag < 0)
                KasbeheerStortenOfAfhalen = new List<String>() { "uitgaven", "inkomsten" };
            else //default nieuwe transactie
                KasbeheerStortenOfAfhalen = new List<String>() { "uitgaven", "inkomsten" };
        }

        public void CancelClicked()
        {
            EditMode = false;
            ClosePopups();
            StateHasChanged();
        }
        public void DeletItem()
        {

        }
        public void TransactieTypeChanged(ChangeEventArgs args)
        {
            if (args == null)
                return;

            this.InputItem.Transactie.TransactieCode.Kode = String.Empty;
            if (String.IsNullOrWhiteSpace(args.Value.ToString()) == false)
            {
                int newId;
                if (Int32.TryParse(args.Value.ToString(), out newId))
                    this.InputItem.Transactie.TransactieType = this.KasbeheerTransactieTypes.Where(x => x.Id == newId).FirstOrDefault();

            }
            StateHasChanged();
        }




        public void StortenOfAfhalenChanged(ChangeEventArgs args)
        {
            if (args == null)
                return;

            if (String.IsNullOrWhiteSpace(args.Value.ToString()) == false)
            {
                InputItem.StortenOfAfhalen = args.Value.ToString();

 

            }
            StateHasChanged();
        }
        public async void DeleteItem()
        {
            try
            {
                ClosePopups();

                if (InputItem.Transactie.Afgerekend)
                    return;

    
                bool deleted = await _kasBeheerService.DeleteTransactie( this.InputItem.Transactie.Id);
                EditMode = false;
                ClientenPopupIsVisible = false;
                await Refresh();
                StateHasChanged();
                _toaster.Success("De transactie is verwijderd.", "Transactie");

            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }

        }
          
        public void OnQuestionNo()
        {


             ClosePopups();

        }

        public async void SaveItem()
        {
            try
            {
                ClosePopups();

                if (InputItem.Transactie.Afgerekend)
                    return;

                bool InsertMode = this.InputItem.Transactie.Id == 0;
                String SuccesMessage = "De transactie is toegevoegd.";
                if (!InsertMode)
                    SuccesMessage = "De transactie is gewijzigd.";
                if (this.InputItem.TransactieBedrag <= 0)
                    throw new Exception("Gelieve een positief bedrag in te geven.");
                this.InputItem.Transactie.Validate();
                KasbeheerTransactie tm = await _kasBeheerService.SaveTransactie( this.SelectedLeefgroepId, this.InputItem.Transactie);
                //copy contents into existing object with automapper
                this.InputItem.Transactie.Clienten.Clear();
                _mapper.Map<KasbeheerTransactieCM, KasbeheerTransactieCM>(this.InputItem.Transactie, this.SelectedItem);
                //add new transaction to visual tree 
                if (InsertMode)
                {
                    this.TransactieItems.Add(tm);
                    CopyToVisual();
                }
                //correct total amount
                CorrectSumAfterUpdate(this.SelectedRekening.Id, this.InputItem.OldTransactieBedrag, this.SelectedItem.TransactieBedrag);
                EditMode = false;

                StateHasChanged();
                _toaster.Success(SuccesMessage, "Transactie");

            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
        }

        private void ClosePopups()
        {
            QuestionPopupVisible = false;
            ClientenPopupIsVisible = false;
        }

        private void CorrectSumAfterUpdate(int rekeningId, decimal prevValue, decimal newValue)
        {
            KasbeheerRekening rek = KasbeheerRekeningen.Where(x => x.Id == rekeningId).FirstOrDefault();
            if (rek != null)
            {
                rek.Bedrag += newValue;
                rek.Bedrag -= prevValue;
            }
        }
        public bool ClientenPopupIsVisible { get; set; } = false;
        public bool QuestionPopupVisible { get; set; } = false;

        public void OnActionOnClose(Client client)
        {
            if (client != null)
            { 
                this.InputItem.Transactie.AddClient(client);
  
            }
            ClosePopups();
            StateHasChanged();
        }
        public void ButtonAddClientClick()
        {
            ClientenPopupIsVisible = true;
            StateHasChanged();
        }

        public void Dispose()
        {

            if (InputItem != null)
            {
                if (InputItem.Transactie != null)
                {
                    if (InputItem.Transactie.Clienten != null)
                    {
                        InputItem.Transactie.Clienten.Clear();
                        InputItem.Transactie.Clienten = null;
                    }
                    InputItem.Transactie.Leefgroep = null;

                }
                InputItem = null;
            }


            if (TransactieItems != null)
            {
                TransactieItems.Clear();
                TransactieItems = null;
            }
            if (TransactieItems != null)
            {
                TransactieItems.Clear();
                TransactieItems = null;
            }
            
        
            if (SelectedItem!= null && SelectedItem.Clienten  != null)
            {
                SelectedItem.Clienten.Clear();
                SelectedItem.Clienten = null;
            }
            if (SelectedItem != null)
            {
                SelectedItem.Rekening = null;
                SelectedItem.Leefgroep = null;
                SelectedItem = null;
            
            }
            if (AllClientsForLeefGroep != null)
            {
                AllClientsForLeefGroep.Clear();
                AllClientsForLeefGroep = null;
            }

            if (TransactieItemsVisual != null)
            {
                TransactieItemsVisual.Clear();
                TransactieItemsVisual = null;
            }
            if (KasbeheerRekeningen != null)
            {
                KasbeheerRekeningen.Clear();
                KasbeheerRekeningen = null;
            }
            //if (KasbeheerTransactieTypes != null)
            //{
            //    KasbeheerTransactieTypes.Clear();
            //    KasbeheerTransactieTypes = null;
            //}
            //if (KasbeheerTransactieTypes != null)
            //{
            //    KasbeheerTransactieTypes.Clear();
            //    KasbeheerTransactieTypes = null;
            //}
            //if (KasbeheerStortenOfAfhalen != null)
            //{
            //    KasbeheerStortenOfAfhalen.Clear();
            //    KasbeheerStortenOfAfhalen = null;
            //}
            if (LeefgroepItemsVisual != null)
            {
                LeefgroepItemsVisual.Clear();
                LeefgroepItemsVisual = null;
            }
         
        }
        ~KasBeheerPageModel()
        {
            Dispose();
        }

    }

}
 