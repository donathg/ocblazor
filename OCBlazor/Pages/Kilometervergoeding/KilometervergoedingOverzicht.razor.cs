﻿using Microsoft.AspNetCore.Components;
using OCBlazor.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using OCBlazor.Models;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.Kilometervergoeding;
using Logic.WagenNS;
using System;
using DevExpress.Blazor;
using DataBase.Services;
using System.Linq;

namespace OCBlazor.Pages.Kilometervergoeding
{
    public class KilometervergoedingOverzichtPageModel : ComponentBase
    {
        [Inject]

        protected IUserService _userService { get; set; }
      

        [Inject]
        protected IKilometervergoedingService _kilometervergoedingService { get; set; }

        public DxDataGrid<Kilometer> _gridRef { get; set; }

        protected IWagenService _wagenService { get; set; }
        public List<Kilometer> Kilometers { get; set; } = new List<Kilometer>();

        public Kilometer SelectedKilometer { get; set; } = new Kilometer();
      
        public bool SavePopupIsVisible { get; set; } = false;

        protected async override void OnInitialized()
        {
            Kilometers =  await _kilometervergoedingService.GetKilometers();
        }
        public void OnActionOnClose(Kilometer kilometer)
        {
            SavePopupIsVisible = false;
            if (kilometer != null)
            {
                Kilometer existing = this.Kilometers.Where(x => x.Id == kilometer.Id).FirstOrDefault();
                if (existing==null)
                {
                    this.Kilometers.Insert(0, kilometer);
                }
                else
                {
                    existing.CopyProperties(kilometer);
                    SelectedKilometer = kilometer;
                }
               
                _gridRef.Refresh();
                StateHasChanged();
            }
        
        }
        public void OpenPopup (Kilometer km)
        {

            SelectedKilometer = km;
            SavePopupIsVisible = true;
            StateHasChanged();
        }
        public void OpenPopupDelete(Kilometer km)
        {

            DeleteQuestionText = $"Zeker dat u deze lijn wil verwijderen ? ";
            SelectedKilometer = km;
            _kilometerMarkedForDelete = km;
            DeleteQuestionPopupVisible = true;
            StateHasChanged();
            DeleteQuestionPopupVisible = true;
            StateHasChanged();
        }

        
        public void AddKilometers(object o)
        {
            SavePopupIsVisible = true;
            SelectedKilometer = new Kilometer();
            SelectedKilometer.DatumRit = DateTime.Now.Date;
            StateHasChanged();
        }
        public void OpenQuestionDelete(Kilometer km)
        {
            DeleteQuestionPopupVisible = true;
            StateHasChanged();

        }


        #region delete 

        public String DeleteQuestionText { get; set; }
        public bool DeleteQuestionPopupVisible { get; set; } = false;


        private Kilometer _kilometerMarkedForDelete = null;

        [Inject]
        Sotsera.Blazor.Toaster.IToaster _toaster { get; set; }

        public void OnQuestionDeleteYes()
        {
            if (_kilometerMarkedForDelete != null)
            {
                try
                {
                    _kilometervergoedingService.DeleteKilometers(_kilometerMarkedForDelete.Id.GetValueOrDefault());
                    this.Kilometers.Remove(_kilometerMarkedForDelete);
                    _kilometerMarkedForDelete = null;
                    DeleteQuestionPopupVisible = false;
                    _gridRef.Refresh();
                    StateHasChanged();
                }
                catch (Exception ex)
                {
                    _toaster.Error(ex.Message, "Fout");
                }
            }
        }
        public void OnQuestionDeleteNo()
        {
            _kilometerMarkedForDelete = null;
            DeleteQuestionPopupVisible = false;
            StateHasChanged();
        }
        public void OnHtmlDataCellDecoration(DataGridHtmlDataCellDecorationEventArgs<Kilometer> eventArgs)
        {
            if (eventArgs.FieldName == nameof(Kilometer.Kilometers))
            {
                if (eventArgs.DataItem.Kilometers.GetValueOrDefault() == 0)
                    eventArgs.Style += " background-color: red;";
                else
                    eventArgs.Style += " background-color: white;";
            }
        }
        #endregion
    }



}
