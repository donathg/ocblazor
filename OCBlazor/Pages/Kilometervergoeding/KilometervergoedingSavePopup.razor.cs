﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.Kilometervergoeding;
using Logic.WagenNS;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.LeefgroepNS;
using Logic.Kampen;
using DevExpress.Blazor;
using System.Collections.ObjectModel;
using Common;
using Common.CloneExtensions;

namespace OCBlazor.Pages.Kilometervergoeding
{


    public class KilometervergoedingSavePopupModel : ComponentBase
    {

       public List<String> Routelijst { get; set; } = new List<string>();
        public List<String> Redenlijst { get; set; } = new List<string>();

        [Parameter]
        public Kilometer SelectedKilometer { get; set; } = new Kilometer();


        public Kilometer  EditedKilometer { get; set; } = new Kilometer();

        [Parameter]
        public bool PopupVisible { get; set; } = true;

        [Parameter]
        public Action<Kilometer> ActionOnClose { get; set; }

        [Inject]
        public IKilometervergoedingTypesService _typeService { get; set; }
        public bool ClientenPopupIsVisible { get; set; } = false;
        public bool LeefgroepPopupIsVisible { get; set; } = false;
        public bool KampenPopupIsVisible { get; set; } = false;
        [Inject]
        public IWagenService _wagenService { get; set; }

        [Inject]
        protected IKilometervergoedingService _kilometervergoedingService { get; set; }
        [Inject]
        public IClientService _clientenService { get; set; }

        [Inject]
        public ILeefgroepService _leefgroepenService { get; set; }

        [Inject]
        public IKampenService _kampenService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        public List<Wagen> Wagens { get; set; } = new List<Wagen>();
        
        public List<KilometerType> KilometerTypes { get; set; } = new List<KilometerType>();

        public IEnumerable<Client> Clienten { get; set; }
        public IEnumerable<Leefgroep> Leefgroepen { get; set; }
        public IEnumerable<Kamp> Kampen { get; set; }

        [Inject]
        Sotsera.Blazor.Toaster.IToaster _toaster { get; set; }
        public void OnActionOnCloseClientPopupMulti(List<Client> clienten)
        {
            if (clienten == null)
                return;

            if (Clienten == null)
                Clienten = new List<Client>();
            foreach (Client c in clienten)
            {
                if (EditedKilometer.Clienten.Contains(c) == false)
                    EditedKilometer.Clienten.Add(c);
            }
            ClosePopups();
            StateHasChanged();
        }

      
        public void OnActionOnCloseLeefgroepPopupMulti(List<Leefgroep> leefgroepen)
        {
            if (leefgroepen == null)
                return;

            if (Leefgroepen == null)
                Leefgroepen = new List<Leefgroep>();
            foreach (Leefgroep lfg in leefgroepen)
            {
                if (EditedKilometer.Leefgroepen.Contains(lfg) == false)
                    EditedKilometer.Leefgroepen.Add(lfg);
            }
            ClosePopups();
            StateHasChanged();
        }

        public void OnActionOnCloseKampenPopupMulti(List<Kamp> kampen)
        {
            if (kampen == null)
                return;

            if (Kampen == null)
                Kampen = new List<Kamp>();
            foreach (Kamp lfg in kampen)
            {
                if (EditedKilometer.Kampen.Contains(lfg) == false)
                    EditedKilometer.Kampen.Add(lfg);
            }
            ClosePopups();
            StateHasChanged();
        }
        private void ClosePopups()
        {
            KampenPopupIsVisible = false;
            ClientenPopupIsVisible = false;
            LeefgroepPopupIsVisible = false;
        }
        
        protected  override async void OnParametersSet()
        {
             EditedKilometer = SelectedKilometer.DeepClone();

            if (EditedKilometer.Id.HasValue && EditedKilometer.KilometerType.Code == "CLIENT")
                EditedKilometer.Clienten = await _kilometervergoedingService.GetClienten(EditedKilometer.Id.GetValueOrDefault());

            if (EditedKilometer.Id.HasValue && EditedKilometer.KilometerType.Code == "LFGR")
                EditedKilometer.Leefgroepen = await _kilometervergoedingService.GetLeefgroepen(EditedKilometer.Id.GetValueOrDefault());

            if (EditedKilometer.Id.HasValue && EditedKilometer.KilometerType.Code == "KAMP")
                EditedKilometer.Kampen = await _kilometervergoedingService.GetKampen(EditedKilometer.Id.GetValueOrDefault());

            if (this.EditedKilometer.KilometerType != null && this.EditedKilometer.Wagen != null)
            {
                RefreshRedenCombobox(this.EditedKilometer.Wagen.Id, this.EditedKilometer.KilometerType.Code);
                RefreshRouteCombobox(this.EditedKilometer.Wagen.Id, this.EditedKilometer.KilometerType.Code);
            }

            StateHasChanged();
            base.OnParametersSet();
        }

        protected override void OnInitialized()
        {
            Wagens = _wagenService.GetWagens();
            KilometerTypes = _typeService.GetKilometersTypes();
        }
        public void OnSaveButtonClick()
        {
            try
            {
                Validate();
                Save();
                CloseWindow();
            }
            catch(Exception ex) {
                _toaster.Error(ex.Message, "Fout");
            }
          
        }

        private void Validate()
        {

            if (EditedKilometer.Wagen == null || EditedKilometer.Wagen.Id==0)
                throw new Exception("Gelieve een vervoermiddel in te geven.");
            if (EditedKilometer.KilometerType == null ||  String.IsNullOrWhiteSpace(EditedKilometer.KilometerType.Code))
                throw new Exception("Gelieve type in te geven.");

            if (String.IsNullOrWhiteSpace(EditedKilometer.Reden))
                throw new Exception("Gelieve reden in te geven.");

            if (String.IsNullOrWhiteSpace(EditedKilometer.Reisroute))
                throw new Exception("Gelieve reisroute in te geven.");

            if (EditedKilometer.KilometerType.Code == "LFGR" && EditedKilometer.Leefgroepen.Count==0)
                throw new Exception("Gelieve leefgroep(en) uit te kiezen.");

            if (EditedKilometer.KilometerType.Code == "CLIENT" && EditedKilometer.Clienten.Count == 0)
                throw new Exception("Gelieve cliënt(en) uit te kiezen.");

            if (EditedKilometer.KilometerType.Code == "KAMP" && EditedKilometer.Kampen.Count == 0)
                throw new Exception("Gelieve een kamp uit te kiezen.");

            if (EditedKilometer.Kilometers.GetValueOrDefault() < 0 )
                throw new Exception("Gelieve een correct aantal kilometers in te geven.");
        }

        public void OnCancelButtonClick()
        {
            EditedKilometer  = null;
            CloseWindow();
        }


        public void DeleteClient(Client client)
        {
            this.EditedKilometer.Clienten.Remove(client);
            StateHasChanged();
        }
        public void DeleteLeefgroep(Leefgroep lfg)
        {
            this.EditedKilometer.Leefgroepen.Remove(lfg);
            StateHasChanged();
        }
        public void DeleteKamp(Kamp kamp)
        {
            this.EditedKilometer.Kampen.Remove(kamp);
            StateHasChanged();
        }


        public void OnOpenClientPopup()
        {
            ClientenPopupIsVisible = true;
            StateHasChanged();
        }
        public void OnOpenLeefgroepPopup()
        {

            LeefgroepPopupIsVisible = true;
            StateHasChanged();
        }
        public void OnOpenKampPopup()
        {

            KampenPopupIsVisible = true;
            StateHasChanged();
        }
        

        private void Save()
        {
            _kilometervergoedingService.SaveKilometers(EditedKilometer);
            StateHasChanged();
        }
        private void CloseWindow()
        {
            if (ActionOnClose != null)
                ActionOnClose(EditedKilometer);
            PopupVisible = false;
            StateHasChanged();
        }

        public void SelectedWagenChanged(Wagen o)
        {
            if (this.EditedKilometer.KilometerType != null  && !String.IsNullOrWhiteSpace(this.EditedKilometer.KilometerType.Code))
            {
                RefreshRedenCombobox(o.Id, this.EditedKilometer.KilometerType.Code);
                RefreshRouteCombobox(o.Id, this.EditedKilometer.KilometerType.Code);
            }
        }
        public void SelectedTypeChanged(KilometerType o)
        {
            if (this.EditedKilometer.Wagen != null && !String.IsNullOrWhiteSpace(o.ToString()))
            {
                RefreshRedenCombobox(this.EditedKilometer.Wagen.Id, o.Code);
                RefreshRouteCombobox(this.EditedKilometer.Wagen.Id, o.Code);
            }
        }
        private void RefreshRedenCombobox(int wagenId , string typeCode)
        {
            this.Redenlijst.Clear();
          
              this.Redenlijst.AddRange(_kilometervergoedingService.GetRedenSuggestions(wagenId, typeCode));
            StateHasChanged();

        }
        private void RefreshRouteCombobox(int wagenId, string typeCode)
        {
            this.Routelijst.Clear();
      
                this.Routelijst.AddRange(_kilometervergoedingService.GetRouteSuggestions(this.EditedKilometer.Wagen.Id, this.EditedKilometer.KilometerType.Code));
            StateHasChanged();

        }
    }

}
