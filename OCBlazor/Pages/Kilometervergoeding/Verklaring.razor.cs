﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.Kilometervergoeding;
using Logic.WagenNS;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Logic.LeefgroepNS;
using Logic.Kampen;
using DevExpress.Blazor;
using System.Collections.ObjectModel;
using Common;
using Common.CloneExtensions;
using DataBase.Services;
using DevExpress.XtraRichEdit.Commands;
using Microsoft.JSInterop;

namespace OCBlazor.Pages.Kilometervergoeding
{


    public class VerklaringModel : ComponentBase
    {
        [Inject]
        protected IUserService _userService { get; set; }
        [Inject]
        protected IKilometervergoedingService _kilometervergoedingService { get; set; }
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }
        [Inject]
        Sotsera.Blazor.Toaster.IToaster _toaster { get; set; }
        [Inject]
        public IJSRuntime _runtime { get; set; }



        public VerklaringResult VerklaringResult { get; set; } = new VerklaringResult();
        public List<VerklaringItem> VerklarinItems { get; set; } = new List<VerklaringItem>();


        [Parameter]
        public String BatchCode { get; set; }

        public bool AllesIngevuld { get; set; } = false;

        protected override void OnInitialized()
        {
            VerklarinItems = _kilometervergoedingService.GetVerklaringItems();
            VerklaringResult = _kilometervergoedingService.GetVerklaringResult(_userService.LoggedOnUser.UserId, BatchCode);
            VerklaringResult.BatchNr = this.BatchCode;
            VerklaringResult.GebruikerId = _userService.LoggedOnUser.UserId;
        }
        public void Save()
        {


            AllesIngevuld = false;

            try
            {
                if (!VerklaringResult.VerklaringItemId.HasValue)
                {
                    throw new Exception("Gelieve een keuze te maken uit de lijst.");
                }

                if (VerklaringResult.VerklaringItemId > 1 && !VerklaringResult.AfstandEnkeleReisFiets.HasValue)
                {
                    throw new Exception("Gelieve afstand fiets in km in te vullen.");
                }

                if ((VerklaringResult.VerklaringItemId != 2 && VerklaringResult.VerklaringItemId != 0) && !VerklaringResult.AfstandEnkeleReisWagen.HasValue)
                {
                    throw new Exception("Gelieve afstand wagen in km in te vullen.");
                }

                if (VerklaringResult.VerklaringOndertekend == false)
                {
                    throw new Exception("Gelieve aan te vinken voor ondertekening.");
                }
              
                //Save Here =>
                _kilometervergoedingService.SaveVerklaringResult(VerklaringResult);

                _toaster.Success("De gegevens zijn opgeslagen.");

                AllesIngevuld = true;

              //  _runtime.InvokeVoidAsync("myLib.CloseWindow", null);

            }
            catch (Exception ex)
            {

                _toaster.Error(ex.Message);
            }
        }
    }
}
