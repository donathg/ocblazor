﻿using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Webservices;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects;
using System.IO;
using Reporting.Maaltijdlijsten;
using Logic.Maaltijdlijsten;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using System.Linq;
using Microsoft.JSInterop;
using Logic.LeefgroepNS;
using Common.DateTimeExtensions;

namespace OCBlazor.Pages.Maaltijdlijsten
{
    public class MaaltijdlijstenPageModel : ComponentBase/*, IDisposable*/
    {
        [Inject]
        ClientData_eCQare_Client _client { get; set; }

        [Inject]
        IJSRuntime js { get; set; }

        [Inject]
        IMaaltijdlijstenReportService _maaltijdlijstenService { get; set; }

        [Inject]
        ILeefgroepService _LeefgroepService { get; set; }

        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now;

        public Byte[] DocumentContent { get; set; } = new byte[0];

        public MaaltijdLijstenReport Reports { get; set; } = new MaaltijdLijstenReport(false);

        public IEnumerable<MaaltijdLijstenReportItem> SelectedReports { get; set; }

        public async Task GetMaaltijdLijsten()
        {
            XtraReport report;

            DateTime startDateVacuum;
            DateTime endDateVacuum;
            (startDateVacuum, endDateVacuum) = GetReportEndDate(StartDate);
            EndDate = StartDate;

            List<Leefgroep> leefgroepen =  _LeefgroepService.GetLeefgroepenMaaltijdlijstenKeuken();
            List<EcqareAlivioMealplanFile> mealPlans = (await _client.Get_eCQareAlivioMealplansFiles("ClaraFey", StartDate.Date, EndDate.Date)).ToList();
            List<EcqareAlivioMealplanFile> mealPlansVacuum = (await _client.Get_eCQareAlivioMealplansFiles("ClaraFey", startDateVacuum, endDateVacuum)).ToList();

            foreach (MaaltijdLijstenReportItem reportItem in SelectedReports)
            {
                switch (reportItem.Id)
                {
                    case 1:
                        MaaltijdlijstenLeefgroepenTotalen data1 = _maaltijdlijstenService.GetLeefgroepenTotalenReportData(leefgroepen,reportItem, StartDate.Date, EndDate.Date,
                            mealPlans.ToList());
                        report = new LeefgroepenTotalenReport(data1);
                        break;
                    case 2:
                        MaaltijdlijstenLeefgroepenDaglijst data2 = _maaltijdlijstenService.GetMaaltijdlijstenLeefgroepenDaglijstReportData(reportItem, StartDate.Date, 
                            EndDate.Date, EcqareAlivioMealplanByLeefgroep.ConvertLeefgroepenForLeefgroepenDaglijst(mealPlans, leefgroepen, false));
                        report = new LeefgroepenDaglijstReport(data2);
                        break;
                    case 3:
                        MaaltijdlijstenLeefgroepenDaglijstWeekend data3 = _maaltijdlijstenService.GetMaaltijdlijstenLeefgroepenDaglijstWeekendReportData(reportItem, 
                            StartDate.Date, EndDate.Date, EcqareAlivioMealplanByLeefgroep.ConvertLeefgroepenForLeefgroepenDaglijst(mealPlans, leefgroepen, true));
                        report = new LeefgroepenDaglijstWeekendReport(data3);
                        break;
                    case 4:
                        MaaltijdlijstenVacuumTotalen data4 = _maaltijdlijstenService.GetMaaltijdlijstenVacuumTotalen(leefgroepen,reportItem, startDateVacuum, endDateVacuum, 
                            mealPlansVacuum.ToList());
                        report = new VacuumTotalenReport(data4);
                        break;
                    case 5:
                        MaaltijdlijstenVacuumDaglijst data5 = _maaltijdlijstenService.GetMaaltijdlijstenVacuumDaglijst(reportItem, startDateVacuum, endDateVacuum,
                            EcqareAlivioMealplanByLeefgroep.ConvertLeefgroepenForVacuumDaglijst(mealPlansVacuum, leefgroepen));
                        report = new VacuumDaglijstReport(data5);
                        break;
                    case 6:
                        MaaltijdlijstenExtra data6 = _maaltijdlijstenService.GetMaaltijdlijstenExtra(reportItem, StartDate.Date, EndDate.Date,
                            EcqareAlivioMealplanByLeefgroep.ConvertLeefgroepenForExtras(mealPlans, leefgroepen));
                        report = new ExtraReport(data6);
                        break;
                    case 7:
                        MaaltijdlijstenLunchpakketten data7 = _maaltijdlijstenService.GetMaaltijdlijstenLunchpakketten(reportItem, StartDate.Date, 
                            EndDate.Date, EcqareAlivioMealplanByLeefgroep.ConvertLeefgroepenForLunchpakketten(mealPlans, leefgroepen));
                        report = new LunchpakkettenReport(data7);
                        break;
                    default:
                        throw new Exception("Report does not exist");
                }
              
                MemoryStream memoryStream = new MemoryStream();
                report.ExportToPdf(memoryStream);

                string filename = $"{reportItem.Naam}_{StartDate.Date.Year}_{StartDate.Date.Month:D2}_{StartDate.Date.Day:D2}";
                DownloadFilePDF pdf = new DownloadFilePDF(filename, "pdf", memoryStream.ToArray());
                
                SaveAs($"{pdf.FileName}.{pdf.Extension}", pdf.Data);
            }
        }

        private (DateTime,DateTime) GetReportEndDate(DateTime startDate)
        {
            DateTime start = startDate;
            DateTime end = startDate;

            switch (startDate.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = startDate.AddDays(-3);
                    break;
                case DayOfWeek.Tuesday:
                    end = startDate.AddDays(2);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(-1);
                    end = startDate.AddDays(1);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(-2);
                    break;
                case DayOfWeek.Friday:
                    end = startDate.AddDays(3);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(-1);
                    end = startDate.AddDays(2);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(-2);
                    end = startDate.AddDays(1);
                    break;
            }

            return (start.Date, end.Date);
        }

        public void SaveAs(string filename, byte[] data)
        {
            js.InvokeAsync<object>(
               "myLib.saveAsFile",
               filename,
               Convert.ToBase64String(data));
        }
    }
    //public void PrintPDF(GOGItem gOGItem)
    //{
    //    if (gOGItem == null)
    //        return;

    //    DownloadFilePDF pdf = GetReportGOG(_gogItemService.GetGOGItemFromId(gOGItem.Id));
    //    SaveAs($"{pdf.FileName}.{pdf.Extension}", pdf.Data);
    //}
    //private DownloadFilePDF GetReportGOG(GOGItem GOGItem)
    //{
    //    GogReport reportData = _gOGReportService.GetGogReport(GOGItem, this.AllMultiSelect.DeepClone());

    //    DateTime date = DateTime.Now;
    //    XtraReport r = new GOGReport(reportData);

    //    MemoryStream memoryStream = new MemoryStream();
    //    r.ExportToPdf(memoryStream);
    //    String filename = "";

    //    filename = $"{"GOG_Melding_nr"}_{GOGItem.Id}_{date.Date.Year}_{date.Date.Month:D2}_{date.Date.Day:D2}";
    //    DownloadFilePDF file = new DownloadFilePDF(filename, "pdf", memoryStream.ToArray());
    //    return file;
    //}
   
}
