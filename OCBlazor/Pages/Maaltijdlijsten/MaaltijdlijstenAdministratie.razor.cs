﻿using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Webservices;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects;
using System.IO;
using Reporting.Maaltijdlijsten.Reports.Administratie;
using Logic.Maaltijdlijsten;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using System.Linq;
using Microsoft.JSInterop;
using Logic.LeefgroepNS;
using Common.DateTimeExtensions;
using Logic.DataBusinessObjects.Maaltijdlijsten.Administratie;
using Logic.Prijzen;

namespace OCBlazor.Pages.Maaltijdlijsten
{
    public class MaaltijdlijstenAdministratiePageModel : ComponentBase/*, IDisposable*/
    {
        [Inject]
        ClientData_eCQare_Client _client { get; set; }
        [Inject]
        IJSRuntime js { get; set; }
        [Inject]
        IMaaltijdlijstenReportService _maaltijdlijstenService { get; set; }
        [Inject]
        ILeefgroepService _LeefgroepService { get; set; }
        [Inject]
        IPrijzenService _prijzenService { get; set; }

        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now;
        public Byte[] DocumentContent { get; set; } = new byte[0];
        public MaaltijdLijstenReport Reports { get; set; } = new MaaltijdLijstenReport(true);
        public IEnumerable<MaaltijdLijstenReportItem> SelectedReports { get; set; }

        public async Task GetMaaltijdLijstenAdministratie()
        {
            XtraReport report;

            DateTime startDatum = StartDate.GetFirstDayOfMonth();
            DateTime eindDatum = StartDate.GetLastDayOfMonth();

            List<Leefgroep> leefgroepen = _LeefgroepService.GetLeefgroepenMaaltijdlijstenAdministratie();
            List<EcqareAlivioMealplanFile> mealPlans = (await _client.Get_eCQareAlivioMealplansFiles("ClaraFey", startDatum, eindDatum)).ToList();
            List<EcqareAlivioMealplanFile> mealPlansKook = mealPlans.Where(x => x.MealChoiceFull == MealChoiceEnum.Kookactiviteit).ToList();
            List<BudgetVervangdeMaaltijdenByLeefgroep> budgetVervangdeMaaltijdenPerleefgroepItems = BudgetVervangdeMaaltijdenByLeefgroep.ConvertBudgetVervangdeMaaltijden(mealPlans, leefgroepen);

            foreach (MaaltijdLijstenReportItem reportItem in SelectedReports)
            {
                switch (reportItem.Id)
                {
                    case 1:
                        MaaltijdenAdministratieBudgetVervangend data1 = _maaltijdlijstenService.GetMaaltijdenBudgetVervangendData(leefgroepen, reportItem, startDatum, eindDatum,
                            budgetVervangdeMaaltijdenPerleefgroepItems, _prijzenService);
                        report = new MaaltijdlijstAdministratieBudgetVervangendReport(data1);
                        break;
                    case 2:
                        MaaltijdenAdministratieGeenLevering data3 = _maaltijdlijstenService.GetMaaltijdenGeenLevering(leefgroepen, reportItem, startDatum, eindDatum,
                            budgetVervangdeMaaltijdenPerleefgroepItems, _prijzenService);
                        report = new MaaltijdlijstenAdministratieGeenReport(data3);
                        break;
                    default:
                        throw new Exception("Report does not exist");
                }

                MemoryStream memoryStream = new MemoryStream();
                report.ExportToPdf(memoryStream);

                string filename = $"{reportItem.Naam}_{StartDate.Date.Year}_{StartDate.Date.Month:D2}_{StartDate.Date.Day:D2}";
                DownloadFilePDF pdf = new DownloadFilePDF(filename, "pdf", memoryStream.ToArray());

                SaveAs($"{pdf.FileName}.{pdf.Extension}", pdf.Data);
            }
        }

        public void SaveAs(string filename, byte[] data)
        {
            js.InvokeAsync<object>(
               "myLib.saveAsFile",
               filename,
               Convert.ToBase64String(data));
        }
    }   
}
