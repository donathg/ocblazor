﻿using Logic.ArtikelNs;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using Logic.DataBusinessObjects.Onkostennota;
using System;
using DevExpress.Blazor;
using Sotsera.Blazor.Toaster;
using DevExpress.XtraRichEdit.Model;
using System.Linq;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using OCBlazor.Services;
using Microsoft.Identity.Client;
using Logic.DataBusinessObjects.Kilometervergoeding;
using Common.CloneExtensions;
using System.Data.SqlClient;
using Logic.Prijzen;
using Logic.LeefgroepNS;
using Logic.AankoopNs;
using Logic.Prijzen;
using Microsoft.AspNetCore.Mvc.TagHelpers;

namespace OCBlazor.Pages.Onkostennota
{
    public class MaaltijdRegistratieGridModel : ComponentBase
    {
        [Inject]
        public IPrijzenService _prijzenService {  get; set; }
        [Inject]
        public ILeefgroepService _leefgroepService { get; set; }


        [Inject]
        public IToaster _toaster { get; set; }
        [Inject] 
        public IOnkostennotaService _onkostennotaService { get; set; }
        [Inject]
        public IClientService _clientService { get; set; }



        public List<OnkostennotaItem> AllOnkostennotas { get; set; }
        public OnkostennotaItem OnkostennotaItemMarkedForDelete { get; set; } = null;
        public List<OnkostennotaCategorieItem> OnkostennotaCategorieItems { get; set; }
        public OnkostennotaItem NewOnkostennotaItem { get; set; } = new OnkostennotaItem();
        public bool QuestionPopupVisible { get; set; } = false;       
        public bool IngavePopupVisible { get; set; } = false;
        public string PopupTitle { get; set; } = "";
        public int AantalBijlages { get; set; } = 0;
        public bool CanDeleteBijlage { get { return true; } }
        public bool PopupBijlageMananagerReadOnyVisible { get; set; }
        public int? PopupBijlageMananagerID { get; set; }
        public bool ClientenPopupIsVisible { get; set; } = false;
        public List<Client> AllMyClienten { get; set; } = new List<Client>();
        public List<Client> SelectedClienten { get; set; } = new List<Client>();
        public List<DateTime?> SelectedDates { get; set; } = new List<DateTime?>();

        public List<MaaltijdAandachtspuntenItem> Aandachtspunten { get; set; } = new List<MaaltijdAandachtspuntenItem>();

        public MaaltijdAandachtspuntenItem SelectedAandachtspunt { get; set; } = null;
        public DateTime MaxDate
        {
            get
            {

                DateTime today = DateTime.Today;
                int daysUntilNextFriday = ((int)DayOfWeek.Friday - (int)today.DayOfWeek + 7) % 7;
                daysUntilNextFriday = daysUntilNextFriday == 0 ? 7 : daysUntilNextFriday; // Ensure it's the next Friday
                DateTime nextFriday = today.AddDays(daysUntilNextFriday);
                return nextFriday.AddDays(7);
            }

        }
        public DateTime MinDate
        {
            get
            {
                return GetMinDate();
            }
        }
        public List<DateTime> DisabledDates
        {
            get
            {
                List<DateTime> DisabledDates = new List<DateTime>();
                if (_onkostennotaService.IsNietBegeleidendPesoneel(NewOnkostennotaItem.CategorieId))
                {
                    for (int i = 0; i < 50; i++)
                    {
                        DateTime dt = DateTime.Now.Date.AddDays(i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            DisabledDates.Add(dt);
                        }
                    }
                    return DisabledDates;
                }
                else
                    return new List<DateTime>();
            }
        }
        public List<Leefgroep> MyDoorgeefLeefgroepen { get; set; } = new List<Leefgroep>();
        public bool LeefgroepPopupIsVisible { get; set; } = false;
        public void OnSelectLeefGroep()
        {
            LeefgroepPopupIsVisible = true;

            StateHasChanged();
        }

        protected override void OnInitialized()
        {
         
            OnkostennotaCategorieItems = _onkostennotaService.GetCategorienMaaltijdRegistraties();
            MyDoorgeefLeefgroepen = _leefgroepService.GetLeefgroepenMaaltijdlijstenKeuken();
            Aandachtspunten = _onkostennotaService.LoadAandachtspunten();
            RefreshGrid();
            base.OnInitialized();
        }
        public void OnActionOnCloseLeefgroepPopup(Leefgroep leefgroep)
        {
            if (leefgroep != null)
            {
                NewOnkostennotaItem.LeefgroepId = leefgroep.Id;
                NewOnkostennotaItem.LeefgroepName  = leefgroep.Name;
            }
            LeefgroepPopupIsVisible = false;
            StateHasChanged();
        }

        public void RefreshGrid()
        {
            AllOnkostennotas = _onkostennotaService.LoadMaaltijdregistraties();
          
        }
        public void OnkostennotaToevoegen_Click()
        {
            NewOnkostennotaItem = new OnkostennotaItem() { AkkoordVerklaring = true };
            SelectedDates = new List<DateTime?>();
            OpenPopup();
            StateHasChanged();
        }
        public void OnClientenKiezen_Click()
        {

            ClientenPopupIsVisible = true;
            StateHasChanged();
        }
        private string ValidatePage1()
        {
            return NewOnkostennotaItem.Validate();
        }
        public bool CanRegisterMeal(DateTime mealDate)
        {
            DateTime now = DateTime.Now;
            
            if (mealDate.Date <= DateTime.Now.Date)
                throw new Exception("Er kan geen maaltijd in het verleden worden geregistreerd - " + mealDate.ToString());

            if (mealDate.Date.DayOfWeek == DayOfWeek.Saturday && _onkostennotaService.IsNietBegeleidendPesoneel(NewOnkostennotaItem.CategorieId))
            {

                throw new Exception("Er kan geen maaltijd in het weekend worden geregistreerd - " + mealDate.ToString());
            }
            if (mealDate.Date.DayOfWeek == DayOfWeek.Sunday && _onkostennotaService.IsNietBegeleidendPesoneel(NewOnkostennotaItem.CategorieId))
            {

                throw new Exception("Er kan geen maaltijd in het weekend worden geregistreerd - " + mealDate.ToString());
            }

            return true;
        }
        public void OnPopupSaveAndCloseClick()
        {
            try
            {
                if (NewOnkostennotaItem.CategorieId == 0)
                    throw new Exception("Gelieve een type uit te kiezen.");

                if (_onkostennotaService.IsBegeleidendPesoneel(NewOnkostennotaItem.CategorieId) && NewOnkostennotaItem.LeefgroepId.HasValue==false)
                    throw new Exception("Gelieve een leefgroep/locatie aan te duiden waar de maaltijd geleverd moet worden.");

                foreach (DateTime d in SelectedDates.Where(x => x.HasValue).Distinct())
                {
                    if (d.Date <= DateTime.Now.Date)
                        throw new Exception("Er kan geen maaltijd in het verleden worden geregistreerd - " + d.ToString());

                    if (CanRegisterMeal(d)==false)
                    {
                        throw new Exception("De maaltijd moet uiterlijk op de vorige werkdag worden geregistreerd - " + d.ToString());
                    }
                }
                var dates = SelectedDates.Where(x => x.HasValue).Distinct();
                if (dates == null || dates.Count()==0)
                    throw new Exception("Gelieve een datum uit te kiezen");

                foreach (DateTime d in SelectedDates.Where(x => x.HasValue).Distinct())
                {
                    try
                    {
                        OnkostennotaItem c = NewOnkostennotaItem.DeepClone();
                         
                        c.Bedrag = (double)_prijzenService.GetBedragByModuleCode_Datum_Subtypes("MAALTIJDREG", d.Date, _onkostennotaService.IsBegeleidendPesoneel(c.CategorieId) ? "begeleidend personeel" : "niet begeleidend personeel",null);
                        c.OnkostenNotaDate = d;
                        c.AkkoordVerklaring = true;
                        if (SelectedAandachtspunt != null)
                            c.AandachtspuntenEcqareId = SelectedAandachtspunt.Id;
                        _onkostennotaService.InsertOnkostenNota(c, "MAALTIJDREG");
                        _onkostennotaService.UpdateOnkostenNota(c);
                    }
                    catch (SqlException ex) when (ex.Number == 2627 || ex.Number == 2601)
                    {
                        // 2627: Violation of UNIQUE KEY constraint
                        // 2601: Cannot insert duplicate key row in a unique index
                        Console.WriteLine("Duplicate key error: " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        _toaster.Error(ex.Message, "Fout");
                    }
                }
                ClosePopup();
                RefreshGrid();
                StateHasChanged();
            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
        }
        public DateTime GetMinDate()
        {
            if (_onkostennotaService.IsNietBegeleidendPesoneel(NewOnkostennotaItem.CategorieId))
            {
                DateTime dt = DateTime.Now;
                Console.WriteLine("CheckDate : " + dt.ToString());
                Console.WriteLine("CheckHour : " + dt.Hour.ToString());
                if (dt.Date.DayOfWeek == DayOfWeek.Friday && dt.Hour < 10)
                {
                    Console.WriteLine("1");
                    return dt.Date.AddDays(+3); //maandag
                }
                else if (dt.Date.DayOfWeek == DayOfWeek.Friday && dt.Hour >= 10)
                {
                    Console.WriteLine("2");
                    return dt.Date.AddDays(+4); //dinsag
                }
                else if (dt.Date.DayOfWeek == DayOfWeek.Thursday && dt.Hour >= 10)
                {
                    Console.WriteLine("3");
                    return dt.Date.AddDays(+4); //maandag
                }
                else if (dt.Date.DayOfWeek == DayOfWeek.Saturday)
                {
                    Console.WriteLine("4");
                    return dt.Date.AddDays(+3); //dinsdag
                }
                else if (dt.Date.DayOfWeek == DayOfWeek.Sunday)
                {
                    Console.WriteLine("5");
                    return dt.Date.AddDays(+2); //dinsdag
                }
                else if (dt.Hour < 10)
                {
                    Console.WriteLine("6");
                    return dt.Date.AddDays(+1);
                }
                else
                {
                    Console.WriteLine("7");
                    return dt.Date.AddDays(+2);
                }
            }


            /*
            Donderdag voor 10 uur bestellen voor vrijdag, zaterdag, zondag
            Vrijdag voor 10 uur bestellen maandag
            Zaterdag en zondag bestellen = Dinsdag
            */
            if (_onkostennotaService.IsBegeleidendPesoneel(NewOnkostennotaItem.CategorieId))
            {

                DateTime dt = DateTime.Now;
                Console.WriteLine("CheckDate : " + dt.ToString());
                Console.WriteLine("CheckHour : " + dt.Hour.ToString());
                if (dt.Date.DayOfWeek == DayOfWeek.Friday && dt.Hour < 10)
                {
                    Console.WriteLine("1");
                    return dt.Date.AddDays(+3); //maandag

                }
                if (dt.Date.DayOfWeek == DayOfWeek.Friday && dt.Hour >= 10)
                {
                    Console.WriteLine("2");
                    return dt.Date.AddDays(+4); //dinsag

                }
                else if (dt.Date.DayOfWeek == DayOfWeek.Thursday && dt.Hour >= 10)
                {
                    Console.WriteLine("3");
                    return dt.Date.AddDays(+4); //maandag

                }
                else if (dt.Date.DayOfWeek == DayOfWeek.Thursday && dt.Hour < 10)
                {
                    Console.WriteLine("3");
                    return dt.Date.AddDays(+1); //vrijdag

                }

                else if (dt.Date.DayOfWeek == DayOfWeek.Saturday)
                {
                    Console.WriteLine("4");
                    return dt.Date.AddDays(+3); //dinsdag

                }
                else if (dt.Date.DayOfWeek == DayOfWeek.Sunday)
                {
                    Console.WriteLine("5");
                    return dt.Date.AddDays(+2); //dinsdag

                }
                else if (dt.Hour < 10)
                {
                    Console.WriteLine("6");
                    return dt.Date.AddDays(+1);

                }
                else
                {
                    Console.WriteLine("7");
                    return dt.Date.AddDays(+2);

                }

            }

            return DateTime.Now.Date.AddDays(+2);

        }
        public void OnPopupCancelClick()
        {
            ClosePopup();
            StateHasChanged();
        }
        private void ClosePopup()
        {
            IngavePopupVisible = false;
            PopupBijlageMananagerID = null;
            PopupBijlageMananagerReadOnyVisible =  false;
        }
        private void OpenPopup()
        {
            PopupTitle = "Maaltijdregistratie";
            IngavePopupVisible = true;
        }
        public void ShowDownload(OnkostennotaItem item)
        {
            PopupBijlageMananagerReadOnyVisible = true;
            PopupBijlageMananagerID = item.Id;
            StateHasChanged();
        }
        public void OnActionOnCloseClientenPopupMulti(List<Client> clienten)
        {
           NewOnkostennotaItem.Clienten= clienten;

             ClientenPopupIsVisible = false;
            StateHasChanged();
        }
        public void OnButtonDelete(OnkostennotaItem item)
        {
            OnkostennotaItemMarkedForDelete = item;
            QuestionPopupVisible = true;
            StateHasChanged();
        }
        public async void DeleteItem()
        {
            try
            {
                if (OnkostennotaItemMarkedForDelete == null)
                    throw new Exception("Geen maaltijdregistratie geslecteerd om te verwijderen.");
                _onkostennotaService.DeleteOnkostenNota(OnkostennotaItemMarkedForDelete);
                RefreshGrid();
                StateHasChanged();
                _toaster.Success("De maaltijdregistratie is verwijderd.", "Onkostennota");

            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
            finally
            {
                ClosePopups();
            }
        }
        public void OnQuestionNo()
        {
            ClosePopups();
        }
        public void ClosePopups()
        {
            OnkostennotaItemMarkedForDelete = null;
            QuestionPopupVisible = false;
           
            IngavePopupVisible = false;
        }
        public void OnSelectedItemChanged(OnkostennotaCategorieItem selectedItem)
        {

          
            DateTime dayToCheck;
            //In toekomst hier de nieuwe Price-Service raaddplegen.
            var d = SelectedDates.Where(x => x.HasValue).FirstOrDefault();
            if (d == null)
                dayToCheck = DateTime.Now.Date;
            else
                dayToCheck = d.GetValueOrDefault();

            SelectedDates = SelectedDates.Select(_ => (DateTime?)null).ToList();
            if (selectedItem == null)
                NewOnkostennotaItem.Bedrag = 0;
            else
                NewOnkostennotaItem.Bedrag = (double)_prijzenService.GetBedragByModuleCode_Datum_Subtypes("MAALTIJDREG", dayToCheck, _onkostennotaService.IsBegeleidendPesoneel(selectedItem.Id) ? "begeleidend personeel" : "niet begeleidend personeel", null); ; /*begeleidend personeel*/

            StateHasChanged();
        }
    }
}
