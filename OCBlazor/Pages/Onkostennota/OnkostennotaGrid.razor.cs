﻿using Logic.ArtikelNs;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using Logic.DataBusinessObjects.Onkostennota;
using System;
using DevExpress.Blazor;
using Sotsera.Blazor.Toaster;
using DevExpress.XtraRichEdit.Model;
using System.Linq;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using OCBlazor.Services;
using Microsoft.Identity.Client;

namespace OCBlazor.Pages.Onkostennota
{
    public class OnkostennotaGridModel : ComponentBase
    {
        [Inject]
        public IToaster _toaster { get; set; }
        [Inject] 
        IOnkostennotaService _onkostennotaService { get; set; }
        public List<OnkostennotaItem> AllOnkostennotas { get; set; }

        public OnkostennotaItem OnkostennotaItemMarkedForDelete { get; set; } = null;
        public List<OnkostennotaCategorieItem> OnkostennotaCategorieItems { get; set; }
        public OnkostennotaItem NewOnkostennotaItem { get; set; } = new OnkostennotaItem();



        public bool QuestionPopupVisible { get; set; } = false;
        public bool IngavePopupPage1Visible { get; set; } = false;

        public bool IngavePopupPage2Visible { get; set; } = false;

        public bool IngavePopupPage3Visible { get; set; } = false;

        public bool IngavePopupVisible { get; set; } = false;

        public string PopupTitle { get; set; } = "";

        public int AantalBijlages { get; set; } = 0;

        public bool CanDeleteBijlage { get { return true; } }

        public bool PopupBijlageMananagerReadOnyVisible { get; set; }
        public int? PopupBijlageMananagerID { get; set; }


        public bool ClientenPopupIsVisible { get; set; } = false;

        [Inject]
        public IClientService _clientService { get; set; }

        public List<Client> AllMyClienten { get; set; } = new List<Client>();

        public List<Client> SelectedClienten { get; set; } = new List<Client>();

        protected override void OnInitialized()
        {
         
            OnkostennotaCategorieItems = _onkostennotaService.GetCategorienOnkostenNota();
            AllMyClienten = _clientService.GetActiveClienten();

            RefreshGrid();
            base.OnInitialized();
        }


        public void RefreshGrid()
        {
            AllOnkostennotas = _onkostennotaService.LoadOnkostenNotas();
            //foreach(OnkostennotaItem n in AllOnkostennotas)
            //{
            //    n.CategorieName = OnkostennotaCategorieItems.Where(x=>x.Id == n.Id).Select(x=>x.Naam).FirstOrDefault();   
            //}
        }
        public void OnkostennotaToevoegen_Click()
        {
            NewOnkostennotaItem = new OnkostennotaItem();
            OpenPopup();
            StateHasChanged();
        }
        public void OnClientenKiezen_Click()
        {

            ClientenPopupIsVisible = true;
            StateHasChanged();
        }
        public void OnPopupToPage2Click()
        {

            try 
            {
                String errorMessage = ValidatePage1();
                 if (!String.IsNullOrWhiteSpace(errorMessage))
                   throw new Exception(errorMessage);
                 SaveNewOnkostenNota();
                 IngavePopupPage1Visible = false;
                 IngavePopupPage2Visible = true;
                 IngavePopupPage3Visible = false;
                 PopupTitle = "Nieuwe onkostennota -Stap 2/3 Uploaden Bijlage(s)";
                 StateHasChanged();
            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
        }

        private string ValidatePage1()
        {
            return NewOnkostennotaItem.Validate();
        }
        private bool ValidatePage2()
        {
            return AantalBijlages > 0;
        }

        private bool ValidatePage3()
        {
            return NewOnkostennotaItem.AkkoordVerklaring;
        }

        public void OnPopupToPage3Click()
        {
            try
            {
                if (!ValidatePage2())
                    throw new Exception("Gelieve min. 1 bijlage toe te voegen.");
                IngavePopupPage1Visible = false;
                IngavePopupPage2Visible = false;
                IngavePopupPage3Visible = true;
                PopupTitle = "Nieuwe onkostennota -Stap 3/3 Samenvatting";
                StateHasChanged();
            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
        
        }

        public void OnPopupSaveAndCloseClick()
        {
            try
            {
                if (NewOnkostennotaItem.AkkoordVerklaring)
            {
                _onkostennotaService.UpdateOnkostenNota(NewOnkostennotaItem);
                ClosePopup();
                RefreshGrid();
                StateHasChanged();
            }
            else
                throw new Exception("Gelieve uw akkoord te geven.");


                _toaster.Success("Uw onkostennota is toegevoegd.", "OK");
            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
        }


        
        private void SaveNewOnkostenNota()
        {
           
                _onkostennotaService.InsertOnkostenNota(NewOnkostennotaItem, "ONKOSTENNOTA");
                
            

        }

        public void AantalBijlagesChanged(int aantal)
        {
            this.AantalBijlages = aantal;
     

        }


        public void OnPopupCancelClick()
        {
            ClosePopup();
            StateHasChanged();
        }
        private void ClosePopup()
        {
            IngavePopupPage1Visible = false;
            IngavePopupPage2Visible = false;
            IngavePopupPage3Visible = false;
            IngavePopupVisible = false;
            PopupBijlageMananagerID = null;
            PopupBijlageMananagerReadOnyVisible =  false;
        }
        private void OpenPopup()
        {
            PopupTitle = "Nieuwe onkostennota -Stap 1/3 Invullen basisgegevens";
            IngavePopupVisible = true;
            IngavePopupPage1Visible = true;
            IngavePopupPage2Visible = false;
            IngavePopupPage3Visible = false;

        }
        public void ShowDownload(OnkostennotaItem item)
        {
            PopupBijlageMananagerReadOnyVisible = true;
            PopupBijlageMananagerID = item.Id;
            StateHasChanged();
        }
      
        public void OnActionOnCloseClientenPopupMulti(List<Client> clienten)
        {

           NewOnkostennotaItem.Clienten= clienten;

             ClientenPopupIsVisible = false;
            StateHasChanged();
        }

        public void OnButtonDelete(OnkostennotaItem item)
        {
            OnkostennotaItemMarkedForDelete = item;
            QuestionPopupVisible = true;
            StateHasChanged();
        }


        public async void DeleteItem()
        {
            try
            {
                if (OnkostennotaItemMarkedForDelete == null)
                    throw new Exception("Geen onkosntennota geslecteerd om te verwijderen.");
                _onkostennotaService.DeleteOnkostenNota(OnkostennotaItemMarkedForDelete);
                RefreshGrid();
                StateHasChanged();
                _toaster.Success("De onkostennota is verwijderd.", "Onkostennota");

            }
            catch (Exception ex)
            {
                _toaster.Error(ex.Message, "Fout");
            }
            finally
            {

                ClosePopups();
            }

        }

        public void OnQuestionNo()
        {


            ClosePopups();

        }
        public void ClosePopups()
        {
            OnkostennotaItemMarkedForDelete = null;
            QuestionPopupVisible = false;
            IngavePopupPage1Visible = false;
            IngavePopupPage2Visible = false;
            IngavePopupPage3Visible = false;
            IngavePopupVisible = false;
        }

    }
}
