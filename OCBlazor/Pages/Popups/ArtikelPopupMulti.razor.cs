﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.ArtikelNs;
using DevExpress.Blazor;

namespace OCBlazor.Pages.Popups
{
    public class ArtikelenPopupMultiModel : ComponentBase, IDisposable
    {
        [Parameter]
        public int ArtikelCategorieID { get; set; } = 1;

        [Parameter]
        public bool PopupVisible { get; set; } = true;

        [Parameter]
        public Action<List<Artikel>> ActionOnClose { get; set; }

        [Inject]
        public IArtikelenService _artikelService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        private List<Artikel> _selectedArtikelen = new List<Artikel>();
        public List<Artikel> SelectedArtikelen { get { return _selectedArtikelen; } set { _selectedArtikelen = value; StateHasChanged(); } }
        public List<Artikel> Artikelen { get; set; } = new List<Artikel>();

        public String WindowTitle { get; set; } = "";

        protected async Task OnSelectionChanged(DataGridSelection<Artikel> selection)
        {
            var selectedKeys = await selection.SelectedKeys;
            _selectedArtikelen.Clear();
            List<int> idList = new List<int>();
            foreach (object o in selectedKeys)
                idList.Add((int)o);
            _selectedArtikelen = Artikelen.Where(t => idList.Contains(t.ArtikelId)).ToList();
            StateHasChanged();
        }
      
        protected override void OnParametersSet()
        {
            if (Artikelen == null || Artikelen.Count==0)
                  Artikelen = _artikelService.GetArtikelen(ArtikelCategorieID);

            this.WindowTitle = _artikelService.GetArtikelCategoryDescription(ArtikelCategorieID);
            base.OnParametersSet();
        }
        public void OnOkButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(_selectedArtikelen);

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(null);
            PopupVisible = false;
            StateHasChanged();
        }

        public void Dispose()
        {
           
            if (Artikelen!=null)
           {
                Artikelen.Clear();
                Artikelen = null;

           }
           if (SelectedArtikelen != null)
           {
                SelectedArtikelen.Clear();
                SelectedArtikelen = null;

           }
           _selectedArtikelen = null;

        }
        ~ArtikelenPopupMultiModel()
        {
            Dispose();
        }
    }
}
