﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.Modules;

namespace OCBlazor.Pages.Popups
{

 
    public class BijlageManagerPopupModel : ComponentBase, IDisposable
    {

        [Parameter]
        public int Id { get; set; }

        [Parameter]
        public ModuleType Module { get; set; }

        [Parameter]
        public bool CanDelete { get; set; } = true;

        public String HeaderText { get; set; } = "Bijlagemanager";

        [Parameter]
        public bool PopupVisible { get; set; } = true;

        [Parameter]
        public Action ActionOnClose { get; set; }

        [Inject]
        public IClientService _clientenService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

 
        protected override void OnParametersSet()
        {
            if (Module == ModuleType.WERKOPDRACHT)
                HeaderText = "Bijlagemanager Werkopdrachten";
            if (Module == ModuleType.INVENTARIS)
                HeaderText = "Bijlagemanager Inventaris";
            base.OnParametersSet();
        }
        protected override bool ShouldRender()
        {
            return base.ShouldRender();
        }

        public void OnOkButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose();

            PopupVisible = false;
            StateHasChanged();
        }
        public void Dispose()
        {

         


        }
        ~BijlageManagerPopupModel()
        {
            Dispose();
        }

    }

}
