﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using DevExpress.XtraRichEdit.Import.Doc;

namespace OCBlazor.Pages.Popups
{

 
    public class ClientenPopupModel : ComponentBase,IDisposable
    {
        [Parameter]
        public bool PopupVisible { get; set; } = true;
        [Parameter]
        public Action<Client> ActionOnClose { get; set; }
        [Parameter]
        public Action<ClientOverzichtClass> ActionOnOkClick { get; set; }
        [Parameter]
        public List<Client> ClientenPool { get; set; }         

        [Inject]
        public IClientService _clientenService { get; set; }
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        private Client _selectedClient;
        public Client SelectedClient { get { return _selectedClient; } set { _selectedClient = value; StateHasChanged(); } }
        public List<Client> Clienten { get; set; } = new List<Client>();
        public List<Client> AlleActiveClienten { get; set; } = new List<Client>();
        protected override void OnParametersSet()
        {



            if (ClientenPool == null || ClientenPool.Count == 0)
            {
                if (AlleActiveClienten == null || AlleActiveClienten.Count == 0)
                    AlleActiveClienten = _clientenService.GetActiveClienten(); 
                Clienten = AlleActiveClienten; 

            }
            else
                Clienten = ClientenPool;
            base.OnParametersSet();
        }

        protected override void OnInitialized()
        {
            Clienten = _clientenService.GetActiveClienten();
        }
        public void OnOkButtonClick()
        {
            ActionOnClose?.Invoke(SelectedClient);
            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            ActionOnClose?.Invoke(null);
            PopupVisible = false;
            StateHasChanged();
        }

        public void Dispose()
        {
            if (ClientenPool != null)
            {
                ClientenPool.Clear();
                ClientenPool = null;

            }
            if (ClientenPool != null)
            {
                ClientenPool.Clear();
                ClientenPool = null;
            }

            ActionOnClose = null;
            ActionOnOkClick = null;
            SelectedClient = null;

         //   GC.SuppressFinalize(this);
        }
        ~ClientenPopupModel()
        {
            Dispose();

        }
    }

}
