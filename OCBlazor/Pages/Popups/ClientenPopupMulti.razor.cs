﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using DevExpress.Blazor;
 

namespace OCBlazor.Pages.Popups
{


    public class ClientenPopupMultiModel : ComponentBase, IDisposable
    {
        [Parameter]
        public bool PopupVisible { get; set; } = true;
        [Parameter]
        public Action<List<Client>> ActionOnClose { get; set; }
        [Parameter]
        public List<Client> SelectedClienten { get; set; } = new List<Client>();
        [Parameter]
        public List<Client> ClientenPool { get; set; }
        [Parameter]
        public List<Client> AllClientenPool { get; set; }

        [Inject]
        public IClientService _clientenService { get; set; }
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

 
        protected IReadOnlyList<Object> SelectedClientenGridLower { get; set; }
        protected IReadOnlyList<Object> SelectedClientenGridUpper { get; set; }

        // protected IEnumerable<Client> _selectedClienten = new List<Client>();
        public List<Client> ClientenGridLower { get; set; } = new List<Client>();

        public List<Client> ClientenGridUpper { get; set; } = new List<Client>();

        public List<Client> AlleActiveClienten { get; set; } = new List<Client>();


        public IGrid refClientenGridUpper { get; set; }
        public IGrid refClientenGridLower { get; set; }
 
        protected bool AllClientenButtonVisible => AllClientenPool != null && AllClientenPool.Count != 0 && AllClientenPool.Count > ClientenPool.Count;
        protected bool AllClientenToggleChecked = false;

        protected override void OnParametersSet()
        {
            base.OnParametersSet();

            if (ClientenPool == null || ClientenPool.Count == 0)
            {
                if (AlleActiveClienten == null || AlleActiveClienten.Count == 0)
                {
                    AlleActiveClienten = _clientenService.GetActiveClienten();
                    ClientenGridLower = AlleActiveClienten;
                }
            }
            else
                ClientenGridLower = ClientenPool.Where(x=>x.Id>=0).OrderBy(x=>x.LastName).ToList(); //Just to have a clone

            SelectedClientenGridLower = new List<Client>();
   
            ClientenGridUpper = SelectedClienten.OrderBy(x => x.LastName).ToList();
            SelectedClientenGridUpper = ClientenGridUpper.ToList();

            foreach (Client c in SelectedClienten)
            {
                Client f = ClientenGridLower.Where(x => x.Id == c.Id).FirstOrDefault();
                if (f != null)
                    ClientenGridLower.Remove(f);

            }
        }

        protected override bool ShouldRender()
        {
            if (!this.PopupVisible)
                return false;
            return base.ShouldRender();
        }
        //protected override void OnAfterRender(bool firstRender)
        //{
        //    base.OnAfterRender(firstRender);
        //    //if (refClientenGridUpper != null && firstRender)
        //    //{
        //    //   // refClientenGridUpper.SortBy("LastName", GridColumnSortOrder.Descending);
        //    //    refClientenGridUpper.SelectAllAsync();
        //    //}
        //    //if (refClientenGridLower != null && firstRender)
        //    //{
        //    //    refClientenGridLower.DeselectAllAsync();

        //    //    // refClientenGridLower.SortBy("LastName", GridColumnSortOrder.Descending);
        //    //} 
        //    StateHasChanged();
        //}

        
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {

           await base.OnAfterRenderAsync(firstRender);
            if (refClientenGridUpper != null)
            {
                // refClientenGridUpper.SortBy("LastName", GridColumnSortOrder.Descending);
                refClientenGridUpper.SelectAllAsync();
            }
            if (refClientenGridLower != null)
            {
                refClientenGridLower.DeselectAllAsync();

                // refClientenGridLower.SortBy("LastName", GridColumnSortOrder.Descending);
            }
        }

        private void SelectGridItemsByRef(DxDataGrid<Client> myRef, IEnumerable<Client> myClienten)
        {
            if (myRef != null)
            {
                foreach (Client client in myClienten)
                    myRef.SetDataRowSelectedByKey(client.Id, true);
            }
        }
        private void DeSelectGridItemsByRef(DxDataGrid<Client> myRef, IEnumerable<Client> myClienten)
        {
            if (myRef != null)
            {
                foreach (Client client in myClienten)
                    myRef.SetDataRowSelectedByKey(client.Id, false);
            }
        }
        public void OnOkButtonClick()
        {

            
            ActionOnClose?.Invoke(ClientenGridUpper.ToList());

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            ActionOnClose?.Invoke(null);

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnAllClientenButtonClick()
        {
            // als de huidige status van de togglebutton false: verander de lijst naar allClienten pool
            if (!AllClientenToggleChecked)
                ClientenGridLower = AllClientenPool;
            // als de huidige status van de togglebutton true is en de clientenpool leeg is: verander de lijst naar alleActieveClienten
            else if (ClientenPool == null || ClientenPool.Count == 0)
            {
                if (AlleActiveClienten == null || AlleActiveClienten.Count == 0)
                {
                    AlleActiveClienten = _clientenService.GetActiveClienten();
                    ClientenGridLower = AlleActiveClienten;
                }
            }
            // als de huidige status van de togglebutton true is en de clientenPool niet leeg is: verander de lijst naar clientenPool
            else
                ClientenGridLower = ClientenPool;
            // verander de togglebutton status naar het omgekeerde (toggle)
            AllClientenToggleChecked = !AllClientenToggleChecked;
            // voer de veranderingen door
            StateHasChanged();
        }
      
        public void OnSelectedDataItemsChangedUpper(IReadOnlyList<object> newSelection)
        {
            if (newSelection is IGridSelectionChanges changes)
            {


                foreach (object o in changes.DeselectedDataItems)
                {
                    Client c = o as Client;
                    Client f = ClientenGridUpper.Where(x => x.Id == c.Id).FirstOrDefault();
                    if (f != null) //Only add when not exists
                    {
                        ClientenGridUpper.Remove(f);



                    }
                    Client x = ClientenGridLower.Where(x => x.Id == c.Id).FirstOrDefault();
                    if (x == null) //Only add when not exists
                        ClientenGridLower.Add(c);



                }



            }
            ClientenGridLower = ClientenGridLower.OrderBy(x => x.LastName).ToList();  
            refClientenGridLower.DeselectAllAsync();
           
        }
        public void OnSelectedDataItemsChangedLower(IReadOnlyList<object> newSelection)
        {
            if (newSelection is IGridSelectionChanges changes)
            {
                foreach (object o in changes.SelectedDataItems)
                {
                    Client c = o as Client;
                    Client f = ClientenGridUpper.Where(x => x.Id == c.Id).FirstOrDefault();
                    if (f == null) //Only add when not exists
                    {
                        ClientenGridUpper.Add(c);



                    }
                    Client x = ClientenGridLower.Where(x => x.Id == c.Id).FirstOrDefault();
                    if (x != null) //Only add when not exists
                        ClientenGridLower.Remove(x);



                }


                //SelectedItemsInfo = string.Join(";  ", changes.SelectedDataItems
                //    .Cast<Product>()
                //    .Select(p => p.ProductName)
                //);
                //DeselectedItemsInfo = string.Join(";  ", changes.DeselectedDataItems
                //    .Cast<Product>()
                //    .Select(p => p.ProductName)
                //);
                refClientenGridUpper.SelectAllAsync();
                InvokeAsync(StateHasChanged);
            }
        }

        public void Dispose()
        {
            SelectedClientenGridLower = null;
            if (ClientenGridLower != null)
            {
                ClientenGridLower.Clear();
                ClientenGridLower = null;
            }
            if (ClientenPool != null)
            {
                ClientenPool.Clear();
                ClientenPool = null;
            }
            if (AllClientenPool != null)
            {
                AllClientenPool.Clear();
                AllClientenPool = null;
            }
          //  gridRefLower = null;
            ActionOnClose = null;

        //    GC.SuppressFinalize(this);
        }
        ~ClientenPopupMultiModel()
        {
            Dispose();
        }
    }
}
