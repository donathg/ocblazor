﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using DevExpress.Blazor;
using Logic.Gebruikers;

namespace OCBlazor.Pages.Popups
{

 
    public class GebruikerPopupMultiModel : ComponentBase, IDisposable
    {
        [Parameter]
        public bool PopupVisible { get; set; } = true;
        [Parameter]
        public Action<List<Gebruiker>> ActionOnClose { get; set; }
        [Parameter]
        public List<Gebruiker> SelectedGebruikers { get; set; } = new List<Gebruiker>();

        [Inject]
        public IGebruikerService _gebruikerService { get; set; }

        protected IEnumerable<Gebruiker> _selectedGebruikers = new List<Gebruiker>();      
        public List<Gebruiker> Gebruikers { get; set; } = new List<Gebruiker>();
        public DxDataGrid<Gebruiker> gridRef;

        protected override void OnParametersSet()        
        {
            if (Gebruikers == null | Gebruikers.Count==0)
                Gebruikers = _gebruikerService.GetAllActiveGebruikers();
            _selectedGebruikers = SelectedGebruikers;
        }
        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);
            if (gridRef != null)
            {
                foreach (Gebruiker gebruiker in _selectedGebruikers)
                    gridRef.SetDataRowSelectedByKey(gebruiker.Id, true);
            }
            StateHasChanged();
        }
        public void OnOkButtonClick()
        {
            ActionOnClose?.Invoke(_selectedGebruikers.ToList());

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            ActionOnClose?.Invoke(null);

            PopupVisible = false;
            StateHasChanged();
        }
        protected async Task OnSelectionChanged(DataGridSelection<Gebruiker> selection)
        {
            _selectedGebruikers.ToList().Clear();

            var selectedKeys = await selection.SelectedKeys;
            _selectedGebruikers = Gebruikers.Where(x => selectedKeys.Contains(x.Id)).ToList();

            StateHasChanged();
        }
        public void Dispose()
        {


            if (Gebruikers != null)
            {
                Gebruikers.Clear();
                Gebruikers = null;

            }
            SelectedGebruikers = null;
            gridRef = null;
            ActionOnClose = null;
        //    GC.SuppressFinalize(this);
        }
        ~GebruikerPopupMultiModel()
        {
            Dispose();

        }
    }
}
