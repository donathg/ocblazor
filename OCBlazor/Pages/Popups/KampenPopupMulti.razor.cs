﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using DevExpress.Blazor;
using Logic.Kampen;

namespace OCBlazor.Pages.Popups
{

 
    public class KampenPopupMultiModel : ComponentBase
    {
        [Parameter]
        public bool PopupVisible { get; set; } = true;

        [Parameter]
        public Action<List<Kamp>> ActionOnClose { get; set; }


 


        

        [Inject]
        public IKampenService _kampenService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }



        private List<Kamp> _selectedKamp = new List<Kamp>();
        public List<Kamp> SelectedKamp { get { return _selectedKamp; } set { _selectedKamp = value; StateHasChanged(); } }
    

        
        public List<Kamp> Kampen { get; set; } = new List<Kamp>();


        protected override void OnInitialized()
        {
            Kampen = _kampenService.GetKampenActief();
        }
        public void OnOkButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(SelectedKamp);

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(null);
            PopupVisible = false;
            StateHasChanged();
        }


        protected async Task OnSelectionChanged(DataGridSelection<Kamp> selection)
        {
            var selectedKeys = await selection.SelectedKeys;

            _selectedKamp.Clear();
            List<int> idList = new List<int>();
            foreach (object o in selectedKeys)
                idList.Add((int)o);
            _selectedKamp = Kampen.Where(t => idList.Contains(t.Id)).ToList();
            StateHasChanged();

        }


    }

}
