﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.ArtikelNs;
using DevExpress.Blazor;
using Logic.LeefgroepNS;

namespace OCBlazor.Pages.Popups
{
    public class LeefgroepPopupModel : ComponentBase, IDisposable
    {
        [Parameter]
        public bool PopupVisible { get; set; } = true;
        [Parameter]
        public Action<Leefgroep> ActionOnClose { get; set; }
        [Parameter]
        public List<Leefgroep> LeefgroepPool { get; set; }

        [Inject]
        public ILeefgroepService _leefgroepService { get; set; }
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        public List<Leefgroep> Leefgroepen { get; set; } = new List<Leefgroep>();
        public List<Leefgroep> AllLeefgroepen { get; set; } = new List<Leefgroep>();
        private Leefgroep _selectedLeefgroep;



        public Leefgroep SelectedLeefgroep 
        { 
            get => _selectedLeefgroep; 
            set 
            { 
                _selectedLeefgroep = value;
                StateHasChanged(); 
            }
        }


        protected override void OnParametersSet()
        {
            if (LeefgroepPool == null || LeefgroepPool.Count == 0)
            {
                if (AllLeefgroepen==null || AllLeefgroepen.Count == 0)
                    AllLeefgroepen = _leefgroepService.GetAllLeefgroepen();
                Leefgroepen = AllLeefgroepen;

            }
            else
                Leefgroepen = LeefgroepPool;
            base.OnParametersSet();
        }

        public void OnOkButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(_selectedLeefgroep);

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(null);
            PopupVisible = false;
            StateHasChanged();
        }

        public void Dispose()
        {

            if (LeefgroepPool != null)
            {
                LeefgroepPool.Clear();
                LeefgroepPool = null;

            }
            if (Leefgroepen != null)
            {
                Leefgroepen.Clear();
                Leefgroepen = null;

            }
            if (AllLeefgroepen != null)
            {
                AllLeefgroepen.Clear();
                AllLeefgroepen = null;

            }
            SelectedLeefgroep = null;
            ActionOnClose = null;
      //      GC.SuppressFinalize(this);

        }
        ~LeefgroepPopupModel()
        {
            Dispose();

        }
    }
}
