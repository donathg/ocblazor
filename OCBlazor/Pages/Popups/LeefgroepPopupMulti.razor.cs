﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.ArtikelNs;
using DevExpress.Blazor;
using Logic.LeefgroepNS;

namespace OCBlazor.Pages.Popups
{
    public class LeefgroepPopupMultiModel : ComponentBase, IDisposable
    {
        [Parameter]
        public bool PopupVisible { get; set; } = true;

        [Parameter]
        public Action<List<Leefgroep>> ActionOnClose { get; set; }

        [Inject]
        public ILeefgroepService _leefgroepService { get; set; }

        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        private List<Leefgroep> _selectedLeefgroepen = new List<Leefgroep>();
        public List<Leefgroep> SelectedLeefgroepen { get { return _selectedLeefgroepen; } set { _selectedLeefgroepen = value; StateHasChanged(); } }
        public List<Leefgroep> Leefgroepen { get; set; } = new List<Leefgroep>();
        public List<Leefgroep> AllLeefgroepen { get; set; } = new List<Leefgroep>();
        [Parameter]
        public List<Leefgroep> LeefgroepPool { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
        protected override void OnInitialized()
        {
            //if (AllLeefgroepen==null || AllLeefgroepen.Count == 0)
            //    AllLeefgroepen = _leefgroepService.GetLeefgroepen();
            //Leefgroepen = AllLeefgroepen;

            if (LeefgroepPool == null || LeefgroepPool.Count == 0)
            {
                if (AllLeefgroepen == null || AllLeefgroepen.Count == 0)
                    AllLeefgroepen = _leefgroepService.GetAllActiveLeefgroepen(true);

                Leefgroepen = AllLeefgroepen;
            }
            else
                Leefgroepen = LeefgroepPool;

            base.OnParametersSet();
        }

        protected async Task OnSelectionChanged(DataGridSelection<Leefgroep> selection)
        {
            var selectedKeys = await selection.SelectedKeys;
            _selectedLeefgroepen.Clear();
            List<int> idList = new List<int>();
            foreach (object o in selectedKeys)
                idList.Add((int)o);
            _selectedLeefgroepen = Leefgroepen.Where(t => idList.Contains(t.Id)).ToList();
            StateHasChanged();
        }

        public void OnOkButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(_selectedLeefgroepen);

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(null);
            PopupVisible = false;
            StateHasChanged();
        }

        public void Dispose()
        {

            if (AllLeefgroepen != null)
            {
                AllLeefgroepen.Clear();
                AllLeefgroepen = null;

            }
            if (Leefgroepen != null)
            {
                Leefgroepen.Clear();
                Leefgroepen = null;

            }

            if (SelectedLeefgroepen != null)
            {
                SelectedLeefgroepen.Clear();
                SelectedLeefgroepen = null;

            }

            ActionOnClose = null;
      //      GC.SuppressFinalize(this);
        }
        ~LeefgroepPopupMultiModel()
        {
            Dispose();

        }
    }
}
