﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.LocatieNs;
using DevExpress.Blazor;

namespace OCBlazor.Pages.Popups
{

 
    public class LocatiePopupModel : ComponentBase 
    {

        [Parameter]
        public bool Enabled { get; set; } = true;

        [Parameter]
        public bool PopupVisible { get; set; } = true;

        [Parameter]
        public Action<Locatie> ActionOnClose { get; set; }


        [Parameter]
        public Action<Locatie> ActionOnOkClick { get; set; }

     



        private Locatie _selectedLocatie;
        public Locatie SelectedLocatie { get { return _selectedLocatie; } set { _selectedLocatie = value; StateHasChanged(); } }
    

      

      

      
      
        public void OnOkButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(SelectedLocatie);

            PopupVisible = false;
            StateHasChanged();
        }
        public void OnCancelButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose(null);
            PopupVisible = false;
            StateHasChanged();
        }

        protected void OnSelectedTreeItemChanged(Locatie locatie)
        {
            SelectedLocatie = locatie;
        }
    }

}
