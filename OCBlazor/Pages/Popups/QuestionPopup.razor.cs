﻿using Microsoft.AspNetCore.Components;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace OCBlazor.Pages.Popups
{

 
    public class QuestionPopupModel : ComponentBase
    {



        [Parameter]
        public String Question { get; set; } = "";

        [Parameter]
        public String YesButtonText { get; set; } = "";

        [Parameter]
        public String NoButtonText { get; set; } = "";

        [Parameter]
        public bool PopupVisible { get; set; } = true;


        [Parameter]
        public Action ActionYes { get; set; }


        [Parameter]
        public Action ActionNo { get; set; }

        public void OnOkButtonClick()
        {
            PopupVisible = false;
            StateHasChanged();

            if (ActionYes != null)
                ActionYes();
        }
        public void OnCancelButtonClick()
        {
         
            PopupVisible = false;
            StateHasChanged();

            if (ActionNo != null)
                ActionNo();
        }
    }

}
