﻿using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using OCBlazor.Models;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.Reports;

namespace OCBlazor.Pages.Popups
{

 
    public class ReportPopupModel : ComponentBase
    {

        [Parameter]
        public int Id { get; set; }

        [Parameter]
        public ReportType ReportType { get; set; }

        [Parameter]
        public bool CanDelete { get; set; } = true;

        public String HeaderText { get; set; } = "Rapport";

        [Parameter]
        public bool PopupVisible { get; set; } = true;

        [Parameter]
        public Action ActionOnClose { get; set; }


      
        public String ReportUrl { get; set; }



        

  
        [Inject]
        public IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }




        protected override void OnParametersSet()
        {
            if (ReportType == ReportType.Werkopdracht)
                ReportUrl = $"WerkopdrachtReport?Id={Id}";
          

            base.OnParametersSet();
        }

        public void OnOkButtonClick()
        {
            if (ActionOnClose != null)
                ActionOnClose();

            PopupVisible = false;
            StateHasChanged();
        }
    }

}
