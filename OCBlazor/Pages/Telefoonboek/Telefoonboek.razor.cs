﻿using Microsoft.AspNetCore.Components;
using OCBlazor.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using OCBlazor.Models;
using Logic;
 
using Logic.DataBusinessObjects;

namespace OCBlazor.Pages
{
    public class TelefoonBoekPageModel : ComponentBase
    {
        [Inject]
        protected ITelefoonboekServiceInterface _telefoonboekService { get; set; }

         [Inject]
        protected IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }

        public List<Gebruiker> TelefoonBoekVisual { get; set; } = new List<Gebruiker>();
        protected override void OnInitialized()
        {
           TelefoonBoekVisual = _telefoonboekService.GetTelefoonboek();
        }

        protected void OnSelectedTreeItemChanged(Locatie locatie)
        {

        }
    }

}
