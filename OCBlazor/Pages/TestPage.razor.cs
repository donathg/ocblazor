﻿using Microsoft.AspNetCore.Components;
using OCBlazor.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using OCBlazor.Models;
using Logic;
 
using Logic.DataBusinessObjects;
using Logic.Reports;
using System;
using Microsoft.JSInterop;
using OCBlazor.Pages.GOG;
using Logic.DataBusinessObjects.GOG;
using Logic.GOG;
using Logic.LeefgroepNS;
using DevExpress.Blazor;
using DevExpress.Web;
using System.Linq;
using System.Text.Json;
using Common.DateTimeExtensions;

namespace OCBlazor.Pages
{
    public class TestPageModel : ComponentBase
    {
        [Inject]

        IJSRuntime JSRuntime { get; set; }
        [Inject]
        IJSRuntime js { get; set; }

        [Inject]
        protected IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }
        [Inject]
        protected IWerkbonnenbeheerReportService _werkbonnenbeheheerReportService { get; set; }

        public List<GOGItem> GOGLijst { get; set; } = new List<GOGItem>();

        [Inject]
        public IGOGItemService _gogItemService { get; set; }

        [Inject]
        public ILeefgroepService _leefgroepService { get; set; }
        public List<Leefgroep> MyLeefgroepen { get; set; } = new List<Leefgroep>();


        const string LocalStorageKey = "Grid-LayoutPersistence-Data";
        public bool PreRendered { get; set; }
        public  IGrid Grid { get; set; }
        public object GridData { get; set; }

        protected override void OnInitialized()
        {
        
        }

        protected void OnSelectedTreeItemChanged(Locatie locatie)
        {

        }
        
       public  void OnClickColumnChooser()
        {
            Grid.ShowColumnChooser(".column-chooser-button");
        }

        public async  void  SaveSettings(int id)
        {
            var  layout =  this.Grid.SaveLayout();
            await  SaveLayoutToLocalStorageAsync(layout, id);
        }

        public async void LoadSettings(int id)
        {
            var layout = await LoadLayoutFromLocalStorageAsync(id);
            this.Grid.LoadLayout(layout);
           
        }

        async Task<GridPersistentLayout> LoadLayoutFromLocalStorageAsync(int id)
        {
            try
            {
                var json = await JSRuntime.InvokeAsync<string>("localStorage.getItem", LocalStorageKey+id);
                return JsonSerializer.Deserialize<GridPersistentLayout>(json);
            }
            catch
            {
                // Mute exceptions for the server prerender stage
                return null;
            }
        }

        async Task SaveLayoutToLocalStorageAsync(GridPersistentLayout layout, int id)
        {
            try
            {
                var json = JsonSerializer.Serialize(layout);
                await JSRuntime.InvokeVoidAsync("localStorage.setItem", LocalStorageKey+id, json);
            }
            catch
            {
                // Mute exceptions for the server prerender stage
            }
        }
        async Task RemoveLayoutFromLocalStorageAsync()
        {
            try
            {
                await JSRuntime.InvokeVoidAsync("localStorage.removeItem", LocalStorageKey);
            }
            catch
            {
                // Mute exceptions for the server prerender stage
            }
        }





       public void Grid_CustomizeCellDisplayText(GridCustomizeCellDisplayTextEventArgs e)
        {
            if (e.FieldName == "Leefgroep.Id")
            {
    
                    e.DisplayText = MyLeefgroepen.Where (x=>x.Id == (int)e.Value).Select(x=>x.Name).FirstOrDefault();
            }
        }
   
        public void Grid_CustomizeElement(GridCustomizeElementEventArgs e)
        {
            if (e.ElementType == GridElementType.DataCell && ((IGridDataColumn)e.Column).FieldName == "Shipped")
            {
                e.CssClass = "p-0";
            }
        }





        public void DownloadWerkbonReport(int id)
        {
            DownloadFilePDF pdf = _werkbonnenbeheheerReportService.GetPDF(id);
           // SaveAs($"{pdf.FileName}.{pdf.Extension}", pdf.Data);
            js.InvokeVoidAsync(
      "myLib.OpenPDFByteArrayInNewTab",
      Convert.ToBase64String(pdf.Data)
       );

        }
        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                MyLeefgroepen = _leefgroepService.GetLeefgroepenLoggedOnUser("GOG");
                GOGLijst = _gogItemService.GetGOGItemsForLeefgroepenAndOwn(MyLeefgroepen,DateTime.Now.GetFirstDayOfMonth(), DateTime.Now.GetLastDayOfMonth());
                StateHasChanged();
            }
            base.OnAfterRender(firstRender);
        }

     


        public void SaveAs(string filename, byte[] data)
        {
            js.InvokeAsync<object>(
   "myLib.saveAsFile",
   filename,
   Convert.ToBase64String(data));
        }
    }

}
