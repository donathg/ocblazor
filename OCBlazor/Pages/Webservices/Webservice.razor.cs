﻿using AutoMapper;
using Database.DataAccessObjects;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using OCBlazor.PageModels;
using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using OCBlazor.Models;
using Logic;
using Logic.DataBusinessObjects;
using Logic.Clienten;
using Logic.LeefgroepNS;
using Logic.DataBusinessObjects.MultiSelectNS;
using OCBlazor.Pages.GOG;
using Logic.DataBusinessObjects.Webservices;
using Logic.WagenNS;
using DevExpress.Office.Utils;
using DevExpress.Blazor;
using Logic.Modules;
//using Serilog;

namespace OCBlazor.Pages
{
    public class WebservicePageModel : ComponentBase/*, IDisposable*/
    {
        [Parameter]
        public ModuleType Module { get; set; }

        [Inject]
        NavigationManager _navigationManager { get; set; }

        [Inject]
        protected IMapper _mapper { get; set; }

      
        [Inject]
        protected LocalStorageService _localStorage { get; set; }

        [Inject]
        protected IWebservicesService _webservicesService { get; set; }

        [Inject]
        protected IAuthenticationServiceInterface _authenticationServiceInterface { get; set; }
        [Inject]
        Sotsera.Blazor.Toaster.IToaster _toaster { get; set; }
        [Inject]
        IJSRuntime _jsRuntime { get; set; }

        protected WebserviceExternal LastRunWebservice { get; set; }
        public bool ErrorVisible { get; set; } = false;
        public string MyError { get; set; }
        public List<WebserviceExternal> WebserviceExternalList { get; set; } = new List<WebserviceExternal>();
        protected override async void OnInitialized()
        {
            WebserviceExternalList = _webservicesService.Get();
        }
        protected void OnFileUploaded(FileUploadEventArgs args)
        {
            _toaster.Success("Het bestand is geüpload.", "OK");
        }
        public string GetUploadUrl(string url)
        {
            int Id = 2;//Acerta
            String path = _navigationManager.ToAbsoluteUri(url).AbsoluteUri + $"?module={ModuleType.WEBSERVICES_ACERTA_PERSONEEL_XML}&modulePrimaryKey={Id}";
            return path;
        }
        protected void OnUploadError(FileUploadErrorEventArgs e)
        {
            MyError = e.RequestInfo.ResponseText;
            ErrorVisible = true;
            InvokeAsync(StateHasChanged);
        }

        protected async Task RunWebservice(WebserviceExternal ws)
        {
            LastRunWebservice = ws;

            await _webservicesService.Run(ws);
            try
            {
                await _webservicesService.Run(ws);
            }
            catch(Exception ex) { 
            }
        }

        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
               
                 
            }
            base.OnAfterRender(firstRender);
        }
        protected override void OnParametersSet()
        {
          
            base.OnParametersSet();
        }
      
      
        

    }

}
 