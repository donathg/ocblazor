﻿using Microsoft.AspNetCore.Components;
using OCBlazor.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using OCBlazor.Models;
using Logic;
 
using Logic.DataBusinessObjects;
using Logic.Werkbonnenbeheer;
using Logic.DataBusinessObjects.Werkbonnenbeheer;

namespace OCBlazor.Pages.Werkbonnenbeheer
{
    public class WerkaanvraagPageModel : ComponentBase
    {
        [Inject]
        protected IWerkbonnenbeheerService _werkbonnenbeheerService { get; set; }


        public List<Werkbon> Werkbonnen { get; set; } = new List<Werkbon>();

        protected async override void OnInitialized()
        {
            Werkbonnen =  await _werkbonnenbeheerService.GetActiveWerkbonnen();
        }
    }
}
