﻿using DevExpress.DataAccess.ObjectBinding;
using Logic.Clienten;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Reports.Werkopdracht
{




    public static class WerkopdrachtDatasource
    {
       public static object GetDatasource(int id, IClientService clientService)
        {
            ObjectDataSource objectDataSource = new ObjectDataSource();
            objectDataSource.DataSource = typeof(ReportDataSourceWerkopdracht);
            objectDataSource.DataMember = "GetWerkopdracht";
            ReportDataSourceWerkopdracht.InjectServices(clientService);
            objectDataSource.Parameters.Add(new Parameter() { Name = "id", Type = typeof(int), Value = id });
            objectDataSource.Constructor = null;
            objectDataSource.RebuildResultSchema();
            return objectDataSource;
        }

        public static class ReportDataSourceWerkopdracht
        {
            static IClientService _clientService;
            public static void InjectServices(IClientService clientService)
            {
                _clientService = clientService;
            }
            public static List<WerkopdrachtReportItem> GetWerkopdracht(int id)
            {
                var items = new List<WerkopdrachtReportItem>();
                items.Add(new WerkopdrachtReportItem() { Id = id });
                return items;
            }
        }

    }
}
