﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.XtraReports.UI;
using Logic.Clienten;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Microsoft.AspNetCore.Hosting;
using OCBlazor.Reports;
using OCBlazor.Reports.Werkopdracht;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OCBlazor.Reports
{
    public class ReportStorageWebExtension1 : DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension
    {
        readonly string ReportDirectory;
        const string FileExtension = ".repx";
        private IClientService _clientService;
        public ReportStorageWebExtension1(IWebHostEnvironment env, IClientService clientService )
        {
            ReportDirectory = Path.Combine(env.ContentRootPath, "Reports");
            if (!Directory.Exists(ReportDirectory))
            {
                Directory.CreateDirectory(ReportDirectory);
            }
            _clientService = clientService;
        }

        private bool IsWithinReportsFolder(string url, string folder)
        {

            String reportName = GetReportNameFromUrl(url);

            var rootDirectory = new DirectoryInfo(folder);
            var fileInfo = new FileInfo(Path.Combine(folder, reportName));
            return fileInfo.Directory.FullName.ToLower().StartsWith(rootDirectory.FullName.ToLower());
        }

        public override bool CanSetData(string url)
        {
            // Determines whether it is possible to store a report by a given URL. 
            // For instance, make the CanSetData method return false for reports that should be read-only in your storage. 
            // This method is called only for valid URLs (for example, if the IsValidUrl method returns true) before the SetData method is called.

            return true;
        }

        public override bool IsValidUrl(string url)
        {
            // Determines whether the URL passed to the current Report Storage is valid. 
            // For instance, implement your own logic to prohibit URLs that contain white spaces or other special characters. 
            // This method is called before the CanSetData and GetData methods.
            String reportName = GetReportNameFromUrl(url);
            return Path.GetFileName(reportName) == reportName;
        }
        private string GetReportNameFromUrl(string url)
        {
            if (String.IsNullOrWhiteSpace(url))
                return string.Empty;
            else
            {
                if (url.Contains("?"))
                {
                    return url.Split("?")[0];
                }
                else return url;
            }
        }
       
        public override byte[] GetData(string url)
        {
            // Returns report layout data stored in a Report Storage using the specified URL. 
            // This method is called only for valid URLs after the IsValidUrl method is called.

            String reportName = url;
            String parameters = "";
            string paramValue = "";

            try
            {
                XtraReport report = null;
                if (url.Contains("?"))
                {
                    String[] splitted = url.Split("?");
                    reportName = splitted[0];
                    parameters = splitted[1];
                    paramValue = parameters.Split("=")[1];
                }
                using (MemoryStream ms = new MemoryStream(File.ReadAllBytes(Path.Combine(ReportDirectory, reportName + FileExtension))))
                {
                    switch (reportName)
                    {
                        case "WerkopdrachtReport":
                            report = new WerkopdrachtReport();
                            report.LoadLayoutFromXml(ms);// this needs to come before passing data to report;
                            report.DataSource = WerkopdrachtDatasource.GetDatasource(Int32.Parse(paramValue),this._clientService);
                            break;
                    }
                    if (report != null)
                    {
                         using (MemoryStream msOutput = new MemoryStream())
                        {
                            report.SaveLayoutToXml(msOutput);
                            return msOutput.ToArray();
                        }
                    }
                }


                if (report == null && Directory.EnumerateFiles(ReportDirectory).Select(Path.GetFileNameWithoutExtension).Contains(reportName))
                {
                    return File.ReadAllBytes(Path.Combine(ReportDirectory, reportName + FileExtension));
                }

            }
            catch (Exception ex)
            {
                throw new DevExpress.XtraReports.Web.ClientControls.FaultException("Could not get report data.", ex);
            }
            throw new DevExpress.XtraReports.Web.ClientControls.FaultException(string.Format("Could not find report '{0}'.", reportName));
        }

        public override Dictionary<string, string> GetUrls()
        {
            // Returns a dictionary of the existing report URLs and display names. 
            // This method is called when running the Report Designer, 
            // before the Open Report and Save Report dialogs are shown and after a new report is saved to storage.

            return Directory.GetFiles(ReportDirectory, "*" + FileExtension)
                                     .Select(Path.GetFileNameWithoutExtension)
                                     .ToDictionary<string, string>(x => x);
        }

        public override void SetData(XtraReport report, string url)
        {


            String reportName = GetReportNameFromUrl(url);
            // Stores the specified report to a Report Storage using the specified URL. 
            // This method is called only after the IsValidUrl and CanSetData methods are called.
            if (!IsWithinReportsFolder(reportName, ReportDirectory))
                throw new DevExpress.XtraReports.Web.ClientControls.FaultException("Invalid report name.");
            report.SaveLayoutToXml(Path.Combine(ReportDirectory, reportName + FileExtension));
        }

        public override string SetNewData(XtraReport report, string defaultUrl)
        {
            // Stores the specified report using a new URL. 
            // The IsValidUrl and CanSetData methods are never called before this method. 
            // You can validate and correct the specified URL directly in the SetNewData method implementation 
            // and return the resulting URL used to save a report in your storage.

            SetData(report, defaultUrl);
            return defaultUrl;
        }
    }
}
