﻿ 
using DevExpress.Blazor;
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Services
{
    public class CurrentSessionCache : ICurrentSessionCache
    {
        public event UserNameChangedDelegate UserNameChangedEventHandler;
        public event RefreshScreenEvent RefreshScreenEventHandler;

        public void OnRefreshScreenRequest(object source, string changedPropertyName, object propertyValue = null)
        {
            if (RefreshScreenEventHandler != null)
            {
                RefreshScreenEventArgs args = new RefreshScreenEventArgs() { Source = source, ChangedPropertyName = changedPropertyName, PropertyValue = propertyValue };
                RefreshScreenEventHandler(args);
            }

        }

        private void OnUserNameChanged(string username)
        {
            if (UserNameChangedEventHandler != null)
            {
                UserNameChangedEventHandler(username);
            }
        }

        private string _userName =   Guid.NewGuid().ToString();
        public string Username
        {
            get
            {
                return _userName;
            }
            set
            {
                if (_userName != value)
                {
                    _userName = value;
                    OnUserNameChanged(value);
                }
            }
        }

        #region project selection
        public string ProjectFilter { get; set; } = "";
       
        public DxTreeView TreeView { get; set; }
        #endregion

        #region LvTree selection
        public string ActualUaAuftragsnr { get; set; } = "";
        public DxTreeView LvTreeView { get; set; }
  
        #endregion


        public int StartUur { get; set; } = 8;
        public int AantalUur { get; set; } = 15;
        public int AantalDagen { get; set; } = 5;
 
    }

    public delegate void UserNameChangedDelegate(String name);
    public delegate void RefreshScreenEvent(RefreshScreenEventArgs refreshScreenEventArgs);

    public class RefreshScreenEventArgs : EventArgs
    { 
        public object Source { get; set; }
        public string ChangedPropertyName { get; set; }
        public object PropertyValue { get; set; }
    }
}
