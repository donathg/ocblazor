﻿using OCBlazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Services
{



    public class DragDropResize : IDragDropResize
    {

        private Action<DragDropResizeInfo> ActionDropped { get; set; }

        public DragDropResizeInfo Info { get; set; } = new DragDropResizeInfo();

        public void ExcecuteDrop(object destinationContent, bool ctrlKey, bool shiftKey)
        {
            this.Info.Content.DestinationData = destinationContent;
            this.Info.CtrlKey = ctrlKey;
            this.Info.ShiftKey = shiftKey;
            this.Info.Mode = DragDropResizeMode.dropped;
            BroadCastChange();
        }
        public void ChangeMode(DragDropResizeMode dragMode)
        {
        
            this.Info.Mode = dragMode;
            BroadCastChange();
        }
        public void SetSourceContent(object sourceContent)
        {
            this.Info.Content.SourceData = sourceContent;
        }
        public void SetDestinationContent(object destinationContent)
        {
            this.Info.Content.DestinationData = destinationContent;
        }

        public event BroadCastEventHandler BroadCastEvent;
        public void BroadCastChange()
        {
            if (BroadCastEvent != null)
                BroadCastEvent(this.Info);
        }

        public void Reset()
        {

            this.Info.Mode = DragDropResizeMode.none;
            BroadCastChange();
        }

    }
}
