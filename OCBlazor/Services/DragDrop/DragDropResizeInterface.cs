﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Services
{
    public enum DragDropResizeMode
    {
        none,
        dragging,
        startResizing,
        resizing,
        stoppedResizing,
        dropped,
        clicked,
        cancel
 

    }

    public class DragDropResizeContent
    {
    
        public Object SourceData { get; set; }
        public Object DestinationData { get; set; }

    }

    public delegate void BroadCastEventHandler(DragDropResizeInfo dragInfo);
    public class DragDropResizeInfo
    {

        public Object Tag { get; set; }
        public DragDropResizeContent Content { get; set; } = new DragDropResizeContent();
        private DragDropResizeMode _mode = DragDropResizeMode.none;
        public DragDropResizeMode Mode
        {
            get { return _mode; }
            set { if (_mode != value) {
                    
                    OldDragMode = _mode; 
                    _mode = value;

                    Debug.WriteLine($"Mode-change from :{OldDragMode} to:{Mode} ");
               } }
        } 
        public DragDropResizeMode OldDragMode
        {
            get;
            set;
        } = DragDropResizeMode.none;
        public bool CtrlKey { get; set; } = false;
        public bool ShiftKey { get; set; } = false;
        public DragDropResizeInfo()
        {
            Reset();
        }
        private void Reset()
        {
            this.Mode = this.OldDragMode = DragDropResizeMode.none;
            this.Content = null;
            this.Content = new DragDropResizeContent();
            this.CtrlKey = false;
            this.ShiftKey = false;
        }
    }
    public interface IDragDropResize
    {

        DragDropResizeInfo Info { get;set;}

        void ExcecuteDrop(object destinationContent, bool ctrlKey, bool shiftKey);
         
        void SetSourceContent(object sourceContent);
        void SetDestinationContent(object destinationContent);
        void ChangeMode (DragDropResizeMode dragMode);

        event BroadCastEventHandler BroadCastEvent;
        void BroadCastChange();
        void Reset();
    }
}
