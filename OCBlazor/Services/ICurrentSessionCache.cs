﻿using DevExpress.Blazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCBlazor.Services
{
    public interface ICurrentSessionCache
    {

        event UserNameChangedDelegate UserNameChangedEventHandler;
        event RefreshScreenEvent RefreshScreenEventHandler;
        void OnRefreshScreenRequest(object source, string changedPropertyName, object propertyValue = null);

        string Username { get; set; }

 


        int StartUur { get; set; }
        int AantalUur { get; set; }
        int AantalDagen { get; set; }
        
    }
}
