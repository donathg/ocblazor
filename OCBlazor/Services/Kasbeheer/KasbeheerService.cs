 
using DataBase.Global;
using OCBlazor.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
 
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using Logic.Kasbeheer;
using DataBase.Services;
using Common;

namespace OCBlazor.Services
{
    public class KasbeheerService : IKasbeheerServiceInterface
    {

        IDbConnection _DbConnection;
        IKasbeheerRepository _kasbeheerRepository;
        IUserService _userService;
        public KasbeheerService(IDbConnection connection, IKasbeheerRepository kasbeheerRepository , IUserService userService)
        {
            this._DbConnection = connection;
            _kasbeheerRepository = kasbeheerRepository;
             _userService = userService;
        }
        public async Task<List<KasbeheerTransactieClientInfo>> GetSelectedClientenForTransactie( int transactieId)
        {
            var clients = _kasbeheerRepository.GetSelectedClientenForTransactie(transactieId);
            return await Task.FromResult(clients);
        }  
        public async Task<List<KasbeheerTransactieClientInfo>> GetPreviousUsedClientsForLeefgroepTransactie( int transactieId)
        {
            var clients = _kasbeheerRepository.GetPreviousUsedClientsForLeefgroepTransactie(transactieId);
            return await Task.FromResult(clients);
        }
        public async Task<List<KasbeheerRekening>> GetRekeningen( int leefgroepId)
        {
            var rekeningen = _kasbeheerRepository.GetRekeningen(leefgroepId);
            return await Task.FromResult(rekeningen);
        }
        static List<KasbeheerTransactieType> transactieTypes = null;
        public async Task<List<KasbeheerTransactieType>> GetTransactieTypes()
        {
            if (transactieTypes == null || transactieTypes.Count == 0)
            {
                transactieTypes = _kasbeheerRepository.GetKasbeheerTransactieTypes();
            }
            return await Task.FromResult(transactieTypes);
        }
        static List<KasbeheerTransactieCodering> transactieCoderingen = null;
        public async Task<List<KasbeheerTransactieCodering>> GetTransactieCoderingen()
        {
            if (transactieCoderingen == null || transactieCoderingen.Count == 0)
            {
                transactieCoderingen = _kasbeheerRepository.GetKasbeheerTransactieCoderingen();
            }
            return await Task.FromResult(transactieCoderingen);
        }
        public async Task<List<KasbeheerTransactie>> GetTransacties( int leefgroepId)
        {
            var transacties = _kasbeheerRepository.GetTransacties(leefgroepId);
            return await Task.FromResult(transacties);
        }
        public async Task<KasbeheerTransactie> GetTransactie(int leefgroepId, int transactieId)
        {
        
            var transactie = _kasbeheerRepository.GetTransactie(transactieId);
            return await Task.FromResult(transactie);
        }

        public async Task<KasbeheerTransactie> SaveTransactie(int leefgroepId, KasbeheerTransactie transactie)
        {
          
            var savedTransactie = _kasbeheerRepository.SaveTransactie(leefgroepId, transactie);
            return await Task.FromResult(savedTransactie);
        }

        public async Task<bool> DeleteTransactie(int transactieId)
        {

            _kasbeheerRepository.DeleteTransactie(transactieId);
            return await Task.FromResult(true);
        }



        public async Task<List<Leefgroep>> GetLeefgroepRechten()
        {
#if DEBUG
            var leefgroepen = _kasbeheerRepository.GetLeefgroepTesting();
            return await Task.FromResult(leefgroepen);
#else
            var leefgroepen = _kasbeheerRepository.GetLeefgroepRechten(_userService.LoggedOnUser.UserId);
              return await Task.FromResult(leefgroepen); 

#endif 
        }

    }
}
