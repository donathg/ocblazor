﻿ 
using OCBlazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
 
using Database.DataAccessObjects;

namespace OCBlazor.Services
{
    public interface IKasbeheerServiceInterface
    {
        
        Task<List<KasbeheerRekening>> GetRekeningen( int leefgroepId);
        Task<List<KasbeheerTransactie>> GetTransacties(int leefgroepId);
        Task<KasbeheerTransactie> GetTransactie( int leefgroepId, int transactieId);
        Task<KasbeheerTransactie> SaveTransactie( int leefgroepId, KasbeheerTransactie transactie);

        Task<bool> DeleteTransactie( int transactieId);
        Task<List<KasbeheerTransactieType>> GetTransactieTypes();
        Task<List<KasbeheerTransactieCodering>> GetTransactieCoderingen();
        Task<List<KasbeheerTransactieClientInfo>> GetSelectedClientenForTransactie( int transactieId);

        Task<List<KasbeheerTransactieClientInfo>> GetPreviousUsedClientsForLeefgroepTransactie(int transactieId);

        Task<List<Leefgroep>> GetLeefgroepRechten();
    }
}
