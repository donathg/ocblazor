﻿ 
using Logic.DataBusinessObjects;
using Microsoft.JSInterop;
using OCBlazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace OCBlazor.Services
{
    public class LocalStorageService
    {

        public async Task<LogonInfoTM> ReadLogonInfoFromLocalStorage(IJSRuntime jsRuntime)
        {
            DotNetJavascriptCaller caller = new DotNetJavascriptCaller(jsRuntime);
           Object v =  await caller.ReadFromLocalStorage("LastLogonInfo");
            return (LogonInfoTM)v;

        }

        public void WriteLogonInfoToLocalStorage(IJSRuntime jsRuntime, LogonInfoTM logonIno)
        {
            DotNetJavascriptCaller caller = new DotNetJavascriptCaller(jsRuntime);
            caller.WriteToLocalStorage("LastLogonInfo", logonIno);

        }

    }
}
