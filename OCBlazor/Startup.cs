using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OCBlazor.PageModels;
using OCBlazor.Services;
using Sotsera.Blazor.Toaster.Core.Models;
using DevExpress.Blazor;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using OCBlazor.Models;
using Logic;
using Database.DataAccessObjects;
using Logic.DataBusinessObjects;
using Logic.Telefoonboek;
using DataBase.Services;
using Logic.Kasbeheer;
using Logic.Authentication;
using Logic.Clienten;
using Logic.LeefgroepNS;
using Logic.ArtikelNs;
using Logic.AankoopNs;
using DataBase.Repositories.Telefoonboek;
using DataBase.Repositories.Artikelen;
using DataBase.Repositories.Leefgroepen;
using DataBase.Repositories.Authentication;
using DataBase.Repositories.Clienten;
using DataBase.Repositories.Kasbeheer;
using DataBase.Repositories.Aankoop;
using Logic.Settings;
using Logic.Werkbonnenbeheer;
using Database.Repositories.Werkbonnenbeheer;
using Database.Sql;
using Logic.Cache;
using Logic.LocatieNs;
using DataBase.Repositories.LocatieNs;
using Logic.BijlageManagerServiceNS;
using Logic.EmailNS;
using OCBlazor.Reports;
using DevExpress.XtraReports.Web.Extensions;
using DevExpress.Blazor.Reporting;
using Database.Repositories.Kilometervergoeding;
using Logic.Kilometervergoeding;
using Logic.WagenNS;
using Logic.Kampen;
using Logic.Reports;
using Reporting.Werkbonnenbeheer;
using Reporting.Maaltijdlijsten;
using Logic.MultiSelectNS;
using Logic.GOG;
using Database.Repositories.GOG;
using Logic.Gebruikers;
using DataBase.Repositories.Gebruikers;
using DevExpress.DataAccess.Native;
using Logic.ExternalDashboard;
using System;
using Duende.AccessTokenManagement;
using Logic.Webservices;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Extensions.Options;
using OCBlazor.Pages.Webservices;
using Logic.DataBusinessObjects.Webservices;
using System.Net;
using Hangfire.Dashboard;
using System.Diagnostics.CodeAnalysis;
using DevExpress.XtraCharts;
using Logic.Maaltijdlijsten;
using Reporting.Maaltijdlijsten.Services;
using Logic.ExitFormulier;
using Database.Repositories.ExitFormulier;
using Logic.Prijzen;
using Database.Repositories.Prijzen;

namespace OCBlazor
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationSection appSettings = Configuration.GetSection("AppSettings");
            IConfigurationSection eCQare = Configuration.GetSection("eCQare");
            services.Configure<AppSettings>(appSettings);
            services.Configure<eCQare>(eCQare);
 

            var eCQareConfiguration = Configuration.GetSection("eCQare");

            services.AddSingleton(Configuration);

            services.AddDistributedMemoryCache();

            services.AddClientCredentialsTokenManagement();

            services.Configure<ClientCredentialsClient>("ecqare", eCQareConfiguration.GetSection("Authentication"));

            services.AddHttpClient<ClientData_eCQare_Client>(client =>
            {
                client.BaseAddress = client.BaseAddress = new Uri(eCQareConfiguration["APIBaseAddress"]);
            })
                .AddClientCredentialsTokenHandler("ecqare");

            services.AddRazorPages();

            services.AddServerSideBlazor().AddCircuitOptions(options => { options.DetailedErrors = true; });
            services.AddDevExpressBlazor(configure => configure.BootstrapVersion = BootstrapVersion.v5);
            services.AddDevExpressServerSideBlazorReportViewer();

            // Auto Mapper Configurations
            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddSingleton<ISQLManager, SQLManager>();
            services.AddSingleton<ICache, Cache>();
            services.AddTransient<IDbConnection, SqlConnection>();
            services.AddScoped<IAuthenticationServiceInterface, AuthenticationService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICurrentSessionCache, CurrentSessionCache>();
            services.AddScoped<IDragDropResize, DragDropResize>();
            services.AddScoped<IKasbeheerServiceInterface, KasbeheerService>();
            services.AddScoped<IKasbeheerRepository, KasbeheerRepository>();
            services.AddScoped<ITelefoonboekServiceInterface, TelefoonboekService>();
            services.AddScoped<ITelefoonboekRepository, TelefoonboekRepository>();
            services.AddScoped<ILeefgroepService, LeefgroepService>();
            services.AddScoped<ILeefgroepRepository, LeefgroepRepository>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IGebruikerService, GebruikerService>();
            services.AddScoped<IGebruikerRepository, GebruikerRepository>();
            services.AddScoped<LocalStorageService, LocalStorageService>();
            services.AddScoped<IArtikelenService, ArtikelenService>();
            services.AddScoped<IArtikelRepository, ArtikelRepository>();
            services.AddScoped<IAuthenticationRepository, AuthenticationRepository>();
            services.AddScoped<IAankoopService, AankoopService>();
            services.AddScoped<IAankoopRepository, AankoopRepository>();
            services.AddScoped<ILocatieService, LocatieService>();
            services.AddScoped<ILocatieRepository, LocatieRepository>();
            services.AddScoped<IWerkbonnenbeheerService, WerkbonnenbeheerService>();
            services.AddScoped<IWerkbonnenbeheerRepository, WerkbonnenbeheerRepository>();
            services.AddScoped<IBijlageManagerService, BijlageManagerService>();
            services.AddScoped<IBijlageManagerRepository, BijlageManagerRepository>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IKilometervergoedingRepository, KilometervergoedingRepository>();
            services.AddScoped<IKilometervergoedingService, KilometervergoedingService>();
            services.AddScoped<IKilometervergoedingTypesService, KilometervergoedingTypeService>();
            services.AddScoped<IKilometervergoedingTypesRepository, KilometervergoedingTypesRepository>();
            services.AddScoped<IWagenService, WagenService>();
            services.AddScoped<IWagenRepository, WagenRepository>();
            services.AddScoped<IKampenService, KampenService>();
            services.AddScoped<IKampenRepository, KampenRepository>();
            services.AddScoped<ReportStorageWebExtension, ReportStorageWebExtension1>();
            services.AddScoped<IWerkbonnenbeheerReportService, WerkbonnenbeheheerReportService>();
            services.AddScoped<IMaaltijdlijstenReportService, MaaltijdlijstenReportService>();
  
            //GOG formulier
            services.AddTransient<IGOGItemRepository, GOGItemRepository>();
            services.AddTransient<IGOGItemService, GOGItemService>();
            services.AddTransient<IMultiSelectRepository, MultiSelectRepository>();
            services.AddTransient<IMultiSelectService, MultiSelectService>();
            services.AddTransient<IGOGReportService, GOGReportService>();

            //exit formulier
            services.AddTransient<IExitFormRepository, ExitFormRepository>();
            services.AddTransient<IExitFromService, ExitFormService>();

            services.AddTransient<IExternalDashboardRepository, ExternalDashboardRepository>();
            services.AddTransient<IExternalDashboardService, ExternalDashboardService>();


            services.AddScoped<IWebservicesService, WebserviceService>();
            services.AddScoped<IWebserviceRepository, WebServicesRepository>();

            services.AddScoped<IOnkostennotaService, OnkostennotaService>();
            services.AddScoped<IOnkostennotaRepository, OnkostennotaRepository>();

            services.AddScoped<IPrijzenService, PrijzenService>();
            services.AddScoped<IPrijzenRepository, PrijzenRepository>();

            Database.InitializeMappings.Initialize();

            services.AddToaster(config =>
            {
                config.PositionClass = Defaults.Classes.Position.TopRight;
                config.PreventDuplicates = true;
                config.NewestOnTop = false;
            });

            var x = Configuration.GetSection("AppSettings").GetSection("ConnectionString");

            services.AddHangfire(configuration => 
                configuration
               .SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
               .UseSimpleAssemblyNameTypeSerializer()
               .UseRecommendedSerializerSettings()
               .UseSqlServerStorage(x.Value.ToString()));

            // Add the processing server as IHostedService
            services.AddHangfireServer();

            // Add framework services.
            services.AddMvc();

        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IWebHostEnvironment env, IWebservicesService webservices)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
          //  app.UseDevExpressServerSideBlazorReportViewer();
 
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
                endpoints.MapHangfireDashboard();
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions()
            {
                Authorization = new[]
                {
                    new HangFireAuthorizationFilter()
                }
            });

            AddJobs(webservices);
        }
        public  void AddJobs(IWebservicesService webservices)
        {
            /*
             * minute (0�59)
             * hour (0�23)
             * day of the month (1�31)
             * month (1�12)
             * day of the week (0�6) (Sunday to Saturday;7 is also Sunday on some systems)
             *
             * https://en.wikipedia.org/wiki/Cron#CRON_expression
             */

            // RecurringJob.RemoveIfExists("eCQare");
            RecurringJob.AddOrUpdate("eCQare", () => webservices.Run(
                new WebserviceExternal() { 
                    CustomerName = "eCQare", 
                    ApiName = "Alles", 
                    ApiLocation= "webservices.webservice",
                    ApiDescription= "Ophalen van Leefgroepen (Departements), Cli�nten & Maaltijden uit eCqare",
                    ApiContent="JSON" 
                }), "30 * * * *"); // elk uur om 30 (GMT)

            RecurringJob.AddOrUpdate("eCQare-maaltijdenHistoriek", () => webservices.Run(
                new WebserviceExternal() { 
                    CustomerName = "eCQare",
                    ApiName = "MaaltijdenHistoriek",
                    ApiLocation = "webservices.webservice",
                    ApiDescription = "Ophalen van Maaltijden uit eCqare",
                    ApiContent = "JSON"
                }), "15 * * * *"); // elk uur om 15

            RecurringJob.AddOrUpdate("Acerta-Personeel", () => webservices.Run(
                new WebserviceExternal() { 
                    CustomerName = "Acerta", 
                    ApiName = "Personeel" , 
                    FileLocation = @"\\s118-0011\AcertaConnect\ClaraFey_data_sftp" 
                }), "0 * * * *"); // elk uur om 0

            RecurringJob.AddOrUpdate("Blazor-Caches", () => webservices.Run(
                new WebserviceExternal()
                {
                    CustomerName = "Blazor",
                    ApiName = "Caches",
                    ApiLocation = "webservices.webservice",
                    ApiDescription = "Ophalen van Maaltijden uit eCqare"
                }), "0 1 * * *"); //elke dag om 1u (GMT)
        }
    }
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            //  var httpCtx = context.GetHttpContext();
            return true;// httpCtx.User.Identity.IsAuthenticated; //is always false

        }
    }
}