﻿window.myLib = {

    CloseWindow: function () {
        window.external.CloseWindow();
    },

    ReadStorage: function (key) {
        console.log("ReadStorage " + key);
        return localStorage.getItem(key);
    },
    WriteStorage: function (key, value) {
        localStorage.setItem(key, value);
        console.log("WriteStorage " + key);
    },
    Alert: function (message) {
        alert(message);
    },
    WriteCookie: function (name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    focusElement: function (element) {
        element.focus();
    },
    hardRefreshF5: function () {
        location.reload(true);
    },
    showPrompt: function (message) {
        return prompt(message, 'Type anything here');
    },
    setFullScreen: function () {

        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) {
            document.documentElement.msRequestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    },
    scrollToTop: function () {
        window.scrollTo(0, 0);
    },

    BlazorScrollToId : function (id) {
        const element = document.getElementById(id);
        if (element instanceof HTMLElement) {
            element.scrollIntoView({
                behavior: "smooth",
                block: "start",
                inline: "nearest"
            });
        }
    },

    OpenPDFByteArrayInNewTab: function (byteArray) {
        window.open("<iframe src='data:application/pdf;base64, " + encodeURI(byteArray) + "'></iframe>")
    },

    DownloadFile:function(url, token, parameters, fileName) {
        let anchor = document.createElement("a");
        document.body.appendChild(anchor);
        let file = url + "?" + parameters;
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token);

        fetch(file, { headers })
            .then(response => response.blob())
            .then(blobby => {
                let objectUrl = window.URL.createObjectURL(blobby);


            anchor.href = objectUrl;

            anchor.download = fileName;
            anchor.click();

            window.URL.revokeObjectURL(objectUrl);
            }
        );
    },

    saveAsFile:function(filename, bytesBase64) {
    if (navigator.msSaveBlob) {
        //Download document in Edge browser
        var data = window.atob(bytesBase64);
        var bytes = new Uint8Array(data.length);
        for (var i = 0; i < data.length; i++) {
            bytes[i] = data.charCodeAt(i);
        }
        var blob = new Blob([bytes.buffer], { type: "application/octet-stream" });
        navigator.msSaveBlob(blob, filename);
    }
    else {
        var link = document.createElement('a');
        link.download = filename;
        link.href = "data:application/octet-stream;base64," + bytesBase64;
        document.body.appendChild(link); // Needed for Firefox
        link.click();
        document.body.removeChild(link);
    }
}

}