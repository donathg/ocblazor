using System;
using System.Collections.Generic;
using System.Drawing;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.GOG;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Werkbonnenbeheer;

namespace Reporting.Maaltijdlijsten
{
    public partial class GOGReport : XtraReport
    {
        GogReport _reportData;
        public GOGReport(GogReport reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            List<GogReport> list = new List<GogReport>
            {
                reportData
            };
            objectDataSource3.DataSource = reportData;
            this.DataSource = objectDataSource3;          
        }
    }
}
 