﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Logic.DataBusinessObjects;
using Database.DataAccessObjects;
using System.Data;
using Logic.Cache;
using Microsoft.Extensions.Caching.Memory;
using Logic.Reports;
 
using Logic.Werkbonnenbeheer;
using System.Collections.ObjectModel;
using Logic.GOG;
using Logic.DataBusinessObjects.GOG;
using Logic.DataBusinessObjects.MultiSelectNS;
 

namespace Reporting.Maaltijdlijsten
{
    public class GOGReportService : IGOGReportService
    {
        //private readonly GOGItemService _gogItemService;

        public GOGReportService()
        {
          //    _gogItemService = gogItemService;


        }
        public GogReport GetGogReport(GOGItem gogItem, List<MultiSelect> AllMultiSelect)
        {
            GogReport gogReport = new GogReport();

            FillResults(gogItem, AllMultiSelect);

            gogReport.Id = gogItem.Id;  
            gogReport.Leefgroep = gogItem.Leefgroep;
            gogReport.BetrokkenClientenString = gogItem.BetrokkenBewonersString;
            gogReport.BetrokkenBegeleidersString = gogItem.BetrokkenBegeleidersString;

            foreach (MultiSelect comp in AllMultiSelect.Where(x => !string.IsNullOrEmpty(x.GetResultFormatted())).OrderBy(x=> x.OrderNumReport))
            {
                GogReportItem reportItem = new GogReportItem
                {
                    Title = comp.Title,
                    Result = comp.GetResultFormatted()
                };
                gogReport.Items.Add(reportItem);
            }

            return gogReport;
        }

        private static void FillResults(GOGItem gogItem, List<MultiSelect> AllMultiSelect)
        {
            foreach (MultiSelect component in AllMultiSelect)
            {
                component.Items.ForEach((MultiSelectItem item) =>
                {
                    if (gogItem.SelectedMultiSelectItemsResults.FirstOrDefault(x => x.MultiSelectItemId == item.Id) is MultiSelectItemResult res)
                    {
                        item.Result = res;
                    }
                });
            }
        }




        //public MaaltijdlijstenDagelijkseLeveringReportData GetXtraReport(DateTime dayDate, string categorie)
        //{



        //      LeefgroepDagelijksRows = _maaltijdlijstenRepository.GetDagelijkseLevering(dayDate.Date);
        //      LoadLeefgroepenGroepering = _maaltijdlijstenRepository.LoadLeefgroepenGroepering(categorie);



        //     LeefgroepDagelijksRowResultList = new List<LeefgroepDagelijksRowResult>();
        //    LeefgroepDagelijksRowResultList2 = new List<MaaltijdlijstenDagelijkseLeveringRow>();



        //    //LoadLeefgroepenGroepering();
        //    //LoadMaaltijden(day);
        //    Parse(LeefgroepDagelijksRowResultList, "ONTBIJT", "");
        //    Parse(LeefgroepDagelijksRowResultList, "MIDDAG", "KOUD");
        //    Parse(LeefgroepDagelijksRowResultList, "MIDDAG", "WARM");
        //    Parse(LeefgroepDagelijksRowResultList, "AVOND", "KOUD");
        //    Parse(LeefgroepDagelijksRowResultList, "AVOND", "WARM");
        //    TransformData();

        //    MaaltijdlijstenDagelijkseLeveringReportData result =  new MaaltijdlijstenDagelijkseLeveringReportData();
        //    result.Day = dayDate;
        //    if (categorie=="WEEKEND")
        //    result.ReportTitle = "Leefgroepen - Daglijst - Weekend & Vakantie";
        //    else result.ReportTitle = "Leefgroepen - Daglijst";
        //    result.Categorie = categorie;
        //    result.Rows = LeefgroepDagelijksRowResultList2;
        //    return result;
        //}


        //private void TransformData()
        //{
        //    LeefgroepDagelijksRowResultList2 = new List<MaaltijdlijstenDagelijkseLeveringRow>();
        //    foreach (String leefgroep in this.LeefgroepDagelijksRowResultList.OrderBy(x=>x.Sortorder).Select(x => x.LeefgroepCombinedString).Distinct())
        //    {

        //        MaaltijdlijstenDagelijkseLeveringRow dest = new MaaltijdlijstenDagelijkseLeveringRow();
        //        LeefgroepDagelijksRowResult ONTBIJT = LeefgroepDagelijksRowResultList.Where(x => x.LeefgroepCombinedString == leefgroep && x.DagDeel == "ONTBIJT").FirstOrDefault();
        //        LeefgroepDagelijksRowResult MIDDAGWARM = LeefgroepDagelijksRowResultList.Where(x => x.LeefgroepCombinedString == leefgroep && x.DagDeel == "MIDDAG" && x.KoudWarm == "WARM").FirstOrDefault();
        //        LeefgroepDagelijksRowResult MIDDAGKOUD = LeefgroepDagelijksRowResultList.Where(x => x.LeefgroepCombinedString == leefgroep && x.DagDeel == "MIDDAG" && x.KoudWarm == "KOUD").FirstOrDefault();
        //        LeefgroepDagelijksRowResult AVONDKOUD = LeefgroepDagelijksRowResultList.Where(x => x.LeefgroepCombinedString == leefgroep && x.DagDeel == "AVOND" && x.KoudWarm == "KOUD").FirstOrDefault();
        //        LeefgroepDagelijksRowResult AVONDWARM = LeefgroepDagelijksRowResultList.Where(x => x.LeefgroepCombinedString == leefgroep && x.DagDeel == "AVOND" && x.KoudWarm == "WARM").FirstOrDefault();


        //            dest.LeefgroepCombinedString = leefgroep;
        //        String groepNaam = LeefgroepDagelijksRowResultList.Where(x => x.LeefgroepCombinedString == leefgroep).FirstOrDefault().GroepNaam;
        //        if (!String.IsNullOrWhiteSpace(groepNaam))
        //            dest.LeefgroepCombinedString = groepNaam;
        //        dest.Sortorder = LeefgroepDagelijksRowResultList.Where(x => x.LeefgroepCombinedString == leefgroep).FirstOrDefault().Sortorder;
        //        dest.Ontbijt = "0";
        //        dest.MiddagKoud = "0";
        //        dest.MiddagWarm = "0";
        //        dest.AvondKoud = "0";
        //        dest.AvondWarm = "0";


        //        if (ONTBIJT != null)
        //            dest.Ontbijt = ONTBIJT.AantalMaaltijden.ToString();
        //        if (MIDDAGKOUD != null)
        //            dest.MiddagKoud = MIDDAGKOUD.AantalMaaltijden.ToString();
        //        if (MIDDAGWARM != null)
        //            dest.MiddagWarm = /*MIDDAGWARM.AantalMaaltijden.ToString() + " (" +*/ MIDDAGWARM.DieetEnBereidingString;/* + " ) ";*/
        //        if (AVONDKOUD != null)
        //            dest.AvondKoud = AVONDKOUD.AantalMaaltijden.ToString();
        //        if (AVONDWARM != null)
        //            dest.AvondWarm = /*AVONDWARM.AantalMaaltijden.ToString() + " (" +*/ AVONDWARM.DieetEnBereidingString;/*+ " ) ";*/

        //        LeefgroepDagelijksRowResultList2.Add(dest);

        //    }

        //}


        //private void Parse(List<LeefgroepDagelijksRowResult> leefgroepDagelijksRowResult, string dagDeel, String koudWarm)
        //{

        //    leefgroepDagelijksRowResult = new List<LeefgroepDagelijksRowResult>();

        //    foreach (LeefgroepGroeperingRow r in LoadLeefgroepenGroepering)
        //    {
        //        LeefgroepDagelijksRowResult result = new LeefgroepDagelijksRowResult();


        //        List<LeefgroepDagelijksRow> dagelijkseRowsVoorLeefgroepGroep = LeefgroepDagelijksRows.Where(x => r.LeefgroepenSamenIdList.Contains(x.LeefgroepId) && x.DagDeel == dagDeel && x.KoudWarm == koudWarm).ToList();
        //        if (dagelijkseRowsVoorLeefgroepGroep.Count == 0)
        //            continue;


        //        LeefgroepDagelijksRowResultList.Add(result);
        //        result.DagDeel = dagDeel;
        //        result.KoudWarm = koudWarm;
        //        result.LeefgroepCombinedString = r.LeefgroepCombinedString;
        //        result.GroepNaam = r.GroepNaam;
        //        result.Sortorder = r.Sortorder;
        //        foreach (LeefgroepDagelijksRow d in dagelijkseRowsVoorLeefgroepGroep)
        //        {

        //            result.AantalMaaltijden++;

        //            String[] parts = d.DieetEnBereiding.Split(',');
        //            foreach (String part in parts.Where(x => x.Trim().Length > 0))
        //            {
        //                String partTrimmed = part.Trim();
        //                if (!result.DieetEnBereiding.ContainsKey(partTrimmed))
        //                    result.DieetEnBereiding.Add(partTrimmed, 0);

        //                result.DieetEnBereiding[partTrimmed]++;

        //                result.AantalMaaltijdenUitzondering++;

        //            }

        //        }

        //    }
        //}











        //public class LeefgroepDagelijksRowResult
        //{
        //    public int Sortorder { get; set; }
        //    public string LeefgroepCombinedString
        //    { get; set; } = "";


        //    public string GroepNaam
        //    { get; set; } = "";
        //    public string DagDeel { get; set; }

        //    public int AantalMaaltijden { get; set; } = 0;
        //    public int AantalMaaltijdenUitzondering { get; set; } = 0;
        //    public string KoudWarm { get; set; }


        //    public Dictionary<string, int> DieetEnBereiding { get; set; } = new Dictionary<string, int>();


        //    public string DieetEnBereidingString
        //    {
        //        get
        //        {
        //            if (DagDeel == "ONTBIJT" || KoudWarm == "KOUD")
        //                return "";

        //            String text = "";
        //            foreach (KeyValuePair<string, int> kvp in DieetEnBereiding)
        //            {

        //                if (text == "")
        //                    text = kvp.Value + " " + kvp.Key;
        //                else
        //                    text = text + " + " + kvp.Value + " " + kvp.Key;


        //            }
        //            if (AantalMaaltijdenUitzondering > 0)
        //                text = (AantalMaaltijden - AantalMaaltijdenUitzondering) + " + " + text;
        //            else
        //                text = AantalMaaltijden + "";



        //            return text;
        //        }
        //    }
        //}
    }
}
