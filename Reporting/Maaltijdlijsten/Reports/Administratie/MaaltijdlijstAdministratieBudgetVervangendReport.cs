﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Maaltijdlijsten.Administratie;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Reporting.Maaltijdlijsten.Reports.Administratie
{
    public partial class MaaltijdlijstAdministratieBudgetVervangendReport : DevExpress.XtraReports.UI.XtraReport
    {
        MaaltijdenAdministratieBudgetVervangend _reportData;
        public MaaltijdlijstAdministratieBudgetVervangendReport(MaaltijdenAdministratieBudgetVervangend reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            objectDataSource2.DataSource = reportData;
            this.DataSource = objectDataSource2;
        }
        public MaaltijdlijstAdministratieBudgetVervangendReport()
        {
            InitializeComponent();
        }
    }
}
