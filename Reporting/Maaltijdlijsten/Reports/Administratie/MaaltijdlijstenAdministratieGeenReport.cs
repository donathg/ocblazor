﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Maaltijdlijsten.Administratie;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Reporting.Maaltijdlijsten.Reports.Administratie
{
    public partial class MaaltijdlijstenAdministratieGeenReport : DevExpress.XtraReports.UI.XtraReport
    {
        MaaltijdenAdministratieGeenLevering _reportData;
        public MaaltijdlijstenAdministratieGeenReport(MaaltijdenAdministratieGeenLevering reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            objectDataSource1.DataSource = reportData;
            this.DataSource = objectDataSource1;
        }
        public MaaltijdlijstenAdministratieGeenReport()
        {
            InitializeComponent();
        }
    }
}
