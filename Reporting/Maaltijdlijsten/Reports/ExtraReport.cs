﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Reporting.Maaltijdlijsten
{
    public partial class ExtraReport : DevExpress.XtraReports.UI.XtraReport
    {
        MaaltijdlijstenExtra _reportData;
        public ExtraReport(MaaltijdlijstenExtra reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            objectDataSource2.DataSource = reportData;
            this.DataSource = objectDataSource2;
        }
        public ExtraReport()
        {
            InitializeComponent();
        }

    }
}
