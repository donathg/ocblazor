﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Reporting.Maaltijdlijsten
{
    public partial class LeefgroepenDaglijstReport : DevExpress.XtraReports.UI.XtraReport
    {
        MaaltijdlijstenLeefgroepenDaglijst _reportData;
        public LeefgroepenDaglijstReport(MaaltijdlijstenLeefgroepenDaglijst reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            objectDataSource2.DataSource = reportData;
            this.DataSource = objectDataSource2;
        }
        public LeefgroepenDaglijstReport()
        {
            InitializeComponent();
        }

    }
}
