﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Reporting.Maaltijdlijsten
{
    public partial class LeefgroepenDaglijstWeekendReport : DevExpress.XtraReports.UI.XtraReport
    {
        MaaltijdlijstenLeefgroepenDaglijstWeekend _reportData;
        public LeefgroepenDaglijstWeekendReport(MaaltijdlijstenLeefgroepenDaglijstWeekend reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            objectDataSource3.DataSource = reportData;
            this.DataSource = objectDataSource3;
        }
        public LeefgroepenDaglijstWeekendReport()
        {
            InitializeComponent();
        }

    }
}
