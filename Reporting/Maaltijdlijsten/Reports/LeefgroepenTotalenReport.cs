﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Reporting.Maaltijdlijsten
{
    public partial class LeefgroepenTotalenReport : DevExpress.XtraReports.UI.XtraReport
    {
        MaaltijdlijstenLeefgroepenTotalen _reportData;
        public LeefgroepenTotalenReport(MaaltijdlijstenLeefgroepenTotalen reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            objectDataSource1.DataSource = reportData;
            this.DataSource = objectDataSource1;
        }
        public LeefgroepenTotalenReport()
        {
            InitializeComponent();
        }

    }
}
