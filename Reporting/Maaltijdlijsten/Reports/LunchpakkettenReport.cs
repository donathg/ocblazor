﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Reporting.Maaltijdlijsten
{
    public partial class LunchpakkettenReport : DevExpress.XtraReports.UI.XtraReport
    {
        MaaltijdlijstenLunchpakketten _reportData;
        public LunchpakkettenReport(MaaltijdlijstenLunchpakketten reportData)
        {
            this._reportData = reportData;
            InitializeComponent();
            objectDataSource2.DataSource = reportData;
            this.DataSource = objectDataSource2;
        }
        public LunchpakkettenReport()
        {
            InitializeComponent();
        }

    }
}
