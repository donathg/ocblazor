﻿using Logic.DataBusinessObjects.Maaltijdlijsten.Administratie;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraReports;
using Logic.Maaltijdlijsten;
using Logic.Prijzen;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService
    {

        public MaaltijdenAdministratieBudgetVervangend GetMaaltijdenBudgetVervangendData(List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem reportItem, DateTime startDatum, DateTime eindDatum,
            List<BudgetVervangdeMaaltijdenByLeefgroep> ecqareAlivioMealplanFiles, IPrijzenService _prijzenService)
        {
            MaaltijdenAdministratieBudgetVervangend reportData = new MaaltijdenAdministratieBudgetVervangend
            {
                ReportTitle = reportItem.ReportTitle + " " + ecqareAlivioMealplanFiles.FirstOrDefault().MealMonth,
                DateFrom = startDatum,
                DateTo = eindDatum
            };

            List<MaaltijdenAdministratieBudgetVervangendRow> daglijstRows = new List<MaaltijdenAdministratieBudgetVervangendRow>();
            foreach (BudgetVervangdeMaaltijdenByLeefgroep vervangendeMaaltijd in ecqareAlivioMealplanFiles.Where(x => x.Aantal != 0).OrderBy(y => y.LeefgroepNaam))
            {
                daglijstRows.Add(new MaaltijdenAdministratieBudgetVervangendRow()
                {
                    LeefgroepNaam = vervangendeMaaltijd.LeefgroepNaam,
                    Type = "Kookactiviteit " + vervangendeMaaltijd.Ondersteuningsvorm,
                    Aantal = vervangendeMaaltijd.Aantal,
                    Prijs = _prijzenService.GetBedragByModuleCode_Datum_Subtypes("MAALTIJDLIJS", eindDatum, "E", vervangendeMaaltijd.Ondersteuningsvorm),
                    Boeking = ""
                });
            }
            reportData.Rows = daglijstRows;

            return reportData;
        }
    }
}
