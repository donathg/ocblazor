﻿using Logic.DataBusinessObjects.Maaltijdlijsten.Administratie;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.DataBusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic.Maaltijdlijsten;
using Logic.Prijzen;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService
    {
        public MaaltijdenAdministratieGeenLevering GetMaaltijdenGeenLevering(List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem reportItem, DateTime startDatum, DateTime eindDatum,
            List<BudgetVervangdeMaaltijdenByLeefgroep> ecqareAlivioMealplanFiles, IPrijzenService _prijzenService)
        {
            MaaltijdenAdministratieGeenLevering reportData = new MaaltijdenAdministratieGeenLevering
            {
                ReportTitle = reportItem.ReportTitle + " " + ecqareAlivioMealplanFiles.FirstOrDefault().MealMonth,
                DateFrom = startDatum,
                DateTo = eindDatum
            };

            List<MaaltijdenAdministratieGeenLeveringRow> ReportRows = new List<MaaltijdenAdministratieGeenLeveringRow>();
            foreach (BudgetVervangdeMaaltijdenByLeefgroep vervangendeMaaltijd in ecqareAlivioMealplanFiles.OrderBy(x => x.LeefgroepNaam))
            {
                // prepare objects to be used
                MaaltijdenAdministratieGeenLeveringRow tempRow; 

                // add line specific data
                if (vervangendeMaaltijd.AantalOntbijt != 0)
                {
                    tempRow = new MaaltijdenAdministratieGeenLeveringRow
                    {
                        LeefgroepId = vervangendeMaaltijd.LeefgroepObject.Id,
                        LeefgroepNaam = vervangendeMaaltijd.LeefgroepNaam,
                        Boeking = string.Empty,
                        Type = "Ontbijt PVF",
                        Aantal = vervangendeMaaltijd.AantalOntbijt,
                        Prijs = _prijzenService.GetBedragByModuleCode_Datum_Subtypes("MAALTIJDLIJS", vervangendeMaaltijd.Mealdate.Value, "D", vervangendeMaaltijd.Ondersteuningsvorm)
                    };

                    // add totaallijn to totaalLeefgroep on end of operation
                    tempRow.TotaalLeefgroep += tempRow.TotaalLijn;

                    // add tempRow to list
                    ReportRows.Add(tempRow);
                }
                if (vervangendeMaaltijd.AantalKoud != 0)
                {
                    tempRow = new MaaltijdenAdministratieGeenLeveringRow();
                    MaaltijdenAdministratieGeenLeveringRow foundRow = ReportRows.Where(x => x.LeefgroepId == vervangendeMaaltijd.LeefgroepObject.Id && 
                                                                                            x.TotaalLeefgroep != decimal.Zero).FirstOrDefault();

                    // remove naam from following lines + add totaalleefgroep to new line and remove from previous one if found
                    if (foundRow == null)
                    {
                        tempRow.LeefgroepId = vervangendeMaaltijd.LeefgroepObject.Id;
                        tempRow.LeefgroepNaam = vervangendeMaaltijd.LeefgroepNaam;
                        tempRow.Boeking = string.Empty;
                    }
                    else
                    {
                        tempRow.LeefgroepId = vervangendeMaaltijd.LeefgroepObject.Id;
                        tempRow.LeefgroepNaam = string.Empty;
                        tempRow.Boeking = string.Empty;

                        tempRow.TotaalLeefgroep = foundRow.TotaalLeefgroep; // get totaalLeefgroep from previous line
                        foundRow.TotaalLeefgroep = decimal.Zero; // set previous row's totaalLeefgroep to 0. zeroes are removed on display
                    }

                    tempRow.Type = "Koud PVF";
                    tempRow.Aantal = vervangendeMaaltijd.AantalKoud;
                    tempRow.Prijs = _prijzenService.GetBedragByModuleCode_Datum_Subtypes("MAALTIJDLIJS", vervangendeMaaltijd.Mealdate.Value, "B", vervangendeMaaltijd.Ondersteuningsvorm);

                    // add totaallijn to totaalLeefgroep on end of operation
                    tempRow.TotaalLeefgroep += tempRow.TotaalLijn;

                    // add tempRow to list
                    ReportRows.Add(tempRow);
                }
                if (vervangendeMaaltijd.AantalWarm != 0)
                {
                    tempRow = new MaaltijdenAdministratieGeenLeveringRow();
                    MaaltijdenAdministratieGeenLeveringRow foundRow = ReportRows.Where(x => x.LeefgroepId == vervangendeMaaltijd.LeefgroepObject.Id &&
                                                                                            x.TotaalLeefgroep != decimal.Zero).FirstOrDefault();

                    // remove naam from following lines + add totaalleefgroep to new line and remove from previous one if found
                    if (foundRow == null)
                    {
                        tempRow.LeefgroepId = vervangendeMaaltijd.LeefgroepObject.Id;
                        tempRow.LeefgroepNaam = vervangendeMaaltijd.LeefgroepNaam;
                        tempRow.Boeking = string.Empty;
                    }
                    else
                    {
                        tempRow.LeefgroepId = vervangendeMaaltijd.LeefgroepObject.Id;
                        tempRow.LeefgroepNaam = string.Empty;
                        tempRow.Boeking = string.Empty;

                        tempRow.TotaalLeefgroep = foundRow.TotaalLeefgroep; // get totaalLeefgroep from previous line
                        foundRow.TotaalLeefgroep = decimal.Zero; // set previous row's totaalLeefgroep to 0. zeroes are removed on display
                    }

                    tempRow.Type = "Warm PVF";
                    tempRow.Aantal = vervangendeMaaltijd.AantalWarm;
                    tempRow.Prijs = _prijzenService.GetBedragByModuleCode_Datum_Subtypes("MAALTIJDLIJS", vervangendeMaaltijd.Mealdate.Value, "A", vervangendeMaaltijd.Ondersteuningsvorm);

                    // add totaallijn to totaalLeefgroep on end of operation
                    tempRow.TotaalLeefgroep += tempRow.TotaalLijn;

                    // add tempRow to list
                    ReportRows.Add(tempRow);
                }
            }

            reportData.Rows = ReportRows;

            return reportData;
        }
    }
}
