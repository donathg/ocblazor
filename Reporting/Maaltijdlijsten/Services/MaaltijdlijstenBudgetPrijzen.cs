﻿using DataBase.Global;
using DevExpress.Blazor.Internal.Services;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using Logic.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService
    {
        private readonly AppSettings _appSettings;

        public decimal GetMaaltijdPrijs(DateTime day, string ondersteuningsvorm, string maaltijdkeuze)
        {
            decimal prijs = 0;

            using (DatabaseCentral db = new DatabaseCentral(_appSettings.ConnectionString))
            {
                try
                {
                    string sql = @" SELECT prijs
                                    FROM maaltijdlijsten.budgetprijs
                                    where (@date >= startdatum and @date <= Einddatum)
	                                    and maaltijdkeuze = @maaltijdkeuze
	                                    and ondersteuningsvorm = @ondersteuningsvorm";

                    List<DbParam> parameters = new List<DbParam>()
                    {
                        new DbParam() { Name = "@date", Value = day , DbType = DbTypes.db_datetime },
                        new DbParam() { Name = "@maaltijdkeuze", Value = maaltijdkeuze, DbType = DbTypes.db_string },
                        new DbParam() { Name = "@ondersteuningsvorm", Value = ondersteuningsvorm, DbType = DbTypes.db_string }
                    };

                    DbCommand command = new DbCommand(DbCommandType.crud, sql, parameters.ToArray());


                    prijs = (decimal) db.ExcecuteScalar(command);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return prijs;
           
        }
    }
}
