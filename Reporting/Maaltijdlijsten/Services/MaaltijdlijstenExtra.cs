﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService 
    {

        public MaaltijdlijstenExtra GetMaaltijdlijstenExtra(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList)
        {
            MaaltijdlijstenExtra reportData = new MaaltijdlijstenExtra
            {
                ReportTitle = report.ReportTitle + " " + dateFrom.ToString("dd/MM/yyyy"),
                DateFrom = dateFrom
            };

            List<MaaltijdlijstenExtraRow> daglijstRows = new List<MaaltijdlijstenExtraRow>();
            foreach (EcqareAlivioMealplanByLeefgroep mealplan in mealPlanList)
            {
                daglijstRows.Add(new MaaltijdlijstenExtraRow()
                {                    
                    Leefgroep = mealplan.LeefgroepName,
                    Extras = mealplan.ExtrasString
                });
            }
            reportData.Rows = daglijstRows;

            return reportData;
        }
    }
}
