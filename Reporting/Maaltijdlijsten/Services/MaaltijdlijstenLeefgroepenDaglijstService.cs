﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService 
    {

        public MaaltijdlijstenLeefgroepenDaglijst GetMaaltijdlijstenLeefgroepenDaglijstReportData(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList)
        {
            MaaltijdlijstenLeefgroepenDaglijst reportData = new MaaltijdlijstenLeefgroepenDaglijst
            {
                ReportTitle = report.ReportTitle + "" + dateFrom.ToString("dd/MM/yyyy"),
                DateFrom = dateFrom    
            };

            List<MaaltijdlijstenLeefgroepenDaglijstRow> daglijstRows = new List<MaaltijdlijstenLeefgroepenDaglijstRow>();
            foreach (EcqareAlivioMealplanByLeefgroep mealplan in mealPlanList)
            {
                daglijstRows.Add(new MaaltijdlijstenLeefgroepenDaglijstRow()
                {
                    Leefgroep = mealplan.LeefgroepName,
                    Ontbijt = mealplan.AantalOntbijt,
                    MiddagKoud = mealplan.AantalMiddagKoud,
                    MiddagWarm = mealplan.BijzonderhedenMiddagWarmString,
                    AvondKoud = mealplan.AantalAvondKoud,
                    AvondWarm = mealplan.BijzonderhedenAvondWarmString
                });
            }
            reportData.Rows = daglijstRows;

            return reportData;
        }

    }
}
