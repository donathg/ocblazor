﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService 
    {
        public MaaltijdlijstenLeefgroepenDaglijstWeekend GetMaaltijdlijstenLeefgroepenDaglijstWeekendReportData(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList)
        {
            MaaltijdlijstenLeefgroepenDaglijstWeekend reportData = new MaaltijdlijstenLeefgroepenDaglijstWeekend
            {
                ReportTitle = report.ReportTitle + "" + dateFrom.ToString("dd/MM/yyyy"),
                DateFrom = dateFrom
            };

            List<MaaltijdlijstenLeefgroepenDaglijstWeekendRow> daglijstRows = new List<MaaltijdlijstenLeefgroepenDaglijstWeekendRow>();
            foreach (EcqareAlivioMealplanByLeefgroep mealplan in mealPlanList)
            {
                daglijstRows.Add(new MaaltijdlijstenLeefgroepenDaglijstWeekendRow()
                {
                    Leefgroep = mealplan.LeefgroepName,
                    Ontbijt = mealplan.AantalOntbijt,
                    MiddagKoud = mealplan.AantalMiddagKoud,
                    MiddagWarm = mealplan.BijzonderhedenMiddagWarmString,
                    AvondKoud = mealplan.AantalAvondKoud,
                    AvondWarm = mealplan.BijzonderhedenAvondWarmString
                });
            }
            reportData.Rows = daglijstRows;

            return reportData;
        }

    }
}
