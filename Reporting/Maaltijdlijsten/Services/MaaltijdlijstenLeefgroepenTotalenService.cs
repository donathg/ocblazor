﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService 
    {

        public MaaltijdlijstenLeefgroepenTotalen GetLeefgroepenTotalenReportData(List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanFile> mealPlanList)
        {
            MaaltijdlijstenLeefgroepenTotalen reportData = new MaaltijdlijstenLeefgroepenTotalen();
            reportData.ReportTitle = report.ReportTitle + "" + dateFrom.ToString("dd/MM/yyyy");
            reportData.DateFrom = dateFrom;
            reportData.Convert(mealPlanList, leefgroepen.Where(x => !x.MaaltijdlijstenVacuumVerpakking).ToList());

            return reportData;
        }

    }
}
