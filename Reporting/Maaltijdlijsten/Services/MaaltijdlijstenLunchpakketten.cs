﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService 
    {

        public MaaltijdlijstenLunchpakketten GetMaaltijdlijstenLunchpakketten(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList)
        {
            MaaltijdlijstenLunchpakketten reportData = new MaaltijdlijstenLunchpakketten
            {
                ReportTitle = report.ReportTitle + "" + dateFrom.ToString("dd/MM/yyyy"),
                DateFrom = dateFrom
            };

            List<MaaltijdlijstenLunchpakkettenRow> daglijstRows = new List<MaaltijdlijstenLunchpakkettenRow>();
            foreach (EcqareAlivioMealplanByLeefgroep mealplan in mealPlanList)
            {
                daglijstRows.Add(new MaaltijdlijstenLunchpakkettenRow()
                {
                    Leefgroep = mealplan.LeefgroepName,
                    AantalLunchpakketten = mealplan.Lunchpakketten
                });
            }
            reportData.Rows = daglijstRows;

            return reportData;
        }

    }
}
