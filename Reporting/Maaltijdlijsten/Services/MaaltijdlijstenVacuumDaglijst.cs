﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService 
    {

        public MaaltijdlijstenVacuumDaglijst GetMaaltijdlijstenVacuumDaglijst(MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanByLeefgroep> mealPlanList)
        {
            MaaltijdlijstenVacuumDaglijst reportData = new MaaltijdlijstenVacuumDaglijst
            {
                ReportTitle = report.ReportTitle,
                DateFrom = dateFrom
            };

            List<MaaltijdlijstenVacuumDaglijstRow> rowList = new List<MaaltijdlijstenVacuumDaglijstRow>();

            foreach (EcqareAlivioMealplanByLeefgroep mealplan in mealPlanList)
            {
                rowList.Add(new MaaltijdlijstenVacuumDaglijstRow()
                {
                    DayDate = mealplan.MealDay,
                    Leefgroep = mealplan.LeefgroepName,
                    Ontbijt = mealplan.AantalOntbijt,
                    Warm = mealplan.BijzonderhedenWarmString,
                    Koud = mealplan.BijzonderhedenKoudString
                });
            }
            reportData.Rows = rowList;

            return reportData;
        }

    }
}
