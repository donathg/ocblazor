﻿using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Maaltijdlijsten;
using Logic.DataBusinessObjects.Webservices.eCQare;
using Logic.Maaltijdlijsten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.Maaltijdlijsten.Services
{
    public partial class MaaltijdlijstenReportService 
    {

        public MaaltijdlijstenVacuumTotalen GetMaaltijdlijstenVacuumTotalen (List<Leefgroep> leefgroepen, MaaltijdLijstenReportItem report, DateTime dateFrom, DateTime dateTo, List<EcqareAlivioMealplanFile> mealPlanList)
        {
            MaaltijdlijstenVacuumTotalen reportData = new MaaltijdlijstenVacuumTotalen();
            reportData.ReportTitle = report.ReportTitle;
            reportData.DateFrom = dateFrom;
            reportData.DateTo = dateTo;
            reportData.Convert(mealPlanList, leefgroepen.Where(x => x.MaaltijdlijstenVacuumVerpakking).ToList());

            return reportData;
        }

    }
}
