using System;
using DevExpress.XtraReports.UI;
using Logic.DataBusinessObjects.Werkbonnenbeheer;

namespace Reporting.Werkbonnenbeheer
{
    public partial class WerkbonReport
    {
        public WerkbonReport(WerkopdrachtReportItem reportData)
        {
            InitializeComponent();
            objectDataSource1.DataSource = reportData;

        }
    }
}
 