﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic.DataBusinessObjects;
using Logic.DataBusinessObjects.Werkbonnenbeheer;
using Logic.Reports;
using Logic.Werkbonnenbeheer;


namespace Reporting.Werkbonnenbeheer
{
    public class WerkbonnenbeheheerReportService : IWerkbonnenbeheerReportService
    {
        private IWerkbonnenbeheerService _werkbonnenbeheerService;

        public WerkbonnenbeheheerReportService(IWerkbonnenbeheerService werkbonnenbeheerService)
        {
            _werkbonnenbeheerService = werkbonnenbeheerService;
        }

        public DownloadFilePDF GetPDF(int id)
        {
            WerkopdrachtReportItem wb = new WerkopdrachtReportItem()
            {
                Id = 9999
            };
            WerkbonReport neg = new WerkbonReport(wb);
            MemoryStream memoryStream = new MemoryStream();
            neg.ExportToPdf(memoryStream);
            var filename = $"Werkbon_{id}_{DateTime.Now.Year}_{DateTime.Now.Month:D2}_{DateTime.Now.Day:D2}";
            return new DownloadFilePDF(filename, "pdf", memoryStream.ToArray());
        }
    }
}
