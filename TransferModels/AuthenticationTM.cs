﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TransferModels
{

    [DataContract]
    public class LogonSendTM
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string DatabaseAliasName { get; set; }
        
    }


    [DataContract]
    public class LogonInfoTM
    {
        [DataMember]
        public bool LoggedOn { get; set; }
        [DataMember]
        public String ErrorMessage { get; set; }
        [DataMember]
        public bool NeedNewPassword { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public String Email { get; set; }
        [DataMember]
        public String AuthorisationToken { get; set; }
        [DataMember]
        public int CompanyId { get; set; }

        [DataMember]
        public Byte[] CompanyLogo { get; set; }

        [DataMember]
        public string DatabaseAliasName { get; set; }

        [DataMember]
        public DateTime? LogonDate { get; set; }

        public LogonInfoTM()
        {



        }
   
    }

    






}
