
CREATE TABLE [aankoop].[incontinentiemateriaal](
 
  Id int identity(1,1) primary key,
  BewonerId int not null,
  ArtikelId int not null,
  ArtikelPrijs Decimal(10,2) not null,
  Aantal    int not null,
  LeefgroepIdsString varchar(64) not null,
  CreationDate Date not null,
  Creator int not null,
  ClosedDate Date null,
 )  



ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (BewonerId) REFERENCES bewoner(id); 

ALTER TABLE [aankoop].[incontinentiemateriaal]
ADD FOREIGN KEY (ArtikelId) REFERENCES artikelen(id); 